<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initResourceAutoloader()
    {
        $resourceloader = new Zend_Loader_Autoloader_Resource(array(
            'basePath'  => APPLICATION_PATH,
            'namespace' => 'Intensives_',
        ));

        $resourceloader->setDefaultResourceType('Model');
        $resourceloader->addResourceType('Model', 'modules/default/models/', 'Model');
        return $resourceloader;
    }

    protected function _initExtjs()
    {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->headTitle('АС "Интенсив"')->setSeparator(' - ');
        $view->headLink()->appendStylesheet('/extjs/resources/css/ext-all.css');
        $view->headLink()->appendStylesheet('/css/extjs-add.css');
        $view->headLink()->appendStylesheet('/extjs/resources/css/calendar.css');
        $view->headLink()->appendStylesheet('/extjs/examples/shared/examples.css');
        $view->headLink()->appendStylesheet('/extjs/examples/ux/fileuploadfield/css/fileuploadfield.css');
        $view->headLink()->appendStylesheet('/extjs/resources/css/visual/pivotgrid.css');
        $view->headLink()->appendStylesheet('/js/Ext.ux.DatePickerPlus/datepickerplus.css');

        $view->headLink()->headLink(array('rel' => 'shortcut icon', 'href' => 'http://vstu.ru/favicon.ico'));

        $view->headScript()->appendFile('/extjs/adapter/ext/ext-base.js');
        $view->headScript()->appendFile('/extjs/ext-all.js');
        $view->headScript()->appendFile('/extjs/calendar-all.js');
        $view->headScript()->appendFile('/extjs/src/calendar/templates/DayHeaderTemplate.js');
        $view->headScript()->appendFile('/extjs/src/calendar/templates/DayBodyTemplate.js');
        $view->headScript()->appendFile('/extjs/src/calendar/templates/DayViewTemplate.js');
        $view->headScript()->appendFile('/extjs/src/calendar/templates/BoxLayoutTemplate.js');
        $view->headScript()->appendFile('/extjs/src/calendar/templates/MonthViewTemplate.js');
        $view->headScript()->appendFile('/extjs/src/calendar/dd/CalendarScrollManager.js');
        $view->headScript()->appendFile('/extjs/src/dd/StatusProxy.js');
        $view->headScript()->appendFile('/extjs/src/calendar/dd/CalendarDD.js');
        $view->headScript()->appendFile('/extjs/src/calendar/dd/DayViewDD.js');
        $view->headScript()->appendFile('/extjs/src/calendar/EventRecord.js');
        $view->headScript()->appendFile('/extjs/src/calendar/views/MonthDayDetailView.js');
        $view->headScript()->appendFile('/extjs/src/calendar/widgets/CalendarPicker.js');
        $view->headScript()->appendFile('/extjs/src/calendar/WeekEventRenderer.js');
        $view->headScript()->appendFile('/extjs/src/calendar/views/CalendarView.js');
        $view->headScript()->appendFile('/extjs/src/calendar/views/MonthView.js');
        $view->headScript()->appendFile('/extjs/src/calendar/views/DayHeaderView.js');
        $view->headScript()->appendFile('/extjs/src/calendar/views/DayBodyView.js');
        $view->headScript()->appendFile('/extjs/src/calendar/views/DayView.js');
        $view->headScript()->appendFile('/extjs/src/calendar/views/WeekView.js');
        $view->headScript()->appendFile('/extjs/src/calendar/widgets/DateRangeField.js');
        $view->headScript()->appendFile('/extjs/src/calendar/widgets/ReminderField.js');
        $view->headScript()->appendFile('/extjs/src/calendar/EventEditForm.js');
        $view->headScript()->appendFile('/extjs/src/calendar/EventEditWindow.js');
        $view->headScript()->appendFile('/extjs/src/calendar/CalendarPanel.js');
        $view->headScript()->appendFile('/extjs/examples/ux/ColumnHeaderGroup.js');
        $view->headScript()->appendFile('/extjs/examples/ux/CheckColumn.js');
        $view->headScript()->appendFile('/extjs/src/locale/ext-lang-ru.js');

        $view->headScript()->appendFile('/js/on_load.js');
        $view->headScript()->appendFile('/js/Ext.ux.Exporter/Exporter-all.js');
        $view->headScript()->appendFile('/extjs/examples/ux/fileuploadfield/FileUploadField.js');
        $view->headScript()->appendFile('/js/Ext.ux.DatePickerPlus/ext.ux.datepickerplus.js');
        $view->headScript()->appendFile('/js/Ext.ux.DatePickerPlus/ext.ux.datepickerplus-lang-ru.js');
    }

    protected function _initACL()
    {
        $acl = new Zend_Acl();

        $acl->addRole('methodist');
        $acl->addRole('teacher');
        $acl->addRole('admin');

        $acl->addResource('json');
        $acl->addResource('print');
        $acl->addResource('schedule');
        $acl->addResource('storage');

        $acl->addResource('auditor');

        $acl->allow('methodist', 'json', 'addlistenertolesson');
        $acl->allow('methodist', 'json', 'checklessonpresence');
        $acl->allow('methodist', 'json', 'deleteauditory');
        $acl->allow('methodist', 'json', 'deletecontract');
        $acl->allow('methodist', 'json', 'deleteintensive');
        $acl->allow('methodist', 'json', 'deletelesson');
        $acl->allow('methodist', 'json', 'deletelistener');
        $acl->allow('methodist', 'json', 'deletelistenerfromlesson');
        $acl->allow('methodist', 'json', 'deleteteacher');
        $acl->allow('methodist', 'json', 'duplicatelesson');
        $acl->allow('methodist', 'json', 'getauditoriesforgrid');
        $acl->allow('methodist', 'json', 'getauditorydata');
        $acl->allow('methodist', 'json', 'getcalendarschedule');
        $acl->allow('methodist', 'json', 'getcontractdata');
        $acl->allow('methodist', 'json', 'getcontractdetails');
        $acl->allow('methodist', 'json', 'getcontractsforgrid');
        $acl->allow('methodist', 'json', 'getintensivedata');
        $acl->allow('methodist', 'json', 'getintensivesforgrid');
        $acl->allow('methodist', 'json', 'getintensivesfortoolbar');
        $acl->allow('methodist', 'json', 'getlessondata');
        $acl->allow('methodist', 'json', 'getlessonpresence');
        $acl->allow('methodist', 'json', 'getlistenercontracts');
        $acl->allow('methodist', 'json', 'getlistenerdata');
        $acl->allow('methodist', 'json', 'getlistenerpresence');
        $acl->allow('methodist', 'json', 'getlistenersforaddingtolesson');
        $acl->allow('methodist', 'json', 'getlistenersforgrid');
        $acl->allow('methodist', 'json', 'getteacherdata');
        $acl->allow('methodist', 'json', 'getteacherlessons');
        $acl->allow('methodist', 'json', 'getteachersforfilterpanel');
        $acl->allow('methodist', 'json', 'getteachersforgrid');
        $acl->allow('methodist', 'json', 'paycontract');
        $acl->allow('methodist', 'json', 'saveauditorydata');
        $acl->allow('methodist', 'json', 'savecontractdata');
        $acl->allow('methodist', 'json', 'saveintensivedata');
        $acl->allow('methodist', 'json', 'savelessondata');
        $acl->allow('methodist', 'json', 'savelistenerdata');
        $acl->allow('methodist', 'json', 'saveteacherdata');
        $acl->allow('methodist', 'json', 'unpaycontract');
        $acl->allow('methodist', 'json', 'getchairlessonthemes');
        $acl->allow('methodist', 'json', 'getintensivethemes');
        $acl->allow('methodist', 'json', 'getlessonthemedata');
        $acl->allow('methodist', 'print', 'journallisteners');
        $acl->allow('methodist', 'print', 'printcontract');
        $acl->allow('methodist', 'print', 'printlistenerslist');
        $acl->allow('methodist', 'print', 'printorder');
        $acl->allow('methodist', 'print', 'teachercontract');
        $acl->allow('methodist', 'print', 'printflowfunds');
        $acl->allow('methodist', 'schedule', 'scheduleupload');
        $acl->allow('methodist', 'schedule', 'parseschedule');
        $acl->allow('methodist', 'schedule', 'getxlsschedule');
        $acl->allow('methodist', 'schedule', 'getchairschedule');
        $acl->allow('methodist', 'schedule', 'getvstulesson');
        $acl->allow('methodist', 'schedule', 'savevstulesson');
        $acl->allow('methodist', 'schedule', 'deletevstulesson');
        $acl->allow('methodist', 'storage', 'getdocuments');
        $acl->allow('methodist', 'storage', 'getdocument');
        $acl->allow('methodist', 'storage', 'deletedocument');
        $acl->allow('methodist', 'storage', 'archivedocuments');
        $acl->allow('methodist', 'storage', 'getlistarchiveddocuments');
        $acl->allow('methodist', 'storage', 'getarchiveddocument');
        $acl->allow('methodist', 'auditor', 'getindividualachievement');
        $acl->allow('methodist', 'auditor', 'getsummaryachievement');
        $acl->allow('methodist', 'auditor', 'getteachers');
        $acl->allow('methodist', 'auditor', 'getstudentsgroups');
        $acl->allow('methodist', 'auditor', 'getlessontypes');
        $acl->allow('methodist', 'auditor', 'getstudents');

        $acl->allow('teacher', 'auditor', 'getindividualachievement');
        $acl->allow('teacher', 'auditor', 'getsummaryachievement');
        $acl->allow('teacher', 'auditor', 'getteachers');
        $acl->allow('teacher', 'auditor', 'getstudentsgroups');
        $acl->allow('teacher', 'auditor', 'getlessontypes');
        $acl->allow('teacher', 'auditor', 'getstudents');
        $acl->allow('teacher', 'storage', 'getarchiveddocument');
        $acl->allow('teacher', 'storage', 'getlistarchiveddocuments');
        $acl->allow('teacher', 'storage', 'archivedocuments');
        $acl->allow('teacher', 'storage', 'getdocuments');
        $acl->allow('teacher', 'storage', 'getdocument');
        $acl->allow('teacher', 'storage', 'deletedocument');
        $acl->allow('teacher', 'print', 'teachercontract');
        $acl->allow('teacher', 'print', 'teacherreport');
        $acl->allow('teacher', 'schedule', 'parseschedule');
        $acl->allow('teacher', 'schedule', 'getcalendarschedule');
        $acl->allow('teacher', 'schedule', 'getconsultation');
        $acl->allow('teacher', 'schedule', 'saveconsultation');
        $acl->allow('teacher', 'schedule', 'deleteconsultation');
        $acl->allow('teacher', 'schedule', 'getvstulesson');
        $acl->allow('teacher', 'schedule', 'savevstulesson');
        $acl->allow('teacher', 'schedule', 'deletevstulesson');
        $acl->allow('teacher', 'schedule', 'changevstulessontime');
        $acl->allow('teacher', 'json', 'getintensivethemes');
        $acl->allow('teacher', 'json', 'getteacherdatasafe');
        $acl->allow('teacher', 'json', 'saveteacherdatasafe');
        $acl->allow('teacher', 'json', 'getteacherlessons');
        $acl->allow('teacher', 'json', 'getlessondata');
        $acl->allow('teacher', 'json', 'getlessonpresence');
        $acl->allow('teacher', 'json', 'getcalendarschedule');
        $acl->allow('teacher', 'json', 'checklessonpresence');
        $acl->allow('teacher', 'json', 'getintensivesforgrid');
        $acl->allow('teacher', 'json', 'getauditorydata');
        $acl->allow('teacher', 'json', 'savelessondata');
        $acl->allow('teacher', 'json', 'getintensivedata');
        $acl->allow('teacher', 'json', 'getauditoriesforgrid');
        $acl->allow('teacher', 'json', 'getteachercontractsforgrid');
        $acl->allow('teacher', 'json', 'getlessonthemedata');

        $registry = Zend_Registry::getInstance();
        $registry->set('acl', $acl);
    }
}

