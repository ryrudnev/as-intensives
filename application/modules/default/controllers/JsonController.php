<?php
/**
 * Контроллер для обработки AJAJ-запросов.
 *
 * Основное назначение констроллера заключается в принятии GET или POST запроса,
 * его обработка и выдача ответа в виде JSON-объекта. JSON-объект должен
 * содержать, как минимум, одно свойство под названием 'success', которое
 * определяет успешность выполненной операции. Т.е. если 'success' равно '1', то
 * операция выполнена успешно, а если 'success' равно '0', то операция выполнена
 * неуспешно. При неуспешной операции желательно наличие свойства
 * 'error_message', которое содержит информацию о возникшей ошибке (данная
 * информация должна отображаться пользователю на клиентской стороне).
 *
 * LICENSE: GPLv3
 *
 * @copyright  Copyright (c) 2010-2011 Team Liberty
 * @license    http://www.gnu.org/licenses/gpl.html
 * @version    $Id:$
 * @link       http://intensives.vstu.ru/json
 * @since      File available since Release 1.0.0
 */

/**
 * Класс контроллера для обработки AJAJ-запросов.
 *
 * @copyright  Copyright (c) 2010-2011 Team Liberty
 * @license    http://www.gnu.org/licenses/gpl.html
 * @link       http://intensives.vstu.ru/json
 * @since      Class available since Release 1.0.0
 */
class JsonController extends Zend_Controller_Action
{

    /**
     * Инициализация класса-контроллера: отключение авторендеринга, так как
     * действия класса будут выдавать только строку с JSON-объектом - рендеринг
     * здесь не нужен.
     */
    public function init()
    {
        // Проверка аутентификации
        $auth = Zend_Auth::getInstance();
        if ($this->getRequest()->getActionName() != 'login' && !$auth->hasIdentity()) {
            $answer = array(
                'success' => 0,
                'error_code' => 1,
                'error_message' => 'Вы не вошли в систему'
            );

            die(Zend_Json::encode($answer));
        }

        // Отключение рендеринга лайаута страницы
        $this->_helper->layout()->disableLayout();

        // Отключение рендеринга представлений действий
        $this->_helper->viewRenderer->setNoRender(true);
    }

    /**
     * Добавление слушателя на занятие.
     *
     * @internal int $lesson_id идентификатор занятия (POST-параметр).
     * @internal int $listener_id идентификатор слушателя (POST-параметр).
     *
     * @return string при успешном добавлении: JSON-объект со свойством
     * 'success' равным '1', свойством 'lessonid' равным идентификатору занятия,
     * на которое добавляем слушателя и свойством 'listenerid' равным
     * идентификатору добавленного слушателя.
     * при неуспешном добавлении: JSON-объект со свойством 'success' равным '0'
     * и свойством 'error_message', которое содержить сообщение об ошибке,
     * которое будет выдано пользователю.
     */
    public function addlistenertolessonAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        if ($this->getRequest()->isPost() && isset($_POST['lesson_id']) && isset($_POST['listener_id'])) {
            $lessonId = (int)$_POST['lesson_id'];
            $listenerId = (int)$_POST['listener_id'];
            if (0 != $lessonId && 0 != $listenerId) {
                $presencesModel = new Intensives_Model_DbTable_LessonToListener();
                $presencesModel->insert(array(
                    'lesson_id' => $lessonId,
                    'listener_id' => $listenerId
                ));

                $answer['success'] = 1;
                $answer['lessonid'] = $lessonId;
                $answer['listenerid'] = $listenerId;
            }
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Отметка посещаемости слушателями занятия.
     *
     * @internal param int $lid идентификатор занятия (POST-параметр).
     * @internal param string $states JSON-массив с парами: идентификатор слушателя и
     * признак присутствия на занятии (POST-параметр).
     *
     * @return string при успешной отметке: JSON-объект со свойством 'success'
     * равным '1' и свойством 'lessonid' равным идентификатору занятия.
     * при неуспешной отметке: JSON-объект со свойством 'success' равным '0'
     * и свойством 'error_message', которое содержить сообщение об ошибке,
     * которое будет выдано пользователю.
     */
    public function checklessonpresenceAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        if ($this->getRequest()->isPost() && isset($_POST['lid']) && isset($_POST['states'])) {
            $lessonId = (int)$_POST['lid'];
            if (0 != $lessonId) {
                $presencesModel = new Intensives_Model_DbTable_LessonToListener();
                $states = Zend_Json::decode(stripslashes($_POST['states']));
                foreach ($states as $state) {
                    $where = array(
                        'lesson_id = ?' => $lessonId,
                        'listener_id = ?' => $state['id']
                    );
                    $presencesModel->update(array('presented' => $state['presented']), $where);
                }

                $answer['success'] = 1;
                $answer['lessonid'] = $lessonId;
            }
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Пометка аудитории, как 'удалённая'.
     *
     * @internal int $aid идентификатор помечаемой аудитории (POST-параметр).
     *
     * @return string при успешном удалении: JSON-объект со свойством 'success'
     * равным '1' и свойством 'auditoryid' равным идентификатору помеченной
     * аудитории.
     * при неуспешном удалении: JSON-объект со свойством 'success' равным '0'
     * и свойством 'error_message', которое содержить сообщение об ошибке,
     * которое будет выдано пользователю.
     */
    public function deleteauditoryAction()
    {
        $answer = array('success' => 0);

        if ($this->getRequest()->isPost() && isset($_POST['aid'])) {
            Intensives_Model_Funcs::asyncCheckAccess($this);

            $auditoryId = (int)$_POST['aid'];
            if (0 != $auditoryId) {
                $auditoriesModel = new Intensives_Model_DbTable_Auditory();
                if ($auditoriesModel->deleteAuditory($auditoryId)) {
                    $answer['success'] = 1;
                    $answer['auditoryid'] = $auditoryId;
                }
            }
        }

        echo Zend_Json::encode($answer);
    }


    /**
     * Удаление договора из базы.
     *
     * @internal int $cid идентификатор договора, которые необходимо удалить
     * (POST-параметр).
     *
     * @return string при успешном удалении: JSON-объект со свойством 'success'
     * равным '1' и свойством 'contractid' равным идентификатору удалённого
     * договора.
     * при неуспешном удалении: JSON-объект со свойством 'success' равным '0'
     * и свойством 'error_message', которое содержить сообщение об ошибке,
     * которое будет выдано пользователю.
     */
    public function deletecontractAction()
    {
        $answer = array('success' => 0);

        if ($this->getRequest()->isPost() && isset($_POST['cid'])) {
            Intensives_Model_Funcs::asyncCheckAccess($this);

            $contractId = (int)$_POST['cid'];
            if (0 != $contractId) {
                $contractsModel = new Intensives_Model_DbTable_Contract();
                if ($contractsModel->deleteContract($contractId)) {
                    $answer['success'] = 1;
                    $answer['contractid'] = $contractId;
                }
            }
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Пометка интенсива, как 'удалённый'.
     *
     * @internal $iid идентификатор интенсива, который необходимо пометить как
     * 'удалённый' (POST-параметр).
     *
     * @return string при успешном удалении: JSON-объект со свойством 'success'
     * равным '1' и свойством 'intensiveid' равным идентификатору помеченного
     * интенсива.
     * при неуспешном удалении: JSON-объект со свойством 'success' равным '0'
     * и свойством 'error_message', которое содержить сообщение об ошибке,
     * которое будет выдано пользователю.
     */
    public function deleteintensiveAction()
    {
        $answer = array('success' => 0);

        if ($this->getRequest()->isPost() && isset($_POST['iid'])) {
            Intensives_Model_Funcs::asyncCheckAccess($this);

            $intensiveId = (int)$_POST['iid'];
            if (0 != $intensiveId) {
                $intensivesModel = new Intensives_Model_DbTable_Intensive();
                if ($intensivesModel->update(array('deleted' => 1), array('id = ?' => $intensiveId))) {
                    $answer['success'] = 1;
                    $answer['intensiveid'] = $intensiveId;
                }
            }
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Пометка занятия, как 'удалённое'.
     *
     * @internal int $lid идентификатор занятия, которое необходимо пометить как
     * 'удалённое' (POST-параметр).
     *
     * @return string при успешном удалении: JSON-объект со свойством 'success'
     * равным '1' и свойством 'lessonid' равным идентификатору помеченного
     * занятия.
     * при неуспешном удалении: JSON-объект со свойством 'success' равным '0'
     * и свойством 'error_message', которое содержить сообщение об ошибке,
     * которое будет выдано пользователю.
     */
    public function deletelessonAction()
    {
        $answer = array('success' => 0);

        if ($this->getRequest()->isPost() && isset($_POST['lid'])) {
            Intensives_Model_Funcs::asyncCheckAccess($this);

            $lessonId = (int)$_POST['lid'];
            if (0 != $lessonId) {
                $lessonsModel = new Intensives_Model_DbTable_Lesson();
                if ($lessonsModel->update(array('deleted' => 1), array('id = ?' => $lessonId))) {
                    $answer['success'] = 1;
                    $answer['lessonid'] = $lessonId;
                }
            }
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Пометка слушателя, как 'удалённый'.
     *
     * @internal int $lid идентификатор слушателя, которого необходимо пометить как
     * 'удалённый' (POST-параметр).
     *
     * @return string при успешном удалении: JSON-объект со свойством 'success'
     * равным '1' и свойством 'listenerid' равным идентификатору помеченного
     * слушателя.
     * при неуспешном удалении: JSON-объект со свойством 'success' равным '0'
     * и свойством 'error_message', которое содержить сообщение об ошибке,
     * которое будет выдано пользователю.
     */
    public function deletelistenerAction()
    {
        $answer = array('success' => 0);

        if ($this->getRequest()->isPost() && isset($_POST['lid'])) {
            Intensives_Model_Funcs::asyncCheckAccess($this);

            $listenerId = (int)$_POST['lid'];
            if (0 != $listenerId) {
                $listenersModel = new Intensives_Model_DbTable_Listener();
                if ($listenersModel->update(array('deleted' => 1), array('id = ?' => $listenerId))) {
                    $answer['success'] = 1;
                    $answer['listenerid'] = $listenerId;
                }
            }
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Удаление слушателя с занятия.
     *
     * @internal int $listener_id идентификатор удаляемого слушателя
     * (POST-параметр).
     * @internal int $lesson_id идентификатор занятия, с которого удаляем
     * слушателя (POST-параметр).
     *
     * @return string при успешном удалении: JSON-объект со свойством 'success'
     * равным '1', свойством 'listenerid' равным идентификатору удалённого с
     * занятия слушателя и свойством 'lessonid' равны идентификатору занятия, с
     * которого был удалён слушатель.
     * при неуспешном удалении: JSON-объект со свойством 'success' равным '0'
     * и свойством 'error_message', которое содержить сообщение об ошибке,
     * которое будет выдано пользователю.
     */
    public function deletelistenerfromlessonAction()
    {
        $answer = array('success' => 0);

        if ($this->getRequest()->isPost() && isset($_POST['listener_id']) && isset($_POST['lesson_id'])) {
            Intensives_Model_Funcs::asyncCheckAccess($this);

            $listenerId = (int)$_POST['listener_id'];
            $lessonId = (int)$_POST['lesson_id'];
            if (0 != $listenerId) {
                $listenersModel = new Intensives_Model_DbTable_LessonToListener();
                if ($listenersModel->delete(array('listener_id = ?' => $listenerId, 'lesson_id' => $lessonId))) {
                    $answer['success'] = 1;
                    $answer['listenerid'] = $listenerId;
                    $answer['lessonid'] = $lessonId;
                }
            }
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Пометка преподавателя, как 'удалённый'.
     *
     * @internal int $tid идентификатор преподавателя, которого необходимо
     * пометить как 'удалённый' (POST-параметр).
     *
     * @return string при успешном удалении: JSON-объект со свойством 'success'
     * равным '1' и свойством 'teacherid' равным идентификатору помеченного
     * преподавателя.
     * при неуспешном удалении: JSON-объект со свойством 'success' равным '0'
     * и свойством 'error_message', которое содержить сообщение об ошибке,
     * которое будет выдано пользователю.
     */
    public function deleteteacherAction()
    {
        $answer = array('success' => 0);

        if ($this->getRequest()->isPost() && isset($_POST['tid'])) {
            Intensives_Model_Funcs::asyncCheckAccess($this);

            $teacherId = (int)$_POST['tid'];
            if (0 != $teacherId) {
                $teachersModel = new Intensives_Model_DbTable_Teacher();
                if ($teachersModel->update(array('deleted' => 1), array('id = ?' => $teacherId))) {
                    $answer['success'] = 1;
                    $answer['teacherid'] = $teacherId;
                }
            }
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Дублирование занятия.
     *
     * @internal param int $lid идентификатор занятия, которое необходимо продублировать
     * (POST-параметр).
     * @internal param int $period количество дней, через которые будет повторяться
     * занятие (POST-параметр).
     * @internal param int $repeat_count количество копий занятия (POST-параметр).
     *
     * @return string при успешном дублировании: JSON-объект со свойством
     * 'success' равным '1', свойством 'lessonid' равным идентификатору
     * дублируемого занятия и свойством 'processed' равным количеству
     * продублированных занятий.
     * при неуспешном удалении: JSON-объект со свойством 'success' равным '0'
     * и свойством 'error_message', которое содержить сообщение об ошибке,
     * которое будет выдано пользователю.
     */
    public function duplicatelessonAction()
    {
        $answer = array('success' => 0);

        if ($this->getRequest()->isPost() && isset($_POST['lid']) && isset($_POST['period']) && isset($_POST['repeat_count'])) {
            Intensives_Model_Funcs::asyncCheckAccess($this);

            $processed = 0;
            $lessonId = (int)$_POST['lid'];
            $period = (int)$_POST['period'];
            $repeatCount = (int)$_POST['repeat_count'];
            if (0 != $lessonId && 0 != $period && 0 != $repeatCount) {
                $lessonModel = new Intensives_Model_DbTable_Lesson();
                $lesson = $lessonModel->find($lessonId)->current()->toArray();
                require_once(APPLICATION_PATH . '/../library/mysqldatetime2timestamp.php');
                $start = mysqldatetime2timestamp($lesson['start']);
                $end = mysqldatetime2timestamp($lesson['end']);
                while ($repeatCount--) {
                    $delta = $period * 86400;
                    $start += $delta;
                    $end += $delta;
                    if ($lessonModel->insert(array(
                        'start' => date('Y-m-d H:i', $start),
                        'end' => date('Y-m-d H:i', $end),
                        'intensive_id' => $lesson['intensive_id'],
                        'theme' => $lesson['theme'],
                        'teacher_id' => $lesson['teacher_id'],
                        'auditory_id' => $lesson['auditory_id'],
                        'status' => 0,
                        'type' => $lesson['type']
                    ))
                    ) {
                        $processed++;
                    }
                }

                if ($processed > 0) {
                    $answer['success'] = 1;
                    $answer['processed'] = $processed;
                    $answer['lessonid'] = $lessonId;
                }
            }
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получение списка аудиторий для отображения в списке аудиторий.
     *
     * @return string JSON-объект, содержащий свойство 'auditories', которое
     * содержит массив объектов, содержащих информацию об аудиториях.
     */
    public function getauditoriesforgridAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $auditoriesModel = new Intensives_Model_DbTable_Auditory();
        $auditories = $auditoriesModel->fetchAllAuditories()->toArray();

        $answer = array(
            'success' => 1,
            'auditories' => $auditories,
        );

        echo Zend_Json::encode($answer);
    }

    /**
     * Получение данных об аудитории.
     *
     * @internal int $aid идентификатор аудитории.
     *
     * @return string JSON-объект, содержащий свойство 'auditorydata', которое
     * содержит объект, содержащий информацию об указанной аудитории.
     */
    public function getauditorydataAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $auditoryId = $this->getRequest()->getParam('aid');

        $answer = array('success' => 0);

        if (!is_null($auditoryId)) {
            $auditoriesModel = new Intensives_Model_DbTable_Auditory();
            $auditoryData = $auditoriesModel->find($auditoryId)->toArray();

            $answer['success'] = 1;
            $answer['auditorydata'] = $auditoryData;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получение данных о договоре. Данных достаточно для отображения договора в
     * форме редактирования договора. Для получения детальной информации о
     * договоре, необходимо использовать
     * действие @see {getcontractdetailsAction}.
     *
     * @internal param int $cid идентификатор договора.
     *
     * @return string JSON-объект, содержащий свойство 'contractdata', которое
     * содержит объект, содержащий информацию об указанном договоре.
     */
    public function getcontractdataAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $cid = $this->getRequest()->getParam('cid');

        $answer = array('success' => 0);

        if (!is_null($cid)) {
            $contractsModel = new Intensives_Model_DbTable_Contract();
            $contractData = $contractsModel->find($cid)->toArray();

            $listenersModel = new Intensives_Model_DbTable_Listener();
            $rowListener = $listenersModel->find($contractData[0]['listener_id'])->current();

            $contractToLessonTheme = new Intensives_Model_DbTable_ContractToLessonTheme();
            $select = $contractToLessonTheme->select()
                ->setIntegrityCheck(false)
                ->from(array('clt' => 'contract_to_lesson_theme'), array('id' => 'clt.lesson_theme_id'))
                ->joinInner(array('l' => 'lesson_theme'), 'clt.lesson_theme_id = l.id', array('name'))
                ->where('clt.contract_id = ?', $cid)
                ->order(array('name ASC'));
            $rowSet = $contractToLessonTheme->fetchAll($select);

            $contractData[0]['themes'] = $rowSet->toArray();
            $contractData[0]['listener'] = $rowListener->surname . ' ' .
                $rowListener->name . ' ' . $rowListener->patronymic;
            $contractData[0]['listener_group'] = $rowListener->group;
            $contractData[0]['listener_passport_number'] = $rowListener->passport_number;

            $answer['success'] = 1;
            $answer['contractdata'] = $contractData;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получение детальной информации о договоре. В отличии от
     * getcontractdataAction, данное действие производит выборку всех данных
     * необходимых для формирования договора.
     *
     * @internal int $cid идентификатор договора.
     *
     * @return string JSON-объект, содержащий свойство 'contractdetails',
     * которое содержит объект, содержащий детальную информацию об указанном
     * договоре.
     */
    public function getcontractdetailsAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $contractId = $this->getRequest()->getParam('cid');

        $answer = array('success' => 0);

        if (!is_null($contractId)) {
            $contractsModel = new Intensives_Model_DbTable_Contract();
            $contractDetails = $contractsModel->getContractDetails($contractId);

            $answer['success'] = 1;
            $answer['contractdetails'] = $contractDetails;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получение списка договоров, которые закреплены за
     * указанной кафедрой, для отображения в списке договоров.
     *
     * @internal param int $cid идентификатор кафедры, за которой закреплены договоры.
     *
     * @return string JSON-объект, содержащий свойство 'contracts', которое
     * содержит массив объектов, содержащих информацию о договорах, которые
     * закреплены за указанной кафедрой.
     */
    public function getcontractsforgridAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $contracts = array();
        $chairId = $this->getRequest()->getParam('cid');

        if (!is_null($chairId)) {
            $contractsModel = new Intensives_Model_DbTable_Contract();
            $contracts = $contractsModel->fetchAllContracts($chairId)->toArray();
        }

        $answer = array(
            'success' => 1,
            'contracts' => $contracts,
        );

        echo Zend_Json::encode($answer);
    }

    /**
     * Получение данных об интенсиве.
     *
     * @internal int $iid идентификатор интенсива.
     *
     * @return string JSON-объект, содержащий свойство 'intensive', которое
     * содержит объект, содержащий информацию об указанном интенсиве.
     */
    public function getintensivedataAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        if (!is_null($iid = $this->getRequest()->getParam('iid'))) {
            $intensivesModel = new Intensives_Model_DbTable_Intensive();
            $intensive = $intensivesModel->find($iid)->current()->toArray();

            $lessonThemeModel = new Intensives_Model_DbTable_LessonTheme();
            $select = $lessonThemeModel->select()
                ->from(array('lesson_theme'), array('id', 'name', 'number'))
                ->where('intensive_id = ?', $iid)
                ->where('deleted = ?', 0);
            $themes = $lessonThemeModel->fetchAll($select)->toArray();

            /**
             * Кроме обычных полей характерезующих интенсив задает дополнительное поле - status (статус)
             * Статус:
             * - loaded;
             * - created;
             * - updated;
             * - deleted
             * Необходим для корректного задания тем рабочей программы интенсива
             */
            for ($i = 0; $i < count($themes); $i++) {
                $themes[$i]['status'] = 'loaded';
            }

            unset($intensive['deleted']);
            $intensive['themes'] = $themes;
            $intensive['workprogram'] = 'Рабочая программа (' . count($themes) . ' тем.)';
            $answer['intensivedata'] = $intensive;

            $answer['success'] = 1;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получение списка интенсивов, которые закреплены за указанной кафедрой,
     * для отображения в списке интенсивов.
     *
     * @internal int $cid идентификатор кафедры, за которой закреплены интенсивы.
     *
     * @return string JSON-объект, содержащий свойство 'intensives', которое
     * содержит массив объектов, содержащих информацию об интенсивах, которые
     * закреплены за указанной кафедрой.
     */
    public function getintensivesforgridAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $chairId = $this->getRequest()->getParam('cid');

        $answer = array('success' => 0);

        $intensivesModel = new Intensives_Model_DbTable_Intensive();
        $intensives = $intensivesModel->fetchAllIntensives($chairId)->toArray();

        $answer['success'] = 1;
        $answer['intensives'] = $intensives;

        echo Zend_Json::encode($answer);
    }

    /**
     * Получение списка интенсивов, закреплённых за указанной кафедрой, для
     * отображения на панели интенсивов в кабинете методиста.
     *
     * @internal int $cid идентификатор кафедры, за которой закреплены интенсивы.
     *
     * @return string JSON-объект, содержащий свойство 'intensives', которое
     * содержит массив объектов, содержащих информацию об интенсивах, которые
     * закреплены за указанной кафедрой.
     */
    public function getintensivesfortoolbarAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $intensives = array();
        $chairId = $this->getRequest()->getParam('cid');

        if (!is_null($chairId)) {
            $where['chair_id = ?'] = $chairId;
            $where[] = 'deleted = 0';

            $intensivesModel = new Intensives_Model_DbTable_Intensive();
            $intensives = $intensivesModel->fetchAll($where, 'name')->toArray();
        }

        $answer = array(
            'success' => 1,
            'intensives' => $intensives,
        );

        echo Zend_Json::encode($answer);
    }

    /**
     * Получение данных о занятии.
     *
     * @internal int $lid идентификатор занятия.
     *
     * @return string JSON-объект, содержащий свойство 'lessondata', которое
     * содержит объект, содержащий информацию об указанном занятии.
     */
    public function getlessondataAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $lessonId = $this->getRequest()->getParam('lid');

        $answer = array('success' => 0);

        if (!is_null($lessonId)) {
            $lessonsModel = new Intensives_Model_DbTable_Lesson();
            $lessonData = $lessonsModel->find($lessonId)->toArray();
            $answer['success'] = 1;
            $answer['lessondata'] = $lessonData;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получение посещаемости занятия.
     *
     * @internal int $lid идентификатор занятия.
     *
     * @return string JSON-объект, содержащий свойство 'presences', которое
     * содержит массив объектов, содержащих информацию о слушателях, записанных
     * на указанное занятие, и их отметка о посещении занятия.
     */
    public function getlessonpresenceAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $lessonId = $this->getRequest()->getParam('lid');

        $answer = array('success' => 0);

        if (!is_null($lessonId)) {
            $presenceModel = new Intensives_Model_DbTable_LessonToListener();
            $presences = $presenceModel->fetchLessonPresence($lessonId)->toArray();
            $answer['success'] = 1;
            $answer['presences'] = $presences;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получение списка договоров, которые оформленны на указанного слушателя.
     *
     * @internal int $lid идентификатор слушателя, на которого оформлены договоры.
     *
     * @return string JSON-объект, содержащий свойство 'contracts', которое
     * содержит массив объектов, содержащих информацию о контрактах, которые
     * оформлены на указанного слушателя.
     */
    public function getlistenercontractsAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $listenerId = $this->getRequest()->getParam('lid');

        $answer = array('success' => 0);

        $contractsModel = new Intensives_Model_DbTable_Contract();
        $contracts = $contractsModel->getContractsByListenerId($listenerId)->toArray();

        $answer['success'] = 1;
        $answer['contracts'] = $contracts;

        echo Zend_Json::encode($answer);
    }

    /**
     * Получение данных об указанном слушателе.
     *
     * @internal int $lid идентификатор слушателя.
     *
     * @return string JSON-объект, содержащий свойство 'listenerdata', которое
     * содержит объект, содержащий информацию об указанном слушателе.
     */
    public function getlistenerdataAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $listenerId = $this->getRequest()->getParam('lid');

        $answer = array('success' => 0);

        if (!is_null($listenerId)) {
            $listenersModel = new Intensives_Model_DbTable_Listener();
            $listenerData = $listenersModel->find($listenerId)->toArray();
            $answer['success'] = 1;
            $answer['listenerdata'] = $listenerData;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получение посещаемости занятий по указанному слушателю.
     *
     * @internal int $lid идентификатор слушателя.
     *
     * @return string JSON-объект, содержащий свойство 'presences', которое
     * содержит массив объектов, содержащих информацию о занятиях, на которые
     * был записан слушатель, и отметки о присутствии слушателя на данных
     * занятиях.
     */
    public function getlistenerpresenceAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $listenerId = $this->getRequest()->getParam('lid');

        $answer = array('success' => 0);

        if (!is_null($listenerId)) {
            $presenceModel = new Intensives_Model_DbTable_LessonToListener();
            $presenceData = $presenceModel->fetchListenerPresence($listenerId)->toArray();
            $answer['success'] = 1;
            $answer['presences'] = $presenceData;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получение списка слушателей для списка добавления слушателя на указанное
     * занятие. В список не входят слушатели, которые уже добавлены на занятие.
     *
     * @internal int $lid идентификатор занятия.
     *
     * @return string JSON-объект, содержащий свойство 'listeners', которое
     * содержит массив объектов, содержащих информацию о слушателях, которые
     * ещё не добавлены на занятие.
     */
    public function getlistenersforaddingtolessonAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $lessonId = $this->getRequest()->getParam('lid');

        $answer = array('success' => 0);

        if (!is_null($lessonId)) {
            $listenersModel = new Intensives_Model_DbTable_Listener();
            $listeners = $listenersModel->getListenersForAddingToLesson($lessonId);
            $answer['success'] = 1;
            $answer['listeners'] = $listeners;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получение списка слушателей для отображения в списке слушателей.
     *
     * @return string JSON-объект, содержащий свойство 'listeners', которое
     * содержит массив объектов, содержащих информацию о слушателях, которые
     * есть в базе.
     */
    public function getlistenersforgridAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $listenersModel = new Intensives_Model_DbTable_Listener();
        $listeners = $listenersModel->fetchAllListenersWithPaidHours(date('Y-m-d'))->toArray();

        $answer = array(
            'success' => 1,
            'listeners' => $listeners,
        );

        echo Zend_Json::encode($answer);
    }

    /**
     * Получение данных о преподавателе.
     *
     * @internal int $tid идентификатор преподавателя.
     *
     * @return string JSON-объект, содержащий свойство 'teacherdata', которое
     * содержит объект, содержащий информацию об указанном преподавателе.
     */
    public function getteacherdataAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $teacherId = $this->getRequest()->getParam('tid');

        $answer = array('success' => 0);

        if (!is_null($teacherId)) {
            $teachersModel = new Intensives_Model_DbTable_Teacher();
            $teacherData = $teachersModel->find($teacherId)->toArray();
            $answer['success'] = 1;
            $answer['teacherdata'] = $teacherData;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получение данных о преподавателе для преподавателя.
     *
     * @return string JSON-объект, содержащий свойство 'teacherdata', которое
     * содержит объект, содержащий информацию об указанном преподавателе.
     */
    public function getteacherdatasafeAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);
        $userData = Zend_Auth::getInstance()->getStorage()->read();
        $teacherId = $userData->id;

        $answer = array('success' => 0);

        if (!is_null($teacherId)) {
            $teachersModel = new Intensives_Model_DbTable_Teacher();
            $teacherData = $teachersModel->find($teacherId)->toArray();
            $answer['success'] = 1;
            $answer['teacherdata'] = $teacherData;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получение списка занятий, которые преподаватель должен провести (точнее:
     * занятия которые имеют статус 'Утверждено').
     *
     * @internal int $tid идентификатор преподавателя.
     *
     * @return string JSON-объект, содержащий свойство 'lessons', которое
     * содержит массив объектов, содержащих информацию о занятиях, которые
     * преподаватель должен провести.
     */
    public function getteacherlessonsAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        $teacherId = $this->getRequest()->getParam('tid');

        if (!is_null($teacherId)) {
            $lessonsModel = new Intensives_Model_DbTable_Lesson();
            $lessons = $lessonsModel->getTeacherLessons($teacherId)->toArray();
            $answer['success'] = 1;
            $answer['lessons'] = $lessons;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получение списка преподавателей для отображения в панели фильтра
     * календаря.
     *
     * @return string JSON-объект, содержащий свойство 'teachers', которое
     * содержит массив объектов, содержащих информацию о преподавателе (ФИО и
     * идентификатор преподавателя).
     */
    public function getteachersforfilterpanelAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $userData = Zend_Auth::getInstance()->getStorage()->read();

        $chairId = $userData->chair_id;

        $answer = array('success' => 0);

        $teachersModel = new Intensives_Model_DbTable_Teacher();
        $teachers = $teachersModel->fetchTeachersByChairId($chairId)->toArray();

        $teachersArray = array(array('id' => 0, 'fio' => '[отключить]'));
        foreach ($teachers as $teacher) {
            $teachersArray[] = array(
                'id' => $teacher['id'],
                'fio' => $teacher['surname'] . ' ' . $teacher['name'] . ' ' . $teacher['patronymic']
            );
        }
        $answer['success'] = 1;
        $answer['teachers'] = $teachersArray;

        echo Zend_Json::encode($answer);
    }

    /**
     * Получение списка преподавателей для отображения в списке преподавателей.
     *
     * @return string JSON-объект, содержащий свойство 'teachers', которое
     * содержит массив объектов, содержащих информацию о преподавателях.
     */
    public function getteachersforgridAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $teachersModel = new Intensives_Model_DbTable_Teacher();
        $teachers = $teachersModel->fetchAllTeachers()->toArray();

        $answer = array(
            'success' => 1,
            'teachers' => $teachers,
        );

        echo Zend_Json::encode($answer);
    }


    /**
     * Получение списка контрактов преподавателя для отображения в таблице
     *
     * @return string JSON-объект, содержащий свойство 'contracs', которое
     * содержит массив объектов, содержащих информацию о контрактах.
     */
    public function getteachercontractsforgridAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $userData = Zend_Auth::getInstance()->getStorage()->read();

        $teacherID = $userData->id;

        $contractModel = new Intensives_Model_DbTable_TeacherContract();
        $contracts = $contractModel->getTeacherContracts($teacherID);

        $answer = array(
            'success' => 1,
            'contracts' => $contracts,
        );

        echo Zend_Json::encode($answer);
    }

    /**
     * Получение расписания по опеределённому курсу интенсива с применением
     * фильтров: расписание свободных часов преподавателя, расписание загрузки
     * аудитории, расписание других интенсивов.
     *
     * @internal param int $iid идентификатор курса интенсива.
     * @internal param int $tid идентификатор преподавателя или 0 если не требуется
     * отображать расписание свободных часов преподавателя.
     * @internal param int $aid идентификатор аудитории или 0 если на требуется
     * отображать расписание загрузки аудитории.
     * @internal param int $oi признак того, что надо ли выводить расписание других
     * интенсивов, проводимых в указанной аудитории. Если аудитория не указана,
     * то расписание выводиться не будет.
     *
     * @return string JSON-объект, содержащий свойство 'tasks', которое
     * содержит массив объектов, содержащих информацию о дате проведения
     * занятия или о дате, когда преподаватель свободен для курсов или о дате,
     * когда аудитория занята или о дате, когда проводится другой курс в
     * указанной аудитории.
     */
    public function getcalendarscheduleAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        $tasks = array();

        $intensiveId = $this->getRequest()->getParam('iid');

        if (!is_null($intensiveId) && $intensiveId != 0) {
            $lessonsModel = new Intensives_Model_DbTable_Lesson();
            $lessons = $lessonsModel->fetchLessons($intensiveId)->toArray();

            $lessonsRecords = array();
            foreach ($lessons as $lesson) {
                $status = '';
                if ($lesson['status'] == 1) {
                    $status = '[Утверждено]';
                } else if ($lesson['status'] == 2) {
                    $status = '[Проведено]';
                }
                $lessonsRecords[] = array(
                    'id' => $lesson['id'],
                    'cid' => 1,
                    'start' => $lesson['start'],
                    'end' => $lesson['end'],
                    'title' => $status . '<br/>' . $lesson['theme'] . '<br/>Ауд: ' . $lesson['auditory_name'] . '<br/>' . $lesson['teacher_surname'] . ' ' . $lesson['teacher_name'] . ' ' . $lesson['teacher_patronymic'],
                    'notes' => '',
                    'ad' => false
                );
            }

            $tasks = array_merge($tasks, $lessonsRecords);
        }

        $teacherId = $this->getRequest()->getParam('tid');

        if (!is_null($teacherId) && $teacherId != 0) {
            $teacherScheduleModel = new Intensives_Model_DbTable_TeacherSchedule();
            $teacherSchedule = $teacherScheduleModel->getTeacherSchedule($teacherId)->toArray();
            $teacherModel = new Intensives_Model_DbTable_Teacher();
            $teacherData = $teacherModel->find($teacherId)->toArray();
            $teacherData = $teacherData[0];

            $teacherScheduleArray = array();
            foreach ($teacherSchedule as $task) {
                $teacherScheduleArray[] = array(
                    'cid' => 2,
                    'start' => $task['start'],
                    'end' => $task['end'],
                    'title' => $teacherData['surname'] . ' ' . $teacherData['name'] . ' ' . $teacherData['patronymic'],
                    'notes' => '',
                    'ad' => false
                );
            }

            $tasks = array_merge($tasks, $teacherScheduleArray);
        }

//        $auditoryId = $this->getRequest()->getParam('aid');
//
//        if (!is_null($auditoryId) && $auditoryId != -1)
//        {
//            $auditoryScheduleModel = new Intensives_Model_DbTable_AuditoryEmployment();
//            $auditorySchedule = $auditoryScheduleModel->getAuditoryEmployment($auditoryId)->toArray();
//
//            $auditoryScheduleArray = array();
//            foreach ($auditorySchedule as $task)
//            {
//
//            }
//
//            $tasks = array_merge($tasks, $auditoryScheduleArray);
//        }

        $withOtherIntensives = $this->getRequest()->getParam('oi');

        if (!is_null($withOtherIntensives) && $withOtherIntensives == true) {
        } else {
        }

        if (!is_null($intensiveId) || !is_null($teacherId)) {
            $answer['success'] = 1;
            $answer['tasks'] = $tasks;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Вход в систему.
     *
     * @internal param string $login логин пользователя (POST-параметр).
     * @internal param string $password пароль пользователя (POST-параметр).
     * @internal param  string $type тип пользователя (admin, methodist, teacher) (POST-параметр).
     *
     * @return string при успешном входе: JSON-объект со свойством 'success'
     * равным '1' и свойством 'login' равным логину вошедшего пользователя.
     * при неуспешном входе: JSON-объект со свойством 'success' равным '0'
     * и свойством 'error_message', которое содержить сообщение об ошибке,
     * которое будет выдано пользователю.
     */
    public function loginAction()
    {
        $answer = array('success' => 0);

        if ($this->getRequest()->isPost()
            && $_POST['login']
            && $_POST['password']
            && $_POST['type']
        ) {
            $authAdapter = new Zend_Auth_Adapter_DbTable();
            switch ($_POST['type']) {
                case 'teacher':
                    $authAdapter->setTableName('teacher');
                    $authAdapter->setIdentityColumn('login');
                    $authAdapter->setCredentialColumn('password');
                    $authAdapter->setCredentialTreatment('MD5(?)');
                    break;
                case 'methodist':
                    $authAdapter->setTableName('methodist');
                    $authAdapter->setIdentityColumn('login');
                    $authAdapter->setCredentialColumn('password');
                    $authAdapter->setCredentialTreatment('MD5(?)');
                    break;
                default:
                    break;
            }
            $authAdapter->setIdentity($_POST['login']);
            $authAdapter->setCredential($_POST['password']);

            $auth = Zend_Auth::getInstance();
            $result = $auth->authenticate($authAdapter);
            if ($result->isValid()) {
                $userdata = $authAdapter->getResultRowObject();
                $userdata->type = $_POST['type'];
                Zend_Auth::getInstance()->getStorage()->write($userdata);
                $answer['success'] = 1;
                $answer['login'] = $_POST['login'];
            } else {
                $answer['error_message'] = 'Неверный логин или пароль';
            }
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Выход из системы
     */
    public function logoutAction()
    {
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $auth->clearIdentity();
        }
    }

    /**
     * Пометить договор как 'оплаченный'.
     *
     * @internal param int $contract_id идентификатор (номер) договора (POST-параметр).
     * @internal param string $receipt_number номер квитанции (POST-параметр).
     * @internal param float $receipt_total сумма квитанции (POST-параметр).
     * @internal param string $receipt_date дата квитанции (POST-параметр).
     *
     * @return string при успешной пометке: JSON-объект со свойством 'success'
     * равным '1' и свойством 'contractid' равным идентификатору договора.
     * при неуспешной отметке: JSON-объект со свойством 'success' равным '0'
     * и свойством 'error_message', которое содержить сообщение об ошибке,
     * которое будет выдано пользователю.
     */
    public function paycontractAction()
    {
        $answer = array('success' => 0);

        $params = $this->getRequest()->getParams();
        if ($this->getRequest()->isPost()
            && isset($params['contract_id'])
            && isset($params['receipt_number'])
            && isset($params['receipt_total'])
            && isset($params['receipt_date'])
        ) {
            Intensives_Model_Funcs::asyncCheckAccess($this);

            $contractsModel = new Intensives_Model_DbTable_Contract();
            $contractsModel->pay($params['contract_id'], $params['receipt_number'],
                $params['receipt_total'], $params['receipt_date']);

            /**
             * Генерация обновленного документа для данного методиста и заданной категории
             */
            $documentor = $documentor = new Intensives_Model_Documentor();
            $category = new Intensives_Model_DocumentCategoryEnum(
                Intensives_Model_DocumentCategoryEnum::ListenerContract);
            $odt = $documentor->generateOdt($category, array('cid' => $params['contract_id']));

            /**
             * Обновление документа в хранилище документов
             */
            $documentStorageModel = new Intensives_Model_DbTable_DocumentStorage();
            $documentStorageModel->update(array('odt' => $odt), 'id = ' . $params['contract_id']);

            $answer['success'] = 1;
            $answer['contractid'] = $params['contract_id'];
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Сохранение (добавление или редактирование) данных об аудитории в базе.
     *
     * @internal param int $id идентификатор аудитории (если производится добавление,
     * то данный параметр должен быть равен 0) (POST-параметр).
     * @internal param string $name наименование аудитории (POST-параметр).
     *
     * @return string при успешном сохранении: JSON-объект со свойством
     * 'success' равным '1' и свойством 'auditoryid' равным идентификатору
     * сохранённой аудитории.
     * при неуспешном сохранении: JSON-объект со свойством 'success' равным '0'
     * и свойством 'error_message', которое содержить сообщение об ошибке,
     * которое будет выдано пользователю.
     */
    public function saveauditorydataAction()
    {
        $answer = array('success' => 0);

        if ($this->getRequest()->isPost()
            && isset($_POST['id'])
            && isset($_POST['name'])
        ) {
            Intensives_Model_Funcs::asyncCheckAccess($this);

            $auditoryId = $_POST['id'];
            unset($_POST['id']);

            if (isset($_POST['deleted'])) {
                unset($_POST['deleted']);
            }

            $auditoriesModel = new Intensives_Model_DbTable_Auditory();
            $foundedAuditoryId = $auditoriesModel->isExists($_POST['name']);
            if ($foundedAuditoryId && $foundedAuditoryId != $auditoryId) {
                $answer['success'] = 0;
                $answer['error_message'] = 'Аудитория с указанным наименованием уже существует в базе!';
            } else {
                if (0 == $auditoryId) {
                    $auditoryId = $auditoriesModel->insert($_POST);
                } else {
                    $auditoriesModel->update($_POST, array('id = ?' => $auditoryId));
                }

                $answer['success'] = 1;
                $answer['auditoryid'] = $auditoryId;
            }
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Сохранение (добавление или редактирование) данных о договоре в базе.
     *
     * @internal param int $id идентификатор договора (если производится добавление, то
     * данный параметр должен быть равен 0) (POST-параметр).
     * @internal param int $chair_id идентификатор кафедры, за которой закреплён договор
     * (POST-параметр).
     * @internal param string $date дата оформления договора (POST-параметр).
     * @internal param int $listener_id идентификатор слушателя, на которого оформен
     * договор (POST-параметр).
     * @internal param string $start дата начала обучения (POST-параметр).
     * @internal param string $end дата окончания обучения (POST-параметр).
     * @internal param int $hours количество академических часов.
     * @internal param float $total сумма договора (POST-параметр).
     *
     * @return string при успешном сохранении: JSON-объект со свойством
     * 'success' равным '1' и свойством 'contractid' равным идентификатору
     * сохранённого договора.
     * при неуспешном сохранении: JSON-объект со свойством 'success' равным '0'
     * и свойством 'error_message', которое содержить сообщение об ошибке,
     * которое будет выдано пользователю.
     */
    public function savecontractdataAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        $params = $_POST;
        if ($this->getRequest()->isPost()
            && isset($params['methodist_id'])
            && isset($params['contract_id'])
            && isset($params['chair_id'])
            && isset($params['date'])
            && isset($params['listener_id'])
            && isset($params['start'])
            && isset($params['end'])
            && isset($params['hours'])
            && isset($params['total'])
        ) {
            $contractId = $params['contract_id'];
            unset($params['contract_id']);

            $mid = $params['methodist_id'];
            unset($params['methodist_id']);

            // Получение желаемых тем, если они имеются
            if (isset($params['themes'])) {
                $themes = Zend_Json::decode($params['themes']);
                unset($params['themes']);
            }
            // Номер контракта
            $contractNumber = 1;

            // Таблица контрактов
            $contractsModel = new Intensives_Model_DbTable_Contract();
            // Документор для создания документов в формате odt
            $documentor = new Intensives_Model_Documentor();
            // Хранилище документов - для сохранения документа
            $documentStorageModel = new Intensives_Model_DbTable_DocumentStorage();
            // Таблица для првиязки документа к персонала
            $documentToStaffModel = new Intensives_Model_DbTable_DocumentToStaff();
            // Таблица для привязки желаемых тем к контракту
            $contractToLessonThemeModel = new Intensives_Model_DbTable_ContractToLessonTheme();

            /**
             * Если нет такого контракта, то создадим
             */
            if ($contractId == 0) {
                $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
                $db = $bootstrap->getPluginResource('multidb')->getDb('mainDb');
                $db->beginTransaction();

                try {
                    /**
                     * Вставка нового контракта в тбалицу contract
                     */
                    $contractId = $contractsModel->insert($params);

                    /**
                     * Получение вставленной строки и задание имени документа, который будет помещен в хранилище
                     */
                    $row = $contractsModel->fetchRow(
                        $contractsModel->select()
                            ->from(array('c' => 'contract'), array('number'))
                            ->setIntegrityCheck(false)
                            ->join(array('l' => 'listener'), 'c.listener_id = l.id', array(
                                'listenerSurname' => 'surname',
                                'listenerName' => 'name',
                                'listenerPatronymic' => 'patronymic'))
                            ->where('c.id = ?', $contractId)
                            ->limit(1)
                    );

                    $name = $row->listenerSurname . ' ' . $row->listenerName . ' '
                        . $row->listenerPatronymic . ' - ' . $contractNumber = $row->number;
                    $name = $documentToStaffModel->getCorrectDocumentName($name, $mid, null);

                    $category = new Intensives_Model_DocumentCategoryEnum(
                        Intensives_Model_DocumentCategoryEnum::ListenerContract);

                    /**
                     * Генерация документа для данного методиста и заданной категории
                     */
                    $odt = $documentor->generateOdt($category, array('cid' => $contractId));

                    /**
                     * Вставка созданного документа в хранилище document_storage
                     */
                    $documentId = $documentStorageModel->insert(array('odt' => $odt, 'name' => $name,
                        'category' => $category->getValue()));

                    /**
                     * Обновление ранее вставленной строки с контрактом в таблице contract
                     */
                    $contractsModel->update(array('document_id' => $documentId), array('id = ?' => $contractId));

                    /**
                     * Привязка контракта методисту - вставка в таблицу document_to_staff
                     */
                    $documentToStaffModel->insert(array('document_id' => $documentId,
                        'teacher_id' => null, 'methodist_id' => $mid));

                    /**
                     * Вставка в таблицу котракты-темы, если это необходимо
                     */
                    if (isset($themes) && !empty($themes)) {
                        // Мудьтивставка
                        $stmt = $contractToLessonThemeModel->getAdapter()->prepare(
                            'INSERT INTO contract_to_lesson_theme (contract_id, lesson_theme_id) VALUES (?, ?)');
                        foreach ($themes as $theme) {
                            $stmt->execute(array($contractId, $theme['id']));
                        }
                    }

                    $db->commit();
                } catch (Exception $e) {
                    $db->rollBack();
                }

                /**
                 * Иначе обновим контракт по данному id
                 */
            } else {
                unset($params['chair_id']);
                $contractsModel->update($params, array('id = ?' => $contractId));

                /**
                 * Получение контракта и задание имени документа, который будет помещен в хранилище
                 */
                $row = $contractsModel->fetchRow(
                    $contractsModel->select()
                        ->from(array('c' => 'contract'), array('number'))
                        ->setIntegrityCheck(false)
                        ->join(array('l' => 'listener'), 'c.listener_id = l.id', array(
                            'listener_surname' => 'surname',
                            'listener_name' => 'name',
                            'listener_patronymic' => 'patronymic'))
                        ->where('c.id = ?', $contractId)
                        ->limit(1)
                );
                $name = $row->listener_surname . ' ' . $row->listener_name . ' '
                    . $row->listener_patronymic . ' - ' . $contractNumber = $row->number;
                $category = new Intensives_Model_DocumentCategoryEnum(
                    Intensives_Model_DocumentCategoryEnum::ListenerContract);

                /**
                 * Генерация обновленного документа для данного методиста и заданной категории
                 */
                $odt = $documentor->generateOdt($category, array('cid' => $contractId));

                /**
                 * Обновление документа в хранилище документов
                 */
                $documentStorageModel->update(array('odt' => $odt, 'name' => $name), 'id = ' . $contractId);

                /**
                 * Вставка в таблицу котракты-темы, если это необходимо
                 */
                if (isset($themes) && !empty($themes)) {
                    // Удаление старых желаемых тем, связанных с данным контрактом
                    $contractToLessonThemeModel->delete('contract_id = ' . $contractId);

                    // Мультивставка
                    $stmt = $contractToLessonThemeModel->getAdapter()->prepare(
                        'INSERT INTO contract_to_lesson_theme (contract_id, lesson_theme_id) VALUES (?, ?)');
                    foreach ($themes as $theme) {
                        $stmt->execute(array($contractId, $theme['id']));
                    }
                }
            }

            $answer['success'] = 1;
            $answer['contractid'] = $contractId;
            $answer['number'] = $contractNumber;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Сохранение (добавление или редактирование) данных о курсе интенсива в
     * базе.
     *
     * @internal param int $id идентификатор курса (если производится добавление,
     * то данный параметр должен быть равен 0) (POST-параметр).
     * @internal param string $name наименование курса (POST-параметр).
     * @internal param int $chair_id идентификатор кафедры (используется только при
     * добавлении) (POST-параметр).
     *
     * @return string при успешном сохранении: JSON-объект со свойством
     * 'success' равным '1' и свойством 'intensiveid' равным идентификатору
     * сохранённого курса интенсива.
     * при неуспешном сохранении: JSON-объект со свойством 'success' равным '0'
     * и свойством 'error_message', которое содержить сообщение об ошибке,
     * которое будет выдано пользователю.
     */
    public function saveintensivedataAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        $params = $_POST;
        if ($this->getRequest()->isPost()
            && isset($params['id'])
            && isset($params['name'])
            && isset($params['chair_id'])
            && isset($params['themes'])
        ) {
            $id = $params['id'];
            unset($params['id']);

            $themes = Zend_Json::decode($params['themes']);
            unset($params['themes']);

            if (isset($params['deleted'])) {
                unset($params['deleted']);
            }

            $intensivesModel = new Intensives_Model_DbTable_Intensive();
            $lessonThemeModel = new Intensives_Model_DbTable_LessonTheme();
            // Если интенсив только создается
            if ($id == 0) {
                $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
                $db = $bootstrap->getPluginResource('multidb')->getDb();

                /**
                 * Начало транзакции по созданию интенсива и тем рабочей программы
                 */
                $db->beginTransaction();

                try {
                    // Вставка строки в таблицу интенсив
                    $id = $intensivesModel->insert($params);

                    // Вставка связанных с интенсивом тем в таблицу
                    foreach ($themes as $t) {
                        $lessonThemeModel->insert(array(
                            'name' => $t['name'],
                            'number' => $t['number'],
                            'intensive_id' => $id
                        ));
                    }

                    $db->commit();
                } catch (Exception $e) {
                    $db->rollBack();
                }
                // Иначе изменение данных имеющегося интенсива
            } else {
                unset($params['chair_id']);

                $intensivesModel->update($params, array('id = ?' => $id));

                /*
                 * Изменение данных тем, связанных с интенсивом
                 */
                foreach ($themes as $t) {
                    if ($t['status'] == 'updated') {
                        $lessonThemeModel->update(array('name' => $t['name'], 'number' => $t['number']),
                            array('id = ?' => $t['id']));
                    } elseif ($t['status'] == 'deleted') {
                        $lessonThemeModel->update(array('deleted' => 1), array('id = ?' => $t['id']));
                    } elseif ($t['status'] == 'created') {
                        $lessonThemeModel->insert(array(
                            'name' => $t['name'],
                            'number' => $t['number'],
                            'intensive_id' => $id
                        ));
                    }
                }
            }

            $answer['success'] = 1;
            $answer['intensiveid'] = $id;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Сохранение (добавление или редактирование) данных о занятии в базе.
     *
     * @internal param int $id идентификатор занятия (если производится добавление,
     * то данный параметр должен быть равен 0) (POST-параметр).
     * @internal param int $intensive_id идентификатор курса интенсива (POST-параметр).
     * @internal param string $start дата начала занятия (POST-параметр).
     * @internal param string $end дата окончания занятия (POST-параметр).
     * @internal param string $theme тема занятия (POST-параметр).
     * @internal param int $teacher_id идентификатор преподавателя, который будет
     * проводить занятие (POST-параметр).
     * @internal param int $auditory_id идентификатор аудитории, в которой будет
     * проходить занятие (POST-параметр).
     * @internal param int $status статус занятия (Черновик, Утверждено, Проведено)
     * (POST-параметр).
     * @internal param int $type тип занятия (Лекционное, Практическое,
     * Лекционно-практическое) (POST-параметр).
     *
     * @return string при успешном сохранении: JSON-объект со свойством
     * 'success' равным '1' и свойством 'lessonid' равным идентификатору
     * сохранённого занятия.
     * при неуспешном сохранении: JSON-объект со свойством 'success' равным '0'
     * и свойством 'error_message', которое содержить сообщение об ошибке,
     * которое будет выдано пользователю.
     */
    function savelessondataAction()
    {
        $answer = array('success' => 0);

        if ($this->getRequest()->isPost()
            && isset($_POST['id'])
            && isset($_POST['intensive_id'])
            && isset($_POST['start'])
            && isset($_POST['end'])
            && isset($_POST['theme_id'])
            && isset($_POST['teacher_id'])
            && isset($_POST['auditory_id'])
            && isset($_POST['status'])
            && isset($_POST['type'])
        ) {
            Intensives_Model_Funcs::asyncCheckAccess($this);

            $lessonId = $_POST['id'];
            unset($_POST['id']);

            if (isset($_POST['deleted'])) {
                unset($_POST['deleted']);
            }

            $lessonsModel = new Intensives_Model_DbTable_Lesson();
            if (0 == $lessonId) {
                $lessonId = $lessonsModel->insert($_POST);
            } else {
                $lessonsModel->update($_POST, array('id = ?' => $lessonId));
            }

            $answer['success'] = 1;
            $answer['lessonid'] = $lessonId;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Сохранение (добавление или редактирование) данных о слушателе в базе.
     *
     * @internal param int $id идентификатор слушателя (если производится добавление,
     * то данный параметр должен быть равен 0) (POST-параметр).
     * @internal param string $surname фамилия слушателя (POST-параметр).
     * @internal param string $name имя слушателя (POST-параметр).
     * @internal param string $patronymic отчество слушателя (POST-параметр).
     * @internal param string $group группа слушателя (POST-параметр).
     * @internal param string $phone контактный телефон слушателя (POST-параметр).
     * @internal param string $address адрес слушателя (POST-параметр).
     * @internal param string $passport_series серия паспорта слушателя (POST-параметр).
     * @internal param string $passport_number номер паспорта слушателя (POST-параметр).
     * @internal param string $passport_givenout кем выдан паспорт слушателя
     * (POST-параметр).
     * @internal param string $passport_givenout_when когда выдан паспорт слушателя
     * (POST-параметр).
     * @internal param string $email электронная почта слушателя (POST-параметр).
     * @internal param string $additional_info дополнительная информация о слушателе
     * (POST-параметр).
     *
     * @return string при успешном сохранении: JSON-объект со свойством
     * 'success' равным '1' и свойством 'listenerid' равным идентификатору
     * сохранённого слушателя.
     * при неуспешном сохранении: JSON-объект со свойством 'success' равным '0'
     * и свойством 'error_message', которое содержить сообщение об ошибке,
     * которое будет выдано пользователю.
     */
    public function savelistenerdataAction()
    {
        $answer = array('success' => 0);

        if ($this->getRequest()->isPost()
            && isset($_POST['id'])
            && isset($_POST['surname'])
            && isset($_POST['name'])
            && isset($_POST['patronymic'])
            && isset($_POST['group'])
            && isset($_POST['phone'])
            && isset($_POST['address'])
            && isset($_POST['passport_series'])
            && isset($_POST['passport_number'])
            && isset($_POST['passport_givenout'])
            && isset($_POST['passport_givenout_when'])
            && isset($_POST['email'])
            && isset($_POST['additional_info'])
        ) {
            Intensives_Model_Funcs::asyncCheckAccess($this);

            $listenerId = $_POST['id'];
            unset($_POST['id']);

            if (isset($_POST['deleted'])) {
                unset($_POST['deleted']);
            }

            $listenersModel = new Intensives_Model_DbTable_Listener();
            $foundedListenerId = $listenersModel->isExists($_POST['passport_series'], $_POST['passport_number']);
            if ($foundedListenerId && $foundedListenerId != $listenerId) {
                $answer['success'] = 0;
                $answer['error_message'] = 'Слушатель с указанными серией и номером паспорта уже существует в базе!';
            } else {
                if (0 == $listenerId) {
                    $listenerId = $listenersModel->insert($_POST);
                } else {
                    $listenersModel->update($_POST, array('id = ?' => $listenerId));
                }

                $answer['success'] = 1;
                $answer['listenerid'] = $listenerId;
            }
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Сохранение (добавление или редактирование) данных о преподавателе в базе.
     *
     * @internal param int $id идентификатор преподавателя (если производится добавление,
     * то данный параметр должен быть равен 0) (POST-параметр).
     * @internal param int $chair_id идентификатор кафедры, за которой закреплён
     * преподаватель (POST-параметр).
     * @internal param string $surname фамилия преподавателя (POST-параметр).
     * @internal param string $name имя преподавателя (POST-параметр).
     * @internal param string $patronymic отчество преподавателя (POST-параметр).
     * @internal param string $rank должность преподавателя (POST-параметр).
     * @internal param string $att_num № атт.ВАК преподавателя (POST-параметр).
     * @internal param string $diplom_num № диплома преподавателя (POST-параметр).
     * @internal param string $login логин преподавателя (POST-параметр).
     * @internal param string $password пароль преподавателя (POST-параметр).
     *
     * @return string при успешном сохранении: JSON-объект со свойством
     * 'success' равным '1' и свойством 'teacherid' равным идентификатору
     * сохранённого преподавателя.
     * при неуспешном сохранении: JSON-объект со свойством 'success' равным '0'
     * и свойством 'error_message', которое содержить сообщение об ошибке,
     * которое будет выдано пользователю.
     */
    public function saveteacherdataAction()
    {
        $answer = array('success' => 0);

        if ($this->getRequest()->isPost()
            && isset($_POST['id'])
            && isset($_POST['chair_id'])
            && isset($_POST['surname'])
            && isset($_POST['name'])
            && isset($_POST['patronymic'])
            && isset($_POST['rank'])
            && isset($_POST['att_num'])
            && isset($_POST['diplom_num'])
            && isset($_POST['login'])
            && isset($_POST['password'])
        ) {
            Intensives_Model_Funcs::asyncCheckAccess($this);

            $teacherId = $_POST['id'];
            unset($_POST['id']);

            if (isset($_POST['deleted'])) {
                unset($_POST['deleted']);
            }
            if (empty($_POST['password'])) {
                unset($_POST['password']);
            }

            $teachersModel = new Intensives_Model_DbTable_Teacher();
            $foundTeacherId = $teachersModel->isExists($_POST['login']);
            if ($foundTeacherId && $foundTeacherId != $teacherId) {
                $answer['success'] = 0;
                $answer['error_message'] = 'Преподаватель с указанным логином уже существует в базе!';
            } else {
                if (isset($_POST['password'])) {
                    $_POST['password'] = md5($_POST['password']);
                }
                if (0 == $teacherId) {
                    $teacherId = $teachersModel->insert($_POST);
                } else {
                    $teachersModel->update($_POST, array('id = ?' => $teacherId));
                }

                $answer['success'] = 1;
                $answer['teacherid'] = $teacherId;
            }
        }

        echo Zend_Json::encode($answer);
    }

    public function saveteacherdatasafeAction()
    {
        $answer = array('success' => 0);

        if ($this->getRequest()->isPost()
            && isset($_POST['chair_id'])
            && isset($_POST['surname'])
            && isset($_POST['name'])
            && isset($_POST['patronymic'])
            && isset($_POST['rank'])
            && isset($_POST['att_num'])
            && isset($_POST['diplom_num'])
            && isset($_POST['login'])
            && isset($_POST['password'])
        ) {
            Intensives_Model_Funcs::asyncCheckAccess($this);
            $userData = Zend_Auth::getInstance()->getStorage()->read();
            $teacherId = $userData->id;

            if (isset($_POST['deleted'])) {
                unset($_POST['deleted']);
            }
            if (empty($_POST['password'])) {
                unset($_POST['password']);
            }

            $teachersModel = new Intensives_Model_DbTable_Teacher();
            $foundTeacherId = $teachersModel->isExists($_POST['login']);
            if ($foundTeacherId && $foundTeacherId != $teacherId) {
                $answer['success'] = 0;
                $answer['error_message'] = 'Преподаватель с указанным логином уже существует в базе!';
            } else {
                if (isset($_POST['password'])) {
                    $_POST['password'] = md5($_POST['password']);
                }
                if (0 == $teacherId) {
                    $teacherId = $teachersModel->insert($_POST);
                } else {
                    $teachersModel->update($_POST, array('id = ?' => $teacherId));
                }

                $answer['success'] = 1;
                $answer['teacherid'] = $teacherId;
            }
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Пометка договора как 'неоплаченный'.
     *
     * @internal int $contract_id идентификатор (номер) договора (POST-параметр).
     *
     * @return string при успешной пометке: JSON-объект со свойством 'success'
     * равным '1' и свойством 'contractid' равным идентификатору договора.
     * при неуспешной отметке: JSON-объект со свойством 'success' равным '0'
     * и свойством 'error_message', которое содержить сообщение об ошибке,
     * которое будет выдано пользователю.
     */
    public function unpaycontractAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        $params = $_POST;
        if ($this->getRequest()->isPost() && isset($params['contract_id'])
        ) {
            $contractsModel = new Intensives_Model_DbTable_Contract();
            $contractsModel->unpay($params['contract_id']);

            /**
             * Генерация обновленного документа для данного методиста и заданной категории
             */
            $documentor = $documentor = new Intensives_Model_Documentor();
            $category = new Intensives_Model_DocumentCategoryEnum(
                Intensives_Model_DocumentCategoryEnum::ListenerContract);
            $odt = $documentor->generateOdt($category, array('cid' => $params['contract_id']));

            /**
             * Обновление документа в хранилище документов
             */
            $documentStorageModel = new Intensives_Model_DbTable_DocumentStorage();
            $documentStorageModel->update(array('odt' => $odt), 'id = ' . $params['contract_id']);

            $answer['success'] = 1;
            $answer['contractid'] = $params['contract_id'];
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получить доступные темы для данной кафедры
     * @internal int $cid идентификатор кафедры
     * @return string при успешной пометке: JSON-объект со свойством 'success'
     * равным '1' и свойством 'themes' с темами.
     * при неуспешной отметке: JSON-объект со свойством 'success' равным '0'
     * и свойством 'error_message', которое содержить сообщение об ошибке,
     * которое будет выдано пользователю.
     */
    public function getchairlessonthemesAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        if (!is_null($cid = $this->getRequest()->getParam('cid'))) {
            $cid = (int) $cid;

            $lessonThemeModel = new Intensives_Model_DbTable_LessonTheme();
            $select = $lessonThemeModel->select()
                ->setIntegrityCheck(false)
                ->from(array('l' => 'lesson_theme'), array('id', 'name'))
                ->joinInner(array('i' => 'intensive'), 'l.intensive_id = i.id', array())
                ->where('i.chair_id = ?', $cid)
                ->where('l.deleted = 0')
                ->order(array('name ASC'));
            $rowSet = $lessonThemeModel->fetchAll($select);

            $answer['themes'] = $rowSet->toArray();
            $answer['success'] = 1;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получить доступные темы для данного интенсива
     * @internal int $iid идентификатор интенсива
     */
    public function getintensivethemesAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        if (!is_null($iid = $this->getRequest()->getParam('iid'))) {
            $iid = (int) $iid;

            $lessonThemeModel = new Intensives_Model_DbTable_LessonTheme();
            $select = $lessonThemeModel->select()
                ->setIntegrityCheck(false)
                ->from(array('l' => 'lesson_theme'), array('id', 'name'))
                ->where('l.intensive_id = ?', $iid)
                ->where('l.deleted = 0')
                ->order(array('name ASC'));
            $rowSet = $lessonThemeModel->fetchAll($select);

            $answer['themes'] = $rowSet->toArray();
            $answer['success'] = 1;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получиить данные о теме дополнительного занятия
     *
     * @internal int $ltId идентификатор темы дополнительного занятия
     * @return string при успешной пометке: JSON-объект со свойством 'success'
     * равным '1' и свойством 'lessonthemedata' с информацией о теме.
     * при неуспешной отметке: JSON-объект со свойством 'success' равным '0'
     * и свойством 'error_message', которое содержить сообщение об ошибке,
     * которое будет выдано пользователю.
     */
    public function getlessonthemedataAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        if (!is_null($ltId = $this->getRequest()->getParam('ltid'))) {
            $lessonThemeModel = new Intensives_Model_DbTable_LessonTheme();
            $row = $lessonThemeModel->find($ltId);
            if ($row->count() == 1) {
                $answer['lessonthemedata'] = $row->current()->toArray();
                $answer['success'] = 1;
            }
        }

        echo Zend_Json::encode($answer);
    }
}