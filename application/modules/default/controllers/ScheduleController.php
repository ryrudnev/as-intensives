<?php

/**
 * Определяет контроллер для действий с расписанием
 * Class ScheduleController
 */
class ScheduleController extends Zend_Controller_Action
{
    /**
     * Мепперы для массивов дней, пар, времени пар и типов занятий
     */
    private $_daysMapper = array(
        1 => '1 Понедельник',
        2 => '2 Вторник',
        3 => '3 Среда',
        4 => '4 Четверг',
        5 => '5 Пятница',
        6 => '6 Суббота',
        '1 Понедельник' => 1,
        '2 Вторник' => 2,
        '3 Среда' => 3,
        '4 Четверг' => 4,
        '5 Пятница' => 5,
        '6 Суббота' => 6
    );
    private $_pairsMapper = array(
        '1 пара' => array(1, 2),
        '2 пара' => array(3, 4),
        '3 пара' => array(5, 6),
        '4 пара' => array(7, 8),
        '5 пара' => array(9, 10),
        '6 пара' => array(11, 12),
        1 => array(1, 2),
        2 => array(3, 4),
        3 => array(5, 6),
        4 => array(7, 8),
        5 => array(9, 10),
        6 => array(11, 12)
    );
    private $_timeMapper = array(
        1 => array(8, 30),
        2 => array(10, 00),
        3 => array(10, 10),
        4 => array(11, 40),
        5 => array(11, 50),
        6 => array(13, 20),
        7 => array(13, 30),
        8 => array(15, 00),
        9 => array(15, 10),
        10 => array(16, 40),
        11 => array(16, 50),
        12 => array(18, 20),
        '8-30' => 1,
        '10-00' => 2,
        '10-10' => 3,
        '11-40' => 4,
        '11-50' => 5,
        '13-20' => 6,
        '13-30' => 7,
        '15-00' => 8,
        '15-10' => 9,
        '16-40' => 10,
        '16-50' => 11,
        '18-20' => 12
    );
    private $_lessonTypesMapper = array(
        0 => '',
        1 => 'Л.',
        2 => 'Пр.',
        3 => 'Лб.'
    );

    /**
     * Получить загруженные xls файлы расписания
     * @return array xls файлы
     */
    private function _getUploadedXls()
    {
        $xls = array();

        $session = new Zend_Session_Namespace('schedule');
        if (isset($session->uploadedXlsFiles)) {
            $xls = $session->uploadedXlsFiles;
        }

        return $xls;
    }

    /**
     * Парсить расписание занаятий для составления кафедрального по указанным преподавателям
     * @param array $teachers имена учителей
     * @param array $xls файлы расписания
     * @return array распарсенное расписание
     */
    private function _parseSchedule(array $teachers, array $xls)
    {
        require_once('vstu_schedule_parser.php');

        $parsedSchedule = array();
        if (!empty($teachers)) {
            foreach ($xls as $x) {
                $parsed = parse_schedule($x, $teachers, null);
                foreach ($parsed as $p) {
                    $parsedSchedule[] = $p;
                }

            }
        }

        return $parsedSchedule;
    }

    /**
     * Инициализирует класс-контроллер: отключение авторендеринга, так как
     * действия класса будут выдавать только строку с JSON-объектом - рендеринг
     * здесь не нужен.
     */
    public function init()
    {
        // Проверка аутентификации
        $auth = Zend_Auth::getInstance();
        if (!$auth->hasIdentity()) {
            $answer = array(
                'success' => 0,
                'error_code' => 1,
                'error_message' => 'Вы не вошли в систему'
            );

            die(Zend_Json::encode($answer));
        }

        // Отключение рендеринга лайаута страницы
        $this->_helper->layout()->disableLayout();

        // Отключение рендеринга представлений действий
        $this->_helper->viewRenderer->setNoRender(true);
    }

    /**
     * Загрузить xls файлы с расписанием
     */
    public function scheduleuploadAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        $httpAdapter = new Zend_File_Transfer_Adapter_Http();
        /*$httpAdapter->addValidator('MimeType', false, array(
            'application/vnd.ms-excel', 'application/msexcel', 'application/x-msexcel',
            'application/x-ms-excel', 'application/vnd.ms-excel', 'application/x-excel',
            'application/x-dos_ms_excel', 'application/xls')
        );
		*/
        if ($httpAdapter->receive()) {
            $uploadedFiles = $httpAdapter->getFileName();

            if (!is_array($uploadedFiles)) {
                $uploadedFiles = array($uploadedFiles);
            }

            $session = new Zend_Session_Namespace('schedule');
            $session->uploadedXlsFiles = $uploadedFiles;

            $answer['success'] = 1;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получить кафедральное расписание
     * @internal int $cid идентификатор кафедры
     */
    public function getchairscheduleAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        if ($cid = $this->getRequest()->getParam('cid')) {
            $scheduleModel = new Intensives_Model_DbTable_TeacherVstuSchedule();
            $teachersModel = new Intensives_Model_DbTable_Teacher();

            // Получение всех учетелей с указанной кафедры
            $teachers = $teachersModel->fetchTeachersByChairId($cid)->toArray();

            // Получения расписания для указанной кафедры
            // Задание итератора для обхода занятий в расписании
            $schedule = $scheduleModel->getChairSchedule($cid)->toArray();
            $scheduleObj = new ArrayObject($schedule);
            $lessonItr = $scheduleObj->getIterator();

            // Составление расписания на основе занятий из БД. Если занятие из БД отсутсвует, то вмсето него
            // формируется пустое занятие по шаблону для последующего редактирования.
            $schedule = array();
            // Идентификаторы записей которых нет в БД будут отрицательны.
            $id = 0;
            for ($week = 1; $week < 3; $week++) {
                for ($day = 1; $day < 7; $day++) {
                    for ($pair = 1; $pair < 7; $pair++) {
                        foreach ($teachers as $teacher) {
                            $current = $lessonItr->current();
                            if ($lessonItr->valid()
                                && $current['week'] == $week
                                && $current['day'] == $day
                                && $current['lesson_start'] == $this->_pairsMapper[$pair][0]
                                && $current['lesson_end'] == $this->_pairsMapper[$pair][1]
                                && $current['teacher_id'] == $teacher['id']) {
                                    $lesson = array(
                                        'id' => $current['id'],
                                        'week' => $week,
                                        'day' => $this->_daysMapper[$day],
                                        'pair' => $pair . ' пара',
                                        'teacher' => $current['teacher'],
                                        'teacherId' => $teacher['id'],
                                        'lesson' => $current['lesson_name'],
                                        'lessonType' => $this->_lessonTypesMapper[$current['lesson_type']],
                                        'auditory' => $current['auditory'],
                                    );
                                    // Переход к следующему занятию
                                    $lessonItr->next();
                            } else {
                                $lesson = array(
                                    'id' => --$id,
                                    'week' => $week,
                                    'day' => $this->_daysMapper[$day],
                                    'pair' => $pair . ' пара',
                                    'teacher' => $teacher['surname'] . ' '
                                        . mb_substr($teacher['name'], 0, 1, 'UTF-8') . '. '
                                        . mb_substr($teacher['patronymic'], 0, 1, 'UTF-8') . '.',
                                    'teacherId' => $teacher['id'],
                                    'lesson' => '',
                                    'lessonType' => $this->_lessonTypesMapper[0],
                                    'auditory' => ''
                                );
                            }
                            // Добавление занятия
                            $schedule[] = $lesson;
                        }
                    }
                }
            }

            $answer['schedule'] = $schedule;
            $answer['success'] = 1;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получить кафедральное расписание в виде xls файла
     * @internal int $cid идентификатор кафедры
     */
    public function getxlsscheduleAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        if ($cid = $this->getRequest()->getParam('cid')) {
            $scheduleModel = new Intensives_Model_DbTable_TeacherVstuSchedule();
            $teachersModel = new Intensives_Model_DbTable_Teacher();

            // Получение расписания
            $schedule = $scheduleModel->getChairSchedule($cid)->toArray();
            $scheduleObj = new ArrayObject($schedule);
            $lessonItr = $scheduleObj->getIterator();

            // Получение всех учетелей с указанной кафедры в формате (id => имя преподавателя)
            $rowSet = $teachersModel->fetchTeachersByChairId($cid);
            $teachers = array();
            while ($rowSet->valid()) {
                $row = $rowSet->current();
                $teachers[$row->id] = $row->surname . ' '
                    . mb_substr($row->name, 0, 1, 'UTF-8') . '. '
                    . mb_substr($row->patronymic, 0, 1, 'UTF-8') . '.';
                $rowSet->next();
            }

            // Высота и ширина ячейки с занятием в расписании
            $HEIGHT_CELL_SCHEDULE = 3;
            $WIDTH_CELL_SCHEDULE = 5;

            // Количестов строк и столбцов в расписании
            $ROW_SCHEDULE = $HEIGHT_CELL_SCHEDULE * 6 * 6 * 2;
            $COL_SCHEDULE = $WIDTH_CELL_SCHEDULE * count($teachers);

            // Количество всех строк и столбцов
            $ALL_ROWS = $ROW_SCHEDULE + 2;
            $ALL_COLS = $COL_SCHEDULE + 4;

            require_once('phpexcel/PHPExcel.php');

            $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
            $cacheSettings = array( 'memoryCacheSize' => '32');
            PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

            // Создание объекта excel
            $objPHPExcel = new PHPExcel();

            // Формирование столбца недели
            $objPHPExcel->getActiveSheet()->setCellValue('B3', '1 Неделя');
            $range = 'B3:' . 'B' . ($ROW_SCHEDULE / 2 + 2);
            $objPHPExcel->getActiveSheet()->mergeCells($range);

            $objPHPExcel->getActiveSheet()->setCellValue('B' . (3 + $ROW_SCHEDULE / 2), '2 Неделя');
            $range = 'B' . (3 + $ROW_SCHEDULE / 2) . ':B' . ($ROW_SCHEDULE + 2);
            $objPHPExcel->getActiveSheet()->mergeCells($range);

            $range = 'B3:' . 'B' . ($ROW_SCHEDULE + 2);
            $objPHPExcel->getActiveSheet()->getStyle($range)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle($range)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
            $objPHPExcel->getActiveSheet()->getStyle($range)->getAlignment()->setTextRotation(90);
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFont()->setSize(36);

            $days = array(
                1 => 'Понедельник',
                2 => 'Вторник',
                3 => 'Среда',
                4 => 'Четверг',
                5 => 'Пятница',
                6 => 'Суббота',
            );

            // Формирование столбца дня недели
            for ($i = 0; $i < 6; $i++) {
                $i1 = 3 + 6 * $i * $HEIGHT_CELL_SCHEDULE;
                $index1 = 'C' . $i1;
                $i2 = (3 + $ROW_SCHEDULE / 2) + (6 * $HEIGHT_CELL_SCHEDULE * $i);
                $index2 = 'C' . $i2;

                $objPHPExcel->getActiveSheet()->setCellValue($index1, $days[$i + 1])->setCellValue($index2, $days[$i + 1]);
                $range = $index1 . ':' . 'C' . ($i1 + 6 * $HEIGHT_CELL_SCHEDULE - 1);
                $objPHPExcel->getActiveSheet()->mergeCells($range);

                $range = $index2 . ':' . 'C' . ($i2 + 6 * $HEIGHT_CELL_SCHEDULE - 1);
                $objPHPExcel->getActiveSheet()->mergeCells($range);

                // Формирование пары
                for ($j = 0; $j < 6; $j++) {
                    $j1 = $i1 + $j * $HEIGHT_CELL_SCHEDULE;
                    $j2 = $i2 + $j * $HEIGHT_CELL_SCHEDULE;
                    $index1 = 'D' . $j1;
                    $index2 = 'D' . $j2;
                    $objPHPExcel->getActiveSheet()->setCellValue($index1, $this->_pairsMapper[$j + 1][0] . '-' . $this->_pairsMapper[$j + 1][1]);
                    $range = $index1 . ':' . 'D' . ($j1 + $HEIGHT_CELL_SCHEDULE - 1);
                    $objPHPExcel->getActiveSheet()->mergeCells($range);

                    $objPHPExcel->getActiveSheet()->setCellValue($index2, $this->_pairsMapper[$j + 1][0] . '-' . $this->_pairsMapper[$j + 1][1]);
                    $range = $index2 . ':' . 'D' . ($j2 + $HEIGHT_CELL_SCHEDULE - 1);
                    $objPHPExcel->getActiveSheet()->mergeCells($range);
                }
            }

            // Задание стилей для дней и пар
            $range = 'C3:' . 'C' . ($ROW_SCHEDULE + 2);
            $objPHPExcel->getActiveSheet()->getStyle($range)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle($range)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
            $objPHPExcel->getActiveSheet()->getStyle($range)->getAlignment()->setTextRotation(90);
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFont()->setSize(26);

            $range = 'D3:' . 'D' . ($ROW_SCHEDULE + 2);
            $objPHPExcel->getActiveSheet()->getStyle($range)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle($range)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle($range)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFont()->setSize(16);

            $range = 'B2:D2';
            $objPHPExcel->getActiveSheet()->mergeCells($range);
            $objPHPExcel->getActiveSheet()->getStyle($range)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);

            // Задание преподавателей
            $index = 4;
            foreach ($teachers as $teacher) {
                $col1 = PHPExcel_Cell::stringFromColumnIndex($index);
                $index += $WIDTH_CELL_SCHEDULE - 1;
                $col2 = PHPExcel_Cell::stringFromColumnIndex($index);

                $objPHPExcel->getActiveSheet()->setCellValue($col1 . 2, $teacher);
                $range = $col1 . 2 . ':' . $col2 . 2;
                $objPHPExcel->getActiveSheet()->mergeCells($range);

                for ($j = 3; $j <= $ALL_ROWS; $j += $HEIGHT_CELL_SCHEDULE) {
                    $range = $col1 . $j . ':' . $col2 . ($j + $HEIGHT_CELL_SCHEDULE - 1);
                    $objPHPExcel->getActiveSheet()->mergeCells($range);
                }

                $index++;
            }

            $col = PHPExcel_Cell::stringFromColumnIndex($ALL_COLS - 1);
            $range = 'E2:' . $col . 2;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle($range)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle($range)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFont()->setSize(24);
            $range = 'E3:' . $col . $ALL_ROWS;
            $objPHPExcel->getActiveSheet()->getStyle($range)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle($range)->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle($range)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
            $objPHPExcel->getActiveSheet()->getStyle($range)->getFont()->setSize(11);

            $schedule = array();
            for ($week = 1; $week < 3; $week++) {
                for ($day = 1; $day < 7; $day++) {
                    for ($pair = 1; $pair < 7; $pair++) {
                        $row = array();
                        foreach ($teachers as $tid => $teacher) {
                            $current = $lessonItr->current();
                            if ($lessonItr->valid()
                                && $current['week'] == $week
                                && $current['day'] == $day
                                && $current['lesson_start'] == $this->_pairsMapper[$pair][0]
                                && $current['lesson_end'] == $this->_pairsMapper[$pair][1]
                                && $current['teacher_id'] == $tid) {
                                $lesson = $current['auditory'] . ' ';
                                $lesson .= $this->_lessonTypesMapper[$current['lesson_type']] . ' ';
                                $lesson .= $current['lesson_name'] . "\n";
                                $lesson .= $current['groups'] . "\n";
                                $lesson .= $current['additional_dates'];
                                $lessonItr->next();
                            } else {
                                $lesson = null;
                            }
                            $row[] = $lesson;
                            for ($i = 1; $i < $WIDTH_CELL_SCHEDULE; $i++) {
                                $row[] = null;
                            }
                        }

                        $schedule[] = $row;
                        for ($i = 1; $i < $HEIGHT_CELL_SCHEDULE; $i++) {
                            $row = array();
                            for ($j = 0; $j < count($teachers); $j++) {
                                for ($k = 0; $k < $WIDTH_CELL_SCHEDULE; $k++) {
                                    $row[] = null;
                                }
                            }
                            $schedule[] = $row;
                        }
                    }
                }
            }

            $objPHPExcel->getActiveSheet()->fromArray($schedule, null, 'E3');
            $objPHPExcel->getActiveSheet()->setTitle('Кафедральное расписание');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Кафедральное расписание.xls"');
            $objWriter->save('php://output');

            $answer['success'] = 1;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Парсить загруженные xls файлы с расписанием и сформировать на основе них кафедральное
     * @internal int $cid идентификатор кафедры
     */
    public function parsescheduleAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        if (!is_null($cid = $this->getRequest()->getParam('cid'))) {
            $cid = (int) $cid;

            // Получение преподавателей
            $teachersModel = new Intensives_Model_DbTable_Teacher();
            $rowSet = $teachersModel->fetchTeachersByChairId($cid);
            $teachers = array();
            while ($rowSet->valid()) {
                $row = $rowSet->current();
                $teachers[$row->id] = $row->surname;
                $rowSet->next();
            }

            // Распарсить xls документ и получить занятия преподавателей
            $schedule = $this->_parseSchedule(array_values($teachers), $this->_getUploadedXls());
            if (!empty($schedule)) {
                // Удалить существующее расписание для данных преподавателей
                $scheduleModel = new Intensives_Model_DbTable_TeacherVstuSchedule();
                $scheduleModel->deleteTeachersSchedule(array_keys($teachers));
                // Поиск аудиторий, в случаи если нет аудитории в БД, то добавить.
                $auditoryModel = new Intensives_Model_DbTable_Auditory();
                foreach ($schedule as $lesson) {
                    // Для каждого из преподавателей найти его занятие и добавить в БД
                    foreach ($teachers as $id => $teacher) {
                        $quotedTeacher = preg_quote($teacher);
                        if (preg_match("/{$quotedTeacher}/ui", $lesson['name'])) {
                            $matches = array();
                            preg_match('/([0-9]+)\-([0-9]+)/', $lesson['time'], $matches);
                            $start = (int)$matches[1];
                            $end = (int)$matches[2];

                            // Отловить не валидно распарсенные занятия
                            // Елси нет недели, дня, номера пары и названия предмета - то не добавлять такое занятие в БД
                            if ($lesson['week'] && $lesson['day'] && $start && $end && $lesson['lesson']) {
                                // Проверка наличия адуитории - если нет создать, если нет наименования - пропустить
                                $result = $auditoryModel->isExists($lesson['auditory'], true);
                                if (!empty($lesson['auditory']) && $result == false) {
                                    $result = $auditoryModel->insert(array('name' => $lesson['auditory'], 'deleted' => 0));
                                }
                                $scheduleModel->insert(array(
                                    'teacher_id' => $id,
                                    'lesson_name' => $lesson['lesson'],
                                    'lesson_start' => $start,
                                    'lesson_end' => $end,
                                    'auditory_id' => $result == false ? null : $result,
                                    'lesson_type' => 0,
                                    'additional_dates' => $lesson['additional_dates'],
                                    'groups' => '',
                                    'day' => $lesson['day'],
                                    'week' => $lesson['week']
                                ));
                                break;
                            }
                        }
                    }
                }

                $answer['success'] = 1;
            }
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получить вузвовское занятие по указанному идентификатору
     * @internal int $id идентификатор вузовского занятия
     */
    public function getvstulessonAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);
        $answer['lessondata'] = array();

        if (!is_null($id = $this->getRequest()->getParam('id'))) {
            $id = (int) $id;
            $scheduleModel = new Intensives_Model_DbTable_TeacherVstuSchedule();
            try {
                $rowSet = $scheduleModel->fetchVstuLessons('tvs.id = '. $id);
                if ($rowSet->count()) {
                    $row = $rowSet->current();
                    $answer['lessondata']['id'] = $row->id;
                    $answer['lessondata']['teacherId'] = $row->teacher_id;
                    $answer['lessondata']['teacher'] = $row->teacher;
                    $answer['lessondata']['lessonStart'] = $row->lesson_start;
                    $answer['lessondata']['lessonEnd'] = $row->lesson_end;
                    $answer['lessondata']['lessonName'] = $row->lesson_name;
                    $answer['lessondata']['lessonType'] = $row->lesson_type;
                    $answer['lessondata']['auditoryId'] = $row->auditory_id;
                    $answer['lessondata']['auditory'] = $row->auditory;
                    $answer['lessondata']['addDates'] = $row->additional_dates;
                    $answer['lessondata']['groups'] = $row->groups;
                    $answer['lessondata']['day'] = $row->day;
                    $answer['lessondata']['week'] = $row->week;

                    $answer['success'] = 1;
                }
            } catch (Exception $exc) {

            }
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Сохранить вузвовское занятие с указанными данными
     * @internal int $id идентификатор вузовского занятия в случаи обновления, иначе 0 в случаи создания нового
     * @internal string (json) $data данные вузовского занятия:
     * ключи - {teacherId, lessonName, lessonStart, lessonEnd, auditory, lessonType, addDates, groups, day, week}
     */
    public function savevstulessonAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        if (!is_null($id = $this->getRequest()->getParam('id')) &&
            !is_null($data = $this->getRequest()->getParam('data'))
        ) {
            $id = (int)$id;
            $data = Zend_Json::decode($data);
            $isset = isset($data['teacherId']) && isset($data['lessonName']) && isset($data['lessonStart']) &&
                isset($data['lessonEnd']) && isset($data['auditoryId']) && isset($data['lessonType']) &&
                isset($data['addDates']) && isset($data['groups']) && isset($data['day']) && isset($data['week']);
            if ($isset) {
                $scheduleModel = new Intensives_Model_DbTable_TeacherVstuSchedule();
                $result = $scheduleModel->checkAvailableTime($data['teacherId'], $data);
                if (is_bool($result) || ($id && $result == $id)) {
                    $d = array();
                    $d['teacher_id'] = $data['teacherId'];
                    $d['lesson_name'] = $data['lessonName'];
                    $d['lesson_start'] = $data['lessonStart'];
                    $d['lesson_end'] = $data['lessonEnd'];
                    $d['auditory_id'] = $data['auditoryId'];
                    $d['lesson_type'] = $data['lessonType'];
                    $d['additional_dates'] = $data['addDates'];
                    $d['groups'] = $data['groups'];
                    $d['day'] = $data['day'];
                    $d['week'] = $data['week'];
                    try {
                        if ($id) {
                            $scheduleModel->update($d, 'id = ' . $id);
                        } else {
                            $id = $scheduleModel->insert($d);
                        }
                        $answer['success'] = 1;
                        $answer['id'] = $id;
                    } catch (Exception $exc) {

                    }
                }
            }
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Удалить вузовское занятие
     * @internal int $id идентификатор вузовского занятия
     */
    public function deletevstulessonAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        if (!is_null($id = $this->getRequest()->getParam('id'))) {
            $id = (int)$id;

            $scheduleModel = new Intensives_Model_DbTable_TeacherVstuSchedule();
            try {
                $scheduleModel->delete('id = '. $id);
                $answer['success'] = 1;
            } catch (Exception $exc) {

            }
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Изменить фактическое время вузовского занятия
     * @internal int $id идентификатор вузовского занятия
     * @internal int $time временные характеристики вузовского занятия
     */
    public function changevstulessontimeAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);
        if (!is_null($id = $this->getRequest()->getParam('id')) &&
            !is_null($time = $this->getRequest()->getParam('time'))
        ) {
            $id = (int)$id;
            $time = Zend_Json::decode($time);

            $scheduleModel = new Intensives_Model_DbTable_TeacherVstuSchedule();
            if ($row = $scheduleModel->fetchRow('id = ' . $id)) {
                $time['week'] = isset($time['week']) ? $time['week'] : $row->week;
                $time['day'] = isset($time['day']) ? $time['day'] : $row->day;
                $time['lessonStart'] = isset($time['lessonStart']) ? $time['lessonStart'] : $row->lesson_start;
                $time['lessonEnd'] = isset($time['lessonEnd']) ? $time['lessonEnd'] : $row->lesson_end;

                $result = $scheduleModel->checkAvailableTime($row->teacher_id, $time);
                if (is_bool($result) || ($id && $result == $id)) {
                    $d = array(
                        'lesson_start' => $time['lessonStart'],
                        'lesson_end' => $time['lessonEnd'],
                        'day' => $time['day'],
                        'week' => $time['week']
                    );
                    try {
                        $scheduleModel->update($d, 'id = ' . $id);
                        $answer['success'] = 1;
                    } catch (Exception $exc) {

                    }
                }
            }
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получение личного расписания преподавателя.
     * @internal int $id идентификатор преподавателя
     */
    public function getcalendarscheduleAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $teacherId = (int)$this->getRequest()->getParam('teacher_id');

        $answer = array('success' => 0);

        $superId = 0;
        if ($teacherId) {

            $tasks = array();

            $lessonsModel = new Intensives_Model_DbTable_Lesson();
            $lessons = $lessonsModel->getTeacherLessonsByType($teacherId)->toArray();
            foreach ($lessons as $lesson) {
                if ($lesson['id'] > $superId) {
                    $superId = $lesson['id'];
                }
                $status = '';
                if ($lesson['status'] == 1) {
                    $status = '[Утверждено]';
                } else if ($lesson['status'] == 2) {
                    $status = '[Проведено]';
                }
                $tasks[] = array(
                    'id' => $lesson['id'],
                    'realID' => $lesson['id'],
                    'cid' => 2,
                    'start' => $lesson['start'],
                    'end' => $lesson['end'],
                    'title' => $status . '<br/>' . $lesson['theme'] . '<br/>Ауд: ' . $lesson['auditory_name'],
                    'notes' => '',
                    'ad' => false
                );
            }

            $scheduleModel = new Intensives_Model_DbTable_TeacherVstuSchedule();
            $schedule = $scheduleModel->getScheduleForTeacher($teacherId)->toArray();
            foreach ($schedule as $lesson) {
                if (!empty($lesson['additional_dates'])) {
                    $dates = explode(', ', $lesson['additional_dates']);
                    foreach($dates as $date) {
                        $dStart = DateTime::createFromFormat('Y-m-d', $date);
                        $dEnd = clone $dStart;
                        $dStart->setTime($this->_timeMapper[$lesson['lesson_start']][0], $this->_timeMapper[$lesson['lesson_start']][1]);
                        $dEnd->setTime($this->_timeMapper[$lesson['lesson_end']][0], $this->_timeMapper[$lesson['lesson_end']][1]);
                        $html = '<b>' . $lesson['auditory'] . '</br>' .
                            '<p style="text-align:center"><b>'.
                            $this->_lessonTypesMapper[$lesson['lesson_type']] . ' ' . $lesson['lesson_name'] . '</b></br>' .
                            $lesson['groups'] . '</br>' .  $lesson['additional_dates'] . '</p>';
                        $tasks[] = array(
                            'id' => ++$superId,
                            'realID' => $lesson['id'],
                            'cid' => 1,
                            'start' => $dStart->format('Y-m-d H:i:s'),
                            'end' => $dEnd->format('Y-m-d H:i:s'),
                            'title' => $html,
                            'notes' => '',
                            'ad' => false
                        );
                    }
                } else {
                    $startDate = date_create('now');
                    $startDate->sub(new DateInterval('P4M'));
                    $endDate = date_create('now');
                    $endDate->add(new DateInterval('P4M'));
                    $day = $lesson['day'];
                    $week = $lesson['week'];
                    $addDays = $day - date('w', $startDate->getTimestamp());
                    if ($addDays > 0) {
                        $startDate->add(new DateInterval("P${addDays}D"));
                    } else if ($addDays < 0) {
                        $addDays = -$addDays;
                        $startDate->sub(new DateInterval("P{$addDays}D"));
                    }
                    if ($week == 2) {
                        $startDate->add(new DateInterval('P1W'));
                    }
                    $startDate->setTime($this->_timeMapper[$lesson['lesson_start']][0],
                        $this->_timeMapper[$lesson['lesson_start']][1]);
                    $partlyEnd = clone $startDate;
                    $partlyEnd->setTime($this->_timeMapper[$lesson['lesson_end']][0],
                        $this->_timeMapper[$lesson['lesson_end']][1]);
                    while ($startDate <= $endDate) {
                        $html = '<b>' . $lesson['auditory'] . '</br>' .
                            '<p style="text-align:center"><b>'.
                            $this->_lessonTypesMapper[$lesson['lesson_type']] . ' ' . $lesson['lesson_name'] . '</b></br>' .
                            $lesson['groups'] . '</br>' .  $lesson['additional_dates'] . '</p>';

                        $tasks[] = array(
                            'id' => ++$superId,
                            'realID' => $lesson['id'],
                            'cid' => 1,
                            'start' => $startDate->format('Y-m-d H:i:s'),
                            'end' => $partlyEnd->format('Y-m-d H:i:s'),
                            'title' => $html,
                            'notes' => '',
                            'ad' => false
                        );
                        $startDate->add(new DateInterval('P2W'));
                    }
                }
            }

            $consultModel = new Intensives_Model_DbTable_TeacherConsultation();
            $consultations = $consultModel->getTeacherConsultations($teacherId);
            foreach ($consultations as $consultation) {
                $tasks[] = array(
                    'id' => ++$superId,
                    'realID' => $consultation['id'],
                    'cid' => 3,
                    'start' => $consultation['start'],
                    'end' => $consultation['end'],
                    'title' => $consultation['name'],
                    'notes' => '',
                    'ad' => false
                );
            }

            $answer['success'] = 1;
            $answer['tasks'] = $tasks;
        }
        echo Zend_Json::encode($answer);
    }

    public function saveconsultationAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);
        $answer = array('success' => 0);
        $userData = Zend_Auth::getInstance()->getStorage()->read();
        $teacherID = $userData->id;
        if (!is_null($teacherID)) {
            $consulModel = new Intensives_Model_DbTable_TeacherConsultation();
            $start = $this->getRequest()->getParam('start');
            $end = $this->getRequest()->getParam('end');
            $theme = $this->getRequest()->getParam('name');
            $consultID = $this->getRequest()->getParam('id');
            if (!is_null($start) && !is_null($end)) {
                $consulModel->saveConsultation($teacherID, $start, $end, $theme, $consultID);
                $answer['success'] = 1;
            }
        }
        echo Zend_Json::encode($answer);
    }

    public function getconsultationAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);
        $answer = array('success' => 0);
        $userData = Zend_Auth::getInstance()->getStorage()->read();
        $teacherID = $userData->id;
        if (!is_null($teacherID)) {
            $consulModel = new Intensives_Model_DbTable_TeacherConsultation();
            $id = $this->getRequest()->getParam('id');
            if ($id) {
                $cons = $consulModel->getTeacherConsultations($teacherID, $id);
                $answer['success'] = 1;
                $answer['consult'] = $cons;
            }
        }
        echo Zend_Json::encode($answer);
    }

    public function deleteconsultationAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);
        $answer = array('success' => 0);
        $userData = Zend_Auth::getInstance()->getStorage()->read();
        $teacherID = $userData->id;
        if (!is_null($teacherID)) {
            $consulModel = new Intensives_Model_DbTable_TeacherConsultation();
            $id = $this->getRequest()->getParam('id');
            if ($id) {
                $consulModel->removeConsultation($id, $teacherID);
                $answer['success'] = 1;
            }
        }
        echo Zend_Json::encode($answer);
    }
}