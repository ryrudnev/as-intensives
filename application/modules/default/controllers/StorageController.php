<?php

/**
 * Определяет контроллер для взаимодействия с файловым хранилищем на сервере
 *
 * Class StorageController
 */
class StorageController extends Zend_Controller_Action
{
    /**
     * Инициализирует класс-контроллер: отключение авторендеринга, так как
     * действия класса будут выдавать только строку с JSON-объектом - рендеринг
     * здесь не нужен.
     */
    public function init()
    {
        $auth = Zend_Auth::getInstance();
        if (!$auth->hasIdentity()) {
            $answer = array(
                'success' => 0,
                'error_code' => 1,
                'error_message' => 'Вы не вошли в систему'
            );

            die(Zend_Json::encode($answer));
        }

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }

    /**
     * Получить сохраненные документы
     * @internal string mid идентификатор методиста
     * @internal string tid идентификатор преподавателя
     *
     * @return string JSON-объект, содержащий свойство 'documents'
     */
    public function getdocumentsAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        $documentStorageModel = new Intensives_Model_DbTable_DocumentStorage();
        if ($id = $this->getRequest()->getParam('mid')) {
            $where = $documentStorageModel->getDefaultAdapter()->quoteInto('dts.methodist_id = ?', $id);
        } elseif ($id = $this->getRequest()->getParam('tid')) {
            $where = $documentStorageModel->getDefaultAdapter()->quoteInto('dts.teacher_id = ?', $id);
        }

        if (isset($where)) {
            $select = $documentStorageModel->select()
                ->setIntegrityCheck(false)
                ->from(array('ds' => 'document_storage'), array('id', 'name', 'date', 'category'))
                ->joinInner(array('dts' => 'document_to_staff'), 'ds.id = dts.document_id', array())
                ->where($where)
                ->order(array('date ASC'));
            $rowSet = $documentStorageModel->fetchAll($select);

            $documents = array();
            while ($rowSet->valid()) {
                $row = $rowSet->current()->toArray();

                $category = new Intensives_Model_DocumentCategoryEnum($row['category']);
                $row['category'] = (string)$category;
                $row['name'] = $row['name'] . '.' . ($category->getValue() == Intensives_Model_DocumentCategoryEnum::Archive ? 'zip' : 'odt');

                $documents[] = $row;

                $rowSet->next();
            }

            $answer['success'] = 1;
            $answer['documents'] = $documents;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получить документ (в формате odt или zip, если архив) прикрепленный через HTTP
     *
     * @internal string did идентификатор документа.
     * @return void
     */
    public function getdocumentAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        if ($did = $this->getRequest()->getParam('did')) {
            $documentStorageModel = new Intensives_Model_DbTable_DocumentStorage();
            $documentStorageModel->exportAsAttachedFile($did);

            $answer['success'] = 1;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Удалить документ
     *
     * @internal string did идентификатор документа
     * @internal string mid идентификатор методиста
     */
    public function deletedocumentAction()
    {
        //TODO: еще раз разобраться с удалением. Innodb почему-то не синхронизирует данные в таблицах
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        $documentToStaffModel = new Intensives_Model_DbTable_DocumentToStaff();
        if ($did = $this->getRequest()->getParam('did')) {
            if ($mid = $this->getRequest()->getParam('mid')) {
                $where = array($documentToStaffModel->getDefaultAdapter()->quoteInto('document_id = ?', $did),
                    $documentToStaffModel->getDefaultAdapter()->quoteInto('methodist_id = ?', $mid));
            } elseif ($tid = $this->getRequest()->getParam('tid')) {
                $where = array($documentToStaffModel->getDefaultAdapter()->quoteInto('document_id = ?', $did),
                    $documentToStaffModel->getDefaultAdapter()->quoteInto('teacher_id = ?', $tid));
            }
        }

        if (isset($where)) {
            /**
             * Удаление из таблицы document_to_staff
             */
            $documentToStaffModel->delete($where);

            /**
             * Отлов возможности того, что данный документ связан с другим сотрудником (методистом или преподавателем)
             */
            try {
                /**
                 * Попытка удалить документ из document_storage
                 */
                $documentStorageModel = new Intensives_Model_DbTable_DocumentStorage();
                $documentStorageModel->delete(array('id' => $did));

            } catch (Exception $e) {
            }

            $answer['success'] = 1;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Заарзивировать указанные документы
     *
     * @internal string methodist_id идентификатор методиста
     * @internal string teacher_id идентификатор преподавателя
     * @internal string listener_contract флаг
     * @internal string listener_order флаг
     * @internal string flow_funds флаг
     * @internal string listener_journal флаг
     * @internal string listener_list флаг
     * @internal string teacher_contract флаг
     * @internal string teacher_report флаг
     *
     */
    public function archivedocumentsAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        $request = $this->getRequest();
        if ($request->isPost() && ($year = $request->getParam('year')) == true) {
            $whereCategory = '';
            $whereStaff = '';

            $mid = $request->getParam('mid');
            $tid = $request->getParam('tid');

            if ($mid) {
                $contractModel = new Intensives_Model_DbTable_TeacherContract();

                $whereStaff = 'dts.methodist_id = ' . $mid;

                if ($request->getParam('listener_contract') == 'on') {
                    $whereCategory .= 'category = ' . Intensives_Model_DocumentCategoryEnum::ListenerContract;
                }
                if ($request->getParam('listener_order') == 'on') {
                    $whereCategory .= !empty($whereCategory) ? ' OR category = ' . Intensives_Model_DocumentCategoryEnum::ListenerOrder
                        : 'category = ' . Intensives_Model_DocumentCategoryEnum::ListenerOrder;
                }
                if ($request->getParam('flow_funds') == 'on') {
                    $whereCategory .= !empty($whereCategory) ? ' OR category = ' . Intensives_Model_DocumentCategoryEnum::FlowFunds
                        : 'category = ' . Intensives_Model_DocumentCategoryEnum::FlowFunds;
                }
                if ($request->getParam('listener_journal') == 'on') {
                    $whereCategory .= !empty($whereCategory) ? ' OR category = ' . Intensives_Model_DocumentCategoryEnum::ListenerJournal
                        : 'category = ' . Intensives_Model_DocumentCategoryEnum::ListenerJournal;
                }
                if ($request->getParam('listener_list') == 'on') {
                    $whereCategory .= !empty($whereCategory) ? ' OR category = ' . Intensives_Model_DocumentCategoryEnum::ListenerList
                        : 'category = ' . Intensives_Model_DocumentCategoryEnum::ListenerList;
                }
            } elseif ($tid) {
                $contractModel = new Intensives_Model_DbTable_TeacherContract();

                $whereStaff = 'dts.teacher_id = ' . $tid;

                if ($request->getParam('teacher_contract') == 'on') {
                    $whereCategory .= 'category = ' . Intensives_Model_DocumentCategoryEnum::TeacherContract;
                }
                if ($request->getParam('teacher_report') == 'on') {
                    $whereCategory .= !empty($whereCategory) ? ' OR category = ' . Intensives_Model_DocumentCategoryEnum::TeacherReport
                        : 'category = ' . Intensives_Model_DocumentCategoryEnum::TeacherReport;
                }
            }

            if (!empty($whereCategory) && !empty($whereStaff) && isset($contractModel)) {
                $documentStorageModel = new Intensives_Model_DbTable_DocumentStorage();
                $documentToStaffModel = new Intensives_Model_DbTable_DocumentToStaff();

                $select = $documentStorageModel->select()
                    ->from(array('ds' => 'document_storage'))
                    ->joinInner(array('dts' => 'document_to_staff'), 'ds.id = dts.document_id', array())
                    ->where('YEAR(date) = ?', $year)
                    ->where($whereCategory)
                    ->where($whereStaff);

                //TODO: Оптимизировать внешние и внутренние вызовы fetchAll - дублирование данных - лишния потеря времени
                $resultSet = $documentStorageModel->fetchAll($select);
                if ($resultSet->count() > 0) {
                    $ids = array();
                    while ($resultSet->valid()) {
                        $row = $resultSet->current();
                        $ids[] = $row->id;
                        $resultSet->next();
                    }
                    $idsStr = implode(', ', $ids);

                    /**
                     * Попытка заархивировать документы если они есть
                     */
                    $name = 'Архив за ' . $year . ' год';
                    $name = $documentToStaffModel->getCorrectDocumentName($name, $mid, $tid);

                    $result = $documentStorageModel->archiveDocuments($select, array('name' => $name));
                    if ($result) {
                        /**
                         * Вставка новой строки в document_to_staff
                         */
                        $documentToStaffModel->insert(array('document_id' => $result,
                            'methodist_id' => $mid, 'teacher_id' => $tid));

                        /**
                         * Удаление связанных с методистом документов из таблицы document_to_staff
                         */
                        $documentToStaffModel->delete('id IN
                            (SELECT id FROM
                                (SELECT id FROM document_to_staff) AS dts
                                WHERE ' . (!is_null($mid) ? 'methodist_id = ' . $mid : 'teacher_id = ' . $tid) .
                            ' AND document_id IN (' . $idsStr . '))');

                        try {
                            /**
                             * Попытка удалить документы из хранилища document_storage
                             */
                            $documentStorageModel->delete('id IN (' . $idsStr . ')');
                        } catch (Exception $e) {

                        }

                        $answer['success'] = 1;
                    }
                }
            }
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получить список имен документов, которые находяться в архиве
     * @internal string aid идентификатор архива
     */
    public function getlistarchiveddocumentsAction()
    {

        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        if ($aid = $this->getRequest()->getParam('aid')) {
            $documentStorageModel = new Intensives_Model_DbTable_DocumentStorage();
            $path = $documentStorageModel->getFullPathDocument($documentStorageModel->select()->where('id = ?', $aid));

            if ($path && ($fileNames = $documentStorageModel->localFileStorage()->zipGetFileNames($path)) == true) {
                $archive = array();
                foreach ($fileNames as $fileName) {
                    $archive[] = array('document' => basename($fileName), 'category' => dirname($fileName));
                }

                $answer['archive'] = $archive;
                $answer['success'] = 1;

            }
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получить документ из архива
     * @internal string aid идентификатор архива
     * @internal string name имя документа
     */
    public function getarchiveddocumentAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        if (($aid = $this->getRequest()->getParam('aid')) == true &&
            ($name = $this->getRequest()->getParam('name')) == true
        ) {
            $name = urldecode($name);

            $documentStorageModel = new Intensives_Model_DbTable_DocumentStorage();
            $path = $documentStorageModel->getFullPathDocument($documentStorageModel->select()->where('id = ?', $aid));

            if ($path && ($document = $documentStorageModel->localFileStorage()->zipGetFileData($path, $name)) == true) {
                $tmp = tempnam($documentStorageModel->localFileStorage()->getLocalDirectory(), md5(uniqid()));

                $h = fopen($tmp, "w");
                fwrite($h, $document);
                fclose($h);

                header('Content-type: application/vnd.oasis.opendocument.text');
                header('Content-Disposition: attachment; filename="' . basename($name) . '"');
                readfile($tmp);

                unlink($tmp);

                $answer['success'] = 1;
            }

           echo Zend_Json::encode($answer);
        }
    }
}