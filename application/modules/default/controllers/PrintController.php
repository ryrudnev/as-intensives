<?php

require_once('odtphp/odf.php');

/**
 * Контроллер для генерации печатных версий документов.
 *
 * Основное назначение контроллера заключается в принятии GET или POST запроса,
 * его обработка и выдача ответа в виде содержимого ODT-файла, который
 * формируется на основе шаблона и данных хранящихся в системе или полученных
 * через пришедший запрос.
 *
 * LICENSE: GPLv3
 *
 * @copyright  Copyright (c) 2010-2011 Team Liberty
 * @license    http://www.gnu.org/licenses/gpl.html
 * @version    $Id:$
 * @link       http://intensives.vstu.ru/print
 * @since      File available since Release 1.0.0
 */

/**
 * Контроллер для генерации печатных версий документов.
 *
 * @copyright  Copyright (c) 2010-2011 Team Liberty
 * @license    http://www.gnu.org/licenses/gpl.html
 * @link       http://intensives.vstu.ru/print
 * @since      Class available since Release 1.0.0
 */
class PrintController extends Zend_Controller_Action
{
    /**
     * Генератор документов
     * @var Intensives_Model_Documentor
     */
    private $_documentor = null;

    /**
     * Поместить документ в хранилище документов если это необходимо
     *
     * @param array $data массив данных, где ключи:
     * 'odt' - odt документа.
     * 'name' - имя документа.
     * 'date' - дата создания.
     * 'category' - категория документа.
     * 'methodist_id' - id методиста.
     * 'teacher_id' - id преподавателя.
     * @return false|mixed индентификатор вставленного документа
     */
    private function _saveToDocumentStorage(array $data)
    {
        $documentStorage = new Intensives_Model_DbTable_DocumentStorage();
        $documentToStaffModel = new Intensives_Model_DbTable_DocumentToStaff();

        $mid = isset($data['methodist_id']) ? $data['methodist_id'] : null;
        $tid = isset($data['teacher_id']) ? $data['teacher_id'] : null;

        $data['name'] = $documentToStaffModel->getCorrectDocumentName($data['name'], $mid, $tid);

        // Добавление в хранилище документа
        $id = $documentStorage->insert($data);

        $documentToStaffModel->insert(array('document_id' => $id, 'methodist_id' => $mid, 'teacher_id' => $tid));

        // Привязка документа методисту или преподавателю
        return $id;
    }


    /**
     * Проверка аутентификации пользователя и отключение авторендеринга, так как
     * контроллер отвечает в основном на асинхронные запросы.
     */
    public function init()
    {
        // Проверка аутентификации
        $auth = Zend_Auth::getInstance();
        if (!$auth->hasIdentity()) {
            $answer = array(
                'success' => 0,
                'error_code' => 1,
                'error_message' => 'Вы не вошли в систему'
            );

            die(Zend_Json::encode($answer));
        }

        // Отключение рендеринга лайаута страницы
        $this->_helper->layout()->disableLayout();

        // Отключение рендеринга представлений действий
        $this->_helper->viewRenderer->setNoRender(true);

        $this->_documentStorage = new Intensives_Model_DbTable_DocumentStorage();
        $this->_documentor = new Intensives_Model_Documentor();
    }

    /**
     * Генерация печатной версии договора.
     *
     * @internal int $cid идентификатор договора.
     * @internal int $mid идентификатор договора.
     *
     * @return string содержимое ODT-файла, который можно в дальнейшем
     * распечатать в текстовом процессоре OpenOffice.Org или LibreOffice.
     */
    public function printcontractAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $cid = $this->getRequest()->getParam('cid');
        $mid = $this->getRequest()->getParam('mid');

        $contractsModel = new Intensives_Model_DbTable_Contract();
        $details = $contractsModel->getContractDetails($cid);

        if (!is_null($details['documentId'])) {
            $documentStorageModel = new Intensives_Model_DbTable_DocumentStorage();
            $documentStorageModel->exportAsAttachedFile($details['documentId']);
        } else {
            $name = $details['listenerSurname'] . ' ' . $details['listenerName'] . ' '
                . $details['listenerPatronymic'] . ' - ' . $details['number'];

            $category = new Intensives_Model_DocumentCategoryEnum(
                Intensives_Model_DocumentCategoryEnum::ListenerContract);
            $odt = $this->_documentor->generateOdt($category, array('cid' => $cid));
            $odt->exportAsAttachedFile($name . '.odt');

            $this->_saveToDocumentStorage(array(
                    'odt' => $odt,
                    'name' => $name,
                    'date' => (new Zend_Date())->get('YYYY-MM-dd'),
                    'category' => $category->getValue(),
                    'methodist_id' => $mid)
            );
        }
    }

    /**
     * Генерация печатной версии списка выбранных слушателей.
     *
     * @internal string список идентификаторов слушателей (разделены запятой),
     * данные о которых необходимо распечатать.
     *
     * @return string содержимое ODT-файла, который можно в дальнейшем
     * распечатать в текстовом процессоре OpenOffice.Org или LibreOffice.
     */
    public function printlistenerslistAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $methodistId = $this->getRequest()->getParam('mid');
        $listenersIds = $this->getRequest()->getParam('ids');
        $name = 'Слушатели - ' . date('d.m.Y');

        $category = new Intensives_Model_DocumentCategoryEnum(Intensives_Model_DocumentCategoryEnum::ListenerList);
        $odt = $this->_documentor->generateOdt($category, array('ids' => $listenersIds));
        $odt->exportAsAttachedFile($name . '.odt');

        $this->_saveToDocumentStorage(array(
                'odt' => $odt,
                'name' => $name,
                'date' => (new Zend_Date())->get('YYYY-MM-dd'),
                'category' => $category->getValue(),
                'methodist_id' => $methodistId)
        );
    }

    /**
     * Генерация печатной версии приказа о зачислении слушателей на курс.
     *
     * @internal int $cid идентификатор кафедры, по которой формируется приказ.
     * @internal string $from дата начала периода обучения.
     * @internal string $to дата окончания периода обучения.
     *
     * @return string содержимое ODT-файла, который можно в дальнейшем
     * распечатать в текстовом процессоре OpenOffice.Org или LibreOffice.
     */
    public function printorderAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $methodistId = $this->getRequest()->getParam('mid');
        $fromDate = $this->getRequest()->getParam('from');
        $toDate = $this->getRequest()->getParam('to');
        $name = 'Приказ о зачислении слушателей - ' . $fromDate . ' - ' . $toDate;

        $category = new Intensives_Model_DocumentCategoryEnum(Intensives_Model_DocumentCategoryEnum::ListenerOrder);
        $odt = $this->_documentor->generateOdt($category, array('mid' => $methodistId, 'from' => $fromDate,
            'to' => $toDate));
        $odt->exportAsAttachedFile($name . '.odt');

        $this->_saveToDocumentStorage(array(
                'odt' => $odt,
                'name' => $name,
                'date' => (new Zend_Date())->get('YYYY-MM-dd'),
                'category' => $category->getValue(),
                'methodist_id' => $methodistId)
        );
    }

    /**
     * Генерация печатной версии трудового договора на выполнение преподавателем
     * учебной работы.
     *
     * @internal int $teacher_id идентификатор преподавателя.
     * @internal string $make_date дата оформления договора.
     * @internal string $start дата начала периода обучения.
     * @internal string $end дата окончания периода обучения.
     * @internal double $hour_cost стоимость одного часа.
     * @internal int $semester номер семестра.
     * @return string содержимое ODT-файла, который можно в дальнейшем
     * распечатать в текстовом процессоре OpenOffice.Org или LibreOffice.
     */
    public function teachercontractAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $params = $this->getRequest()->getParams();

        $teacherContractModel = new Intensives_Model_DbTable_TeacherContract();
        $contractId = $teacherContractModel->insert(
            array(
                'teacher_id' => $params['teacher_id'],
                'date' => $params['make_date'],
                'start_date' => $params['start'],
                'end_date' => $params['end'],
                'payment' => $params['hour_cost']
            )
        );

        $teachersModel = new Intensives_Model_DbTable_Teacher();
        $teacherData = $teachersModel->getTeacherDataWithChairName($params['teacher_id']);
        $name = $teacherData['surname'] . ' ' . $teacherData['name'] . ' ' . $teacherData['patronymic']
            . '-' . $params['make_date'];

        $category = new Intensives_Model_DocumentCategoryEnum(Intensives_Model_DocumentCategoryEnum::TeacherContract);
        $odt = $this->_documentor->generateOdt($category,
            array('teacher_id' => $params['teacher_id'], 'make_date' => $params['make_date'], 'start' => $params['start'],
                'end' => $params['end'], 'hour_cost' => $params['hour_cost'], 'semester' => $params['semester']));

        $odt->exportAsAttachedFile($name . '.odt');

        $documentId = $this->_saveToDocumentStorage(
            array(
                'odt' => $odt,
                'name' => $name,
                'date' => (new Zend_Date())->get('YYYY-MM-dd'),
                'category' => $category->getValue(),
                'teacher_id' => $params['teacher_id']
            )
        );

        /**
         * Привязка созданного контракта к документу
         */
        $teacherContractModel->update(array('document_id' => $documentId), array('id' => $contractId));
    }

    /**
     * Генерация печатной версии отчёта о выполнении учебной работы
     * преподавателем.
     *
     * @internal int $teacher_id идентификатор преподавателя.
     * @internal string $contract_make_date дата оформления договора на выполнение
     * преподавателем учебной работы.
     * @internal string $make_date дата формирования отчёта.
     * @internal string $start дата начала периода обучения.
     * @internal string $end дата окончания периода обучения.
     * @internal double $hour_cost стоимость одного часа обучения.
     *
     * @return string содержимое ODT-файла, который можно в дальнейшем
     * распечатать в текстовом процессоре OpenOffice.Org или LibreOffice.
     */
    public function teacherreportAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $teacherId = $this->getRequest()->getParam('teacher_id');
        $contractMakeDate = $this->getRequest()->getParam('contract_make_date');
        $makeDate = $this->getRequest()->getParam('make_date');
        $fromDate = $this->getRequest()->getParam('start');
        $toDate = $this->getRequest()->getParam('end');
        $hourCost = (double)$this->getRequest()->getParam('hour_cost');

        $teachersModel = new Intensives_Model_DbTable_Teacher();
        $teacherData = $teachersModel->getTeacherDataWithChairName($teacherId);
        $name = $teacherData['surname'] . ' ' . $teacherData['name'] . ' ' . $teacherData['patronymic'] .
            ' отчет о выполненных учебных работах - ' . $makeDate;

        $category = new Intensives_Model_DocumentCategoryEnum(Intensives_Model_DocumentCategoryEnum::TeacherReport);
        $odt = $this->_documentor->generateOdt($category, array('teacher_id' => $teacherId,
            'contract_make_date' => $contractMakeDate, 'make_date' => $makeDate, 'start' => $fromDate,
            'end' => $toDate, 'hour_cost' => $hourCost));
        $odt->exportAsAttachedFile($name . '.odt');

        $this->_saveToDocumentStorage(array(
                'odt' => $odt,
                'name' => $name,
                'date' => (new Zend_Date())->get('YYYY-MM-dd'),
                'category' => $category->getValue(),
                'teacher_id' => $teacherId)
        );
    }

    /**
     * Генерация печатной версии отчета о движении денежных средств по подразделению "Интенсив" за указанный период.
     *
     * @internal param int $mid идентификатор методиста.
     * @internal param string $from дата начала периода отчета.
     * @internal param string $to дата окончания периода отчета.
     *
     * @return string содержимое ODT-файла, который можно в дальнейшем
     * распечатать в текстовом процессоре OpenOffice.Org или LibreOffice.
     */
    public function printflowfundsAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $methodistId = $this->getRequest()->getParam('mid');
        $fromDate = $this->getRequest()->getParam('from');
        $toDate = $this->getRequest()->getParam('to');

        $name = 'Отчет о движении денежных средств за период от ' . $fromDate . ' до '
            . $toDate;

        $category = new Intensives_Model_DocumentCategoryEnum(Intensives_Model_DocumentCategoryEnum::FlowFunds);
        $odt = $this->_documentor->generateOdt($category, array('mid' => $methodistId, 'from' => $fromDate,
            'to' => $toDate));
        $odt->exportAsAttachedFile($name . '.odt');

        $this->_saveToDocumentStorage(array(
                'odt' => $odt,
                'name' => $name,
                'date' => (new Zend_Date())->get('YYYY-MM-dd'),
                'category' => $category->getValue(),
                'methodist_id' => $methodistId)
        );
    }

    /* Вспомагательные методы для составления журанала слушателей  */

    /**
     * Перевести статус в строковое представление
     * @param integer $status статус
     * @return string статус
     */
    private function _statusToString($status)
    {
        $str = 'Назначено';
        switch ($status) {
            case -1:
                $str = 'Оплачено';
                break;
            case 1:
                $str = 'Назначено';
                break;
            case 2:
                $str = 'Проведено';
                break;
        }
        return $str;
    }

    /**
     * Перевести тип в строковое представление
     * @param integer $type тип занятия
     * @return string тип занятия
     */
    private function _typeToString($type)
    {
        $str = 'Теор.';
        switch ($type) {
            case -1:
                $str = '';
                break;
            case 1:
                $str = 'Прак.';
                break;
            case 2:
                $str = 'Лекц-прак.';
                break;
        }
        return $str;
    }

    /**
     * Создать столбцы журнала слушателей по заданным темам
     * @param array $themes темы
     * @return array столбцы журнала слушателей
     */
    private function _journalCreateColumns(array $themes)
    {
        $columns = array('fields' => array(), 'names' => array(), 'tooltips' => array());
        $columns['fields'][] = array('name' => 'id');

        $columns['fields'][] = array('name' => 'listener');
        $columns['names'][] = 'ФИО';
        for ($i = 0; $i < count($themes); $i++) {
            $columns['fields'][] = array('name' => 'theme' . ($i + 1));
            $columns['names'][] = (string)($i + 1);
            $columns['tooltips']['theme' . ($i + 1)] = $themes[$i]['themeName'];
        }
        $columns['fields'][] = array('name' => 'listenerPhone');
        $columns['names'][] = 'Телефон';

        $columns['fields'][] = array('name' => 'note');
        $columns['names'][] = 'Примечание';

        return $columns;
    }

    /**
     * Создать строку журнала слушателей
     * @param array $themes темы
     * @return array строка журнала слушателей
     */
    private function _journalNewRow(array $themes)
    {
        $row = array('id' => null, 'listener' => null, 'listenerPhone' => null, 'note' => null);

        for ($i = 0; $i < count($themes); $i++) {
            $row['theme' . ($i + 1)] = array();
        }

        return $row;
    }

    /**
     * Проверить наличия данной темы в контракте слушателя
     * @param array $theme тема
     * @param array $contract контракт слушателя
     * @param array $contracts контракты
     * @return bool результат проверки
     */
    private function _isThemeOnContract(array $theme, array $contract, array &$contracts)
    {
        if (!is_array($contract['themesId'])) {
            $index = array_search($contract, $contracts);
            $contracts[$index]['themesId'] = $contract['themesId'] = explode(',', $contract['themesId']);
        }

        return in_array($theme['themeId'], $contract['themesId']);
    }

    /**
     * Проверить наличие данного слушателя на занятии
     * @param string $listener слушатель
     * @param array $lesson занятие
     * @param array $lessons занятия
     * @return bool результат проверки
     */
    private function _isListenerOnLesson($listener, array $lesson, array &$lessons)
    {
        if (!is_array($lesson['listenersId'])) {
            $index = array_search($lesson, $lessons);
            $lessons[$index]['listenersId'] = $lesson['listenersId'] = explode(',', $lesson['listenersId']);
        }

        return in_array($listener, $lesson['listenersId']);
    }

    /**
     * Получить занятия у которых есть данная тема
     * @param array $theme тема
     * @param array $lessons занятия
     * @param array $lessonsByTheme занятия по темам
     * @return array занятия с данной темой
     */
    private function _getLessonsByTheme(array $theme, array $lessons, array &$lessonsByTheme)
    {
        if (array_key_exists($theme['themeId'], $lessonsByTheme)) {
            return $lessonsByTheme[$theme['themeId']];
        }
        $arr = array();
        foreach ($lessons as $lesson) {
            if ($lesson['lessonThemeId'] == $theme['themeId']) {
                $arr[] = $lesson;
            }
        }

        $lessonsByTheme[$theme['themeId']] = $arr;
        return $arr;
    }

    /**
     * Получить данные о журнале слушателей
     *
     * Сформировать информацию для журнала слушателей на текущий момент для указанной кафедры
     * @internal int $cid идентификатор кафедры
     * @return string при успешной пометке: JSON-объект со свойством 'success'
     * равным '1' и свойством 'journaldata' с информацией о журнале слушателей.
     * при неуспешной отметке: JSON-объект со свойством 'success' равным '0'
     * и свойством 'error_message', которое содержить сообщение об ошибке,
     * которое будет выдано пользователю.
     */
    public function journallistenersAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        $params = $this->getRequest()->getParams();
        if (isset($params['cid']) && isset($params['from']) && isset($params['to']) && isset($params['mid'])) {
            /**
             * Получение всех необходимымх данных для составлення журнала слушателей
             */

            // Получение всех тем для данной кафедры
            $lessonThemeModel = new Intensives_Model_DbTable_LessonTheme();
            $select = $lessonThemeModel->select()
                ->setIntegrityCheck(false)
                ->from(array('lt' => 'lesson_theme'), array(
                    'themeId' => 'id',
                    'themeName' => 'name',
                    'themeNumber' => 'number'))
                ->joinInner(array('i' => 'intensive'), 'lt.intensive_id = i.id', array())
                ->where('i.chair_id = ?', $params['cid'])
                ->order(array('lt.number ASC'));
            $rowSet = $lessonThemeModel->fetchAll($select);
            $themes = $rowSet->toArray();

            // Получение всех слушателей и их данных о контрактах и желаемых темах
            $contractModel = new Intensives_Model_DbTable_Contract();
            $select = $contractModel->select()
                ->setIntegrityCheck(false)
                ->from(array('l' => 'listener'), array(
                    'listenerId' => 'id',
                    'listener' => 'CONCAT(l.surname,\' \',l.name,\' \',l.patronymic)',
                    'listenerPhone' => 'phone'))
                ->joinInner(array('c' => 'contract'), 'l.id = c.listener_id', array('contactPaid' => 'paid', 'contractId' => 'id'))
                ->joinLeft(array('ctlt' => 'contract_to_lesson_theme'), 'c.id = ctlt.contract_id', array())
                ->joinLeft(array('lt' => 'lesson_theme'), 'lt.id = ctlt.lesson_theme_id', array())
                ->columns('GROUP_CONCAT(DISTINCT lt.id ORDER BY lt.id ASC SEPARATOR \',\') AS themesId')
                ->distinct()
                ->where('c.start = ?', $params['from'])
                ->where('c.end = ?', $params['to'])
                ->where('l.deleted = 0')
                ->group('c.id')
                ->order('l.surname ASC');
            $rowSet = $contractModel->fetchAll($select);
            $contracts = $rowSet->toArray();

            // Получение списка всех занятий с указанием слушателей, которые к ним относятся
            $lessonModel = new Intensives_Model_DbTable_Lesson();
            $select = $lessonModel->select()
                ->from(array('l' => 'lesson'), array('lessonId' => 'id', 'lessonStatus' => 'status',
                    'lessonType' => 'type', 'lessonThemeId' => 'theme_id'))
                ->setIntegrityCheck(false)
                ->joinInner(array('ltl' => 'lesson_to_listener'), 'l.id = ltl.lesson_id', array())
                ->joinInner(array('ls' => 'listener'), 'ls.id = ltl.listener_id', array())
                ->joinInner(array('c' => 'contract'), 'ls.id = c.listener_id', array())
                ->columns('GROUP_CONCAT(DISTINCT ls.id ORDER BY ls.id ASC SEPARATOR \',\') AS listenersId')
                ->distinct()
                ->where('c.start = ?', $params['from'])
                ->where('c.end = ?', $params['to'])
                ->where('DATE(l.start) BETWEEN ' . '\'' . $params['from'] . '\'' . ' AND ' . '\'' . $params['to'] . '\'')
                ->where('l.deleted = 0')
                ->where('ls.deleted = 0')
                ->group('l.id')
                ->order('l.id ASC');
            $rowSet = $lessonModel->fetchAll($select);
            $lessons = $rowSet->toArray();

            /**
             * Формирование данных для журнала слушателей
             * Стркутрура журанала слушателей
             * | ФИО | темы занятий (1...n) | Номер тел. слушателя | Примечание (свободное поле) |
             */
            $journal = array();
            // словарь всех занятий по темам, key - themeId, value - array()
            $lessonsByTheme = array();

            // Количество назначенных и оплаченных занятий для одного слушателя
            $curPaidLessonsMap = null; // текущие для данного слушателя оплаченные занятия по данной теме - словарь - (themeId, countPaid)
            $curAssigLessonsMap = null; // текущие для данного слушателя назначенные занятия по данной теме - словарь - (themeId, countAssig)

            $curListenerId = 0; // идентификатор текущего слушателя
            $curJournalRow = null; // текущая строка в журнале

            $iterator = new CachingIterator(new ArrayIterator($contracts));
            foreach ($iterator as $contract) {
                // Если нет контракт оформлен на нового слушателя...
                if ($curListenerId != $contract['listenerId']) {
                    if (!is_null($curJournalRow)) {
                        $journal[] = $curJournalRow;
                    }
                    $curJournalRow = $this->_journalNewRow($themes);
                    $curJournalRow['id'] = $contract['listenerId'];
                    $curJournalRow['listener'] = $contract['listener'];
                    $curJournalRow['listenerPhone'] = $contract['listenerPhone'];

                    $curPaidLessonsMap = array();
                    $curAssigLessonsMap = array();

                    $curListenerId = $contract['listenerId'];
                }

                // Просмотр всех тем для данного слушателя
                for ($j = 0; $j < count($themes); $j++) {
                    $theme = $themes[$j];

                    // Если данная тема для слушателя еще не рассматривалась
                    if (!array_key_exists($theme['themeId'], $curAssigLessonsMap)) {
                        $curAssigLessonsMap[$theme['themeId']] = 0;

                        // То выбрать все занятия с данной темой
                        $lens = $this->_getLessonsByTheme($theme, $lessons, $lessonsByTheme);
                        foreach ($lens as $lesson) {
                            if ($this->_isListenerOnLesson($contract['listenerId'], $lesson, $lessons)) {
                                // добавление статуса занятия
                                $curJournalRow['theme' . ($j + 1)][] = array(
                                    'status' => $this->_statusToString($lesson['lessonStatus']),
                                    'type' => $this->_typeToString($lesson['lessonType'])
                                );
                                $curAssigLessonsMap[$theme['themeId']]++;
                            }
                        }
                    }

                    // Если данная тема для слушателя еще не рассматривалась
                    if (!array_key_exists($theme['themeId'], $curPaidLessonsMap)) {
                        $curPaidLessonsMap[$theme['themeId']] = 0;
                    }

                    // Если тема входит в список желаемых тем слушателя и оплачена
                    if ($this->_isThemeOnContract($theme, $contract, $contracts) && $contract['contactPaid']) {
                        $curPaidLessonsMap[$theme['themeId']]++;
                    }

                    // Заполенние разницы между назначенными и оплаченами занятиями для вывода статуса - оплачено
                    if ($curPaidLessonsMap[$theme['themeId']] > $curAssigLessonsMap[$theme['themeId']]) {
                        while ($curPaidLessonsMap[$theme['themeId']] - $curAssigLessonsMap[$theme['themeId']] != 0) {
                            $curJournalRow['theme' . ($j + 1)][] = array('status' => $this->_statusToString(-1), 'type' => $this->_typeToString(-1));

                            $curPaidLessonsMap[$theme['themeId']]--;
                        }
                    }
                }

                if (!$iterator->hasNext()) {
                    $journal[] = $curJournalRow;
                }
            }

            $columns = $this->_journalCreateColumns($themes);

            // Идентификатор документа
            $documentId = 0;
            // Генерация документа, если он не пуст
            if (!empty($journal)) {
                /**
                 * Генерация документа в формате odt
                 */
                $category = new Intensives_Model_DocumentCategoryEnum(Intensives_Model_DocumentCategoryEnum::ListenerJournal);
                $odt = $this->_documentor->generateOdt($category, array(
                    'fromDate' => $params['from'],
                    'toDate' => $params['to'],
                    'journalColumns' => $columns['names'],
                    'journalData' => $journal
                ));

                $name = 'Журнал слушателей за период от ' . $params['from'] . ' до ' . $params['to'];
                $documentId = $this->_saveToDocumentStorage(array(
                        'odt' => $odt,
                        'name' => $name,
                        'date' => (new Zend_Date())->get('YYYY-MM-dd'),
                        'category' => $category->getValue(),
                        'methodist_id' => $params['mid'])
                );
            }

            $answer['metaData'] = array(
                'idProperty' => 'id',
                'root' => 'journalData',
                'fields' => $columns['fields'],
                'fieldsTooltip' => $columns['tooltips'],
                'documentId' => $documentId,
                'sortInfo' => array('field' => 'listener', 'direction' => 'ASC')
            );
            $answer['journalData'] = $journal;
        }

        echo Zend_Json::encode($answer);
    }
}