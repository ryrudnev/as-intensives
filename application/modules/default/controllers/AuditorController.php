<?php

/**
 * Контроллер для управления модулем Аудитора (успеваемость слушателей кафедры ПОАС)
 */
class AuditorController extends Zend_Controller_Action
{
    /**
     * @var Zend_Db_Adapter_Abstract
     */
    private $_db = null;

    /**
     * Инициализация класса-контроллера: отключение авторендеринга, так как
     * действия класса будут выдавать только строку с JSON-объектом - рендеринг
     * здесь не нужен.
     */
    public function init()
    {
        // Проверка аутентификации
        $auth = Zend_Auth::getInstance();
        if ($this->getRequest()->getActionName() != 'login' && !$auth->hasIdentity()) {
            $answer = array(
                'success' => 0,
                'error_code' => 1,
                'error_message' => 'Вы не вошли в систему'
            );

            die(Zend_Json::encode($answer));
        }

        // Отключение рендеринга лайаута страницы
        $this->_helper->layout()->disableLayout();
        // Отключение рендеринга представлений действий
        $this->_helper->viewRenderer->setNoRender(true);

        // Получение БД
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $resource = $bootstrap->getPluginResource('multidb');
        $this->_db = $resource->getDb('auditorDb');
    }

    /**
     * Получить всех учителей
     */
    public function getteachersAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 1);

        $select = $this->_db->select()
            ->from('teachers', array('id', 'CONCAT(surname,\' \',name) AS teacher'));
        $teachers = $this->_db->fetchAll($select);
        array_unshift($teachers, array('id' => 0, 'teacher' => 'Не важно'));

        $answer['teachers'] = $teachers;

        echo Zend_Json::encode($answer);
    }

    /**
     * Получить всех студентческие группы
     */
    public function getstudentsgroupsAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 1);

        $select = $this->_db->select()
            ->from('student_groups', array('id' => 'id', 'group' => 'name'));
        $groups = $this->_db->fetchAll($select);
        array_unshift($groups, array('id' => 0, 'group' => 'Не важно'));

        $answer['groups'] = $groups;

        echo Zend_Json::encode($answer);
    }

    /**
     * Получить все типы занятия
     */
    public function getlessontypesAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 1);

        $select = $this->_db->select()
            ->from('lesson_types', array('id' => 'id', 'type' => 'name'))
            ->where('num > ?', 0);
        $types = $this->_db->fetchAll($select);
        array_unshift($types, array('id' => 0, 'type' => 'Не важно'));

        $answer['types'] = $types;

        echo Zend_Json::encode($answer);
    }

    /**
     * Получить всех студентов
     */
    public function getstudentsAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 1);

        $select = $this->_db->select()
            ->from('students', array('id', 'CONCAT(surname,\' \',name) AS student'))
            ->order('surname ASC')
            ->distinct();

        if ($gid = $this->getRequest()->getParam('gid')) {
            $select->where('groupid = ?', $gid);
        }

        $students = $this->_db->fetchAll($select);
        array_unshift($students, array('id' => 0, 'student' => 'Не важно'));

        $answer['students'] = $students;

        echo Zend_Json::encode($answer);
    }

    /**
     * Получить индивидуальную успеваемость студентов
     * @internal int $teacher идентификатор преподавателя или 0
     * @internal int $group идентификатор группы или 0
     * @internal int $type идентификатор типа занятия или 0
     * @internal int $student идентификатор студента или 0
     */
    public function getindividualachievementAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        if (!is_null($teacher = $this->getRequest()->getParam('teacher')) &&
            !is_null($group = $this->getRequest()->getParam('group')) &&
            !is_null($type = $this->getRequest()->getParam('type')) &&
            !is_null($student = $this->getRequest()->getParam('student'))
        ) {

            $columns = array();
            $columns[0] = array('name' => 'id');
            $columns[1] = array('name' => 'student');
            $columns[2] = array('name' => 'group');
            $columns[3] = array('name' => 'type');
            $columns[4] = array('name' => 'quiz');
            $columns[5] = array('name' => 'srs');
            $columns[6] = array('name' => 'mod');

            $select = $this->_db->select()
                ->from(array('vld' => 'vg_lab_details'))
                ->joinInner(array('s' => 'students'), 's.id = vld.studentid', array())
                ->joinInner(array('sg' => 'student_groups'), 'sg.id = s.groupid', array('group' => 'sg.name'))
                ->joinInner(array('lt' => 'lesson_types'), 'lt.id = vld.lessontypeid', array('type' => 'lt.name'))
                ->where('lt.num > ?', 0);

            if ($student) {
                $select->where('vld.studentid = ?', $student);

                unset($columns[1]);
                unset($columns[2]);
            }
            if ($teacher) {
                $where1 = $this->_db->quoteInto("vld.quiz_teacherid = ?", $teacher);
                $where2 = $this->_db->quoteInto("vld.srs_teacherid = ?", $teacher);
                $where3 = $this->_db->quoteInto("vld.mod_teacherid = ?", $teacher);
                $select->where($where1 . ' AND ' . $where2 . ' AND ' . $where3);
            }
            if ($group) {
                $select->where('sg.id = ?', $group);

                unset($columns[2]);
            }
            if ($type) {
                $select->where('vld.lessontypeid = ?', $type);

                unset($columns[3]);
            }

            $rows = $this->_db->fetchAll($select);

            $data = array();
            $id = 0;

            foreach ($rows as $row) {
                $d = array();
                $d['id'] = ++$id;
                $d['student'] = $row['student_name'];
                $d['group'] = $row['group'];
                $d['type'] = $row['type'];
                $d['quiz'] = array(
                    'grade' => is_null($row['quiz_grade']) ? -1 : $row['quiz_grade'],
                    'attemptCount' => is_null($row['quiz_attempt_count']) ? -1 : $row['quiz_attempt_count']
                );
                $d['srs'] = array(
                    'done' => is_null($row['srs_done']) ? -1 : $row['srs_done'],
                    'variant' => is_null($row['varnum']) ? -1 : $row['varnum'],
                    'attemptCount' => is_null($row['srs_attempt_count']) ? -1 : $row['srs_attempt_count']
                );
                $d['mod'] = array(
                    'done' => is_null($row['mod_done']) ? -1 : $row['mod_done'],
                    'modnum' => is_null($row['modnum']) ? -1 : $row['modnum'],
                    'attemptCount' => is_null($row['mod_attempt_count']) ? -1 : $row['mod_attempt_count'],
                    'testingCount' => is_null($row['mod_testing_count']) ? -1 : $row['mod_testing_count'],
                    'kc' => is_null($row['kc']) ? -1 : $row['kc']
                );
                $data[] = $d;
            }

            $answer['success'] = 1;
            $answer['metaData'] = array(
                'idProperty' => 'id',
                'root' => 'data',
                'fields' => array_values($columns)
            );
            $answer['data'] = $data;
        }

        echo Zend_Json::encode($answer);
    }

    /**
     * Получить сводную успеваемость
     * @internal int $to начало периода
     * @internal int $from конец периода
     */
    public function getsummaryachievementAction()
    {
        Intensives_Model_Funcs::asyncCheckAccess($this);

        $answer = array('success' => 0);

        if (!is_null($to = $this->getRequest()->getParam('to')) &&
            !is_null($from = $this->getRequest()->getParam('from'))
        ) {

            $where1 = $this->_db->quoteInto("l.dateoflesson > ?", $from);
            $where2 = $this->_db->quoteInto("l.dateoflesson < ?", $to);

            $select = $this->_db->select()
                ->from(array('l' => 'lessons'), array('id'))
                ->joinInner(array('lt' => 'lesson_types'), 'l.lessontypeid = lt.id', array())
                ->joinInner(array('t' => 'teachers'), 'l.teacherid = t.id', array('CONCAT(t.surname,\' \',t.name) AS teacher'))
                ->joinInner(array('p' => 'places'), 'l.placeid = p.id', array('auditory' => 'p.placename'))
                ->where('lt.num = ?', -1)
                ->where('lt.name = ?', 'Интенсив')
                ->where($where1 . ' AND ' . $where2);

            $data = array();
            $columns = array();

            $lessons = $this->_db->fetchAll($select);
            if (!empty($lessons)) {
                $rows = array();
                $listeners = array();
                $types = array();

                foreach ($lessons as $l) {
                    $select = $this->_db->select()
                        ->from(array('qa' => 'quiz_attempts'), array('quizGrade' => 'grade'))
                        ->joinInner(array('s' => 'students'), 'qa.studentid = s.id', array('id', 'CONCAT(s.surname,\' \',s.name) AS listener'))
                        ->joinInner(array('lt' => 'lesson_types'), 'qa.lessontypeid = lt.id', array('lessonId' => 'lt.id', 'lessonType' => 'lt.name'))
                        ->where('qa.lessonid = ?', $l['id'])
                        ->distinct();

                    $quizs = $this->_db->fetchAll($select);
                    foreach ($quizs as $q) {
                        if (isset($rows[$q['id']])) {
                            $qz = $rows[$q['id']][$q['lessonId']];
                            $qz['quiz'] = isset($qz['quiz']) ? (!$qz['quiz'] ? (($q['quizGrade']) > 60 ? 1 : 0) : 1) : (($q['quizGrade']) > 60 ? 1 : 0);
                            $rows[$q['id']][$q['lessonId']] = $qz;

                            $types[$q['lessonId']] = $q['lessonType'];
                        } else {
                            $rows[$q['id']] = array($q['lessonId'] => array('quiz' => ($q['quizGrade']) > 60 ? 1 : 0));

                            $listeners[$q['id']] = $q['listener'];
                        }
                    }

                    $select = $this->_db->select()
                        ->from(array('srsa' => 'srs_attempts'), array('srsDone' => 'srsa.alltestspassed'))
                        ->joinInner(array('s' => 'students'), 'srsa.studentid = s.id', array('id', 'CONCAT(s.surname,\' \',s.name) AS listener'))
                        ->joinInner(array('v' => 'variants'), 'srsa.variantid = v.id', array())
                        ->joinInner(array('lt' => 'lesson_types'), 'v.lessontypeid = lt.id', array('lessonId' => 'lt.id', 'lessonType' => 'lt.name'))
                        ->where('srsa.lessonid = ?', $l['id'])
                        ->distinct();

                    $srs = $this->_db->fetchAll($select);
                    foreach ($srs as $q) {
                        if (isset($rows[$q['id']])) {
                            $s = $rows[$q['id']][$q['lessonId']];
                            $s['srs'] = isset($s['srs']) ? (!$s['srs'] ? $q['srsDone'] : 1) : $q['srsDone'];
                            $rows[$q['id']][$q['lessonId']] = $s;

                            $types[$q['lessonId']] = $q['lessonType'];
                        } else {
                            $rows[$q['id']] = array($q['lessonId'] => array('srs' => $q['srsDone']));

                            $listeners[$q['id']] = $q['listener'];
                        }
                    }

                    $select = $this->_db->select()
                        ->from(array('ma' => 'mod_attempts'), array())
                        ->joinInner(array('s' => 'students'), 'ma.studentid = s.id', array('id', 'CONCAT(s.surname,\' \',s.name) AS listener'))
                        ->joinInner(array('m' => 'modifications'), 'ma.modid = m.id', array('mod' => 'm.num'))
                        ->joinInner(array('v' => 'variants'), 'm.variantid = v.id', array('variant' => 'v.num'))
                        ->joinInner(array('lt' => 'lesson_types'), 'v.lessontypeid = lt.id', array('lessonId' => 'lt.id', 'lessonType' => 'lt.name'))
                        ->joinLeft(array('mt' => 'mod_testing'), 'ma.id = mt.modattemptid', array('modDone' => 'mt.alltestspassed'))
                        ->where('ma.lessonid = ?', $l['id'])
                        ->where('ma.active = ?', 1)
                        ->distinct();

                    $mods = $this->_db->fetchAll($select);
                    foreach ($mods as $q) {
                        if (isset($rows[$q['id']])) {
                            $m = $rows[$q['id']][$q['lessonId']];
                            $m['mod'] = isset($m['mod']) ? (!$m['mod'] ? $q['modDone'] : 1) : $q['modDone'];
                            $rows[$q['id']][$q['lessonId']] = $m;

                            $types[$q['lessonId']] = $q['lessonType'];
                        } else {
                            $rows[$q['id']] = array($q['lessonId'] => array('mod' => $q['modDone']));

                            $listeners[$q['id']] = $q['listener'];
                        }
                    }
                }

                $columns[] = array('name' => 'id');
                $columns[] = array('name' => 'listener');
                foreach ($types as $kv) {
                    $columns[] = array('name' => $kv);
                }
                sort($columns);

                foreach ($rows as $key => $val) {
                    $d = array();
                    $d['id'] = $key;
                    $d['listener'] = $listeners[$key];
                    foreach ($types as $kt => $kv) {
                        $cur = $val[$kt];
                        $d[$kv] = isset($cur) ? array(
                            'quiz' => is_null($cur['quiz']) ? -1 : $cur['quiz'],
                            'srs' => is_null($cur['srs']) ? -1 : $cur['srs'],
                            'mod' => is_null($cur['mod']) ? -1 : $cur['mod']
                        ) : array('quiz' => -1, 'srs' => -1, 'mod' => -1);
                    }
                    $data[] = $d;
                }
            }

            $answer['success'] = 1;
            $answer['metaData'] = array(
                'idProperty' => 'id',
                'root' => 'data',
                'fields' => $columns
            );
            $answer['data'] = $data;
        }

        echo Zend_Json::encode($answer);
    }
}