<?php
/**
 * Контроллер для отображения кабинета методиста.
 *
 * Основное назначение контроллера заключается в отрисовке соотвествующего ему
 * представления, в котором присутствует подключение библиотеки Sencha и
 * подгрузка всех визуальных компонентов (календарь, окна).
 *
 * LICENSE: GPLv3
 *
 * @copyright  Copyright (c) 2010-2011 Team Liberty
 * @license    http://www.gnu.org/licenses/gpl.html
 * @version    $Id:$
 * @link       http://intensives.vstu.ru/methodist
 * @since      File available since Release 1.0.0
 */

/**
 * Контроллер для отображения кабинета методиста.
 *
 * @copyright  Copyright (c) 2010-2011 Team Liberty
 * @license    http://www.gnu.org/licenses/gpl.html
 * @link       http://intensives.vstu.ru/methodist
 * @since      Class available since Release 1.0.0
 */
class MethodistController extends Zend_Controller_Action
{

    public function init()
    {
        parent::init();
        $this->view->headTitle('Кабинет методиста');
    }

    public function indexAction()
    {
        // Единственное действие, которое необходимо для отрисовки кабинета.
    }


}
