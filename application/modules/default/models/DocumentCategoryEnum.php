<?php

/**
 * Перечисление задающие категории документов в хранилише
 */
class Intensives_Model_DocumentCategoryEnum extends Intensives_Model_EnumBase
{
    const Unknown = 0;
    const TeacherContract = 1;
    const TeacherReport = 2;
    const ListenerContract = 3;
    const ListenerOrder = 4;
    const FlowFunds = 5;
    const ListenerJournal = 6;
    const ListenerList = 7;
    const Archive = 8;

    /**
     * Перевести в строку
     * @return string строковое значение категории документа
     */
    function __toString()
    {
        $str = 'Неизвестно';
        switch ($this->_value) {
            case self::TeacherContract:
                $str = 'Договор на выполнении учебной работы';
                break;
            case self::TeacherReport:
                $str = 'Отчет о выполненных учебных работах';
                break;
            case self::ListenerContract:
                $str = 'Договор об оказании дополнительных образовательных услуг';
                break;
            case self::ListenerOrder:
                $str = 'Приказ о зачислении слушателей';
                break;
            case self::FlowFunds:
                $str = 'Отчет о движении денежных средств подразделения Интенсив';
                break;
            case self::ListenerJournal:
                $str = 'Журнал слушателей';
                break;
            case self::ListenerList:
                $str = 'Список слушателей';
                break;
            case self::Archive:
                $str = 'Архив';
                break;
        }

        return $str;
    }
}