<?php

/**
 * Определяет локальное хранилище файлов данных.
 */
class Intensives_Model_LocalFileStorage
{
    /**
     * Адаптер хранилища для взаимодействия с файловой системой сервера
     * @var Zend_Cloud_StorageService_Adapter_FileSystem
     */
    protected $_adapter = null;

    /**
     * Получить путь к файлу на основание данных
     * @param string $data данные
     * @param string $extension расширение файла
     * @return array информация о пути
     */
    private function _getPathByData($data, $extension = '')
    {
        $filename = md5($data);

        $levelPath = DIRECTORY_SEPARATOR . substr($filename, 0, 2) . DIRECTORY_SEPARATOR .
            substr($filename, -1, 2) . DIRECTORY_SEPARATOR;
        $path = $levelPath . $filename . (empty($extension) ? '' : '.' . $extension);

        return array('levelpath' => $levelPath, 'path' => $path, 'filename' => $filename, 'extension' => $extension);
    }

    /**
     * Конструктор
     */
    public function __construct($pathStorage)
    {
        $this->_adapter = new Zend_Cloud_StorageService_Adapter_FileSystem(array(
            Zend_Cloud_StorageService_Adapter_FileSystem::LOCAL_DIRECTORY => $pathStorage));
    }

    /**
     * Получить полный путь к файлу в хранилище
     * @param string $path относительный путь к файлу
     * @return false|string полный путь к файлу или false
     */
    public function getFullPathFile($path)
    {
        $filePath = $this->getLocalDirectory() . DIRECTORY_SEPARATOR . $path;
        $path = realpath($filePath);

        return ($path && file_exists($path)) ? $path : false;
    }

    /**
     * Проверить существование файла в хранилище
     *
     * @param string $path относительный путь к файлу
     * @return bool результат проверки
     */
    public function fileIsExist($path)
    {
        return $this->getFullPathFile($path) != false;
    }

    /**
     * Сохранить в хранилищие файл
     *
     * При этом возвращается относительный путь к файлу. Если файл с таким содержимым уже существует,
     * то возвращается имеющийся путь
     *
     * @param mixed $data содержимое файла
     * @param string $extension расширение файла
     * @return string относительный путь к файлу в хранилище
     */
    public function storeFile($data, $extension = '')
    {
        $pathInfo = $this->_getPathByData($data, $extension);
        if (!$this->fileIsExist($pathInfo['path'])) {
            if (!is_dir($this->getLocalDirectory() . $pathInfo['levelpath'])) {
                mkdir($this->getLocalDirectory() . $pathInfo['levelpath'], 0777, true);
            }

            $this->_adapter->storeItem($pathInfo['path'], $data);
        }

        return $pathInfo['path'];
    }

    /**
     * Получить файл из файлового хранилища
     *
     * @param string $path относительный путь к файлу в хранилище
     * @return bool|string false или файл как строка
     */
    public function fetchFile($path)
    {
        return $this->_adapter->fetchItem($path);
    }

    /**
     * Удалить файл из файлового хранилища
     *
     * @param string $path относительный путь к файлу в хранилище
     * @return void
     */
    public function removeFile($path)
    {
        $this->_adapter->deleteItem($path);
    }

    /**
     * Получить все имена файлов из файлового хранилища для данной директории
     *
     * @param string $directory директория
     * @return array имена файлов
     */
    public function listFiles($directory)
    {
        return $this->_adapter->listItems($directory);
    }

    /**
     * Получить директорию хранилища
     *
     * @return string директория хранилища
     */
    public function getLocalDirectory()
    {
        return $this->_adapter->getClient();
    }

    /**
     * Заархивировать указанные файлы в файловом хранилище.
     *
     * Полученный архив помещается в файловое хранилище. Если архив уже существует,
     * то возвращается уже имеющийся путь. Если в ходе создания архива возникли ошибки, то false
     *
     * @param array $filesInfo информация о архивируемых файлах:
     * array( array('path' => string, 'data' => string, 'name' =>  string, 'extension' => string), ... )
     * При этом в массиве с информацей может быть указан путь к файлу 'path' или его содержимое 'data',
     * но хотя бы одно из них
     *
     * @param string $hasSum md5 хеш-сумма содержимого архива
     * @return false|string относительный путь созданного архива в хранилище
     */
    public function zip(array $filesInfo, $hasSum = '')
    {
        if (!is_array($filesInfo) || empty($filesInfo)) {
            return false;
        }

        $filesData = '';
        $files = array();
        foreach ($filesInfo as $fileInfo) {
            if (isset($fileInfo['data'])) {
                $data = $fileInfo['data'];
            } elseif (isset($fileInfo['path'])) {
                if ($data = $this->_adapter->fetchItem($fileInfo['path'])) {
                    return false;
                }
            } else {
                return false;
            }

            $name = isset($fileInfo['name']) ? $fileInfo['name'] :
                (isset($fileInfo['path']) ? pathinfo($fileInfo['path'], PATHINFO_FILENAME) : md5($data));

            $extension = isset($fileInfo['extension']) ? $fileInfo['extension'] :
                (isset($fileInfo['path']) ? pathinfo($fileInfo['path'], PATHINFO_EXTENSION) : '');

            $files[] = array('data' => $data, 'name' => $name, 'extension' => $extension);

            if (empty($hasSum)) {
                $filesData .= $data;
            }
        }

        $zipName = !empty($hasSum) ? $hasSum : md5($filesData);
        $levelPath = DIRECTORY_SEPARATOR . substr($zipName, 0, 2) . DIRECTORY_SEPARATOR .
            substr($zipName, -1, 2) . DIRECTORY_SEPARATOR;
        $path = $levelPath . $zipName . '.zip';

        if (!$this->fileIsExist($path)) {
            if (!is_dir($this->getLocalDirectory() . $levelPath)) {
                mkdir($this->getLocalDirectory() . $levelPath, 0777, true);
            }

            $zip = new ZipArchive();
            if ($zip->open($this->getLocalDirectory() . $path, ZIPARCHIVE::CREATE) !== true) {
                return false;
            }

            foreach ($files as $file) {
                $zip->addFromString(iconv("UTF-8", "866", $file['name'] . '.' . $file['extension']), $file['data']);
            }

            if (!$zip->status == ZIPARCHIVE::ER_OK) {
                return false;
            }

            $zip->close();
        }

        return $path;
    }

    /**
     * Получить имена файлов, которые имеются в указанном архиве
     * @param string $path путь к архиву
     * @return false|array имена файлов
     */
    public function zipGetFileNames($path)
    {
        $result = false;

        $zip = new ZipArchive();
        if ($zip->open($path) === true) {
            $result = array();

            for ($i = 0; $i < $zip->numFiles; $i++) {
                $result[] = iconv("866", "UTF-8", $zip->getNameIndex($i));
            }
        }

        return $result;
    }

    /**
     * Получить содержимое указанного файла
     * @param string $path путь к архиву
     * @param $filename string имя файла в архиве
     * @return false|mixed содержимое файла
     */
    public function zipGetFileData($path, $filename)
    {
        $result = false;

        $zip = new ZipArchive();
        if ($zip->open($path) === true) {
            $result = $zip->getFromName(iconv("UTF-8", "866", $filename));

            $zip->close();
        }

        return $result;
    }
}