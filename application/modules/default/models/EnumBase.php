<?php

/**
 * Исключение для перечисления
 */
class Intensives_Model_EnumBase_Exception extends Exception {

}

/**
 * Базовый класс для перечислений
 *
 * Значения перечисления указываются потомками как константы
 */
abstract class Intensives_Model_EnumBase {
    /**
     * Массив констант задающих значение перечисления
     * @var array|null
     */
    private static $_constCache = null;

    /**
     * Рефлективный метод определения констант класса
     * @return array массив констант
     */
    private static function getConstants() {
        if (self::$_constCache === null) {
            $reflect = new ReflectionClass(get_called_class());
            self::$_constCache = $reflect->getConstants();
        }

        return self::$_constCache;
    }

    /**
     * Значение перечисления
     * @var int
     */
    protected $_value;

    /**
     * Конструктор
     * @param $value int
     */
    function __construct($value) {
        $this->setValue($value);
    }

    /*
     * Геттер для свойства
     */
    function __get($property) {
        return $this->_value;
    }

    /*
    * Сеттер для свойства
    */
    function __set($property, $value) {
        $this->setValue($value);
    }

    /**
     * Перевод в строку
     * @return string строковое значение перечисления
     */
    function __toString() {
        return (string)$this->_value;
    }

    /**
     * Получить значение перечисления
     * @return int значение перечисления
     */
    public function getValue() {
        return $this->_value;
    }

    /**
     * Задать значение перечисления
     * @param $value int значение перечисления
     * @throws Intensives_Model_EnumBase_Exception
     */
    public function setValue($value) {
        if ($this->isValidEnumValue($value)) {
            $this->_value = $value;
        }
        else {
            throw new Intensives_Model_EnumBase_Exception("Invalid type specified!");
        }
    }

    /**
     * Проверить корректность значения перечисления
     * @param $value int значение перечисления
     * @return bool результат проверки
     */
    public static function isValidEnumValue($value) {
        $values = array_values(self::getConstants());
        return in_array($value, $values, $strict = true);
    }
}