<?php

// Для генерации Odt документов по шаблону
require_once('odtphp/odf.php');
// Для генерации Odt документов
require_once('phpodt-0.3/phpodt.php');

require_once('num2str.php');


/**
 * Определяет класс, для генерации документов в формате odt по указанным шаблонам
 * Данные для генерации документов берутся из БД (если это необходимо)
 */
class Intensives_Model_Documentor
{
    /**
     * Пути к шаблонам документов
     * @var array
     */
    private $_odtTemplates = array();

    /**
     * Конфиг для odt библиотеки
     * @var array
     */
    private $_odtConfig = array();

    /**
     * Получить группу слушателей
     *
     * @param array $listeners слушатели
     * @return string группа
     */
    private function _getGroupString(array $listeners)
    {
        $course = '';
        $courses = array();
        foreach ($listeners as $key => $value) {
            $matches = array();
            $res = preg_match('/([0-9]).*/ui', $value['group'], $matches);
            if ($res && $matches[1] && !in_array($matches[1], $courses)) {
                $courses[] = $matches[1];
            }
        }
        for ($i = 0; $i < count($courses) - 1; ++$i) {
            $course .= '' . $courses[$i] . ', ';
        }
        $course .= $courses[count($courses) - 1];
        return $course;
    }

    /**
     * Сгенерировать документ - контракт слушателя
     * @param array $data данные для документа
     * @return false|Odf odt документ
     */
    private function _generateOdtListenerContract(array $data)
    {
        if (!isset($data['cid'])) {
            return false;
        }

        $odt = new Odf($this->_odtTemplates[Intensives_Model_DocumentCategoryEnum::ListenerContract], $this->_odtConfig);

        $contractsModel = new Intensives_Model_DbTable_Contract();
        $contract = $contractsModel->getContractDetails($data['cid']);
        $months = array(
            '01' => 'января',
            '02' => 'февраля',
            '03' => 'марта',
            '04' => 'апреля',
            '05' => 'мая',
            '06' => 'июня',
            '07' => 'июля',
            '08' => 'августа',
            '09' => 'сентября',
            '10' => 'октября',
            '11' => 'ноября',
            '12' => 'декабря'
        );

        $date = strtotime($contract['date']);
        $start = strtotime($contract['start']);
        $end = strtotime($contract['end']);

        try {
            $odt->setVars('number', $contract['number'], true, 'utf-8');
            $odt->setVars('chair_name', $contract['chairName'], true, 'utf-8');
            $odt->setVars('chair_abbreviation', $contract['chairAbbreviation'], true, 'utf-8');
            $odt->setVars('date_day', date('d', $date), true, 'utf-8');
            $odt->setVars('date_month', $months[date('m', $date)], true, 'utf-8');
            $odt->setVars('date_year', date('Y', $date), true, 'utf-8');
            $odt->setVars('listener_surname', $contract['listenerSurname'], true, 'utf-8');
            $odt->setVars('listener_name', $contract['listenerName'], true, 'utf-8');
            $odt->setVars('listener_patronymic', $contract['listenerPatronymic'], true, 'utf-8');
            $odt->setVars('hours', $contract['hours'], true, 'utf-8');
            $odt->setVars('start_day', date('d', $start), true, 'utf-8');
            $odt->setVars('start_month', $months[date('m', $start)], true, 'utf-8');
            $odt->setVars('start_year', date('Y', $start), true, 'utf-8');
            $odt->setVars('end_day', date('d', $end), true, 'utf-8');
            $odt->setVars('end_month', $months[date('m', $end)], true, 'utf-8');
            $odt->setVars('end_year', date('Y', $end), true, 'utf-8');
            $odt->setVars('total', $contract['total'], true, 'utf-8');
            $odt->setVars('total_inwords', num2str($contract['total']), true, 'utf-8');
            $odt->setVars('listener_address', $contract['listenerAddress'], true, 'utf-8');
            $odt->setVars('listener_passport_series', $contract['listenerPassportSeries'], true, 'utf-8');
            $odt->setVars('listener_passport_number', $contract['listenerPassportNumber'], true, 'utf-8');
            $odt->setVars('listener_passport_givenout', $contract['listenerPassportGivenout'], true, 'utf-8');
            $odt->setVars('listener_passport_givenout_when', $contract['listenerPassportGivenoutWhen'], true, 'utf-8');
            $odt->setVars('listener_phone', $contract['listenerPhone'], true, 'utf-8');
        } catch (Exception $e) {
        }

        return $odt;
    }

    /**
     * Сгенерировать документ - список слушателей
     * @param array $data данные для документа
     * @return false|Odf odt документ
     */
    private function _generateOdtListenerList(array $data)
    {
        if (!isset($data['ids'])) {
            return false;
        }

        $odt = new Odf($this->_odtTemplates[Intensives_Model_DocumentCategoryEnum::ListenerList], $this->_odtConfig);

        $listenersModel = new Intensives_Model_DbTable_Listener();
        $listenersIds = explode(',', $data['ids']);
        $listeners = $listenersModel->fetchAll(array('id IN(?)' => $listenersIds),
            array('surname', 'name', 'patronymic'))->toArray();

        $contractsModel = new Intensives_Model_DbTable_Contract();

        $date = date('d.m.Y');
        try {
            $odt->setVars('date', $date);
        } catch (Exception $e) {
        }

        $listenerSegment = $odt->setSegment('listeners');
        foreach ($listeners as $listener) {
            foreach ($listener as $name => $value) {
                try {
                    $listenerSegment->setVars($name, $value, true, 'utf-8');
                } catch (Exception $e) {
                }
            }
            $contracts = $contractsModel->getContractsByListenerId($listener['id'], 'id')->toArray();
            $contracts_ids = "";
            foreach ($contracts as $contract) {
                $contracts_ids .= $contract['id'] . ", ";
            }
            $contracts_ids = substr($contracts_ids, 0, -2);
            try {
                $listenerSegment->setVars('contracts_ids', $contracts_ids, true, 'utf-8');
            } catch (Exception $e) {
            }
            $listenerSegment->merge();
        }

        $odt->mergeSegment($listenerSegment);

        return $odt;
    }

    /**
     * Сгенерировать документ - приказ о зачислении слушателей
     * @param array $data данные для документа
     * @return false|Odf odt документ
     */
    private function _generateOdtListenerOrder(array $data)
    {
        if (!isset($data['mid']) || !isset($data['from']) || !isset($data['to'])) {
            return false;
        }

        $odt = new Odf($this->_odtTemplates[Intensives_Model_DocumentCategoryEnum::ListenerOrder], $this->_odtConfig);

        $chairModel = new Intensives_Model_DbTable_Chair();
        $select = $chairModel->select()
            ->setIntegrityCheck(false)
            ->from(array('c' => 'chair'), array('name', 'id'))
            ->joinInner(array('m' => 'methodist'), 'c.id = m.chair_id', array())
            ->where('m.id = ?', $data['mid']);
        $chair = $chairModel->fetchRow($select)->toArray();

        $listenersModel = new Intensives_Model_DbTable_Listener();
        $joinWhere = $listenersModel->getDefaultAdapter()->quoteInto(
            'l.id = c.listener_id AND c.paid = 1 AND c.chair_id = ?', $chair['id']);
        $fromCond = $listenersModel->getDefaultAdapter()->quoteInto('start >= ?', $data['from']);
        $toCond = $listenersModel->getDefaultAdapter()->quoteInto('end <= ?', $data['to']);

        $startDate = strtotime($data['from']);
        $startDay = date('d', $startDate);
        $startMonth = date('m', $startDate);
        $startYear = date('Y', $startDate);

        $endDate = strtotime($data['to']);
        $endDay = date('d', $endDate);
        $endMonth = date('m', $endDate);
        $endYear = date('Y', $endDate);

        $listeners = $listenersModel->fetchAll(
            $listenersModel->select()
                ->setIntegrityCheck(false)
                ->from(array('l' => 'listener'))
                ->joinLeft(array('c' => 'contract'), $joinWhere,
                    array('hours' => 'SUM(c.hours)', 'total' => 'SUM(c.receipt_total)', 'start', 'end'))
                ->where($fromCond . ' AND ' . $toCond)
                ->order(array('hours DESC', 'total DESC', 'surname ASC', 'name ASC', 'patronymic ASC'))
                ->group('l.id')
        )->toArray();

        $categorizedListeners = array();
        foreach ($listeners as $listener) {
            $categorizedListeners[$listener['hours']][$listener['total']][] = $listener;
        }

        try {
            $odt->setVars('chair_name', $chair['name'], true, 'utf-8');
        } catch (Exception $e) {
        }

        $categoriesSegment = $odt->setSegment('categories');

        foreach ($categorizedListeners as $hours => $catListeners1) {
            foreach ($catListeners1 as $total => $catListeners2) {
                try {
                    $categoriesSegment->setVars('hours', $hours, true, 'utf-8');
                    $categoriesSegment->setVars('total', $total, true, 'utf-8');
                    $categoriesSegment->setVars('start_day', $startDay, true, 'utf-8');
                    $categoriesSegment->setVars('start_month', $startMonth, true, 'utf-8');
                    $categoriesSegment->setVars('start_year', $startYear, true, 'utf-8');
                    $categoriesSegment->setVars('end_day', $endDay, true, 'utf-8');
                    $categoriesSegment->setVars('end_month', $endMonth, true, 'utf-8');
                    $categoriesSegment->setVars('end_year', $endYear, true, 'utf-8');
                } catch (Exception $e) {
                }

                $num = 1;
                foreach ($catListeners2 as $listener) {
                    try {
                        $categoriesSegment->listeners->setVars('num', $num, true, 'utf-8');
                        $categoriesSegment->listeners->setVars('surname', $listener['surname'], true, 'utf-8');
                        $categoriesSegment->listeners->setVars('name', $listener['name'], true, 'utf-8');
                        $categoriesSegment->listeners->setVars('patronymic', $listener['patronymic'], true, 'utf-8');
                        $categoriesSegment->listeners->setVars('group', $listener['group'], true, 'utf-8');
                    } catch (Exception $e) {
                    }

                    $categoriesSegment->listeners->merge();
                    $num++;
                }

                $categoriesSegment->merge();
            }
        }

        $odt->mergeSegment($categoriesSegment);

        return $odt;
    }

    /**
     * Сгенерировать документ - договор перподавателя
     * @param array $data данные для документа
     * @return false|Odf odt документ
     */
    private function _generateOdtTeacherContract(array $data)
    {
        if (!isset($data['teacher_id']) || !isset($data['make_date']) || !isset($data['start']) ||
            !isset($data['end']) || !isset($data['hour_cost']) || !isset($data['semester'])
        ) {
            return false;
        }

        $odt = new Odf($this->_odtTemplates[Intensives_Model_DocumentCategoryEnum::TeacherContract], $this->_odtConfig);

        $teachersModel = new Intensives_Model_DbTable_Teacher();
        $teacherData = $teachersModel->getTeacherDataWithChairName($data['teacher_id'])->toArray();

        $lessonsModel = new Intensives_Model_DbTable_Lesson();
        $hours = (double)$lessonsModel->getTeacherTotalHoursByPeriod($data['teacher_id'],
            $data['start'], $data['end'], 1);
        $hours += (double)$lessonsModel->getTeacherTotalHoursByPeriod($data['teacher_id'],
            $data['start'], $data['end'], 2);

        $payment = round($data['hour_cost'] * $hours, 2);
        $strBirthDate = $teacherData['birthdate'];

        $months_names = array(
            '01' => 'января',
            '02' => 'февраля',
            '03' => 'марта',
            '04' => 'апреля',
            '05' => 'мая',
            '06' => 'июня',
            '07' => 'июля',
            '08' => 'августа',
            '09' => 'сентября',
            '10' => 'октября',
            '11' => 'ноября',
            '12' => 'декабря'
        );

        $contractData = array();
        $makeDate = strtotime($data['make_date']);
        $contractData['make_date_day'] = date('d', $makeDate);
        $contractData['make_date_month'] = $months_names[date('m', $makeDate)];
        $contractData['make_date_year'] = date('Y', $makeDate);
        $startDate = strtotime($data['start']);
        $contractData['start_day'] = date('d', $startDate);
        $contractData['start_month'] = $months_names[date('m', $startDate)];
        $contractData['start_year'] = date('Y', $startDate);
        $endDate = strtotime($data['end']);
        $contractData['end_day'] = date('d', $endDate);
        $contractData['end_month'] = $months_names[date('m', $endDate)];
        $contractData['end_year'] = date('Y', $endDate);
        $contractData['hours'] = $hours;
        $contractData['payment'] = $payment;
        $birthDate = strtotime($strBirthDate);
        $contractData['birth_day'] = date('d', $birthDate);
        $contractData['birth_month'] = $months_names[date('m', $birthDate)];
        $contractData['birth_year'] = date('Y', $birthDate);

        $lectures = $lessonsModel->getTeacherLessonsByType($data['teacher_id'], 0, $data['start'],
            $data['end'], 1)->toArray();
        $second_lectures = $lessonsModel->getTeacherLessonsByType($data['teacher_id'], 0, $data['start'],
            $data['end'], 2);
        foreach ($second_lectures as $lec) {
            $lectures[] = $lec;
        }

        $practices = $lessonsModel->getTeacherLessonsByType($data['teacher_id'], 1, $data['start'],
            $data['end'], 1)->toArray();
        $second_practices = $lessonsModel->getTeacherLessonsByType($data['teacher_id'], 1, $data['start'],
            $data['end'], 2);
        foreach ($second_practices as $pr) {
            $practices[] = $pr;
        }

        $combined = $lessonsModel->getTeacherLessonsByType($data['teacher_id'], 2, $data['start'],
            $data['end'], 1)->toArray();
        $second_combined = $lessonsModel->getTeacherLessonsByType($data['teacher_id'], 2, $data['start'],
            $data['end'], 2);
        foreach ($second_combined as $c) {
            $combined[] = $c;
        }

        $totalWorkHours = '';
        $contractData['lec_count'] = count($lectures) * 4;
        $contractData['lab_count'] = count($practices) * 4;
        $contractData['sp_count'] = count($combined) * 4;

        if ($contractData['lec_count'] != 0) {
            $contractData['lec_total'] = count($lectures) * 4 * $data['hour_cost'];
            $contractData['hour_cost_lec'] = $data['hour_cost'];
            $totalWorkHours .= 'Лекционных: ' . $contractData['lec_total'] . 'ч.';
        } else {
            $contractData['lec_total'] = '';
            $contractData['hour_cost_lec'] = '';
            $contractData['lec_count'] = '';
        }

        if ($contractData['lab_count'] != 0) {
            $contractData['lab_total'] = count($practices) * 4 * $data['hour_cost'];
            $contractData['hour_cost_lab'] = $data['hour_cost'];
            $totalWorkHours .= ' Практических: ' . $contractData['lab_total'] . 'ч.';
        } else {
            $contractData['lab_total'] = '';
            $contractData['hour_cost_lab'] = '';
            $contractData['lab_count'] = '';
        }

        if ($contractData['sp_count'] != 0) {
            $contractData['sp_total'] = count($combined) * 4 * $data['hour_cost'];
            $contractData['hour_cost_sp'] = $data['hour_cost'];
            $totalWorkHours .= ' Семинарско-практических: ' . $contractData['sp_count'] . 'ч.';
        } else {
            $contractData['sp_total'] = '';
            $contractData['hour_cost_sp'] = '';
            $contractData['sp_count'] = '';
        }

        if ($data['semester'] == 1) {
            $contractData['hours1'] = $contractData['hours'];
            $contractData['hours2'] = '';
            $contractData['total_work_hours1'] = $totalWorkHours;
            $contractData['total_work_hours2'] = '';
        } else if ($data['semester'] == 2) {
            $contractData['hours2'] = $contractData['hours'];
            $contractData['hours1'] = '';
            $contractData['total_work_hours2'] = $totalWorkHours;
            $contractData['total_work_hours1'] = '';
        }

        $contractData['payment_in_words'] = num2str($payment);

        foreach ($teacherData as $key => $value) {
            try {
                $odt->setVars($key, $value, true, 'utf-8');
            } catch (Exception $e) {
            }
        }

        foreach ($contractData as $key => $value) {
            try {
                $odt->setVars($key, $value, true, 'utf-8');
            } catch (Exception $e) {
            }
        }

        return $odt;
    }

    /**
     * Сгенерировать документ - отчет о проделенной работе преподавателя
     * @param array $data данные для документа
     * @return false|Odf odt документ
     */
    private function _generateOdtTeacherReport(array $data)
    {
        if (!isset($data['teacher_id']) || !isset($data['contract_make_date']) || !isset($data['make_date']) ||
            !isset($data['start']) || !isset($data['end']) || !isset($data['hour_cost'])
        ) {
            return false;
        }

        $odt = new Odf($this->_odtTemplates[Intensives_Model_DocumentCategoryEnum::TeacherReport], $this->_odtConfig);

        $teachersModel = new Intensives_Model_DbTable_Teacher();
        $teacherData = $teachersModel->getTeacherDataWithChairName($data['teacher_id'])->toArray();

        $lessonsModel = new Intensives_Model_DbTable_Lesson();
        $hours = (double)$lessonsModel->getTeacherTotalHoursByPeriod($data['teacher_id'],
            $data['start'], $data['end']);
        $lectures = $lessonsModel->getTeacherLessonsByType($data['teacher_id'], 0,
            $data['start'], $data['end'], 2)->toArray();
        $practices = $lessonsModel->getTeacherLessonsByType($data['teacher_id'], 1,
            $data['start'], $data['end'], 2)->toArray();
        $combined = $lessonsModel->getTeacherLessonsByType($data['teacher_id'], 2,
            $data['start'], $data['end'], 2)->toArray();

        $payment = $data['hour_cost'] * $hours;

        $months_names = array(
            '01' => 'января',
            '02' => 'февраля',
            '03' => 'марта',
            '04' => 'апреля',
            '05' => 'мая',
            '06' => 'июня',
            '07' => 'июля',
            '08' => 'августа',
            '09' => 'сентября',
            '10' => 'октября',
            '11' => 'ноября',
            '12' => 'декабря'
        );

        $reportData = array();
        $contractMakeDate = strtotime($data['contract_make_date']);
        $reportData['contract_make_date_day'] = date('d', $contractMakeDate);
        $reportData['contract_make_date_month'] = $months_names[date('m', $contractMakeDate)];
        $reportData['contract_make_date_year'] = date('Y', $contractMakeDate);
        $makeDate = strtotime($data['make_date']);

        $reportData['make_date_day'] = date('d', $makeDate);
        $reportData['make_date_month'] = $months_names[date('m', $makeDate)];
        $reportData['make_date_year'] = date('Y', $makeDate);
        $reportData['payment_in_words'] = num2str($payment);
        $reportData['payment'] = $payment;
        $reportData['hour_cost'] = $data['hour_cost'];
        $reportData['lec_count'] = count($lectures) * 4;
        $reportData['lab_count'] = count($practices) * 4;
        $reportData['sp_count'] = count($combined) * 4;

        $totalWorkHours = '';
        if ($reportData['lec_count'] != 0) {
            $reportData['lec_total'] = count($lectures) * 4 * $data['hour_cost'];
            $reportData['hour_cost_lec'] = $data['hour_cost'];
            $totalWorkHours .= 'Лекционных: ' . $reportData['lec_count'] . 'ч.';
        } else {
            $reportData['lec_total'] = '';
            $reportData['hour_cost_lec'] = '';
            $reportData['lec_count'] = '';
        }
        if ($reportData['lab_count'] != 0) {
            $reportData['lab_total'] = count($practices) * 4 * $data['hour_cost'];
            $reportData['hour_cost_lab'] = $data['hour_cost'];
            $totalWorkHours .= ' Практических: ' . $reportData['lab_count'] . 'ч.';
        } else {
            $reportData['lab_total'] = '';
            $reportData['hour_cost_lab'] = '';
            $reportData['lab_count'] = '';
        }
        if ($reportData['sp_count'] != 0) {
            $reportData['sp_total'] = count($combined) * 4 * $data['hour_cost'];
            $reportData['hour_cost_sp'] = $data['hour_cost'];
            $totalWorkHours .= ' Семинарско-практических: ' . $reportData['sp_count'] . 'ч.';
        } else {
            $reportData['sp_total'] = '';
            $reportData['hour_cost_sp'] = '';
            $reportData['sp_count'] = '';
        }
        $reportData['total_work_hours'] = $totalWorkHours;
        $reportData['hours'] = count($combined) * 4 + count($practices) * 4 + count($lectures) * 4;

        foreach ($teacherData as $key => $value) {
            try {
                $odt->setVars($key, $value, true, 'utf-8');
            } catch (Exception $e) {
            }
        }

        foreach ($reportData as $key => $value) {
            try {
                $odt->setVars($key, $value, true, 'utf-8');
            } catch (Exception $e) {
            }
        }

        $lessons = array();
        foreach ($lectures as $lecture) {
            $lessons[] = $lecture;
        }
        foreach ($practices as $practice) {
            $lessons[] = $practice;
        }
        foreach ($combined as $comb) {
            $lessons[] = $comb;
        }

        $lessonsSegment = $odt->setSegment('lessons');
        $lessonTypeLookup = array('Лекционное', 'Практическое', 'Семинарско-практические');
        $listenerModel = new Intensives_Model_DbTable_LessonToListener();
        for ($i = 1; $i <= count($lessons); $i += 2) {
            if ($i != count($lessons)) {
                $lessonID = $lessons[$i - 1]['id'];
                $listeners = $listenerModel->fetchLessonPresence($lessonID)->toArray();
                $group1 = $this->_getGroupString($listeners);

                $lessonID = $lessons[$i]['id'];
                $listeners = $listenerModel->fetchLessonPresence($lessonID)->toArray();
                $group2 = $this->_getGroupString($listeners);

                $less_data = array('date1' => date_create($lessons[$i - 1]['start'])->format('d.m.Y'),
                    'type1' => $lessonTypeLookup[$lessons[$i - 1]['type']],
                    'hours1' => '4',
                    'course1' => $group1,
                    'date2' => date_create($lessons[$i]['start'])->format('d.m.Y'),
                    'type2' => $lessonTypeLookup[$lessons[$i]['type']],
                    'hours2' => '4',
                    'course2' => $group2);
            } else {
                $lessonID = $lessons[$i - 1]['id'];
                $listeners = $listenerModel->fetchLessonPresence($lessonID)->toArray();
                $group1 = $this->_getGroupString($listeners);
                $less_data = array('date1' => date_create($lessons[$i - 1]['start'])->format('d.m.Y'),
                    'type1' => $lessonTypeLookup[$lessons[$i - 1]['type']],
                    'hours1' => '4',
                    'course1' => $group1,
                    'date2' => '',
                    'type2' => '',
                    'hours2' => '',
                    'course2' => '');
            }
            foreach ($less_data as $key => $value) {

                try {
                    $lessonsSegment->setVars($key, $value, true, 'utf-8');
                } catch (Exception $e) {
                }
            }
            $lessonsSegment->merge();
        }

        $odt->mergeSegment($lessonsSegment);

        return $odt;
    }

    /**
     * Сгенерировать документ - отчет о движении денежных средств подразделения "Интенсив"
     * @param array $data данные для документа
     * @return false|Odf odt документ
     */
    private function _generateOdtFlowFunds(array $data)
    {
        if (!isset($data['mid']) || !isset($data['from']) || !isset($data['to'])) {
            return false;
        }

        $odt = new Odf($this->_odtTemplates[Intensives_Model_DocumentCategoryEnum::FlowFunds], $this->_odtConfig);

        $chairModel = new Intensives_Model_DbTable_Chair();
        $select = $chairModel->select()
            ->setIntegrityCheck(false)
            ->from(array('c' => 'chair'), array('id', 'abbreviation', 'name'))
            ->joinInner(array('m' => 'methodist'), 'c.id = m.chair_id', array())
            ->where('m.id = ?', $data['mid']);
        $chair = $chairModel->fetchRow($select)->toArray();
        $chairName = strlen($chair['abbreviation']) ? $chair['abbreviation'] : $chair['name'];

        $contractModel = new Intensives_Model_DbTable_Contract();
        $select = $contractModel->select()
            ->setIntegrityCheck(false)
            ->from(array('c' => 'contract'),
                array('receipt_number', 'receipt_total', 'receipt_date'))
            ->joinInner(array('l' => 'listener'), 'c.listener_id = l.id', array('lid' => 'l.id',
                'name' => 'CONCAT_WS(\' \', l.surname, l.name, l.patronymic)'))
            ->where('c.paid = ?', 1)
            ->where('c.chair_id = ?', $chair['id'])
            ->where($contractModel->getDefaultAdapter()->quoteInto('c.receipt_date >= ?', $data['from']) . ' AND ' .
                $contractModel->getDefaultAdapter()->quoteInto('c.receipt_date <= ?', $data['to']))
            ->order(array('name ASC'));
        $rows = $contractModel->fetchAll($select)->toArray();

        $listenerSegment = $odt->setSegment('listeners');

        $fromDateTime = strtotime($data['from']);
        $toDateTime = strtotime($data['to']);
        $subdivision = 'Интенсив (ндс)';
        try {
            $odt->setVars('subdivision', $subdivision, true, 'utf-8');
            $odt->setVars('chair_name', $chairName, true, 'utf-8');
            $odt->setVars('start_day', date('d', $fromDateTime), true, 'utf-8');
            $odt->setVars('start_month', date('m', $fromDateTime), true, 'utf-8');
            $odt->setVars('start_year', date('Y', $fromDateTime), true, 'utf-8');
            $odt->setVars('end_day', date('d', $toDateTime), true, 'utf-8');
            $odt->setVars('end_month', date('m', $toDateTime), true, 'utf-8');
            $odt->setVars('end_year', date('Y', $toDateTime), true, 'utf-8');
        } catch (Exception $e) {
        }

        $number = 0;
        $total = 0;
        $previousId = 0;
        foreach ($rows as $row) {
            $dateTime = strtotime($row['receipt_date']);
            $strNumber = '';
            $contractName = $row['name'];

            if ($previousId != $row['lid']) {
                $strNumber = (string)++$number;
                $contractName = $subdivision . ' ' . $chairName . "\n" . $row['name'];
            }

            try {
                $listenerSegment->setVars('number', $strNumber, true, 'utf-8');
                $listenerSegment->setVars('contract_name', $contractName, true, 'utf-8');
                $listenerSegment->setVars('name', $row['name'], true, 'utf-8');
                $listenerSegment->setVars('receipt_number', $row['receipt_number'], true, 'utf-8');
                $listenerSegment->setVars('receipt_day', date('d', $dateTime), true, 'utf-8');
                $listenerSegment->setVars('receipt_month', date('m', $dateTime), true, 'utf-8');
                $listenerSegment->setVars('receipt_year', date('Y', $dateTime), true, 'utf-8');
                $listenerSegment->setVars('receipt_total', $row['receipt_total'], true, 'utf-8');
                $listenerSegment->merge();
            } catch (Exception $e) {
            }

            $total += $row['receipt_total'];
            $previousId = $row['lid'];
        }

        try {
            $odt->mergeSegment($listenerSegment);
            $odt->setVars('total', $total, true, 'utf-8');
        } catch (Exception $e) {
        }

        return $odt;
    }

    /**
     * Сгенерировать документ - журнал слушателей за период
     * @param array $data данные для документа
     * @return false|Odf odt документ
     */
    private function _generateOdtListenerJournal(array $data)
    {
        if (!isset($data['fromDate']) || !isset($data['toDate']) || !isset($data['journalColumns']) || !isset($data['journalData'])) {
            return false;
        }

        $odt = ODT::getInstance();

        $headerStyle = new TextStyle('textstyle1');
        $headerStyle->setBold();
        $headerStyle->setFontSize('14pt');

        $textStyle = new TextStyle('textstyle2');
        $textStyle->setFontSize('10pt');

        $textStyle2 = new TextStyle('textstyle3');
        $textStyle2->setBold();
        $textStyle2->setFontSize('10pt');

        $p = new Paragraph();
        $p->addText('Журнал слушателей за период от ' . $data['fromDate'] . ' до ' . $data['toDate'], $headerStyle);
        $p->addLineBreak();

        // Структура таблицы-журнала: | ФИО | темы занятий (1...n) | Номер тел. слушателя | Примечание (свободное поле)|

        $table = new Table('table1');
        $tableStyle = new TableStyle($table->getTableName());
        $tableStyle->setAlignment(StyleConstants::CENTER);
        $table->setStyle($tableStyle);

        $columns = array();
        foreach ($data['journalColumns'] as $key => $value) {
            $columns[] = $value;
        }

        $table->createColumns(count($columns));
        $table->addHeader($columns);

        $rows = array();
        foreach ($data['journalData'] as $journalRow) {
            $row = array();

            $p = new Paragraph();
            $p->addText($journalRow['listener'], $textStyle);
            $row[] = $p;
            foreach ($journalRow as $key => $value) {
                if (strpos($key, 'theme') !== false) {
                    $ph = new Paragraph();
                    for ($i = 0; $i < count($value); $i++) {
                        $ph->addText($value[$i]['status'], $textStyle2);
                        if (!empty($value[$i]['type'])) {
                            $ph->addText(' (' . $value[$i]['type'] . ')', $textStyle);
                        }

                        if ($i != count($value) - 1) {
                            $ph->addLineBreak();
                        }
                    }
                    $row[] = $ph;
                }
            }
            $p = new Paragraph();
            $p->addText($journalRow['listenerPhone'], $textStyle);
            $row[] = $p;

            $p = new Paragraph();
            $p->addText($journalRow['note'], $textStyle);
            $row[] = $p;

            $rows[] = $row;
        }
        $table->addRows($rows);

        // Получение контекста
        $content = $odt->getDocumentContent()->saveXML();
        // Создание одф файла и свзяка его с шаблоном
        $odt = new Odf($this->_odtTemplates[Intensives_Model_DocumentCategoryEnum::ListenerJournal], $this->_odtConfig);
        $odt->setContent($content);
        return $odt;
    }

    /**
     * Конструктор
     */
    public function __construct()
    {
        $config = Zend_Controller_Front::getInstance()->getParam('bootstrap');

        $this->_odtConfig = array(
            'ZIP_PROXY' => $config->getOption('phpOdt')['zip_proxy'],
            'PATH_TO_TMP' => $config->getOption('phpOdt')['path_to_tmp']
        );

        $documentStorage = $config->getOption('documentStorage');
        $path = $documentStorage['templates']['path'];
        $this->_odtTemplates = array(
            Intensives_Model_DocumentCategoryEnum::TeacherContract =>
                $path . $documentStorage['template']['teacherСontract'],
            Intensives_Model_DocumentCategoryEnum::TeacherReport =>
                $path . $documentStorage['template']['teacherReport'],
            Intensives_Model_DocumentCategoryEnum::ListenerContract =>
                $path . $documentStorage['template']['listenerСontract'],
            Intensives_Model_DocumentCategoryEnum::ListenerOrder =>
                $path . $documentStorage['template']['listenerOrder'],
            Intensives_Model_DocumentCategoryEnum::FlowFunds =>
                $path . $documentStorage['template']['flowFunds'],
            Intensives_Model_DocumentCategoryEnum::ListenerJournal =>
                $path . $documentStorage['template']['listenerJournal'],
            Intensives_Model_DocumentCategoryEnum::ListenerList =>
                $path . $documentStorage['template']['listenerList']
        );
    }

    /**
     * Сгенерировать odt документ по шаблону взависимости от категории
     *
     * Если массив данных пуст, то вернется пустой шаблон для данной категории
     * @param Intensives_Model_DocumentCategoryEnum $category категория документа
     * @param array $data данные
     * @return false|Odf сгенерированный odt документ
     */
    public function generateOdt(Intensives_Model_DocumentCategoryEnum $category, array $data = array())
    {
        if (is_null($category)) {
            return false;
        }

        if (!is_array($data)) {
            return false;
        }

        $result = false;
        if (count($data) == 0 && array_key_exists($category->getValue(), $this->_odtTemplates)) {
            $result = new Odf($this->_odtTemplates[$category->getValue()], $this->_odtConfig);
        } else {
            switch ($category->getValue()) {
                case Intensives_Model_DocumentCategoryEnum::ListenerContract:
                    $result = $this->_generateOdtListenerContract($data);
                    break;
                case Intensives_Model_DocumentCategoryEnum::ListenerList:
                    $result = $this->_generateOdtListenerList($data);
                    break;
                case Intensives_Model_DocumentCategoryEnum::ListenerOrder:
                    $result = $this->_generateOdtListenerOrder($data);
                    break;
                case Intensives_Model_DocumentCategoryEnum::TeacherContract:
                    $result = $this->_generateOdtTeacherContract($data);
                    break;
                case Intensives_Model_DocumentCategoryEnum::TeacherReport:
                    $result = $this->_generateOdtTeacherReport($data);
                    break;
                case Intensives_Model_DocumentCategoryEnum::FlowFunds:
                    $result = $this->_generateOdtFlowFunds($data);
                    break;
                case Intensives_Model_DocumentCategoryEnum::ListenerJournal:
                    $result = $this->_generateOdtListenerJournal($data);
                    break;
            }
        }

        return $result;
    }
}