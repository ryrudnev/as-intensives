<?php

class Intensives_Model_DbTable_Lesson extends Zend_Db_Table_Abstract
{

    protected $_name = 'lesson';

    /**
     * Получение списка занятий по указанному курсу интенсива.
     * @param int $intensiveId идентификатор курса интенсива.
     * @param boolean $withDeleted извлекать также занятия, которые отмечены,
     * как 'удаленное'.
     * @param string $order порядок сортировки (SQL-синтаксис).
     * @param int $count количество занятий в выборке.
     * @param int $offset смещение в выборке.
     * @return Zend_Db_Table_Rowset_Abstract список занятий.
     */
    public function fetchLessons($intensiveId, $withDeleted = false, $order = null, $count = null, $offset = null)
    {
        $where = $this->getDefaultAdapter()->quoteInto('l.intensive_id = ?', $intensiveId);

        if (!$withDeleted)
        {
            $where .= ' AND l.deleted = 0';
        }

        return $this->fetchAll(
            $this->select()
                ->from(array('l' => $this->_name))
                ->setIntegrityCheck(false)
                ->joinLeft(array('t' => 'teacher'), 't.id = l.teacher_id', array('surname AS teacher_surname', 'name AS teacher_name', 'patronymic AS teacher_patronymic'))
                ->joinLeft(array('a' => 'auditory'), 'a.id = l.auditory_id', 'name AS auditory_name')
                ->where($where)
                ->order($order)
                ->limit($count, $offset)
            , $order, $count, $offset);
    }

    /**
     * Получение количества часов, которые провёл преподаватель за указанный
     * период.
     *
     * @param int $teacherId идентификатор преподавателя.
     * @param string $start дата начала периода обучения.
     * @param string $end дата окончания периода обучения.
     *
     * @return int количество часов, которые провёл преподаватель за указанный
     * период.
     */
    public function getTeacherTotalHoursByPeriod($teacherId, $start, $end, $status = 2)
    {
        $where = $this->getDefaultAdapter()->quoteInto('start >= ?', $start);
        $where .= $this->getDefaultAdapter()->quoteInto(' AND end <= ?', $end);
        $where .= $this->getDefaultAdapter()->quoteInto(' AND teacher_id = ?', $teacherId);
        $where .= $this->getDefaultAdapter()->quoteInto(' AND status = ?', $status);

        return $this->getDefaultAdapter()->fetchOne($this->select()
                ->from($this->_name, 'COUNT(*) * 4')
                ->setIntegrityCheck(false)
                ->where($where));
    }

    /**
     * Получение списка занятий, которые назначены преподавателю, но не ещё
     * проведены им.
     *
     * @param int $teacherId идентификатор преподавателя.
     * @param string $order порядок сортировки.
     * @param boolean $withDeleted извлекать также занятия, которые были
     * 'удалены'.
     * @return Zend_Db_Table_Rowset_Abstract список занятий.
     */
    public function getTeacherLessons($teacherId, $order = 'start DESC', $withDeleted = false)
    {
        $where = $this->getDefaultAdapter()->quoteInto('l.teacher_id = ? AND l.status = 1', $teacherId);
        if (!$withDeleted)
        {
            $where .= ' AND l.deleted = 0';
        }

        $select = $this->select()
                ->from(array('l' => $this->_name))
                ->setIntegrityCheck(false)
                ->joinLeft(array('a' => 'auditory'), 'a.id = l.auditory_id', array('name AS auditory'))
                ->joinLeft(array('i' => 'intensive'), 'i.id = l.intensive_id', array('name AS intensive'))
                ->where($where)
                ->order($order);

        return $this->fetchAll($select);
    }
    public function getTeacherLessonsByType($teacherID, $type = null, $fromDate = null, $toDate = null, $status = null)
    {
        $where = '';
        $where = $where . $this->getDefaultAdapter()->quoteInto('l.teacher_id = ?', $teacherID);
        if (!is_null($status)) {
            $where = $where . $this->getDefaultAdapter()->quoteInto(' AND l.status = ?', $status);
        }
        if (!is_null($type)) {
            $where = $where . $this->getDefaultAdapter()->quoteInto(' AND l.type = ?', $type);
        }
        if (!is_null($fromDate)) {
            $where = $where . $this->getDefaultAdapter()->quoteInto(' AND start >= ?', $fromDate);
        }
        if (!is_null($toDate)) {
            $where = $where . $this->getDefaultAdapter()->quoteInto(' AND end <= ?', $toDate);
        }
        $select = $this->select()
                ->from(array('l' => $this->_name))
                ->setIntegrityCheck(false)
                ->joinLeft(array('i' => 'intensive'), 'i.id = l.intensive_id', array('name AS intensive'))
                ->joinLeft(array('a' => 'auditory'), 'a.id = l.auditory_id', 'name AS auditory_name')
                ->where($where)
                ->order($order);

        $query = $this->fetchAll($select);
        return $query;
    }
}
