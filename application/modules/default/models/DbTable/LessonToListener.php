<?php

class Intensives_Model_DbTable_LessonToListener extends Zend_Db_Table_Abstract
{

    protected $_name = 'lesson_to_listener';

    /**
     * Получение списка посещаемости занятия.
     *
     * @param int $lessonId идентификатор занятия.
     *
     * @return Zend_Db_Table_Rowset_Abstract список слушателей с отметкой их
     * присутствия на указанном занятии.
     */
    public function fetchLessonPresence($lessonId)
    {
        $where = $this->getDefaultAdapter()->quoteInto('lesson_id = ?', $lessonId);
        return $this->fetchAll(
            $this->select()
                ->from(array('ll' => $this->_name), array('listener_id', 'lesson_id', 'presented'))
                ->setIntegrityCheck(false)
                ->joinLeft(array('li' => 'listener'), 'll.listener_id = li.id', array('surname', 'name', 'patronymic', 'group'))
                ->where($where)
                ->order('surname ASC')
        );
    }

    /**
     * Получение посещаемости слушателя.
     *
     * @param int $listenerId идентификатор слушателя.
     *
     * @return Zend_Db_Table_Rowset_Abstract список занятий, на которые был
     * записан слушатель, и отметка посещаемости данных занятий слушателем.
     */
    public function fetchListenerPresence($listenerId)
    {
        $where = $this->getDefaultAdapter()->quoteInto('ll.listener_id = ? AND l.status = 2', $listenerId);
        return $this->fetchAll(
            $this->select()
                ->from(array('ll' => $this->_name), array('presented'))
                ->setIntegrityCheck(false)
                ->joinLeft(array('l' => 'lesson'), 'll.lesson_id = l.id', array('theme', 'start'))
                ->joinLeft(array('t' => 'teacher'), 'l.teacher_id = t.id', array('surname', 'name', 'patronymic'))
                ->where($where)
                ->order('start DESC')
        );
    }

    /**
     * Получение по каждому слушателю количества часов, которые ему были
     * прочитаны.
     *
     * @param boolean $withDeleted учитывать 'удалённые' занятий.
     *
     * @return Zend_Db_Table_Rowset_Abstract по каждому слушателю количество
     * часов, которые ему были прочитаны.
     */
    public function fetchListenersReadedHours($withDeleted = false)
    {
        $where = null;
        
        if (!$withDeleted)
        {
            $where = 'deleted = 0';
        }
        
        $select = $this->select()
            ->from(array('ll' => $this->_name), array('listener_id'))
            ->setIntegrityCheck(false)
            ->joinLeft(array('l' => 'lesson'), 'l.id = ll.lesson_id AND l.status = 2', '(COUNT(*) * 4) AS hours')
            ->where($where)
            ->group('ll.listener_id');

        return $this->getDefaultAdapter()->fetchAll($select);
    }
}
