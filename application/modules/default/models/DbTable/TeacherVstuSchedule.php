<?php

/**
 * Определяет класс для работы с таблицей - расписание (teacher_vstu_schedule)
 *
 * Данная таблица хранит вузовские занятия отдельных преподавателей. Позволяет получить расписание для кафедры (по всем преподавателям)
 *
 * Class Intensives_Model_DbTable_TeacherVstuSchedule
 */
class Intensives_Model_DbTable_TeacherVstuSchedule extends Zend_Db_Table_Abstract
{
    protected $_name = 'teacher_vstu_schedule';
    protected $_primary = 'id';
    protected $_sequence = true;

    /**
     * Получить занятия по условию
     * Занятия в расписании отсортированы по следующим полям: неделя, день, пара, преподаватель
     * Доступно для таблиц tvs (teacher_vstu_schedule), t (teacher), a(auditor)
     * @param $where string условие
     * @return Zend_Db_Table_Rowset_Abstract строки с занятиями
     */
    public function fetchVstuLessons($where)
    {
        $select = $this->select()
            ->from(array('tvs' => $this->_name))
            ->setIntegrityCheck(false)
            ->joinInner(array('t' => 'teacher'), 'tvs.teacher_id = t.id',
                array('teacher' => 'CONCAT(t.surname, \' \', SUBSTR(t.name, 1, 1), \'. \', SUBSTR(t.patronymic, 1, 1), \'.\')'))
            ->joinInner(array('a' => 'auditory'), 'tvs.auditory_id = a.id', array('auditory' => 'name'))
            ->where($where)
            ->where('t.deleted = 0')
            ->order(array('tvs.week ASC', 'tvs.day ASC', 'tvs.lesson_start ASC', 't.surname ASC'))
            ->distinct();
        return $this->fetchAll($select);
    }

    /**
     * Получить кафедральное расписание
     * Занятия в расписании отсортированы по следующим полям: неделя, день, пара, преподаватель
     * @param int $chair идентификатор кафедры
     * @return Zend_Db_Table_Rowset_Abstract строки занятий в расписании
     */
    public function getChairSchedule($chair)
    {
        $where = 't.chair_id = ' . $chair;
        return $this->fetchVstuLessons($where);
    }

    /**
     * Получение основного расписания преподавателя из БД
     * @param int $teacher идентификатор преподавателя
     * @return Zend_Db_Table_Rowset_Abstract строки занятий в расписании
     */
    public function getScheduleForTeacher($teacher)
    {
        $where = $this->getDefaultAdapter()->quoteInto('teacher_id = ?', $teacher);
        return $this->fetchAll($where);
    }

    /**
     * Удалить расписание данных преподавателей
     * @param array $teachers идентификаторы преподавателей
     */
    public function deleteTeachersSchedule(array $teachers)
    {
        $ids = implode(', ', $teachers);
        $this->delete('teacher_id IN (' . $ids . ')');
    }

    /**
     * Проверить наличия свободного времени в рассписании преподавателя (наличие окна между парами).
     * Если в это время уже стоит вузвоского занятия - возвращается ее идентификатор
     * @param int $teacher идентификатор преподавателя
     * @param array $times время, ключи - (week, day, lessonStart, lessonEnd)
     * @return bool|int в случаи наличия свободного времени true, иначе id занятия
     */
    public function checkAvailableTime($teacher, array $times) {
        $result = false;

        $tid = (int) $teacher;
        $isset = isset($times['week']) && isset($times['day']) && isset($times['lessonStart']) && isset($times['lessonEnd']);

        if ($tid && $isset) {
            $select = $this->select()
                ->from($this->_name, array('id'))
                ->where('teacher_id = ?', $teacher)
                ->where('week = ?', $times['week'])
                ->where('day = ?', $times['day'])
                ->where('lesson_start = ?', $times['lessonStart'])
                ->where('lesson_end = ?', $times['lessonEnd']);
            $row = $this->fetchRow($select);
            $result = is_null($row) ? true : $row->id ;
        }

        return $result;
    }
}
