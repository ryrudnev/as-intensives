<?php

class Intensives_Model_DbTable_Auditory extends Zend_Db_Table_Abstract
{

    protected $_name = 'auditory';

    /**
     * Получение списка аудиторий из базы.
     *
     * @param boolean $withDeleted извлекать также аудитории, которые помечены,
     * как 'удалённая'.
     * @param string $order порядок сортировки (SQL синтаксис).
     * @param int $count количество аудиторий в выборке.
     * @param int $offset смещение в выборке.
     *
     * @return Zend_Db_Table_Rowset_Abstract список аудиторий.
     */
    public function fetchAllAuditories($withDeleted = false, $order = 'name ASC', $count = null, $offset = null)
    {
        $where = $withDeleted ? '' : 'deleted = 0';

        return $this->fetchAll($where, $order, $count, $offset);
    }

    /**
     * Пометка аудитории, как 'удалённая'.
     *
     * @param int $auditoryId идентификатор аудитории.
     *
     * @return int если 0, то не помечена, иначе помечена.
     */
    public function deleteAuditory($auditoryId)
    {
        $where = $this->getDefaultAdapter()->quoteInto('id = ?', $auditoryId);

        return $this->update(array('deleted' => '1'), $where);
    }

    /**
     * Определить наличия аудитории с указанным наименованием. Если аудитория найдена, то возвращется ее id
     * @param string $name имя искомой аудитории.
     * @param bool $withDeleted производить поиск также среди 'удалённых' аудиторий.
     * @return false|int результат проверки или идентификатор найденной аудитории
     */
    public function isExists($name, $withDeleted = false)
    {
        $select = $this->select()
            ->from($this->_name, array('id'))
            ->where('name LIKE \'' . $name . '\'');

        if (!$withDeleted) {
            $select->where('deleted = 0');
        }

        $result = $this->fetchRow($select);
        return is_null($result) ? false : $result->id;
    }
}
