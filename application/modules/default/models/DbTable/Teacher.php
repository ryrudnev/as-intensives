<?php

class Intensives_Model_DbTable_Teacher extends Zend_Db_Table_Abstract
{
    protected $_name = 'teacher';

    /**
     * Получение списка преподавателей из базы.
     *
     * @param boolean $withDeleted извлекать также 'удалённых' преподавателей.
     * @param string $order порядок сортировки (SQL-синтаксис).
     * @param int $count количество преподавателей в выборке.
     * @param int $offset смещение в выборке.
     *
     * @return Zend_Db_Table_Rowset_Abstract список преподавателей.
     */
    public function fetchAllTeachers($withDeleted = false, $order = null, $count = null, $offset = null)
    {
        if (!$withDeleted) {
            $where = 'deleted = 0';
        }

        if (is_null($order)) {
            $order = array(
                'surname ASC',
                'name ASC',
                'patronymic ASC'
            );
        }

        return $this->fetchAll($where, $order, $count, $offset);
    }

    /**
     * Получение данных преподавателя с указанием кафедры, за которой закреплён
     * преподаватель.
     *
     * @param int $teacherId идентификатор преподавателя.
     *
     * @return Zend_Db_Table_Row_Abstract данные о преподавателе с указанием
     * кафедры, за которой закреплён преподаватель.
     */
    public function getTeacherDataWithChairName($teacherId)
    {
        $where = $this->getDefaultAdapter()->quoteInto('l.id = ?', $teacherId);

        $select = $this->select()
            ->from(array('l' => $this->_name))
            ->setIntegrityCheck(false)
            ->joinLeft(array('c' => 'chair'), 'c.id = l.chair_id', array('name AS chair_name', 'abbreviation AS chair_abbreviation'))
            ->where($where);

        return $this->fetchRow($select);
    }

    /**
     * Получение списка преподавателей закреплённых за указанной кафедрой.
     *
     * @param int $chairId идентификатор кафедры.
     * @param boolean $withDeleted извлекать также 'удалённых' преподавателей.
     * @param string $order порядок сортировки (SQL-синтаксис).
     * @param int $count количество преподавателей в выборке.
     * @param int $offset смещение в выборке.
     *
     * @return Zend_Db_Table_Rowset_Abstract список преподавателей.
     */
    public function fetchTeachersByChairId($chairId, $withDeleted = false, $order = null, $count = null, $offset = null)
    {
        $where = array();
        if (!$withDeleted) {
            $where[] = 'deleted = 0';
        }

        $where['chair_id = ?'] = $chairId;

        if (is_null($order)) {
            $order = array(
                'surname ASC',
                'name ASC',
                'patronymic ASC'
            );
        }

        return $this->fetchAll($where, $order, $count, $offset);
    }

    /**
     * Определение наличия в базе слушателя с указанным логином в базе.
     *
     * @param string $login логин преподавателя.
     * @param boolean $withDeleted учитывать 'удалённых' преподавателей.
     * @return boolean признак наличия слушателя с указанным логином в базе.
     */
    public function isExists($login, $withDeleted = false)
    {
        $where = array(
            'login = ?' => $login,
        );

        if (!$withDeleted) {
            $where[] = 'deleted = 0';
        }

        $teacherData = $this->fetchRow($where);
        if (is_null($teacherData)) {
            return false;
        } else {
            $teacherDataArray = $teacherData->toArray();
            return $teacherDataArray['id'];
        }
    }
}
