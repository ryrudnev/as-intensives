<?php

class Intensives_Model_DbTable_TeacherContract extends Zend_Db_Table_Abstract
{

    protected $_name = 'teacher_contract';
    protected $_primary = 'id';

    public function getTeacherContracts($teacherID)
    {
        $where = $this->getDefaultAdapter()->quoteInto('teacher_id = ?', $teacherID);
        return $this->fetchAll($where)->toArray();
    }
}
