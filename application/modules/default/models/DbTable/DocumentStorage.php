<?php

/**
 * Определяет класс для работы с таблицей - хранилище документов (document_storage)
 *
 * Является "оберткой" для взаимодействия с локальным файловым хранилищем на сервере. Операции над документами
 * производимые в данной таблицей БД автоматически, в соответствие с их логикой, производяться и в хранилище
 *
 * Документами являются файлы в формате odt или архивы, содержащие другие документы.
 * Документы делятся по категориям: 'Договор на выполнении учебной работы', 'Отчет о выполненных учебных работах',
 * 'Договор об оказании дополнительных образовательных услуг', 'Приказ о зачислении слушателей',
 * 'Отчет о движении денежных средств подразделения Интенсив', 'Журнал слушателей', 'Список слушателей' и 'Архив'
 * В случаи документа в формате odt в файловую систему сервера сохраняется контент шаблонного документа в виде xml файла.
 */
class Intensives_Model_DbTable_DocumentStorage extends Zend_Db_Table_Abstract
{
    protected $_primary = 'id';
    protected $_name = 'document_storage';
    protected $_sequence = true;

    /**
     * Локальное файловое хранилище
     * @var Intensives_Model_LocalFileStorage
     */
    private $_localFileStorage = null;

    /**
     * Инициализировать модель таблицы
     */
    public function init()
    {
        $config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $this->_localFileStorage = new Intensives_Model_LocalFileStorage($config->getOption('documentStorage')['path']);
    }

    /**
     * Получить локальное файловое хранилище, где физически храняться документы
     * @return Intensives_Model_LocalFileStorage файловое хранилище в ФС сервера
     */
    public function localFileStorage() {
        return $this->_localFileStorage;
    }

    /**
     * Получить полный путь к документу в хранилише
     * @param mixed $where условие
     * @return false|string путь к документу
     */
    public function getFullPathDocument($where) {
        $path = false;
        if ($row = $this->fetchRow($where)) {
            $path = $this->_localFileStorage->getFullPathFile($row->path);
        }

        return $path;
    }

    /**
     * Вставить документ и информацию о нем в таблицу. Вставлять архивы нельзя.
     *
     * Массив должен содержать ключи:
     * 'odt' - odt документ.
     * 'name' - имя документа.
     * 'date' - дата создания документа.
     * 'category' - категория документа.
     * Содержимое данного документа помещается в хранилище.
     * Если документ с таким содержимым уже существует, то вернется id строки таблицы с существующим документом
     *
     * @param array $data данные о добавляемом документе
     * @return false|integer id вставленного документа, или id существующего документа,
     * если вставки не произошло, иначе false
     */
    public function insert(array $data)
    {
        if (!is_array($data) || !isset($data['odt'])) {
            return false;
        }

        if (!$data['odt'] instanceof Odf) {
            return false;
        }

        $path = $this->_localFileStorage->storeFile($data['odt']->getContent(), 'xml');
        $name = isset($data['name']) ? $data['name'] : pathinfo($path, PATHINFO_FILENAME);
        $date = isset($data['date']) ? $data['date'] : (new Zend_Date())->get('YYYY-MM-dd');
        $category = isset($data['category']) ?
            (Intensives_Model_DocumentCategoryEnum::isValidEnumValue($data['category']) ? $data['category'] :
                Intensives_Model_DocumentCategoryEnum::Unknown) : Intensives_Model_DocumentCategoryEnum::Unknown;
        /**
         * Отлов возможности создания двух одинаковых записей с указанием на один и тот же файл.
         */
        try {
            $result = parent::insert(array(
                'path' => $path,
                'name' => $name,
                'date' => $date,
                'category' => $category
            ));
        } catch (Zend_Db_Exception $e) {
            $result = $this->fetchRow($this->select()->where('path = ?', $path))->id;
        }

        return $result;
    }

    /**
     * Обновить данные о документах, согласно заданному условию
     *
     * @param array $data новые данные о документе, аналогично insert
     * @param array|string $where условие
     * @return false|int количество обновленных документов
     */
    public function update(array $data, $where)
    {
        if (!is_array($data)) {
            return false;
        }

        if (isset($data['path'])) {
            if (!$this->_localFileStorage->fileIsExist($data['path'])) {
                return false;
            }

            $rows = $this->fetchAll($where)->toArray();
        }
        elseif (isset($data['odt'])) {
            if (!$data['odt'] instanceof Odf) {
                return false;
            }

            $data['path'] = $this->_localFileStorage->storeFile($data['odt']->getContent(), 'xml');
            unset($data['odt']);

            $rows = $this->fetchAll($where)->toArray();
        }

        /**
         * Отлов возможности создания двух одинаковых записей с указанием на один и тот же файл.
         */
        try {
            $result = parent::update($data, $where);

            /*
             * Удаление старых файлов, которые были до обновления, если это было необходимо
             */
            if (isset($data['path']) && isset($rows)) {
                foreach($rows as $row) {
                    $this->_localFileStorage->removeFile($row['path']);
                }
            }

        } catch (Zend_Db_Exception $e) {
            $result = false;
        }

        return $result;
    }

    /**
     * Удалить из таблицы и хранилища документов документы в соответствии с условием
     * @param mixed $where условие
     * @return int количество удаленных документов
     */
    public function delete($where)
    {
        $deletedRows = $this->fetchAll($where)->toArray();

        /**
         * Попытка удалить строку (в случаи если id данной строки где-то используется как внешний ключ будет Exception
         */
        $result = parent::delete($where);

        /**
         * В случаи успеха удаляем реальные файлы в ФС сервера
         */
        foreach ($deletedRows as $row) {
            $this->_localFileStorage->removeFile($row['path']);
        }

        return $result;
    }

    /**
     * Получить документы соглсано условию
     *
     * @param mixed $where условие
     * @return array документы
     */
    public function fetchDocuments($where)
    {
        $documentor = new Intensives_Model_Documentor();
        $documents = array();

        $resultSet = $this->fetchAll($where);
        while ($resultSet->valid()) {
            $row = $resultSet->current();
            if ($data = $this->_localFileStorage->fetchFile($row->path)) {
                $category = new Intensives_Model_DocumentCategoryEnum($row->category);
                if ($category->getValue() == Intensives_Model_DocumentCategoryEnum::Archive) {
                    $documents[] = $data;
                } elseif ($category->getValue() != Intensives_Model_DocumentCategoryEnum::Unknown) {
                    $odt = $documentor->generateOdt($category);
                    $odt->setContent($data);

                    $documents[] = $odt->getTmpfileData();
                }
            }

            $resultSet->next();
        }

        return $documents;
    }

    /**
     * Экспортировать документ как прикрепленный файл
     * @param string|int $id индентификатор документа
     */
    public function exportAsAttachedFile($id)
    {
        $resultSet = $this->find($id);
        if ($resultSet->count() == 0) {
            return;
        }

        $row = $resultSet->current();
        $category = new Intensives_Model_DocumentCategoryEnum($row->category);
        if ($category->getValue() == Intensives_Model_DocumentCategoryEnum::Unknown) {
            return;
        } elseif ($category->getValue() == Intensives_Model_DocumentCategoryEnum::Archive) {
            header('Content-type: application/zip');
            header('Content-Disposition: attachment; filename="' . $row->name . '.zip' . '"');
            readfile($this->_localFileStorage->getLocalDirectory() . $row->path);
        } else {
            if ($odtContent = $this->_localFileStorage->fetchFile($row->path)) {
                $documentor = new Intensives_Model_Documentor();
                $odt = $documentor->generateOdt($category);
                $odt->setContent($odtContent);
                $odt->exportAsAttachedFile($row->name . '.odt');
            }
        }
    }

    /**
     * Архивировать документы согласно указанному условию
     *
     * @param mixed $where условие
     * @param array $info информация о создаваемом архиве
     * @return false|mixed false если архивирование завершилось неуспешно, иначе первичный ключ полученного архива
     */
    public function archiveDocuments($where, $info = array())
    {
        if (!is_array($info)) {
            return false;
        }

        $resultSet = $this->fetchAll($where);
        if ($resultSet->count() == 0) {
            return false;
        }

        $documentor = new Intensives_Model_Documentor();

        $data = '';
        $documents = array();

        while ($resultSet->valid()) {
            $row = $resultSet->current();

            $category = new Intensives_Model_DocumentCategoryEnum($row->category);
            if ($category->getValue() != Intensives_Model_DocumentCategoryEnum::Unknown &&
                $category->getValue() != Intensives_Model_DocumentCategoryEnum::Archive
            ) {
                if ($odtContent = $this->_localFileStorage->fetchFile($row->path)) {
                    $odt = $documentor->generateOdt($category);
                    $odt->setContent($odtContent);

                    $documents[] = array(
                        'data' => $odt->getTmpfileData(),
                        'name' => $category . '/' . $row->name,
                        'extension' => 'odt'
                    );

                    $data .= $odtContent;
                }
            }

            $resultSet->next();
        }

        if (($path = $this->_localFileStorage->zip($documents, md5($data))) == false) {
            return false;
        }

        /**
         * Отлов возможности создания двух одинаковых записей с указанием на один и тот же файл.
         */
        try {
            $result = parent::insert(array(
                'path' => $path,
                'name' => isset($info['name']) ? $info['name'] : pathinfo($path, PATHINFO_FILENAME),
                'date' => isset($info['date']) ? $info['date'] : (new Zend_Date())->get('YYYY-MM-dd'),
                'category' => Intensives_Model_DocumentCategoryEnum::Archive
            ));
        } catch (Zend_Db_Exception $e) {
            $result = $this->fetchRow($this->select()->where('path = ?', $path))->id;
        }

        return $result;
    }
}