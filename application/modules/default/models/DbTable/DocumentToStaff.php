<?php

/**
 * Определяет интерфейс для работы с таблицей - документы персонала (document_to_staff).
 */
class Intensives_Model_DbTable_DocumentToStaff extends Zend_Db_Table_Abstract {
    protected $_name = 'document_to_staff';
    protected $_primary = 'id';
    protected $_sequence = true;

    /**
     * Получить корректное имя создаваемого документа для помещения его в хранилище документов
     * В случаи если документ с таким именем уже существует для персонала
     * @param string $name имя нового документа
     * @param null|integer $mid идентификатор методиста (null)
     * @param null|integer $tid идентификатор преподавателя (null)
     * @return string корректное имя документа
     */
    public function getCorrectDocumentName($name, $mid, $tid) {
        if (!is_null($mid)) {
            $where = 'dts.methodist_id = ' . $mid;
        }
        else if (!is_null($tid)) {
            $where = 'dts.teacher_id = ' . $tid;
        }

        if (!isset($where)) {
            return $name;
        }

        $select = $this->select()
            ->from(array('dts' => $this->_name), array())
            ->setIntegrityCheck(false)
            ->joinInner(array('ds' => 'document_storage'), 'dts.document_id = ds.id', array('name'))
            ->where($where)
            ->where('ds.name LIKE \'' . $name . '%\'');
        $count = $this->fetchAll($select)->count();

        return $count == 0 ? $name : $name . ' (' . $count . ')';
    }

    /**
     * Вставить данные в таблицу
     *
     * @param array $data данные
     * @return false|mixed id строки вставленной строки в таблицы или false
     */
    public function insert(array $data) {
        try {
            $result = parent::insert($data);
        }
        catch(Zend_Db_Exception $e) {
            $result = false;
        }

        return $result;
    }

    /**
     * Обновить данные в таблице согласно условию
     *
     * @param array $data данные
     * @param array|string $where условие
     * @return false|int количество обновленных записей
     */
    public function update(array $data, $where) {
        try {
            $result = parent::update($data, $where);
        }
        catch(Zend_Db_Exception $e) {
            $result = false;
        }

        return $result;
    }
}