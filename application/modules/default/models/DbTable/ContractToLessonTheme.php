<?php

class Intensives_Model_DbTable_ContractToLessonTheme extends Zend_Db_Table_Abstract
{
    protected $_name = 'contract_to_lesson_theme';
    protected $_primary = 'id';
    protected $_sequence = true;
}