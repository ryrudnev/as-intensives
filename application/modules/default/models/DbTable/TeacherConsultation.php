<?php

class Intensives_Model_DbTable_TeacherConsultation extends Zend_Db_Table_Abstract
{

    protected $_name = 'teacher_consultations';
    protected $_primary = 'id';

    public function saveConsultation($teacherID, $start, $end, $theme, $id = 0)
    {
        $data = array(
            'teacher_id' => $teacherID,
            'start' => $start,
            'end' => $end,
            'name' => $theme
        );
        if (!$id) {
            $this->insert($data);
        } else {
            $where = $this->getDefaultAdapter()->quoteInto('id = ?', $id);
            $this->update($data, $where);
        }
    }

    public function getTeacherConsultations($teacherID, $id = null)
    {
        $where = $this->getDefaultAdapter()->quoteInto('teacher_id = ?', $teacherID);
        if (!is_null($id)) {
            $where .= $this->getDefaultAdapter()->quoteInto(' AND id = ?', $id);
        }
        return $this->fetchAll($where)->toArray();
    }

    public function removeConsultation($id, $teacherID = null)
    {
        $where = $this->getDefaultAdapter()->quoteInto('id = ?', $id);
        if ($teacherID) {
            $where .= $this->getDefaultAdapter()->quoteInto(' AND teacher_id = ?', $teacherID);
        }
        $this->delete($where);
    }


}
