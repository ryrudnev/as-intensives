<?php

class Intensives_Model_DbTable_Chair extends Zend_Db_Table_Abstract
{

    protected $_name = 'chair';

    /**
     * Получение значения свойства указанной кафедры.
     *
     * @param int $chairId идентификатор кафедры.
     * @param string $propertyName наименование свойства.
     *
     * @return mixed значение указанного свойства.
     */
    public function getChairProperty($chairId, $propertyName)
    {
        $where = $this->getDefaultAdapter()->quoteInto('id = ?', $chairId);

        return $this->getDefaultAdapter()->fetchOne(
            $this->select()
                ->from($this->_name, $propertyName)
                ->where($where)
                ->limit(1)
        );
    }

    /**
     * Получение наименования указанной кафедры.
     *
     * @param int $chairId идентификатор кафедры.
     *
     * @return string наименование указанной кафедры.
     */
    public function getChairName($chairId)
    {
        return $this->getChairProperty($chairId, 'name');
    }

    /**
     * Получение аббревиатуры указанной кафедры.
     *
     * @param int $chairId идентификатор кафедры.
     *
     * @return string аббревиатура указанной кафедры.
     */
    public function getChairAbbreviation($chairId)
    {
        return $this->getChairProperty($chairId, 'abbreviation');
    }

}
