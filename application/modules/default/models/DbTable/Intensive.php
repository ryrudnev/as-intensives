<?php

class Intensives_Model_DbTable_Intensive extends Zend_Db_Table_Abstract
{

    protected $_name = 'intensive';

    /**
     * Получение списка курсов интенсивов.
     * 
     * @param int $chairId идентификатор кафедры, за которой закреплены курсы.
     * @param boolean $withDeleted извлекать также аудитории, которые помечены,
     * как 'удалённая'.
     * @param string $order порядок сортировки (SQL-синтаксис).
     * @param int $count количество курсов в выборке.
     * @param int $offset смещение в выборке.
     *
     * @return Zend_Db_Table_Rowset_Abstract список курсов интенсива.
     */
    function fetchAllIntensives($chairId = null, $withDeleted = false, $order = 'name', $count = null, $offset = null)
    {
        if (!$withDeleted)
        {
            $where = 'deleted = 0';
        }
        if (!is_null($chairId))
        {
            $where .= ' && ' . $this->getDefaultAdapter()->quoteInto('chair_id = ?', $chairId);
        }

        return $this->fetchAll($where, $order, $count, $offset);
    }
}
