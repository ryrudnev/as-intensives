<?php

class Intensives_Model_DbTable_Listener extends Zend_Db_Table_Abstract
{

    protected $_name = 'listener';

    /**
     * Получение списка слушателей для добавления на занятие. В списке
     * отсутствуют слушатели, которые уже записаны на занятие.
     *
     * @param int $lessonId идентификатор занятия.
     * 
     * @return Zend_Db_Table_Rowset_Abstract список слушателей с темами,
     * которые желал прослушать слушатель.
     */
    function getListenersForAddingToLesson($lessonId)
    {
        // Получение списка id слушателей, которые уже записаны на данной занятие.
        // Таблица lesson_to_listener
        $select = $this->select()
            ->from(array('ltl' => 'lesson_to_listener'), array('id' => 'listener_id'))
            ->setIntegrityCheck(false)
            ->where('ltl.lesson_id = ?', $lessonId);
        $rowSet = $this->fetchAll($select);

        // Их необходимо проигнорировать
        $arrIgnoredIds = array();
        while($rowSet->valid()) {
            $row = $rowSet->current();
            $arrIgnoredIds[] = $row->id;
            $rowSet->next();
        }

        // Выбор слушателей среди тех кто имеет контракты и не записаны еще на данное занятие
        $select = $this->select()
            ->from(array('l' => $this->_name), array(
                'id' => 'id',
                'surname' => 'surname',
                'name' => 'name',
                'patronymic' => 'patronymic',
                'group' => 'group'))
            ->setIntegrityCheck(false)
            ->joinInner(array('c' => 'contract'), 'l.id = c.listener_id', array())
            ->joinLeft(array('ctlt' => 'contract_to_lesson_theme'), 'c.id = ctlt.contract_id', array())
            ->joinLeft(array('lt' => 'lesson_theme'), 'lt.id = ctlt.lesson_theme_id', array())
            ->columns('COALESCE(GROUP_CONCAT(DISTINCT lt.name SEPARATOR \'<br/>\'), \'\') AS themes')
            ->distinct()
            ->where('l.deleted = 0')
            ->group('l.id')
            ->order('l.surname ASC');

        // Если есть игнорируемые слушатели, то исключить их
        if (count($arrIgnoredIds) > 0) {
            $select->where('l.id NOT IN(?)', $arrIgnoredIds);
        }

        $rowSet = $this->fetchAll($select);
        return $rowSet->toArray();
    }

    /**
     * Получение списка слушателей из базы.
     * @param boolean $withDeleted извлекать также слушателей, которые были
     * 'удалены'.
     * @param array $where массив с дополнительными условиями отбора.
     * @param string $order порядок сортироки (SQL-синтаксис).
     * @param int $count количество слушателей в выборке.
     * @param int $offset смещение в выборке.
     * @return Zend_Db_Table_Rowset_Abstract список слушателей.
     */
    function fetchAllListeners($withDeleted = false, $where = array(), $order = null, $count = null, $offset = null)
    {
        if (is_null($order))
        {
            $order = array('surname', 'name', 'patronymic');
        }
        if (!is_array($where))
        {
            $where = array($where);
        }
        if (!$withDeleted)
        {
            $where['deleted = ?'] = 0;
        }

        return $this->fetchAll($where, $order, $count, $offset);
    }

    /**
     * Получение списка слушателей с указанием оплаченных ими часов.
     *
     * @param string $currentDate текущая дата, относительно который производить
     * расчёт оплаченных часов.
     * @param boolean $withDeleted извлекать также 'удалённых' слушателей.
     * @param string $order порядок сортировки (SQL-синтаксис).
     * @param int $count количество слушателей в выборке.
     * @param int $offset смещение в выборке.
     *
     * @return Zend_Db_Table_Rowset_Abstract список слушателей с указанием
     * оплаченных ими часов.
     */
    function fetchAllListenersWithPaidHours($currentDate, $withDeleted = false, $order = null, $count = null, $offset = null)
    {
        if (is_null($order))
        {
            $order = array('surname', 'name', 'patronymic');
        }
        $where = "";
        if (!$withDeleted)
        {
            $where = 'l.deleted = 0';
        }

        $joinWhere = $this->getDefaultAdapter()->quoteInto('l.id = c.listener_id AND c.paid = 1 AND c.end >= ?', $currentDate);

        $listeners = $this->fetchAll(
            $this->select()
                ->from(array('l' => $this->_name))
                ->setIntegrityCheck(false)
                ->joinLeft(array('c' => 'contract'), $joinWhere, array('SUM(c.hours) AS hours'))
                ->where($where)
                ->order($order)
                ->group('l.id')
            );

//        $scheduleModel = new Intensives_Model_DbTable_LessonToListener();
//        $readedHours = $scheduleModel->fetchListenersReadedHours();
//        foreach ($listeners as $listener)
//        {
//            
//        }
        
        return $listeners;
    }

    /**
     * Определение наличия в базе слушателя с указанными серией и номером
     * паспорта.
     * 
     * @param string $passportSeries серия паспорта.
     * @param string $passportNumber номер паспорта.
     * @param boolean $withDeleted извлекать также 'удалённых' слушателей.
     *
     * @return boolean признак наличия слушателя с указанными серие и номером
     * паспорта в базе.
     */
    function isExists($passportSeries, $passportNumber, $withDeleted = false)
    {
        $where = array(
            'passport_series = ?' => $passportSeries,
            'passport_number = ?' => $passportNumber,
        );

        if (!$withDeleted)
        {
            $where[] = 'deleted = 0';
        }

        $listenerData = $this->fetchRow($where);
        if(is_null($listenerData))
        {
            return false;
        }
        else
        {
            $listenerDataArray = $listenerData->toArray();
            return $listenerDataArray['id'];
        }
    }
}
