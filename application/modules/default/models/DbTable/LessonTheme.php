<?php
/**
 * Определяет класс для работы с таблицей - тема занятия (lesson_theme)
 */
class Intensives_Model_DbTable_LessonTheme extends Zend_Db_Table_Abstract
{
    protected $_primary = 'id';
    protected $_name = 'lesson_theme';
    protected $_sequence = true;
}