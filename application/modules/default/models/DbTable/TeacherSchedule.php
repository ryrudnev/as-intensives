<?php

class Intensives_Model_DbTable_TeacherSchedule extends Zend_Db_Table_Abstract
{

    protected $_name = 'teacher_schedule';

    /**
     * Получение расписания свободных часов преподавателя.
     *
     * @param int $teacherId идентификатор преподавателя.
     * 
     * @return Zend_Db_Table_Rowset_Abstract список периодов, в которые
     * преподаватель свободен для занятий.
     */
    public function getTeacherSchedule($teacherId)
    {
        return $this->fetchAll(array('teacher_id = ?' => $teacherId));
    }

}

