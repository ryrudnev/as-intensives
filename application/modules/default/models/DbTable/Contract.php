<?php

class Intensives_Model_DbTable_Contract extends Zend_Db_Table_Abstract
{
    protected $_name = 'contract';
    protected $_primary = 'id';

    /**
     * Получение списка договоров из базы.
     *
     * @param int $chairId идентификатор кафедры, за которой закреплены
     * договоры.
     * @param string $order порядок сортировки (SQL-синтаксис).
     * @param int $count количество договоров в выборке.
     * @param int $offset смещение в выборке.
     *
     * @return Zend_Db_Table_Rowset_Abstract список договоров.
     */
    function fetchAllContracts($chairId = null, $order = null, $count = null, $offset = null)
    {
        $where = '';

        if (!is_null($chairId)) {
            $where = $this->getDefaultAdapter()->quoteInto('chair_id = ?', $chairId);
        }

        return $this->fetchAll(
            $this->select()
                ->from(array('c' => $this->_name))
                ->setIntegrityCheck(false)
                ->join(array('l' => 'listener'), 'l.id = c.listener_id',
                    array('CONCAT(surname,\' \',name,\' \',patronymic) AS listener'))
                ->where($where)
                ->order(array('date DESC', 'number DESC'))
        );
    }

    /**
     * Удаление договора из базы.
     *
     * @param int $contractId идентификатор договора.
     *
     * @return int если 0, то не помечена, иначе помечена.
     */
    function deleteContract($contractId)
    {
        return $this->delete(array('id = ?' => $contractId));
    }

    /**
     * Пометка договора, как 'оплаченный'.
     *
     * @param int $contractId идентификатор помечаемого договора.
     * @param string $receiptNumber номер квитаници
     * @param string $receiptTotal сумма в квитанции
     * @param string $receiptDate дата оплаты квитанции
     * @return int если 0, то не помечен, иначе помечен.
     */
    function pay($contractId, $receiptNumber, $receiptTotal, $receiptDate)
    {
        return $this->update(
            array(
                'paid' => 1,
                'receipt_number' => $receiptNumber,
                'receipt_total' => $receiptTotal,
                'receipt_date' => $receiptDate
            ),
            array('id = ?' => $contractId)
        );
    }

    /**
     * Пометка договора, как 'неоплаченный'.
     *
     * @param int $contractId идентификатор помечаемого договора.
     *
     * @return int если 0, то не помечен, иначе помечен.
     */
    function unpay($contractId)
    {
        return $this->update(
            array(
                'paid' => 0,
                'receipt_number' => NULL,
                'receipt_total' => NULL,
                'receipt_date' => NULL,
            ),
            array('id = ?' => $contractId));
    }

    /**
     * Получение списка договоров оформленных на слушателя.
     *
     * @param int $listenerId идентификатор слушателя.
     * @param string $sort порядок сортировки (SQL-синтаксис).
     *
     * @return Zend_Db_Table_Rowset_Abstract список договоров, оформленных на
     * указанного слушателя.
     */
    function getContractsByListenerId($listenerId, $sort = 'date DESC')
    {
        $where = array(
            'listener_id = ?' => $listenerId,
        );

        return $this->fetchAll($where, $sort);
    }

    /**
     * Получить детальное описание контракта
     *
     * @param int $contractId идентификатор контракта
     * @return null|array
     */
    //TODO: оптимизировать метод и его изпользование
    function getContractDetails($contractId)
    {
        $select = $this->select()
            ->from(array('c' => $this->_name), array(
                'id' => 'id',
                'number' => 'number',
                'date' => 'date',
                'hours' => 'hours',
                'start' => 'start',
                'end' => 'end',
                'total' => 'total',
                'paid' => 'paid',
                'receiptNumber' => 'receipt_number',
                'receiptTotal' => 'receipt_total',
                'receiptDate' => 'receipt_date',
                'documentId' => 'document_id'
            ))
            ->setIntegrityCheck(false)
            ->join(array('ch' => 'chair'), 'c.chair_id = ch.id', array(
                'chairName' => 'name',
                'chairAbbreviation' => 'abbreviation',
            ))
            ->join(array('l' => 'listener'), 'c.listener_id = l.id', array(
                'listenerSurname' => 'surname',
                'listenerName' => 'name',
                'listenerPatronymic' => 'patronymic',
                'listenerGroup' => 'group',
                'listenerPhone' => 'phone',
                'listenerAddress' => 'address',
                'listenerPassportSeries' => 'passport_series',
                'listenerPassportNumber' => 'passport_number',
                'listenerPassportGivenout' => 'passport_givenout',
                'listenerPassportGivenoutWhen' => 'DATE_FORMAT(passport_givenout_when, \'%d.%m.%Y\')',
                'listenerEmail' => 'email',
            ))
            ->where('c.id = ?', $contractId)
            ->limit(1);
        $row = $this->fetchRow($select);
        if (is_null($row)) {
            return null;
        }

        $details = $row->toArray();

        $select = $this->select()
            ->from(array('c' => $this->_name), array())
            ->setIntegrityCheck(false)
            ->join(array('clt' => 'contract_to_lesson_theme'), 'c.id = clt.contract_id', array())
            ->join(array('lt' => 'lesson_theme'), 'lt.id = clt.lesson_theme_id', array('name'))
            ->where('c.id = ?', $contractId);
        $rowSet = $this->fetchAll($select);

        $arr = array();
        while($rowSet->valid()) {
            $arr[] = $rowSet->current()->name;
            $rowSet->next();
        }

        $details['themes'] = implode('; ', $arr);

        return $details;
    }

    /**
     * Вставить новый контракт в таблицу.
     *
     * @param  array $data данные.
     * @return mixed первичный ключ вставленного контракта.
     */
    function insert(array $data)
    {
        $data['number'] = (integer)$this->fetchRow($this->select()
                ->from(array('c' => $this->_name), array('number' => 'MAX(number)'))
                ->where('chair_id = ?', $data['chair_id']))->number + 1;

        return parent::insert($data);
    }
}
