<?php

class Intensives_Model_Funcs
{
    /**
     * Проверка прав доступа при асинхронных запросах.
     *
     * @param Zend_Controller_Action $controllerAction объект контроллера, из
     * которого производится вызов функции.
     *
     * @return null|string если пользователь не имеет права на доступ, то
     * выдаётся JSON-объект со свойством 'success' равным 0, свойством
     * 'error_code' равным 2 (в доступе отказано) и свойством 'error_message'
     * содержащим сообщение о том, что в доступе отказано. Дальнейшее выполнение
     * скрипта прекращается.
     * Если пользователь имеет права на доступ, то выполнение скрипта
     * продолжается.
     */
    static function asyncCheckAccess($controllerAction)
    {
        $answer = array('success' => 0);

        $storageData = Zend_Auth::getInstance()->getStorage()->read();

        $acl = Zend_Registry::get('acl');
        if (!$acl->isAllowed(
            $storageData->type,
            $controllerAction->getRequest()->getControllerName(),
            $controllerAction->getRequest()->getActionName())
        )
        {
            $answer['error_code'] = 2;
            $answer['error_message'] = 'В доступе отказано';
            die(Zend_Json::encode($answer));
        }
    }
}