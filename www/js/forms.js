Ext.apply(Ext.form.VTypes, {
    daterange: function (val, field) {
        var date = field.parseDate(val);

        if (!date) {
            return false;
        }
        if (field.startDateField) {
            var start = Ext.getCmp(field.startDateField);
            if (!start.maxValue || (date.getTime() != start.maxValue.getTime())) {
                start.setMaxValue(date);
                start.validate();
            }
        }
        else if (field.endDateField) {
            var end = Ext.getCmp(field.endDateField);
            if (!end.minValue || (date.getTime() != end.minValue.getTime())) {
                end.setMinValue(date);
                end.validate();
            }
        }

        return true;
    }
});

(function () {
    var originalFunction = Ext.grid.PivotAxis.prototype.getTuples;
    Ext.grid.PivotAxis.prototype.getTuples = function () {
        var result = originalFunction.call(this);
        this.lastTuples = result;
        return result;
    };
})();

(function () {

    var ns = Ext.ns('Ext.ux.plugins');
    /**
     * @class Ext.ux.plugins.DefaultButton
     * @extends Object
     *
     * Plugin for Button that will click() the button if the user presses ENTER while
     * a component in the button's form has focus.
     *
     * @author Stephen Friedrich
     * @date 21-JAN-2010
     * @version 0.2
     *
     */
    Ext.ux.plugins.DefaultButton = Ext.extend(Object, {
        init: function (button) {
            button.on('afterRender', setupKeyListener, button);
        }
    });

    function setupKeyListener() {
        var formPanel = this.findParentByType('form');
        //noinspection ObjectAllocationIgnored
        new Ext.KeyMap(formPanel.el, {
            key: Ext.EventObject.ENTER,
            shift: false,
            alt: false,
            fn: function (keyCode, e) {
                if (this.hidden || e.target.type === 'textarea' && !e.ctrlKey) {
                    return true;
                }

                this.el.select('button').item(0).dom.click();
                return false;
            },
            scope: this
        });
    }

    Ext.ComponentMgr.registerPlugin('defaultButton', ns.DefaultButton);

})();

// Настройка календаря
(function () {
    // Замена формата времени отображения и текста меню
    // Переопределение отображения меню перемещения
    Ext.calendar.CalendarView.override({
        ddCreateEventText: 'Создать занятие на {0}',
        ddMoveEventText: 'Переместить занятие на {0}'
    });
    // Переопределение отображения текущего дня
    Ext.calendar.DayView.override({
        todayText: 'Сегодня'
    });
    // Переопределение меню перемещения события
    Ext.calendar.DayViewDropZone.override({
        onNodeOver: function (n, dd, e, data) {
            var dt,
                box,
                endDt,
                text = this.createText,
                curr,
                start,
                end,
                evtEl,
                dayCol;
            if (data.type == 'caldrag') {
                if (!this.dragStartMarker) {
                    this.dragStartMarker = n.el.parent().createChild({
                        style: 'position:absolute;'
                    });
                    this.dragStartMarker.setBox(n.timeBox);
                    this.dragCreateDt = n.date;
                }
                box = this.dragStartMarker.getBox();
                box.height = Math.ceil(Math.abs(e.xy[1] - box.y) / n.timeBox.height) * n.timeBox.height;

                if (e.xy[1] < box.y) {
                    box.height += n.timeBox.height;
                    box.y = box.y - box.height + n.timeBox.height;
                    endDt = this.dragCreateDt.add(Date.MINUTE, 30);
                }
                else {
                    n.date = n.date.add(Date.MINUTE, 30);
                }
                this.shim(this.dragCreateDt, box);

                curr = Ext.calendar.Date.copyTime(n.date, this.dragCreateDt);
                this.dragStartDate = Ext.calendar.Date.min(this.dragCreateDt, curr);
                this.dragEndDate = endDt || Ext.calendar.Date.max(this.dragCreateDt, curr);

                dt = this.dragStartDate.format('H:i-') + this.dragEndDate.format('H:i');
            }
            else {
                evtEl = Ext.get(data.ddel);
                dayCol = evtEl.parent().parent();
                box = evtEl.getBox();

                box.width = dayCol.getWidth();

                if (data.type == 'eventdrag') {
                    if (this.dragOffset === undefined) {
                        this.dragOffset = n.timeBox.y - box.y;
                        box.y = n.timeBox.y - this.dragOffset;
                    }
                    else {
                        box.y = n.timeBox.y;
                    }
                    dt = n.date.format('j/n H:i');
                    box.x = n.el.getLeft();

                    this.shim(n.date, box);
                    text = this.moveText;
                }
                if (data.type == 'eventresize') {
                    if (!this.resizeDt) {
                        this.resizeDt = n.date;
                    }
                    box.x = dayCol.getLeft();
                    box.height = Math.ceil(Math.abs(e.xy[1] - box.y) / n.timeBox.height) * n.timeBox.height;
                    if (e.xy[1] < box.y) {
                        box.y -= box.height;
                    }
                    else {
                        n.date = n.date.add(Date.MINUTE, 30);
                    }
                    this.shim(this.resizeDt, box);

                    curr = Ext.calendar.Date.copyTime(n.date, this.resizeDt);
                    start = Ext.calendar.Date.min(data.eventStart, curr);
                    end = Ext.calendar.Date.max(data.eventStart, curr);

                    data.resizeDates = {
                        StartDate: start,
                        EndDate: end
                    };
                    dt = start.format('H:i-') + end.format('H:i');
                    text = this.resizeText;
                }
            }

            data.proxy.updateMsg(String.format(text, dt));
            return this.dropAllowed;
        }
    });
    // Переопределение отображения месяца
    Ext.calendar.MonthView.override({
        initClock: function () {
            if (Ext.fly(this.id + '-clock') !== null) {
                this.prevClockDay = new Date().getDay();
                if (this.clockTask) {
                    Ext.TaskMgr.stop(this.clockTask);
                }
                this.clockTask = Ext.TaskMgr.start({
                    run: function () {
                        var el = Ext.fly(this.id + '-clock'),
                            t = new Date();

                        if (t.getDay() == this.prevClockDay) {
                            if (el) {
                                el.update(t.format('j, H:i'));
                            }
                        }
                        else {
                            this.prevClockDay = t.getDay();
                            this.moveTo(t);
                        }
                    },
                    scope: this,
                    interval: 1000
                });
            }
        },
        getTemplateEventData: function (evt) {
            var M = Ext.calendar.EventMappings,
                selector = this.getEventSelectorCls(evt[M.EventId.name]),
                title = evt[M.Title.name];

            return Ext.applyIf({
                    _selectorCls: selector,
                    _colorCls: 'ext-color-' + (evt[M.CalendarId.name] ?
                        evt[M.CalendarId.name] : 'default') + (evt._renderAsAllDay ? '-ad' : ''),
                    _elId: selector + '-' + evt._weekIndex,
                    _isRecurring: evt.Recurrence && evt.Recurrence != '',
                    _isReminder: evt[M.Reminder.name] && evt[M.Reminder.name] != '',
                    Title: (evt[M.IsAllDay.name] ? '' : evt[M.StartDate.name].format('H:i ')) + (!title || title.length == 0 ? '(No title)' : title)
                },
                evt);
        }
    });
    // Переопределение отображения дня
    Ext.calendar.DayBodyView.override({
        getTemplateEventData: function (evt) {
            var selector = this.getEventSelectorCls(evt[Ext.calendar.EventMappings.EventId.name]),
                data = {},
                M = Ext.calendar.EventMappings;

            this.getTemplateEventBox(evt);

            data._selectorCls = selector;
            data._colorCls = 'ext-color-' + evt[M.CalendarId.name] + (evt._renderAsAllDay ? '-ad' : '');
            data._elId = selector + (evt._weekIndex ? '-' + evt._weekIndex : '');
            data._isRecurring = evt.Recurrence && evt.Recurrence != '';
            data._isReminder = evt[M.Reminder.name] && evt[M.Reminder.name] != '';
            var title = evt[M.Title.name];
            data.Title = (evt[M.IsAllDay.name] ? '' : evt[M.StartDate.name].format('H:i ')) + (!title || title.length == 0 ? '(No title)' : title);

            return Ext.applyIf(data, evt);
        }
    });
    // Переопределение отображения верхний шапки
    Ext.calendar.BoxLayoutTemplate.override({
        getTodayText: function () {
            var dt = new Date().format('l, F j, Y'),
                todayText = this.showTodayText !== false ? this.todayText : '',
                timeText = this.showTime !== false ? ' <span id="' + this.id + '-clock" class="ext-cal-dtitle-time">' +
                    new Date().format('H:i') + '</span>' : '',
                separator = todayText.length > 0 || timeText.length > 0 ? ' &mdash; ' : '';

            if (this.dayCount == 1) {
                return dt + separator + todayText + timeText;
            }
            fmt = this.weekCount == 1 ? 'D j' : 'j';
            return todayText.length > 0 ? todayText + timeText : Ext.Date.format(new Date(), fmt) + timeText;
        }
    });
    // Переопределение реднеринга отображения времени в календаре
    Ext.calendar.DayBodyTemplate.override({
        applyTemplate: function (o) {
            this.today = new Date().clearTime();
            this.dayCount = this.dayCount || 1;

            var i = 0, days = [],
                dt = o.viewStart.clone(),
                times;

            for (; i < this.dayCount; i++) {
                days[i] = dt.add(Date.DAY, i);
            }

            times = [];
            dt = new Date().clearTime();
            for (i = 0; i < 24; i++) {
                times.push(dt.format('H:i'));
                dt = dt.add(Date.HOUR, 1);
            }

            return Ext.calendar.DayBodyTemplate.superclass.applyTemplate.call(this, {
                days: days,
                dayCount: days.length,
                times: times
            });
        }
    });
})();

// Добавление сеттера для выбора значения комбобокса
(function () {
    Ext.override(Ext.form.ComboBox, {
        setSelectedValue: function (value) {
            var store = this.store;
            var valueField = this.valueField;
            var displayField = this.displayField;

            var index = -1;
            for (var i = 0; i < store.getCount(); i++) {
                var rec = store.getAt(i);
                if (value.toString() == rec.get(valueField)) {
                    index = i;

                    this.setValue(value);
                    this.setRawValue(rec.get(displayField));
                    this.selectedIndex = index;
                }
            }

            return index;
        }
    });
})();

function makeAuthForm(loginType, onSuccess) {
    var authForm = new Ext.FormPanel({
        url: '/json/login/',
        padding: 5,
        items: [
            {
                xtype: 'textfield',
                id: 'login',
                fieldLabel: 'Логин',
                allowBlank: false,
                listeners: {
                    afterrender: function () {
                        this.focus(true, 500);
                    }
                }
            },
            {
                xtype: 'textfield',
                id: 'password',
                fieldLabel: 'Пароль',
                inputType: 'password',
                allowBlank: false
            },
            {
                xtype: 'hidden',
                id: 'type',
                value: loginType
            }
        ],
        buttons: [
            {
                text: 'Войти',
                id: 'submit',
                plugins: 'defaultButton',
                handler: function () {
                    var form = authForm.getForm();
                    if (form.isValid()) {
                        form.submit({
                            waitMsg: 'Производится попытка входа в систему',
                            success: function (form, action) {
                                onSuccess(form, action);
                            },
                            failure: function (form, action) {
                                Ext.Msg.show({
                                    title: 'Ошибка при входе в систему',
                                    msg: action.result.error_message,
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.WARNING
                                });
                            }
                        });
                    } else {
                        Ext.Msg.show({
                            title: 'Ошибка при валидации',
                            msg: 'В форму внесены некорректные данные',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                }
            }
        ]
    });
    var authFormWindow = new Ext.Window({
        title: 'Вход в систему',
        closable: false,
        modal: true,
        width: 262,
        items: [
            authForm
        ]
    });

    return authFormWindow;
}

function makeListenersGridWindow(methodistId) {
    var listenersStore;
    listenersStore = new Ext.data.Store({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'surname',
                'name',
                'patronymic',
                'group',
                'phone',
                'hours'
            ],
            root: 'listeners'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getlistenersforgrid/'
        })
    });

    var listenersGrid = new Ext.grid.GridPanel({
        store: listenersStore,
        anchor: '100%, 100%',
        height: 400,
        loadMask: true,
        columns: [
            {header: 'Фамилия', dataIndex: 'surname', sortable: true},
            {header: 'Имя', dataIndex: 'name', sortable: true},
            {header: 'Отчество', dataIndex: 'patronymic', sortable: true},
            {header: 'Группа', dataIndex: 'group', sortable: true},
            {header: 'Телефон', dataIndex: 'phone', sortable: true},
            {header: 'Оплаченные часы', dataIndex: 'hours', sortable: true},
            {
                xtype: 'actioncolumn',
                width: 40,
                header: 'Операции',
                items: [
                    {
                        icon: '/images/icons/edit.gif',
                        tooltip: 'Редактировать',
                        handler: function (gridObj, rowIndex, colIndex) {
                            var rec = gridObj.getStore().getAt(rowIndex);
                            var listenerAttributes = makeListenerAttributesWindow(rec.data.id,
                                'Редактирование слушателя', listenersStore);
                            listenerAttributes.show();
                        }
                    },
                    {
                        icon: '/images/icons/delete.gif',
                        tooltip: 'Удалить',
                        handler: function (gridObj, rowIndex, colIndex) {
                            var rec = gridObj.getStore().getAt(rowIndex);
                            Ext.Msg.confirm('Удаление слушателя', 'Удалить слушателя ' + rec.data.surname + ' ' +
                                rec.data.name + ' ' + rec.data.patronymic + '?', function (btn) {
                                if (btn == 'yes') {
                                    Ext.Ajax.request({
                                        url: '/json/deletelistener/',
                                        method: 'POST',
                                        params: {lid: rec.data.id},
                                        success: function (response, opts) {
                                            listenersStore.load();
                                        },
                                        failure: function (response, opts) {
                                            Ext.Msg.show({
                                                title: 'Ошибка при удалении',
                                                msg: 'Не удалось удалить слушателя',
                                                buttons: Ext.MessageBox.OK,
                                                icon: Ext.MessageBox.ERROR
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    }
                ]
            }
        ]
    });
    listenersGrid.on('rowdblclick', function (gridObj, rowIndex, e) {
        var rec = gridObj.getStore().getAt(rowIndex);
        var listenerDetails = makeListenerDetailsWindow(rec.data.id);
        listenerDetails.show();
    });

    return new Ext.Window({
        title: 'Список слушателей',
        modal: true,
        layout: 'anchor',
        width: 800,
        items: [
            listenersGrid
        ],
        buttons: [
            {
                text: 'Добавить нового слушателя',
                handler: function () {
                    var listenerAttributes = makeListenerAttributesWindow(0, 'Добавление нового слушателя',
                        listenersStore);
                    listenerAttributes.show();
                }
            },
            {
                text: 'Печать',
                handler: function () {
                    var selectedRows = listenersGrid.getSelectionModel().getSelections();
                    var ids = [];
                    Ext.each(selectedRows, function (item, index, allItems) {
                        ids.push(item.data.id);
                    });
                    if (ids.length > 0) {
                        location.href = '/print/printlistenerslist/?mid=' + methodistId + '&ids=' + ids.join();
                    } else {
                        Ext.Msg.show({
                            title: 'Печать слушателей',
                            msg: 'Слушатели не выбраны. Для печати необходимо сперва выбрать нужных вам слушателей ' +
                                '(при помощи мыши и клавиши "Ctrl" или "Shift"), и потом нажать кнопку "Печать"',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                }
            }
        ]
    });
}

function makeListenerAttributesWindow(listenerId, windowTitle, gridStore) {
    var listenerIdField = new Ext.form.Hidden({
        id: 'listener-id',
        name: 'id',
        value: listenerId
    });
    var surnameField = new Ext.form.TextField({
        id: 'surname',
        fieldLabel: 'Фамилия',
        anchor: '100%',
        allowBlank: false
    });
    var nameField = new Ext.form.TextField({
        id: 'name',
        fieldLabel: 'Имя',
        anchor: '100%',
        allowBlank: false
    });
    var patronymicField = new Ext.form.TextField({
        id: 'patronymic',
        fieldLabel: 'Отчество',
        anchor: '100%',
        allowBlank: false
    });
    var groupField = new Ext.form.TextField({
        id: 'group',
        fieldLabel: 'Группа',
        anchor: '100%'
    });
    var phoneField = new Ext.form.TextField({
        id: 'phone',
        fieldLabel: 'Контактный телефон',
        anchor: '100%'
    });
    var addressField = new Ext.form.TextField({
        id: 'address',
        fieldLabel: 'Адрес',
        anchor: '100%'
    });
    var passportSeriesField = new Ext.form.TextField({
        id: 'passport_series',
        fieldLabel: 'Серия паспорта',
//        anchor: '100%',
        width: '40px',
        allowBlank: false
    });
    var passportNumberField = new Ext.form.TextField({
        id: 'passport_number',
        fieldLabel: 'Номер паспорта',
//        anchor: '100%',
        width: '50px',
        allowBlank: false
    });
    var passportGivenout = new Ext.form.TextField({
        id: 'passport_givenout',
        fieldLabel: 'Кем выдан паспорт',
        anchor: '100%',
        allowBlank: false
    });
    var passportGivenoutWhen = new Ext.form.DateField({
        id: 'passport_givenout_when',
        fieldLabel: 'Когда выдан паспорт',
        format: 'Y-m-d',
        value: new Date(),
        anchor: '100%',
        allowBlank: false
    });
    var emailField = new Ext.form.TextField({
        id: 'email',
        fieldLabel: 'E-mail',
        anchor: '100%'
    });
    var additionalInfo = new Ext.form.TextArea({
        id: 'additional_info',
        fieldLabel: 'Дополнительно',
        anchor: '100%',
        height: 150
    });

    var attributesForm = new Ext.form.FormPanel({
        id: 'listener-attributes',
        padding: 5,
        url: '/json/savelistenerdata/',
        items: [
            listenerIdField,
            surnameField,
            nameField,
            patronymicField,
            groupField,
            phoneField,
            addressField,
            passportSeriesField,
            passportNumberField,
            passportGivenout,
            passportGivenoutWhen,
            emailField,
            additionalInfo
        ],
        buttons: [
            {
                text: 'Сохранить',
                plugins: 'defaultButton',
                handler: function () {
                    var form = attributesForm.getForm();
                    if (form.isValid()) {
                        form.submit({
                            waitMsg: 'Сохранение данных о слушателе',
                            success: function (form, action) {
                                if (gridStore != null) {
                                    gridStore.load();
                                }
                                listenerAttributes.close();
                            },
                            failure: function (form, action) {
                                Ext.Msg.show({
                                    title: 'Ошибка при сохранении',
                                    msg: action.result.error_message,
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.WARNING
                                });
                            }
                        })
                    } else {
                        Ext.Msg.show({
                            title: 'Ошибка при валидации',
                            msg: 'В форму внесены некорректные данные',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                }
            },
            {
                text: 'Отмена',
                handler: function () {
                    listenerAttributes.close();
                }
            }
        ]
    });

    var attributesStore = new Ext.data.Store({
        reader: new Ext.data.JsonReader({
            fields: [
                'surname',
                'name',
                'patronymic',
                'group',
                'phone',
                'address',
                'passport_series',
                'passport_number',
                'passport_givenout',
                'passport_givenout_when',
                'email',
                'additional_info'
            ],
            root: 'listenerdata'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getlistenerdata/?lid=' + listenerId
        }),
        listeners: {
            'load': function () {
                var rec = this.getAt(0);
                surnameField.setValue(rec.data.surname);
                nameField.setValue(rec.data.name);
                patronymicField.setValue(rec.data.patronymic);
                groupField.setValue(rec.data.group);
                phoneField.setValue(rec.data.phone);
                addressField.setValue(rec.data.address);
                passportSeriesField.setValue(rec.data.passport_series);
                passportNumberField.setValue(rec.data.passport_number);
                passportGivenout.setValue(rec.data.passport_givenout);
                passportGivenoutWhen.setValue(rec.data.passport_givenout_when);
                emailField.setValue(rec.data.email);
                additionalInfo.setValue(rec.data.additional_info);
            }
        }
    });

    var listenerAttributes = new Ext.Window({
        title: windowTitle,
        modal: true,
        width: 483,
        items: [
            attributesForm
        ]
    });

    if (listenerId != 0) {
        listenerAttributes.on('show', function () {
            attributesStore.load();
        });
    }

    return listenerAttributes;
}

function makeListenerDetailsWindow(listenerId) {
    var personalDataPanel = new Ext.Panel({
        layout: 'table',
        defaults: {
            bodyStyle: 'padding: 5px'
        },
        layoutConfig: {
            columns: 2
        },
        border: false
    });

    var listenerPersonalDataStore = new Ext.data.Store({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'surname',
                'name',
                'patronymic',
                'group',
                'phone',
                'address',
                'passport_series',
                'passport_number',
                'passport_givenout',
                'passport_givenout_when',
                'email',
                'additional_info'
            ],
            root: 'listenerdata'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getlistenerdata/?lid=' + listenerId
        }),
        listeners: {
            'load': function () {
                var rec = this.getAt(0);
                personalDataPanel.add({html: 'ФИО', width: 150});
                personalDataPanel.add({html: rec.data.surname + ' ' + rec.data.name + ' ' + rec.data.patronymic});
                personalDataPanel.add({html: 'Группа'});
                personalDataPanel.add({html: rec.data.group});
                personalDataPanel.add({html: 'Контактный телефон'});
                personalDataPanel.add({html: rec.data.phone});
                personalDataPanel.add({html: 'Адрес'});
                personalDataPanel.add({html: rec.data.address});
                personalDataPanel.add({html: 'Серия и номер паспорта'});
                personalDataPanel.add({html: rec.data.passport_series + ' ' + rec.data.passport_number});
                personalDataPanel.add({html: 'Кем выдан паспорт'});
                personalDataPanel.add({html: rec.data.passport_givenout});
                personalDataPanel.add({html: 'Когда выдан паспорт'});
                personalDataPanel.add({html: rec.data.passport_givenout_when});
                personalDataPanel.add({html: 'E-mail'});
                personalDataPanel.add({html: rec.data.email});
                personalDataPanel.add({html: 'Дополнительно'});
                personalDataPanel.add({html: Ext.util.Format.nl2br(Ext.util.Format.htmlEncode(rec.data.additional_info))});
                personalDataPanel.doLayout();
            }
        }
    });

    var listenerContractsStore = new Ext.data.Store({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'date',
                'hours',
                'total',
                'receipt_number',
                'paid'
            ],
            root: 'contracts'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getlistenercontracts/?lid=' + listenerId
        })
    });

    var listenerPresenceStore = new Ext.data.Store({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'start',
                'theme',
                'surname',
                'name',
                'patronymic',
                'presented'
            ],
            root: 'presences'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getlistenerpresence/?lid=' + listenerId
        })
    });

    return new Ext.Window({
        title: 'Детальная информация о слушателе',
        modal: true,
        width: 600,
        items: [
            {
                xtype: 'tabpanel',
                activeTab: 0,
                height: 300,
                items: [
                    {
                        title: 'Личные данные',
                        autoScroll: true,
                        items: [personalDataPanel]
                    },
                    {
                        title: 'Договоры',
                        layout: 'fit',
                        items: [
                            new Ext.grid.GridPanel({
                                loadMask: true,
                                sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
                                store: listenerContractsStore,
                                columns: [
                                    {header: 'Оплачено', dataIndex: 'paid', sortable: true, width: 30,
                                        renderer: function (value) {
                                            return (value == 0) ? 'Нет' : 'Да';
                                        }},
                                    {header: '№ договора', dataIndex: 'id', sortable: true},
                                    {header: 'Дата оформления', dataIndex: 'date', sortable: true},
                                    {header: 'Количество часов', dataIndex: 'hours', sortable: true},
                                    {header: 'Сумма', dataIndex: 'total', sortable: true},
                                    {header: '№ квитанции', dataIndex: 'receipt_number', sortable: true}
                                ]
                            })
                        ]
                    },
                    {
                        title: 'Посещаемость',
                        layout: 'fit',
                        items: [
                            new Ext.grid.GridPanel({
                                loadMask: true,
                                sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
                                store: listenerPresenceStore,
                                columns: [
                                    {header: 'Дата занятия', dataIndex: 'start', sortable: true, width: 120},
                                    {header: 'Тема', dataIndex: 'theme', sortable: true, width: 200},
                                    {header: 'Преподаватель', dataIndex: 'teacher', sortable: true, width: 200,
                                        renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                                            return record.get('surname') + ' ' + record.get('name') + ' ' +
                                                record.get('patronymic');
                                        }},
                                    {header: 'Присутствовал', dataIndex: 'presented', sortable: true, width: 50,
                                        renderer: function (value) {
                                            return (value == true) ? 'Да' : 'Нет'
                                        }}
                                ]
                            })
                        ]
                    }
                ]
            }
        ]
    });
}

function makeListenerSelectWindow(chairId, listenerField, listenerIdField) {
    var listenersStore = new Ext.data.Store({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'surname',
                'name',
                'patronymic',
                'group',
                'phone',
                'passport_number',
                'hours'
            ],
            root: 'listeners'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getlistenersforgrid/'
        })
    });

    var listenersGrid = new Ext.grid.GridPanel({
        store: listenersStore,
        anchor: '100%, 100%',
        height: 400,
        loadMask: true,
        sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
        columns: [
            {header: 'Фамилия', dataIndex: 'surname', sortable: true},
            {header: 'Имя', dataIndex: 'name', sortable: true},
            {header: 'Отчество', dataIndex: 'patronymic', sortable: true},
            {header: 'Группа', dataIndex: 'group', sortable: true},
            {header: 'Телефон', dataIndex: 'phone', sortable: true},
            {header: 'Оплаченные часы', dataIndex: 'hours', sortable: true}
        ]
    });
    listenersGrid.on('rowdblclick', function (gridObj, rowIndex, e) {
        var rec = gridObj.getStore().getAt(rowIndex);
        listenerField.setValue(rec.data.surname + ' ' + rec.data.name + ' ' + rec.data.patronymic + ' - '
            + rec.data.passport_number + ' - ' + rec.data.group);
        listenerIdField.setValue(rec.data.id);
        listenerSelectWnd.close();
    });

    var listenerSelectWnd = new Ext.Window({
        title: 'Выбор слушателя',
        modal: true,
        layout: 'anchor',
        width: 800,
        items: [
            listenersGrid
        ],
        buttons: [
            {
                text: 'Добавить нового слушателя',
                handler: function () {
                    var listenerAttributes = makeListenerAttributesWindow(0, 'Добавление нового слушателя', listenersStore);
                    listenerAttributes.show();
                }
            },
            {
                text: 'Выбрать',
                handler: function () {
                    var rec = listenersGrid.getSelectionModel().getSelected();
                    listenerField.setValue(rec.data.surname + ' ' + rec.data.name + ' ' + rec.data.patronymic + ' - '
                        + rec.data.passport_number + ' - ' + rec.data.group);
                    listenerIdField.setValue(rec.data.id);
                    listenerSelectWnd.close();
                }
            }
        ]
    });

    return listenerSelectWnd;
}

function makeContractsGridWindow(chairId, methodistId) {
    var contractsStore = new Ext.data.Store({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'number',
                'date',
                'listener',
                'start',
                'end',
                'total',
                'receipt_total',
                'receipt_number',
                'receipt_date',
                'paid'
            ],
            root: 'contracts'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getcontractsforgrid/?cid=' + chairId
        })
    });

    var contractsGrid = new Ext.grid.GridPanel({
        store: contractsStore,
        anchor: '100%, 100%',
        height: 400,
        loadMask: true,
        sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
        columns: [
            {header: '№ договора', dataIndex: 'number', sortable: true, width: 60},
            {header: 'Дата оформления', dataIndex: 'date', sortable: true},
            {header: 'Слушатель', dataIndex: 'listener', sortable: true, width: 200},
            {header: 'Дата начала', dataIndex: 'start', sortable: true},
            {header: 'Дата окончания', dataIndex: 'end', sortable: true},
            {header: 'Сумма', dataIndex: 'total', sortable: true, width: 60},
            {
                header: 'Оплачено',
                dataIndex: 'paid',
                sortable: true,
                xtype: 'booleancolumn',
                trueText: 'Да',
                falseText: 'Нет',
                width: 30,
                listeners: {
                    'click': function (cellObj, gridObj, rowIndex) {
                        var rec = gridObj.getStore().getAt(rowIndex);
                        if (rec.data.paid == 1) {
                            Ext.Msg.confirm('Отмена оплаты договора', 'Отменить оплату договора №' + rec.data.number +
                                ' оформленный на ' + rec.data.listener + '?', function (btn) {
                                if (btn == 'yes') {
                                    Ext.Ajax.request({
                                        url: '/json/unpaycontract/',
                                        method: 'POST',
                                        params: {
                                            contract_id: rec.data.id
                                        },
                                        success: function () {
                                            contractsStore.load();
                                        }
                                    });
                                }
                            });
                        } else if (rec.data.paid == 0) {
                            var receiptAttributes = makeReceiptAttributesWindow(0, 'Добавление квитанции', rec.data.id,
                                contractsStore);
                            receiptAttributes.show();
                        }
                    }
                }
            },
            {header: 'По квитанции', dataIndex: 'receipt_total', sortable: true, width: 60},
            {header: '№ квитанции', dataIndex: 'receipt_number', sortable: true},
            {header: 'Дата квитанции', dataIndex: 'receipt_date', sortable: true},
            {
                xtype: 'actioncolumn',
                width: 72,
                header: 'Операции',
                items: [
                    {
                        icon: '/images/icons/copy.gif',
                        tooltip: 'Копировать',
                        handler: function (gridObj, rowIndex, colIndex) {
                            var rec = gridObj.getStore().getAt(rowIndex);
                            var contractAttributes = makeContractAttributesWindow(rec.data.id, chairId, methodistId,
                                'Оформление нового договора', contractsStore, true);
                            contractAttributes.show();
                        }
                    },
                    {
                        icon: '/images/icons/edit.gif',
                        tooltip: 'Редактировать',
                        handler: function (gridObj, rowIndex, colIndex) {
                            var rec = gridObj.getStore().getAt(rowIndex);
                            var contractAttributes = makeContractAttributesWindow(rec.data.id, chairId, methodistId,
                                'Редактирование договора №' + rec.data.number, contractsStore, false);
                            contractAttributes.show();
                        }
                    },
                    {
                        icon: '/images/icons/delete.gif',
                        tooltip: 'Удалить',
                        handler: function (gridObj, rowIndex, colIndex) {
                            var rec = gridObj.getStore().getAt(rowIndex);
                            Ext.Msg.confirm('Удаление договора', 'Удалить договор №' + rec.data.number + ' оформленный на '
                                + rec.data.listener + ' ?', function (btn) {
                                if (btn == 'yes') {
                                    Ext.Ajax.request({
                                        url: '/json/deletecontract/',
                                        method: 'POST',
                                        params: {cid: rec.data.id},
                                        success: function (response, opts) {
                                            contractsStore.load();
                                        },
                                        failure: function (response, opts) {
                                            Ext.Msg.show({
                                                title: 'Ошибка при удалении',
                                                msg: 'Не удалось удалить договор',
                                                buttons: Ext.MessageBox.OK,
                                                icon: Ext.MessageBox.ERROR
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    },
                    {
                        icon: '/images/icons/print.gif',
                        tooltip: 'Печать',
                        handler: function (gridObj, rowIndex, colIndex) {
                            var rec = gridObj.getStore().getAt(rowIndex);
                            printContract(rec.data.id, methodistId);
                        }
                    }
                ]
            }
        ]
    });
    contractsGrid.on('rowdblclick', function (gridObj, rowIndex, e) {
        var rec = gridObj.getStore().getAt(rowIndex);
        var contractDetails = makeContractDetailsWindow(rec.data.id, methodistId);
        contractDetails.show();
    });

    return new Ext.Window({
        title: 'Список договоров',
        modal: true,
        layout: 'anchor',
        width: 1020,
        items: [
            contractsGrid
        ],
        buttons: [
            {
                text: 'Оформить новый договор',
                handler: function () {
                    var contractAttributes = makeContractAttributesWindow(0, chairId, methodistId,
                        'Оформление нового договора', contractsStore, false);
                    contractAttributes.show();
                }
            }
        ]
    });
}
/**
 * Создать окно для ввода данных контракта слушателя
 * @param contractId индентификатор контракта
 * @param chairId индентификатор кафедры
 * @param methodistId индентификатор методиста
 * @param windowTitle заголовок окна
 * @param gridStore хранилище для таблицы контрактов
 * @param isCopy флаг копирования
 * @returns {Ext.Window} окно
 */
function makeContractAttributesWindow(contractId, chairId, methodistId, windowTitle, gridStore, isCopy) {
    var methodistIdField = new Ext.form.Hidden({
        id: 'methodist_id',
        value: methodistId
    });
    var contractIdField = new Ext.form.Hidden({
        id: 'contract_id',
        value: (isCopy ? 0 : contractId)
    });
    var chairIdField = new Ext.form.Hidden({
        id: 'chair_id',
        value: chairId
    });
    var dateField = new Ext.form.DateField({
        id: 'date',
        fieldLabel: 'Дата оформления',
        format: 'Y-m-d',
        value: new Date(),
        allowBlank: false,
        anchor: '100%'
    });
    var listenerIdField = new Ext.form.Hidden({
        id: 'listener_id',
        allowBlank: false
    });
    var listenerField = new Ext.form.TriggerField({
        id: 'listener',
        fieldLabel: 'Слушатель',
        editable: false,
        submitValue: false,
        allowBlank: false,
        anchor: '100%'
    });
    listenerField.onTriggerClick = function (ev) {
        var listenerSelect = makeListenerSelectWindow(chairId, this, listenerIdField);
        listenerSelect.show();
    };
    var startField = new Ext.form.DateField({
        id: 'start',
        fieldLabel: 'Дата начала обучения',
        format: 'Y-m-d',
        allowBlank: false,
        value: Ext.state.Manager.get('contractStartDate'),
        anchor: '100%',
        vtype: 'daterange',
        endDateField: 'end'
    });
    var endField = new Ext.form.DateField({
        id: 'end',
        fieldLabel: 'Дата окончания обучения',
        format: 'Y-m-d',
        allowBlank: false,
        value: Ext.state.Manager.get('contractEndDate'),
        anchor: '100%',
        vtype: 'daterange',
        startDateField: 'start'
    });
    var hoursField = new Ext.form.TextField({
        id: 'hours',
        fieldLabel: 'Количество академических часов',
        allowBlank: false,
        maskRe: /[0-9]/,
        anchor: '100%'
    });
    var totalField = new Ext.form.NumberField({
        id: 'total',
        fieldLabel: 'Сумма',
        minValue: 0,
        allowBlank: false,
        anchor: '100%'
    });

    // Скрытое поле для желаемых тем
    var themesFieldHidden = new Ext.form.Hidden({
        id: 'themes',
        value: null
    });
    // Загрузка тем доступных для данной кафедры
    var themesStore = new Ext.data.Store({
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'name'
            ],
            root: 'themes'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getchairlessonthemes/?cid=' + chairId
        }),
        autoLoad: true
    });
    // Триггер для задания желаемых тем
    var themesTriggerField = new Ext.form.TriggerField({
        id: 'desired_themes',
        fieldLabel: 'Желаемые темы занятий',
        allowBlank: false,
        editable: false,
        submitValue: false,
        anchor: '100%',
        onTriggerClick: function (e) {
            var sm = new Ext.grid.CheckboxSelectionModel();
            var grid = new Ext.grid.GridPanel({
                store: themesStore,
                height: 400,
                width: 400,
                cm: new Ext.grid.ColumnModel({
                    defaults: {
                        width: 120,
                        sortable: true
                    },
                    columns: [
                        sm,
                        {id: 'name', header: "Тема", dataIndex: 'name'}
                    ]
                }),
                sm: sm,
                autoExpandColumn: 'name',
                columnLines: true,
                listeners: {
                    viewready: function () {
                        var val = themesFieldHidden.getValue();
                        if (val) {
                            var selections = JSON.parse(val);
                            var rows = [];
                            for (var i = 0; i < selections.length; i++) {
                                var index = themesStore.find('id', selections[i].id);
                                if (index != -1) {
                                    rows.push(index);
                                }
                            }
                            grid.getSelectionModel().selectRows(rows);
                        }
                    }
                }
            });

            var wnd = new Ext.Window({
                title: 'Желаемые темы занятий',
                modal: true,
                layout: 'anchor',
                height: 400,
                width: 400,
                items: [
                    grid
                ],
                buttons: [
                    {
                        text: 'ОК',
                        handler: function () {
                            var selections = grid.getSelectionModel().getSelections();
                            // получение всех строк хранилища
                            var themes = [];
                            var themesName = [];
                            for (var i = 0; i < selections.length; i++) {
                                themes.push({id: selections[i].get('id')});
                                themesName.push(selections[i].get('name'));
                            }
                            // установление заголовка тем
                            themesTriggerField.setValue(themesName.join('; '));
                            // установелние значения скрытого поля
                            themesFieldHidden.setValue(JSON.stringify(themes));

                            // Установка количества академических часов, если указано текущее значение не соответствует
                            // количеству тем
                            if (hoursField.getValue() < themes.length * 2) {
                                hoursField.setValue(themes.length * 2);
                            }

                            wnd.close();
                        }
                    },
                    {
                        text: 'Отмена',
                        handler: function () {
                            wnd.close();
                        }
                    }
                ]
            });

            wnd.show();
        }
    });

    var attributesForm = new Ext.form.FormPanel({
        id: 'contract-attributes',
        padding: 5,
        url: '/json/savecontractdata/',
        items: [
            methodistIdField,
            contractIdField,
            chairIdField,
            dateField,
            listenerIdField,
            listenerField,
            startField,
            endField,
            themesFieldHidden,
            themesTriggerField,
            hoursField,
            totalField
        ],
        buttons: [
            {
                text: 'Сохранить',
                plugins: 'defaultButton',
                handler: function () {
                    var form = attributesForm.getForm();
                    if (form.isValid()) {
                        form.submit({
                            waitMsg: 'Сохранение данных о договоре',
                            success: function (form, action) {
                                Ext.state.Manager.set('contractStartDate', startField.getValue());
                                Ext.state.Manager.set('contractEndDate', endField.getValue());
                                if (gridStore) {
                                    gridStore.reload();
                                }
                                if (contractId == 0) {
                                    Ext.Msg.show({
                                        title: 'Оформление успешно завершено',
                                        msg: 'Договор добавлен в базу под №' + action.result.number,
                                        buttons: Ext.MessageBox.OK,
                                        icon: Ext.MessageBox.INFO
                                    });
                                }
                                contractAttributes.close();
                            }
                        })
                    } else {
                        Ext.Msg.show({
                            title: 'Ошибка при валидации',
                            msg: 'В форму внесены некорректные данные',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                }
            },
            {
                text: 'Сохранить и распечатать',
                handler: function () {
                    var form = attributesForm.getForm();
                    if (form.isValid()) {
                        form.submit({
                            waitMsg: 'Сохранение данных о договоре',
                            success: function (form, action) {
                                Ext.state.Manager.set('contractStartDate', startField.getValue());
                                Ext.state.Manager.set('contractEndDate', endField.getValue());
                                if (gridStore) {
                                    gridStore.reload();
                                }
                                if (contractId == 0) {
                                    Ext.Msg.show({
                                        title: 'Оформление успешно завершено',
                                        msg: 'Договор добавлен в базу под №' + action.result.number,
                                        buttons: Ext.MessageBox.OK,
                                        icon: Ext.MessageBox.INFO
                                    });
                                }
                                contractAttributes.close();
                                printContract(action.result.contractid, methodistId);
                            }
                        })
                    } else {
                        Ext.Msg.show({
                            title: 'Ошибка при валидации',
                            msg: 'В форму внесены некорректные данные',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                }
            },
            {
                text: 'Отмена',
                handler: function () {
                    contractAttributes.close();
                }
            }
        ]
    });

    var attributesStore = new Ext.data.Store({
        reader: new Ext.data.JsonReader({
            fields: [
                'chair_id',
                'date',
                'listener_id',
                'listener',
                'listener_group',
                'listener_passport_number',
                'start',
                'end',
                'hours',
                'total',
                'themes'
            ],
            root: 'contractdata'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getcontractdata/?cid=' + contractId
        }),
        listeners: {
            'load': function () {
                var rec = this.getAt(0);
                chairIdField.setValue(rec.data.chair_id);
                if (isCopy) dateField.setValue(new Date()); else dateField.setValue(rec.data.date);
                listenerIdField.setValue(rec.data.listener_id);
                listenerField.setValue(rec.data.listener + ' - ' +
                    rec.data.listener_passport_number + ' - ' + rec.data.listener_group);
                startField.setValue(rec.data.start);
                endField.setValue(rec.data.end);
                hoursField.setValue(rec.data.hours);
                totalField.setValue(rec.data.total);
                themesFieldHidden.setValue(JSON.stringify(rec.data.themes));

                var ntemes = '';
                for (var i = 0; i < rec.data.themes.length; i++) {
                    ntemes += ' ' + rec.data.themes[i].name;
                    if (i != rec.data.themes.length - 1) {
                        ntemes += ';'
                    }
                }
                themesTriggerField.setValue(ntemes);
            }
        }
    });

    var contractAttributes = new Ext.Window({
        title: windowTitle,
        modal: true,
        width: 483,
        items: [
            attributesForm
        ]
    });

    if (contractId != 0) {
        contractAttributes.on('show', function () {
            attributesStore.load();
        });
    }

    return contractAttributes;
}
/**
 * Задать окно просмотра данных о контракте
 * @param contractId идентификатор контракта
 * @param methodistId идентификатор методиста
 * @returns {Ext.Window} окно
 */
function makeContractDetailsWindow(contractId, methodistId) {
    var contractDetailsPanel = new Ext.Panel({
        layout: 'table',
        defaults: {
            bodyStyle: 'padding: 5px'
        },
        layoutConfig: {
            columns: 2
        },
        border: false
    });

    var contractDetailsStore = new Ext.data.Store({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'number',
                'date',
                'hours',
                'start',
                'end',
                'total',
                'themes',
                'paid',
                'receiptNumber',
                'receiptTotal',
                'receiptDate',
                'chairName',
                'chairAbbreviation',
                'listenerSurname',
                'listenerName',
                'listenerPatronymic',
                'listenerGroup',
                'listenerPhone',
                'listenerAddress',
                'listenerPassportSeries',
                'listenerPassportNumber',
                'listenerPassportGivenout',
                'listenerPassportGivenoutWhen',
                'listenerEmail'
            ],
            root: 'contractdetails'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getcontractdetails/?cid=' + contractId
        }),
        listeners: {
            load: function () {
                var get = function (val) {
                    var str = val.toString();
                    return str.length == 0 ? 'Не указано' : str;
                };

                var data = this.getAt(0).data;

                contractDetailsPanel.add({html: 'Кафедра'});
                contractDetailsPanel.add({html: get(data.chairName)});
                contractDetailsPanel.add({html: '№ договора'});
                contractDetailsPanel.add({html: get(data.number)});
                contractDetailsPanel.add({html: 'Дата оформления'});
                contractDetailsPanel.add({html: get(data.date)});
                contractDetailsPanel.add({html: 'Дата начала обучения'});
                contractDetailsPanel.add({html: get(data.start)});
                contractDetailsPanel.add({html: 'Дата окончания обучения'});
                contractDetailsPanel.add({html: get(data.end)});
                contractDetailsPanel.add({html: 'Сумма договора'});
                contractDetailsPanel.add({html: get(data.total)});
                contractDetailsPanel.add({html: 'Статус оплаты'});
                contractDetailsPanel.add({html: data.paid ? 'Оплачено' : '<b>Не оплачено</b>'});
                contractDetailsPanel.add({html: 'ФИО слушателя'});
                contractDetailsPanel.add({html: get(data.listenerSurname + ' ' + data.listenerName + ' ' +
                    data.listenerPatronymic)});
                contractDetailsPanel.add({html: 'Группа слушателя'});
                contractDetailsPanel.add({html: get(data.listenerGroup)});
                contractDetailsPanel.add({html: 'Контактный телефон'});
                contractDetailsPanel.add({html: get(data.listenerPhone)});
                contractDetailsPanel.add({html: 'Адрес слушателя'});
                contractDetailsPanel.add({html: get(data.listenerAddress)});
                contractDetailsPanel.add({html: 'Серия и номер паспорта слушателя'});
                contractDetailsPanel.add({html: data.listenerPassportSeries + ' ' + data.listenerPassportNumber});
                contractDetailsPanel.add({html: 'Кем выдан паспорт'});
                contractDetailsPanel.add({html: get(data.listenerPassportGivenout)});
                contractDetailsPanel.add({html: 'Когда выдан паспорт'});
                contractDetailsPanel.add({html: get(data.listenerPassportGivenoutWhen)});
                contractDetailsPanel.add({html: 'E-mail'});
                contractDetailsPanel.add({html: get(data.listenerEmail)});
                contractDetailsPanel.add({html: 'Желаемые темы'});
                contractDetailsPanel.add({html: get(data.themes)});
                contractDetailsPanel.doLayout();

                wnd.setTitle('Детальная информация о договоре №' + data.number);
            }
        }
    });

    var wnd = new Ext.Window({
        title: 'Детальная информация о договоре',
        modal: true,
        height: 500,
        width: 600,
        autoScroll: true,
        items: [ contractDetailsPanel ],
        buttons: [
            {
                text: 'Печать',
                handler: function () {
                    printContract(contractId, methodistId);
                }
            }
        ]
    });

    return wnd;
}

function printContract(contractId, methodistId) {
    location.href = '/print/printcontract?cid=' + contractId + '&mid=' + methodistId;
}

function makeReceiptAttributesWindow(receiptId, windowTitle, contractId, gridStore) {
    var contractIdField = new Ext.form.Hidden({
        id: 'contract_id',
        value: contractId
    });
    var numberField = new Ext.form.TextField({
        id: 'receipt_number',
        fieldLabel: '№ квитанции',
        allowBlank: false,
        anchor: '100%'
    });
    var totalField = new Ext.form.NumberField({
        id: 'receipt_total',
        fieldLabel: 'Сумма',
        allowBlank: false,
        anchor: '100%'
    });
    var dateField = new Ext.form.DateField({
        id: 'receipt_date',
        fieldLabel: 'Дата оплаты',
        format: 'Y-m-d',
        allowBlank: false,
        value: new Date(),
        anchor: '100%'
    });

    var attributesForm = new Ext.form.FormPanel({
        id: 'receipt-attributes',
        url: '/json/paycontract/',
        padding: 5,
        items: [
            numberField,
            totalField,
            dateField,
            contractIdField
        ],
        buttons: [
            {
                text: 'Сохранить',
                plugins: 'defaultButton',
                handler: function () {
                    var form = attributesForm.getForm();
                    if (form.isValid()) {
                        form.submit({
                            waitMsg: 'Сохранение данных о квитанции',
                            success: function (form, action) {
                                if (gridStore != null) {
                                    gridStore.load();
                                }
                                receiptArributes.close();
                            }
                        })
                    } else {
                        Ext.Msg.show({
                            title: 'Ошибка при валидации',
                            msg: 'В форму внесены некорректные данные',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                }
            },
            {
                text: 'Отмена',
                handler: function () {
                    receiptArributes.close();
                }
            }
        ]
    });

    var attributesStore = new Ext.data.Store({
        reader: new Ext.data.JsonReader({
            fields: [
                'number',
                'total',
                'date'
            ],
            root: 'receiptdata'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getreceiptdata/rid?=' + receiptId
        }),
        listeners: {
            'load': function () {
                var rec = this.getAt(0);
                numberField.setValue(rec.data.number);
                totalField.setValue(rec.data.total);
                dateField.setValue(rec.data.date);
            }
        }
    });

    var receiptArributes = new Ext.Window({
        title: windowTitle,
        modal: true,
        width: 483,
        items: [
            attributesForm
        ]
    });

    if (receiptId != 0) {
        receiptArributes.on('show', function () {
            attributesStore.load();
        });
    }

    return receiptArributes;
}

function makeIntensivesGridWindow(chairId) {
    var intensivesStore = new Ext.data.Store({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'name'
            ],
            root: 'intensives'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getintensivesforgrid/?cid=' + chairId
        })
    });

    var intensivesGrid = new Ext.grid.GridPanel({
        store: intensivesStore,
        anchor: '100%, 100%',
        height: 400,
        loadMask: true,
        sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
        columns: [
            {header: 'Наименование интенсива', dataIndex: 'name', sortable: true, width: 550},
            {
                xtype: 'actioncolumn',
                width: 40,
                header: 'Операции',
                items: [
                    {
                        icon: '/images/icons/edit.gif',
                        tooltip: 'Редактировать',
                        handler: function (gridObj, rowIndex, colIndex) {
                            var rec = gridObj.getStore().getAt(rowIndex);
                            var intensiveAttributes = makeIntensiveAttributesWindow(rec.data.id, chairId,
                                'Редактирование интенсива', intensivesStore, false);
                            intensiveAttributes.show();
                        }
                    },
                    {
                        icon: '/images/icons/delete.gif',
                        tooltip: 'Удалить',
                        handler: function (gridObj, rowIndex, colIndex) {
                            var rec = gridObj.getStore().getAt(rowIndex);
                            Ext.Msg.confirm('Удаление интенсива', 'Удалить интенсив "' + rec.data.name + '" ?',
                                function (btn) {
                                    if (btn == 'yes') {
                                        Ext.Ajax.request({
                                            url: '/json/deleteintensive/',
                                            method: 'POST',
                                            params: {iid: rec.data.id},
                                            success: function (response, opts) {
                                                intensivesStore.load();
                                                refreshIntensivesToolbar(chairId);
                                            },
                                            failure: function (response, opts) {
                                                Ext.Msg.show({
                                                    title: 'Ошибка при удалении',
                                                    msg: 'Не удалось удалить интенсив',
                                                    buttons: Ext.MessageBox.OK,
                                                    icon: Ext.MessageBox.ERROR
                                                });
                                            }
                                        });
                                    }
                                });
                        }
                    }
                ]
            }
        ]
    });

    return new Ext.Window({
        title: 'Список интенсивов',
        modal: true,
        layout: 'anchor',
        width: 650,
        items: [
            intensivesGrid
        ],
        buttons: [
            {
                text: 'Добавить новый интенсив',
                handler: function () {
                    var intensiveAttributes = makeIntensiveAttributesWindow(0, chairId, 'Добавление нового интенсива',
                        intensivesStore, false);
                    intensiveAttributes.show();
                }
            }
        ]
    });
}
/**
 * Задать окно добавления (редактирования) параметров интенсива
 * @param intensiveId идентификатор интенсива
 * @param chairId идентификатор кафедры
 * @param windowTitle заголовок окна
 * @param gridStore хранилище
 * @returns {Ext.Window} созданное окно
 */
function makeIntensiveAttributesWindow(intensiveId, chairId, windowTitle, gridStore) {
    // скрытое поле для индентификатора интенсива
    var intensiveIdField = new Ext.form.Hidden({
        id: 'id',
        value: intensiveId
    });
    // скрытое поле для индентификатора кафедры
    var chairIdField = new Ext.form.Hidden({
        id: 'chair_id',
        value: chairId
    });

    var nameField = new Ext.form.TextField({
        id: 'name',
        fieldLabel: 'Наименование',
        allowBlank: false,
        anchor: '100%'
    });
    // скрытое поле для тем интенсива
    var themesFieldHidden = new Ext.form.Hidden({
        id: 'themes',
        value: null
    });
    // триггер для задания тем интенсива в виде рабочей программы
    var workprogrammTriggerField = new Ext.form.TriggerField({
        id: 'work_program',
        fieldLabel: 'Рабочая программа',
        editable: false,
        submitValue: false,
        allowBlank: false,
        anchor: '100%',
        onTriggerClick: function (e) {
            var workProgramWnd = makeWorkProgramWindow(this, themesFieldHidden);
            workProgramWnd.show();
        }
    });
    // Форма
    var formPanel = new Ext.form.FormPanel({
        id: 'intensive_form',
        padding: 5,
        url: '/json/saveintensivedata/',
        items: [
            intensiveIdField,
            chairIdField,
            nameField,
            themesFieldHidden,
            workprogrammTriggerField
        ],
        buttons: [
            {
                text: 'Сохранить',
                plugins: 'defaultButton',
                handler: function () {
                    var form = formPanel.getForm();
                    if (form.isValid()) {
                        form.submit({
                            waitMsg: 'Сохранение данных об интенсиве',
                            success: function (form, action) {
                                if (gridStore != null) {
                                    gridStore.load();
                                }
                                refreshIntensivesToolbar(chairId);
                                wnd.close();
                            }
                        })
                    } else {
                        Ext.Msg.show({
                            title: 'Ошибка при валидации',
                            msg: 'В форму внесены некорректные данные',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                }
            },
            {
                text: 'Отмена',
                handler: function () {
                    wnd.close();
                }
            }
        ]
    });
    // Хранилище для отображения данных формы в случаи редаткирования интенсива
    var store = new Ext.data.Store({
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'name',
                'themes',
                'workprogram'
            ],
            root: 'intensivedata'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getintensivedata/?id=' + intensiveId
        }),
        listeners: {
            'load': function () {
                var rec = this.getAt(0);
                nameField.setValue(rec.data.name);
                themesFieldHidden.setRawValue(JSON.stringify(rec.data.themes));
                workprogrammTriggerField.setValue(rec.data.workprogram);
            }
        }
    });

    var wnd = new Ext.Window({
        title: windowTitle,
        modal: true,
        width: 480,
        items: [
            formPanel
        ]
    });

    wnd.on('beforeshow', function () {
        // Загрузка данных об интенсиве, если интенсив редактируется
        if (intensiveId != 0) {
            store.load();
        }
    });

    return wnd;
}

function makeTeachersGridWindow(chairId) {
    var teachersStore = new Ext.data.Store({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'surname',
                'name',
                'patronymic'
            ],
            root: 'teachers'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getteachersforgrid/?cid=' + chairId
        })
    });

    var teachersGrid = new Ext.grid.GridPanel({
        store: teachersStore,
        anchor: '100%, 100%',
        height: 400,
        loadMask: true,
        sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
        columns: [
            {header: 'Фамилия', dataIndex: 'surname', sortable: true, width: 200},
            {header: 'Имя', dataIndex: 'name', sortable: true, width: 200},
            {header: 'Отчество', dataIndex: 'patronymic', sortable: true, width: 200},
            {
                xtype: 'actioncolumn',
                width: 40,
                header: 'Операции',
                items: [
                    {
                        icon: '/images/icons/edit.gif',
                        tooltip: 'Редактировать',
                        handler: function (gridObj, rowIndex, colIndex) {
                            var rec = gridObj.getStore().getAt(rowIndex);
                            var teacherAttributes = makeTeacherAttributesWindow(rec.data.id, chairId,
                                'Редактирование преподавателя', teachersStore, false);
                            teacherAttributes.show();
                        }
                    },
                    {
                        icon: '/images/icons/delete.gif',
                        tooltip: 'Удалить',
                        handler: function (gridObj, rowIndex, colIndex) {
                            var rec = gridObj.getStore().getAt(rowIndex);
                            Ext.Msg.confirm('Удаление преподавателя', 'Удалить преподавателя ' + rec.data.surname + ' '
                                + rec.data.name + ' ' + rec.data.patronymic + '?', function (btn) {
                                if (btn == 'yes') {
                                    Ext.Ajax.request({
                                        url: '/json/deleteteacher/',
                                        method: 'POST',
                                        params: {tid: rec.data.id},
                                        success: function (response, opts) {
                                            teachersStore.load();
                                        },
                                        failure: function (response, opts) {
                                            Ext.Msg.show({
                                                title: 'Ошибка при удалении',
                                                msg: 'Не удалось удалить преподавателя',
                                                buttons: Ext.MessageBox.OK,
                                                icon: Ext.MessageBox.ERROR
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    }
                ]
            }
        ],
        listeners: {
            'rowcontextmenu': function (gridObj, rowIndex, event) {
                event.stopEvent();
                var menu = new Ext.menu.Menu({
                    items: [
                        {
                            text: 'Свободные часы преподавателя',
                            handler: function () {
                                var teacherData = teachersStore.getAt(rowIndex).data;
                                var scheduleWindow = makeTeacherScheduleWindow(teacherData);
                                scheduleWindow.show();
                            }
                        },
                        {
                            text: 'Назначенные занятия',
                            handler: function () {
                                var teacherData = teachersStore.getAt(rowIndex).data;
                                var lessonsGridWindow = makeTeacherLessonGridWindow(teacherData);
                                lessonsGridWindow.show();
                            }
                        },
                        '-',
                        {
                            text: 'Сформировать трудовой договор на выполнение учебной работы',
                            handler: function () {
                                var teacherData = teachersStore.getAt(rowIndex).data;
                                var contractWindow = makeGenerateTeacherContractWindow(teacherData);
                                contractWindow.show();
                            }
                        },
                        {
                            text: 'Сформировать отчёт о выполнении учебной работы',
                            handler: function () {

                            }
                        }
                    ]
                });
                menu.showAt(Ext.EventObject.getXY());
            }
        }
    });

    return new Ext.Window({
        title: 'Список преподавателей',
        modal: true,
        layout: 'anchor',
        width: 660,
        items: [
            teachersGrid
        ],
        buttons: [
            {
                text: 'Добавить нового преподавателя',
                handler: function () {
                    var teacherAttributes = makeTeacherAttributesWindow(0, chairId, 'Добавление нового преподавателя',
                        teachersStore, false);
                    teacherAttributes.show();
                }
            }
        ]
    });
}

function makeTeacherScheduleWindow(teacherData) {
    var scheduleStore = new Ext.data.Store({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            fields: Ext.calendar.EventRecord.prototype.fields.getRange(),
            root: 'tasks'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getteacherschedule/?tid=' + teacherData.id
        })
    });

    var scheduleCalendar = new Ext.calendar.CalendarPanel({
        id: 'teacher-schedule-calendar',
        height: 600,
        showDayView: false,
        showMonthView: false,
        showWeekView: true,
        weekText: 'Расписание',
        activeItem: 1,
        calendarStore: new Ext.data.JsonStore({
            storeId: 'teacherCalendarStore',
            root: 'calendars',
            idProperty: 'id',
            data: {
                "calendars": [
                    {
                        "id": 2,
                        "title": "Занятия"
                    }
                ]
            },
            proxy: new Ext.data.MemoryProxy(),
            autoLoad: true,
            fields: [
                {name: 'CalendarId', mapping: 'id', type: 'int'},
                {name: 'Title', mapping: 'title', type: 'string'}
            ],
            sortInfo: {
                field: 'CalendarId',
                direction: 'ASC'
            }
        }),
        eventStore: scheduleStore,
        listeners: {
            rangeselect: function (calendarObj, dates) {
                if (intensiveId != 0) {
                    var lessonAttributesWnd = makeLessonAttributesWindow(0, intensiveId, chairId,
                        'Добавление нового занятия', dates.StartDate, dates.EndDate, function (form, action) {
                            refreshCalendar();
                        });
                    lessonAttributesWnd.show();
                } else {
                    Ext.Msg.show({
                        title: 'Нет выбранного интенсива',
                        msg: 'Чтобы добавить занятие, сперва необходимо выбрать интенсив',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING
                    });
                }
            },
            eventclick: function (calendarObj, rec, el) {
                if (rec.data.CalendarId == 1) {
                    var menu = new Ext.menu.Menu({
                        items: [
                            {
                                text: 'Состав слушателей',
                                handler: function () {
                                    var staffWnd = makeListenersLessonStaffWindow(rec.id);
                                    staffWnd.show();
                                }
                            },
                            {
                                text: 'Отметка посещаемости занятия',
                                handler: function () {
                                    var presencesLessonWnd = makeLessonPresenceWindow(rec.id);
                                    presencesLessonWnd.show();
                                }
                            },
                            {
                                text: 'Дублировать занятие',
                                handler: function () {
                                    var duplicateWnd = makeDuplicateEventWindow('Дублирование занятия',
                                        function (period, repeatCount) {
                                            Ext.Ajax.request({
                                                url: '/json/duplicatelesson/',
                                                method: 'POST',
                                                params: {
                                                    lid: rec.id,
                                                    period: period,
                                                    repeat_count: repeatCount
                                                },
                                                success: function (response, opts) {
                                                    refreshCalendar();
                                                },
                                                failure: function (response, opts) {
                                                    Ext.Msg.show({
                                                        title: 'Ошибка при дублировании занятия',
                                                        msg: 'Не удалось продублировать занятие',
                                                        buttons: Ext.MessageBox.OK,
                                                        icon: Ext.MessageBox.ERROR
                                                    });
                                                }
                                            });
                                        });
                                    duplicateWnd.show();
                                }
                            },
                            '-',
                            {
                                text: 'Редактировать занятие',
                                handler: function () {
                                    var lessonAttributesWnd = makeLessonAttributesWindow(rec.id, intensiveId, chairId,
                                        'Редактирование занятия', null, null, function (form, action) {
                                            refreshCalendar();
                                        });
                                    lessonAttributesWnd.show();
                                }
                            },
                            {
                                text: 'Удалить занятие',
                                handler: function () {
                                    Ext.Msg.confirm('Удаление занятия', 'Вы точно хотите удалить занятие ?',
                                        function (btn) {
                                            if (btn == 'yes') {
                                                Ext.Ajax.request({
                                                    url: '/json/deletelesson/',
                                                    method: 'POST',
                                                    params: {lid: rec.id},
                                                    success: function (response, opts) {
                                                        refreshCalendar();
                                                    },
                                                    failure: function (response, opts) {
                                                        Ext.Msg.show({
                                                            title: 'Ошибка при удалении',
                                                            msg: 'Не удалось удалить занятие',
                                                            buttons: Ext.MessageBox.OK,
                                                            icon: Ext.MessageBox.ERROR
                                                        });
                                                    }
                                                });
                                            }
                                        }
                                    );
                                }
                            }
                        ]
                    });
                    menu.showAt(Ext.EventObject.getXY());
                }
            }
        }
    });

    var scheduleWindow = new Ext.Window({
        title: 'Расписание: ' + teacherData.surname + ' ' + teacherData.name + ' ' + teacherData.patronymic,
        modal: true,
        layout: 'anchor',
        width: 800,
        items: [
            scheduleCalendar
        ],
        buttons: [
            {
                text: 'Закрыть',
                handler: function () {
                    scheduleWindow.close();
                }
            }
        ]
    });

    return scheduleWindow;
}

function makeTeacherAttributesWindow(teacherId, chairId, windowTitle, gridStore) {
    var teacherIdField = new Ext.form.Hidden({
        id: 'teacher-id',
        name: 'id',
        value: teacherId
    });
    var chairIdField = new Ext.form.Hidden({
        id: 'chair_id',
        value: chairId
    });
    var surnameField = new Ext.form.TextField({
        id: 'surname',
        fieldLabel: 'Фамилия',
        allowBlank: false,
        anchor: '100%'
    });
    var nameField = new Ext.form.TextField({
        id: 'name',
        fieldLabel: 'Имя',
        allowBlank: false,
        anchor: '100%'
    });
    var patronymicField = new Ext.form.TextField({
        id: 'patronymic',
        fieldLabel: 'Отчество',
        allowBlank: false,
        anchor: '100%'
    });
    var rankField = new Ext.form.TextField({
        id: 'rank',
        fieldLabel: 'Должность/звание',
        anchor: '100%'
    });
    var attNumField = new Ext.form.TextField({
        id: 'att_num',
        fieldLabel: '№ атт. ВАК',
        anchor: '100%'
    });
    var diplomNumField = new Ext.form.TextField({
        id: 'diplom_num',
        fieldLabel: '№ диплома',
        anchor: '100%'
    });
    var loginField = new Ext.form.TextField({
        id: 'login',
        fieldLabel: 'Логин',
        allowBlank: false,
        anchor: '100%'
    });
    var passwordField = new Ext.form.TextField({
        id: 'password',
        fieldLabel: 'Пароль',
        allowBlank: teacherId,
        anchor: '100%'
    });

    var attributesForm = new Ext.form.FormPanel({
        id: 'teacher-attributes',
        padding: 5,
        url: '/json/saveteacherdata/',
        items: [
            teacherIdField,
            chairIdField,
            surnameField,
            nameField,
            patronymicField,
            rankField,
            attNumField,
            diplomNumField,
            loginField,
            passwordField
        ],
        buttons: [
            {
                text: 'Сохранить',
                plugins: 'defaultButton',
                handler: function () {
                    var form = attributesForm.getForm();
                    if (form.isValid()) {
                        form.submit({
                            waitMsg: 'Сохранение данных о преподавателе',
                            success: function (form, action) {
                                if (gridStore != null) {
                                    gridStore.load();
                                }
                                teacherAttributes.close();
                            },
                            failure: function (form, action) {
                                Ext.Msg.show({
                                    title: 'Ошибка при сохранении',
                                    msg: action.result.error_message,
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.WARNING
                                });
                            }
                        })
                    } else {
                        Ext.Msg.show({
                            title: 'Ошибка при валидации',
                            msg: 'В форму внесены некорректные данные',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                }
            },
            {
                text: 'Отмена',
                handler: function () {
                    teacherAttributes.close();
                }
            }
        ]
    });

    var attributesStore = new Ext.data.Store({
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'surname',
                'name',
                'patronymic',
                'rank',
                'att_num',
                'diplom_num',
                'login',
                'password'
            ],
            root: 'teacherdata'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getteacherdata/?tid=' + teacherId
        }),
        listeners: {
            'load': function () {
                var rec = this.getAt(0);
                surnameField.setValue(rec.data.surname);
                nameField.setValue(rec.data.name);
                patronymicField.setValue(rec.data.patronymic);
                loginField.setValue(rec.data.login);
                rankField.setValue(rec.data.rank);
                attNumField.setValue(rec.data.att_num);
                diplomNumField.setValue(rec.data.diplom_num);
            }
        }
    });

    var teacherAttributes = new Ext.Window({
        title: windowTitle,
        modal: true,
        width: 483,
        items: [
            attributesForm
        ]
    });

    if (teacherId != 0) {
        teacherAttributes.on('show', function () {
            attributesStore.load();
        });
    }

    return teacherAttributes;
}

function makeTeacherLessonGridWindow(teacherData) {
    var lessonsStore = new Ext.data.Store({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'start',
                'theme',
                'auditory',
                'intensive',
                'type'
            ],
            root: 'lessons'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getteacherlessons/?tid=' + teacherData.id
        })
    });

    var lessonsGrid = new Ext.grid.GridPanel({
        store: lessonsStore,
        anchor: '100%, 100%',
        height: 400,
        loadMask: true,
        sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
        columns: [
            {header: 'Дата начала', dataIndex: 'start', sortable: true, width: 120},
            {header: 'Курс', dataIndex: 'intensive', sortable: true, width: 200},
            {header: 'Тема', dataIndex: 'theme', sortable: true, width: 200},
            {header: 'Аудитория', dataIndex: 'auditory', sortable: true, width: 100},
            {header: 'Тип занятия', dataIndex: 'type', sortable: true, width: 120, renderer: function (value) {
                switch (value) {
                    case 1:
                        return 'Практическое';
                    case 2:
                        return 'Лекционно-практическое';
                }
                return 'Теоретическое';
            }}
        ]
    });

    var lessonsGridWindow = new Ext.Window({
        title: 'Назначенные занятия: ' + teacherData.surname + ' ' + teacherData.name + ' ' + teacherData.patronymic,
        modal: true,
        layout: 'anchor',
        width: 800,
        items: [
            lessonsGrid
        ],
        buttons: [
            {
                text: 'Закрыть',
                handler: function () {
                    lessonsGridWindow.close();
                }
            }
        ]
    });

    return lessonsGridWindow;
}

function makeGenerateTeacherContractWindow(teacherData) {

    var teacherIdField = new Ext.form.Hidden({
        id: 'teacher_id',
        value: teacherData.id
    });
    var startField = new Ext.form.DateField({
        id: 'start',
        fieldLabel: 'С',
        format: 'Y-m-d',
        allowBlank: false,
        value: Ext.state.Manager.get('contractStartDate'),
        anchor: '100%',
        vtype: 'daterange',
        endDateField: 'end'
    });
    var endField = new Ext.form.DateField({
        id: 'end',
        fieldLabel: 'По',
        format: 'Y-m-d',
        allowBlank: false,
        value: Ext.state.Manager.get('contractEndDate'),
        anchor: '100%',
        vtype: 'daterange',
        startDateField: 'start'
    });
    var semesterField = new Ext.form.ComboBox({
        id: 'semester_name',
        hiddenName: 'semester',
        fieldLabel: 'Семестр',
        mode: 'local',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: [
                'id',
                'semesterText'
            ],
            data: [
                [1, '1 семестр'],
                [2, '2 семестр']
            ]
        }),
        valueField: 'id',
        displayField: 'semesterText',
        value: 1,
        triggerAction: 'all',
        allowBlank: false,
        anchor: '100%'
    });
    var makeDateField = new Ext.form.DateField({
        id: 'make_date',
        fieldLabel: 'Дата оформления',
        format: 'Y-m-d',
        value: new Date(),
        allowBlank: false,
        anchor: '100%'
    });
    var hourCostField = new Ext.form.NumberField({
        id: 'hour_cost',
        fieldLabel: 'Оплата за один час',
        allowBlank: false,
        minValue: 0,
        anchor: '100%'
    });

    var contractForm = new Ext.form.FormPanel({
        id: 'teacher-contract-attributes',
        padding: 5,
        url: '/print/teachercontract/',
        items: [
            teacherIdField,
            startField,
            endField,
            semesterField,
            makeDateField,
            hourCostField
        ],
        buttons: [
            {
                text: 'Сформировать',
                plugins: 'defaultButton',
                handler: function () {
                    var form = contractForm.getForm();
                    if (form.isValid()) {
                        form.submit();
                        contractAttributes.close();
                    } else {
                        Ext.Msg.show({
                            title: 'Ошибка при валидации',
                            msg: 'В форму внесены некорректные данные',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                }
            },
            {
                text: 'Отмена',
                handler: function () {
                    contractAttributes.close();
                }
            }
        ]
    });
    contractForm.getForm().standardSubmit = true;
    var contractAttributes = new Ext.Window({
        title: 'Формирование трудового договора на выполнение учебной работы',
        modal: true,
        layout: 'anchor',
        width: 400,
        items: [
            contractForm
        ]
    });

    return contractAttributes;
}

function makeGenerateTeacherReportWindow(teacherData) {
    var teacherIdField = new Ext.form.Hidden({
        id: 'teacher_id',
        value: teacherData.id
    });
    var startField = new Ext.form.DateField({
        id: 'start',
        fieldLabel: 'С',
        format: 'Y-m-d',
        allowBlank: false,
        value: Ext.state.Manager.get('contractStartDate'),
        anchor: '100%',
        vtype: 'daterange',
        endDateField: 'end'
    });
    var endField = new Ext.form.DateField({
        id: 'end',
        fieldLabel: 'По',
        format: 'Y-m-d',
        allowBlank: false,
        value: Ext.state.Manager.get('contractEndDate'),
        anchor: '100%',
        vtype: 'daterange',
        startDateField: 'start'
    });
    var contractMakeDateField = new Ext.form.DateField({
        id: 'contract_make_date',
        fieldLabel: 'Дата составления договора',
        format: 'Y-m-d',
        value: new Date(),
        allowBlank: false,
        anchor: '100%'
    });
    var makeDateField = new Ext.form.DateField({
        id: 'make_date',
        fieldLabel: 'Дата составления отчёта',
        format: 'Y-m-d',
        value: new Date(),
        allowBlank: false,
        anchor: '100%'
    });
    var hourCostField = new Ext.form.NumberField({
        id: 'hour_cost',
        fieldLabel: 'Оплата за один час',
        allowBlank: false,
        minValue: 0,
        anchor: '100%'
    });

    var contractsStore = new Ext.data.Store({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'teacher_d',
                'date',
                'start_date',
                'end_date',
                'payment',
                'document_id'
            ],
            root: 'contracts'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getteachercontractsforgrid/'
        })
    });

    var contractsGrid = new Ext.grid.GridPanel({
        store: contractsStore,
        anchor: '100%, 0%',
        height: 150,
        loadMask: true,
        sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
        layout: 'fit',
        stripeRows: true,
        autoExpandColumn: 'action',
        columns: [
            {header: 'Дата составления', dataIndex: 'date', sortable: true, width: 90},
            {header: 'Дата начала', dataIndex: 'start_date', sortable: true, width: 90},
            {header: 'Дата окончания', dataIndex: 'end_date', sortable: true, width: 90},
            {header: 'Оплата за час', dataIndex: 'payment', sortable: true, width: 70},
            {
                id: 'action',
                xtype: 'actioncolumn',
                width: 30,
                header: 'Просмотр',
                items: [
                    {
                        icon: '/images/icons/print.gif',
                        tooltip: 'Печать',
                        handler: function (gridObj, rowIndex, colIndex) {
                            var rec = gridObj.getStore().getAt(rowIndex);
                            location.href = '/storage/getdocument?did=' + rec.data.document_id;
                        }
                    }
                ]
            }
        ],
        listeners: {
            rowclick: function (panel, index, event) {
                var rec = panel.getSelectionModel().getSelected();
                contractMakeDateField.setValue(rec.data.date);
                startField.setValue(rec.data.start_date);
                endField.setValue(rec.data.end_date);
                hourCostField.setValue(rec.data.payment);
            }
        }
    });

    var reportForm = new Ext.form.FormPanel({
        id: 'teacher-report-attributes',
        padding: 5,
        url: '/print/teacherreport/',
        items: [
            teacherIdField,
            startField,
            endField,
            contractMakeDateField,
            makeDateField,
            hourCostField,
            contractsGrid
        ],
        buttons: [
            {
                text: 'Сформировать',
                plugins: 'defaultButton',
                handler: function () {
                    var form = reportForm.getForm();
                    if (form.isValid()) {
                        var ret = form.submit();
                        console.log(ret);
                        reportAttributes.close();
                    } else {
                        Ext.Msg.show({
                            title: 'Ошибка при валидации',
                            msg: 'В форму внесены некорректные данные',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                }
            },
            {
                text: 'Отмена',
                handler: function () {
                    reportAttributes.close();
                }
            }
        ]
    });

    reportForm.getForm().standardSubmit = true;

    var reportAttributes = new Ext.Window({
        title: 'Сформировать отчёт о выполнении учебной работы',
        modal: true,
        layout: 'anchor',
        width: 450,
        items: [
            reportForm
        ]
    });


    return reportAttributes;
}

function makeAuditoriesGridWindow(chairId) {
    var auditoriesStore = new Ext.data.Store({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'surname',
                'name',
                'patronymic'
            ],
            root: 'auditories'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getauditoriesforgrid/'
        })
    });

    var auditoriesGrid = new Ext.grid.GridPanel({
        store: auditoriesStore,
        anchor: '100%, 100%',
        height: 400,
        loadMask: true,
        sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
        columns: [
            {header: 'Наименование аудитории', dataIndex: 'name', sortable: true, width: 400},
            {
                xtype: 'actioncolumn',
                width: 40,
                header: 'Операции',
                items: [
                    {
                        icon: '/images/icons/edit.gif',
                        tooltip: 'Редактировать',
                        handler: function (gridObj, rowIndex, colIndex) {
                            var rec = gridObj.getStore().getAt(rowIndex);
                            var auditoryAttributes = makeAuditoryAttributesWindow(rec.data.id, chairId,
                                'Редактирование аудитории', auditoriesStore);
                            auditoryAttributes.show();
                        }
                    },
                    {
                        icon: '/images/icons/delete.gif',
                        tooltip: 'Удалить',
                        handler: function (gridObj, rowIndex, colIndex) {
                            var rec = gridObj.getStore().getAt(rowIndex);
                            Ext.Msg.confirm('Удаление аудитории', 'Удалить аудиторию "' + rec.data.name + '" ?',
                                function (btn) {
                                    if (btn == 'yes') {
                                        Ext.Ajax.request({
                                            url: '/json/deleteauditory/',
                                            method: 'POST',
                                            params: {aid: rec.data.id},
                                            success: function (response, opts) {
                                                auditoriesStore.load();
                                            },
                                            failure: function (response, opts) {
                                                Ext.Msg.show({
                                                    title: 'Ошибка при удалении',
                                                    msg: 'Не удалось удалить аудиторию',
                                                    buttons: Ext.MessageBox.OK,
                                                    icon: Ext.MessageBox.ERROR
                                                });
                                            }
                                        });
                                    }
                                });
                        }
                    }
                ]
            }
        ]
    });

    var auditoriesGridWindow = new Ext.Window({
        title: 'Список аудиторий',
        modal: true,
        layout: 'anchor',
        width: 660,
        items: [
            auditoriesGrid
        ],
        buttons: [
            {
                text: 'Добавить новую аудиторию',
                handler: function () {
                    var auditoryAttributes = makeAuditoryAttributesWindow(0, chairId, 'Добавление новой аудитории',
                        auditoriesStore);
                    auditoryAttributes.show();
                }
            },
            {
                text: 'Закрыть',
                handler: function () {
                    auditoriesGridWindow.close();
                }
            }
        ]
    });

    return auditoriesGridWindow;
}

function makeAuditoryAttributesWindow(auditoryId, chairId, windowTitle, gridStore) {
    var auditoryIdField = new Ext.form.Hidden({
        id: 'auditory-id',
        name: 'id',
        value: auditoryId
    });
    var nameField = new Ext.form.TextField({
        id: 'name',
        fieldLabel: 'Наименование',
        allowBlank: false,
        anchor: '100%'
    });

    var attributesForm = new Ext.form.FormPanel({
        id: 'auditory-attributes',
        padding: 5,
        url: '/json/saveauditorydata/',
        items: [
            auditoryIdField,
            nameField
        ],
        buttons: [
            {
                text: 'Сохранить',
                plugins: 'defaultButton',
                handler: function () {
                    var form = attributesForm.getForm();
                    if (form.isValid()) {
                        form.submit({
                            waitMsg: 'Сохранение данных об аудитории',
                            success: function (form, action) {
                                if (gridStore != null) {
                                    gridStore.load();
                                }
                                auditoryAttributes.close();
                            },
                            failure: function (form, action) {
                                Ext.Msg.show({
                                    title: 'Ошибка при сохранении',
                                    msg: action.result.error_message,
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.WARNING
                                });
                            }
                        })
                    } else {
                        Ext.Msg.show({
                            title: 'Ошибка при валидации',
                            msg: 'В форму внесены некорректные данные',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                }
            },
            {
                text: 'Отмена',
                handler: function () {
                    auditoryAttributes.close();
                }
            }
        ]
    });

    var attributesStore = new Ext.data.Store({
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'name'
            ],
            root: 'auditorydata'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getauditorydata/?aid=' + auditoryId
        }),
        listeners: {
            'load': function () {
                var rec = this.getAt(0);
                nameField.setValue(rec.data.name);
            }
        }
    });

    var wg = new Ext.WindowGroup();
    wg.zseed = 30000;

    var auditoryAttributes = new Ext.Window({
        title: windowTitle,
        modal: true,
        width: 483,
        manager: wg,
        items: [
            attributesForm
        ]
    });

    if (auditoryId != 0) {
        auditoryAttributes.on('show', function () {
            attributesStore.load();
        });
    }

    return auditoryAttributes;
}

/**
 * Создать окно для задания пареметров дополнительного занятия
 * @param lessonId идентификатор занятия
 * @param intensiveId идентификатор интенсива
 * @param chairId идентификатор кафедры
 * @param windowTitle заголовок окна
 * @param startTime начало по времени занятия
 * @param endTime конец по времени занятия
 * @param onSuccess функция-колбек
 * @returns {Ext.Window} окно
 */
function makeLessonAttributesWindow(lessonId, intensiveId, chairId, windowTitle, startTime, endTime, onSuccess) {
    var lessonIdField = new Ext.form.Hidden({
        id: 'lesson-id',
        name: 'id',
        value: lessonId
    });
    var intensiveIdField = new Ext.form.Hidden({
        id: 'intensive_id',
        value: intensiveId
    });
    var startField = new Ext.form.Hidden({
        id: 'start',
        allowBlank: false
    });
    var startTimeField = new Ext.form.TimeField({
        id: 'start_time',
        fieldLabel: 'Время начала',
        submitValue: false,
        allowBlank: false,
        value: startTime,
        anchor: '100%',
        increment: 10,
        format: 'H:i'
    });
    var endField = new Ext.form.Hidden({
        id: 'end',
        allowBlank: false
    });
    var endTimeField = new Ext.form.TimeField({
        id: 'end_time',
        fieldLabel: 'Время окончания',
        submitValue: false,
        allowBlank: false,
        value: endTime,
        anchor: '100%',
        increment: 10,
        format: 'H:i'
    });

    var themeIdField = new Ext.form.Hidden({
        id: 'theme_id',
        allowBlank: false
    });
    var themeField = new Ext.form.TriggerField({
        id: 'theme',
        fieldLabel: 'Тема',
        editable: false,
        submitValue: false,
        allowBlank: false,
        anchor: '100%'
    });
    themeField.onTriggerClick = function (ev) {
        var themeSelet = makeThemeSelectWindow(intensiveId, this, themeIdField);
        themeSelet.show();
    };

    var teacherIdField = new Ext.form.Hidden({
        id: 'teacher_id',
        allowBlank: false
    });
    var teacherField = new Ext.form.TriggerField({
        id: 'teacher',
        fieldLabel: 'Преподаватель',
        editable: false,
        submitValue: false,
        allowBlank: false,
        anchor: '100%'
    });
    teacherField.onTriggerClick = function (ev) {
        var teacherSelect = makeTeacherSelectWindow(chairId, this, teacherIdField);
        teacherSelect.show();
    };
    var auditoryIdField = new Ext.form.Hidden({
        id: 'auditory_id',
        allowBlank: false
    });
    var auditoryField = new Ext.form.TriggerField({
        id: 'auditory',
        fieldLabel: 'Аудитория',
        editable: false,
        submitValue: false,
        allowBlank: false,
        anchor: '100%'
    });
    auditoryField.onTriggerClick = function (ev) {
        var auditorySelect = makeAuditorySelectWindow(chairId, this, auditoryIdField);
        auditorySelect.show();
    };
    var statusField = new Ext.form.ComboBox({
        id: 'status_name',
        hiddenName: 'status',
        fieldLabel: 'Статус',
        mode: 'local',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: [
                'id',
                'statusText'
            ],
            data: [
                [0, 'Черновик'],
                [1, 'Утверждено'],
                [2, 'Проведено']
            ]
        }),
        valueField: 'id',
        displayField: 'statusText',
        value: 0,
        triggerAction: 'all',
        allowBlank: false,
        anchor: '100%'
    });
    var typeField = new Ext.form.ComboBox({
        id: 'type_name',
        hiddenName: 'type',
        fieldLabel: 'Тип занятия',
        mode: 'local',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: [
                'id',
                'statusText'
            ],
            data: [
                [0, 'Теоретическое'],
                [1, 'Практическое'],
                [2, 'Лекционно-практическое']
            ]
        }),
        valueField: 'id',
        displayField: 'statusText',
        value: 0,
        triggerAction: 'all',
        allowBlank: false,
        anchor: '100%'
    });

    var attributesStore = new Ext.data.Store({
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'intensive_id',
                'start',
                'end',
                'theme_id',
                'teacher_id',
                'auditory_id',
                'status',
                'type'
            ],
            root: 'lessondata'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getlessondata/?lid=' + lessonId
        }),
        listeners: {
            'load': function () {
                var rec = this.getAt(0);
                lessonIdField.setValue(rec.data.id);
                intensiveIdField.setValue(rec.data.intensive_id);
                startField.setValue(rec.data.start);
                endField.setValue(rec.data.end);
                themeIdField.setValue(rec.data.theme_id);
                teacherIdField.setValue(rec.data.teacher_id);
                auditoryIdField.setValue(rec.data.auditory_id);
                statusField.setValue(rec.data.status);
                typeField.setValue(rec.data.type);

                startTime = Date.parseDate(rec.data.start, "Y-m-d H:i:s");
                startTimeField.setValue(startTime);
                endTime = Date.parseDate(rec.data.end, "Y-m-d H:i:s");
                endTimeField.setValue(endTime);

                // Получение данных о преподавателе
                new Ext.data.Store({
                    autoLoad: true,
                    reader: new Ext.data.JsonReader({
                        fields: [
                            'id',
                            'surname',
                            'name',
                            'patronymic',
                            'login',
                            'password'
                        ],
                        root: 'teacherdata'
                    }),
                    proxy: new Ext.data.HttpProxy({
                        url: '/json/getteacherdata/?tid=' + rec.data.teacher_id
                    }),
                    listeners: {
                        'load': function () {
                            var teacherData = this.getAt(0);
                            teacherField.setValue(teacherData.data.surname + ' ' + teacherData.data.name + ' ' +
                                teacherData.data.patronymic);
                        }
                    }
                });

                // Получение данных об аудитории
                new Ext.data.Store({
                    autoLoad: true,
                    reader: new Ext.data.JsonReader({
                        fields: [
                            'id',
                            'name'
                        ],
                        root: 'auditorydata'
                    }),
                    proxy: new Ext.data.HttpProxy({
                        url: '/json/getauditorydata/?aid=' + rec.data.auditory_id
                    }),
                    listeners: {
                        'load': function () {
                            var rec = this.getAt(0);
                            auditoryField.setValue(rec.data.name);
                        }
                    }
                });

                // Получение ланных о теме дополнительного занятия
                new Ext.data.Store({
                    autoLoad: true,
                    reader: new Ext.data.JsonReader({
                        fields: [
                            'id',
                            'name'
                        ],
                        root: 'lessonthemedata'
                    }),
                    proxy: new Ext.data.HttpProxy({
                        url: '/json/getlessonthemedata/?ltid=' + rec.data.theme_id
                    }),
                    listeners: {
                        'load': function () {
                            var rec = this.getAt(0);
                            themeField.setValue(rec.data.name);
                        }
                    }
                });
            }
        }
    });

    var attributesForm = new Ext.form.FormPanel({
        id: 'lesson-attributes',
        padding: 5,
        url: '/json/savelessondata/',
        items: [
            lessonIdField,
            intensiveIdField,
            startField,
            startTimeField,
            endField,
            endTimeField,
            themeIdField,
            themeField,
            teacherIdField,
            teacherField,
            auditoryIdField,
            auditoryField,
            statusField,
            typeField
        ],
        buttons: [
            {
                text: 'Сохранить',
                plugins: 'defaultButton',
                handler: function () {
                    var form = attributesForm.getForm();
                    if (form.isValid()) {
                        var startDate = dateToString(startTime);
                        var endDate = dateToString(endTime);

                        startField.setValue(startDate + ' ' + startTimeField.value);
                        endField.setValue(endDate + ' ' + endTimeField.value);
                        form.submit({
                            waitMsg: 'Сохранение данных о занятии',
                            success: function (form, action) {
                                onSuccess(form, action);
                                lessonAttributes.close();
                            },
                            failure: function (form, action) {
                                Ext.Msg.show({
                                    title: 'Ошибка при сохранении',
                                    msg: action.result.error_message,
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.WARNING
                                });
                            }
                        });
                    } else {
                        Ext.Msg.show({
                            title: 'Ошибка при валидации',
                            msg: 'В форму внесены некорректные данные',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                }
            },
            {
                text: 'Отмена',
                handler: function () {
                    lessonAttributes.close();
                }
            }
        ]
    });

    var lessonAttributes = new Ext.Window({
        title: windowTitle,
        modal: true,
        width: 600,
        items: [
            attributesForm
        ]
    });

    if (lessonId != 0) {
        lessonAttributes.on('show', function () {
            attributesStore.load();
        });
    }

    return lessonAttributes;
}

/**
 * Создать окно выбора темы занятия
 * @param intensiveId идентификатор интенсива
 * @param themeField поля с именем темы
 * @param themeIdField поле для задания идентификатора темы
 * @returns {Ext.Window} окно
 */
function makeThemeSelectWindow(intensiveId, themeField, themeIdField) {
    var store = new Ext.data.Store({
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'name'
            ],
            root: 'themes'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getintensivethemes/?iid=' + intensiveId
        }),
        autoLoad: true
    });
    var grid = new Ext.grid.GridPanel({
        store: store,
        anchor: '100%, 100%',
        height: 400,
        loadMask: true,
        sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
        columns: [
            {id: 'theme', header: 'Тема', dataIndex: 'name', sortable: true, width: 150}
        ],
        autoExpandColumn: 'theme'
    });
    var wnd = new Ext.Window({
        title: 'Выбор темы',
        modal: true,
        layout: 'anchor',
        width: 470,
        items: [
            grid
        ],
        buttons: [
            {
                text: 'Выбрать',
                handler: function () {
                    var rec = grid.getSelectionModel().getSelected();
                    if (rec) {
                        themeField.setValue(rec.data.name);
                        themeIdField.setValue(rec.data.id);
                        wnd.close();
                    }
                }
            }
        ]
    });

    return wnd;
}

/**
 * Создать окно для выбора преподавателя
 * @param chairId ижентификатор кафедры
 * @param teacherField поле с именем преподавателя
 * @param teacherIdField поле для задания идентификатором преподавателя
 * @returns {Ext.Window} окно
 */
function makeTeacherSelectWindow(chairId, teacherField, teacherIdField) {
    var teachersStore = new Ext.data.Store({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'surname',
                'name',
                'patronymic'
            ],
            root: 'teachers'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getteachersforgrid/'
        })
    });

    var teachersGrid = new Ext.grid.GridPanel({
        store: teachersStore,
        anchor: '100%, 100%',
        height: 400,
        loadMask: true,
        sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
        columns: [
            {header: 'Фамилия', dataIndex: 'surname', sortable: true, width: 150},
            {header: 'Имя', dataIndex: 'name', sortable: true, width: 150},
            {header: 'Отчество', dataIndex: 'patronymic', sortable: true, width: 150}
        ]
    });
    teachersGrid.on('rowdblclick', function (gridObj, rowIndex, e) {
        var rec = gridObj.getStore().getAt(rowIndex);
        teacherField.setValue(rec.data.surname + ' ' + rec.data.name + ' ' + rec.data.patronymic);
        teacherIdField.setValue(rec.data.id);
        teacherSelectWnd.close();
    });

    var teacherSelectWnd = new Ext.Window({
        title: 'Выбор преподавателя',
        modal: true,
        layout: 'anchor',
        width: 470,
        items: [
            teachersGrid
        ],
        buttons: [
            {
                text: 'Добавить нового преподавателя',
                handler: function () {
                    var teacherAttributes = makeTeacherAttributesWindow(0, chairId, 'Добавление нового преподавателя',
                        teachersStore);
                    teacherAttributes.show();
                }
            },
            {
                text: 'Выбрать',
                handler: function () {
                    var rec = teachersGrid.getSelectionModel().getSelected();
                    teacherField.setValue(rec.data.surname + ' ' + rec.data.name + ' ' + rec.data.patronymic);
                    teacherIdField.setValue(rec.data.id);
                    teacherSelectWnd.close();
                }
            }
        ]
    });

    return teacherSelectWnd;
}

/**
 * Создать окно для выбора преподавателя
 * @param chairId ижентификатор кафедры
 * @param auditoryField поле с именем преподавателя
 * @param auditoryIdField поле для задания идентификатора аудитория
 * @returns {Ext.Window} окно
 */
function makeAuditorySelectWindow(chairId, auditoryField, auditoryIdField) {
    var auditoriesStore = new Ext.data.Store({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'name'
            ],
            root: 'auditories'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getauditoriesforgrid/'
        })
    });

    var auditoriesGrid = new Ext.grid.GridPanel({
        store: auditoriesStore,
        anchor: '100%, 100%',
        height: 400,
        loadMask: true,
        sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
        columns: [
            {header: 'Наименование', dataIndex: 'name', sortable: true, width: 350}
        ]
    });
    auditoriesGrid.on('rowdblclick', function (gridObj, rowIndex) {
        var rec = gridObj.getStore().getAt(rowIndex);
        auditoryField.setValue(rec.data.name);
        auditoryIdField.setValue(rec.data.id);
        auditorySelectWnd.close();
    });

    var wg = new Ext.WindowGroup();
    wg.zseed = 20000;

    var auditorySelectWnd = new Ext.Window({
        title: 'Выбор аудитории',
        modal: true,
        layout: 'anchor',
        width: 470,
        manager: wg,
        items: [
            auditoriesGrid
        ],
        buttons: [
            {
                text: 'Добавить новую аудиторию',
                handler: function () {
                    var auditoryAttributes = makeAuditoryAttributesWindow(0, chairId, 'Добавление новой аудитории',
                        auditoriesStore);
                    auditoryAttributes.show();
                }
            },
            {
                text: 'Выбрать',
                handler: function () {
                    var rec = auditoriesGrid.getSelectionModel().getSelected();
                    auditoryField.setValue(rec.data.name);
                    auditoryIdField.setValue(rec.data.id);
                    auditorySelectWnd.close();
                }
            }
        ]
    });

    return auditorySelectWnd;
}

/**
 * Создать окно для задания слушателей на занятие
 * @param lessonId идентификатор занятия
 * @returns {Ext.Window} окно
 */
function makeListenersLessonStaffWindow(lessonId) {
    var staffStore = new Ext.data.Store({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            fields: [
                'listener_id',
                'surname',
                'name',
                'patronymic',
                'group'
            ],
            root: 'presences'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getlessonpresence/?lid=' + lessonId
        })
    });

    var staffGrid = new Ext.grid.GridPanel({
        store: staffStore,
        anchor: '100%, 100%',
        height: 400,
        loadMask: true,
        sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
        columns: [
            {header: 'Фамилия', dataIndex: 'surname', sortable: true, width: 100},
            {header: 'Имя', dataIndex: 'name', sortable: true, width: 100},
            {header: 'Отчество', dataIndex: 'patronymic', sortable: true, width: 100},
            {header: 'Группа', dataIndex: 'group', sortable: true},
            {
                xtype: 'actioncolumn',
                width: 26,
                header: 'Операции',
                items: [
                    {
                        icon: '/images/icons/delete.gif',
                        tooltip: 'Удалить',
                        handler: function (gridObj, rowIndex, colIndex) {
                            var rec = gridObj.getStore().getAt(rowIndex);
                            Ext.Msg.confirm('Удаление слушателя с занятия', 'Удалить слушателя "' + rec.data.surname +
                                ' ' + rec.data.name + ' ' + rec.data.patronymic + '" с занятия ?', function (btn) {
                                if (btn == 'yes') {
                                    Ext.Ajax.request({
                                        url: '/json/deletelistenerfromlesson/',
                                        method: 'POST',
                                        params: {
                                            listener_id: rec.data.listener_id,
                                            lesson_id: lessonId
                                        },
                                        success: function (response, opts) {
                                            staffStore.load();
                                        },
                                        failure: function (response, opts) {
                                            Ext.Msg.show({
                                                title: 'Ошибка при удалении',
                                                msg: 'Не удалось удалить слушателя с занятия',
                                                buttons: Ext.MessageBox.OK,
                                                icon: Ext.MessageBox.ERROR
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    }
                ]
            }
        ]
    });

    var staffWnd = new Ext.Window({
        title: 'Состав слушателей',
        modal: true,
        layout: 'anchor',
        width: 470,
        items: [
            staffGrid
        ],
        buttons: [
            {
                text: 'Добавить слушателя на занятие',
                handler: function () {
                    var addListenerOnLesson = makeListenerToLessonWindow(lessonId, staffStore);
                    addListenerOnLesson.show();
                }
            },
            {
                text: 'Закрыть',
                handler: function () {
                    staffWnd.close();
                }
            }
        ]
    });

    return staffWnd;
}

/**
 * Создать окно по добавлению слушателей на занятие
 * @param lessonId идентификатор занятия
 * @param staffStore стор персонала
 * @returns {Ext.Window} окно
 */
function makeListenerToLessonWindow(lessonId, staffStore) {
    var listenersStore = new Ext.data.Store({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'surname',
                'name',
                'patronymic',
                'group',
                'themes'
            ],
            root: 'listeners'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getlistenersforaddingtolesson/?lid=' + lessonId
        })
    });

    var listenersGrid = new Ext.grid.GridPanel({
        id: 'addlistener-grid',
        store: listenersStore,
        anchor: '100%, 100%',
        height: 400,
        loadMask: true,
        sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
        columns: [
            {header: 'Фамилия', dataIndex: 'surname', sortable: true, width: 100},
            {header: 'Имя', dataIndex: 'name', sortable: true, width: 100},
            {header: 'Отчество', dataIndex: 'patronymic', sortable: true, width: 100},
            {header: 'Группа', dataIndex: 'group', sortable: true, width: 75},
            {header: 'Желаемые темы', dataIndex: 'themes', sortable: true, width: 200}
        ]
    });

    function addListenerToLesson() {
        var rec = listenersGrid.getSelectionModel().getSelected();
        Ext.Ajax.request({
            url: '/json/addlistenertolesson/',
            method: 'POST',
            params: {
                lesson_id: lessonId,
                listener_id: rec.data.id
            },
            success: function (response, opts) {
                staffStore.load();
                listenersWnd.close();
            },
            failure: function (response, opts) {
                Ext.Msg.show({
                    title: 'Ошибка при добавлении слушателя на занятие',
                    msg: 'Не удалось произвести добавление слушателя на занятие',
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.ERROR
                });
            }
        });
    }

    listenersGrid.on('rowdblclick', function (gridObj, rowIndex, e) {
        addListenerToLesson();
    });

    var listenersWnd = new Ext.Window({
        title: 'Запись слушателя на занятие',
        modal: true,
        layout: 'anchor',
        width: 600,
        items: [
            listenersGrid
        ],
        buttons: [
            {
                text: 'Добавить',
                handler: function () {
                    addListenerToLesson();
                }
            },
            {
                text: 'Отмена',
                handler: function () {
                    listenersWnd.close();
                }
            }
        ]
    });

    return listenersWnd;
}

function makeLessonPresenceWindow(lessonId) {
    var presencesStore = new Ext.data.Store({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            fields: [
                'listener_id',
                'surname',
                'name',
                'patronymic',
                'group',
                {name: 'presented', type: 'bool'}
            ],
            root: 'presences'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getlessonpresence/?lid=' + lessonId
        })
    });

    var presencesGrid = new Ext.grid.GridPanel({
        store: presencesStore,
        anchor: '100%, 100%',
        height: 400,
        loadMask: true,
        sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
        columns: [
            {header: 'Фамилия', dataIndex: 'surname', sortable: true, width: 100},
            {header: 'Имя', dataIndex: 'name', sortable: true, width: 100},
            {header: 'Отчество', dataIndex: 'patronymic', sortable: true, width: 100},
            {header: 'Группа', dataIndex: 'group', sortable: true, width: 75},
            {header: 'Присутствовал', dataIndex: 'presented', sortable: true, width: 50, xtype: 'checkcolumn'}
        ]
    });

    var presencesWnd = new Ext.Window({
        title: 'Отметка посещаемости занятия',
        modal: true,
        layout: 'anchor',
        width: 470,
        items: [
            presencesGrid
        ],
        buttons: [
            {
                text: 'Сохранить',
                handler: function () {
                    var presencesStates = [];
                    presencesStore.each(function (rec) {
                        presencesStates.push({
                            id: rec.data.listener_id,
                            presented: rec.data.presented
                        });
                    });
                    Ext.Ajax.request({
                        url: '/json/checklessonpresence/',
                        method: 'POST',
                        params: {
                            lid: lessonId,
                            states: Ext.util.JSON.encode(presencesStates)
                        },
                        success: function (response, opts) {
                            Ext.Msg.show({
                                title: 'Успех при отметке посещаемости',
                                msg: 'Отметка посещений произведена успешно',
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.INFO
                            });
                            presencesWnd.close();
                        },
                        failure: function (response, opts) {
                            Ext.Msg.show({
                                title: 'Ошибка при отметке посещений',
                                msg: 'Не удалось произвести отметку посещаемости занятия',
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        }
                    });
                }
            },
            {
                text: 'Отмена',
                handler: function () {
                    presencesWnd.close();
                }
            }
        ]
    });

    return presencesWnd;
}

function makeAddListenerToLessonWindow(onSelect) {
    var listenersStore = new Ext.data.Store({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            fields: [
                'surname',
                'name',
                'patronymic',
                'group',
                'themes'
            ],
            root: 'listeners'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getlistenersforaddingtolesson/'
        })
    });

    var listenersGrid = new Ext.grid.GridPanel({
        store: listenersStore,
        anchor: '100%, 100%',
        height: 400,
        loadMask: true,
        sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
        columns: [
            {header: 'Фамилия', dataIndex: 'surname', sortable: true, width: 100},
            {header: 'Имя', dataIndex: 'name', sortable: true, width: 100},
            {header: 'Отчество', dataIndex: 'patronymic', sortable: true, width: 100},
            {header: 'Группа', dataIndex: 'group', sortable: true, width: 75},
            {header: 'Желаемые темы', dataIndex: 'themes', sortable: true, width: 200}
        ]
    });

    var listenersWnd = new Ext.Window({
        title: 'Добавление слушателя на занятие',
        modal: true,
        layout: 'anchor',
        width: 600,
        items: [
            listenersGrid
        ],
        buttons: [
            {
                text: 'Выбрать',
                handler: function () {
                    var rec = listenersGrid.getSelectionModel().getSelected();
                    onSelect(rec);
                }
            },
            {
                text: 'Отмена',
                handler: function () {
                    listenersWnd.close();
                }
            }
        ]
    });

    return listenersWnd;
}

function makeDateRangeSelectWindow(windowTitle, onSubmit) {
    var startField = new Ext.form.DateField({
        id: 'start',
        fieldLabel: 'Начало',
        format: 'Y-m-d',
        allowBlank: false,
        value: Ext.state.Manager.get('contractStartDate'),
        anchor: '100%',
        vtype: 'daterange',
        endDateField: 'end'
    });
    var endField = new Ext.form.DateField({
        id: 'end',
        fieldLabel: 'Конец',
        format: 'Y-m-d',
        allowBlank: false,
        value: Ext.state.Manager.get('contractEndDate'),
        anchor: '100%',
        vtype: 'daterange',
        startDateField: 'start'
    });

    var dateRangeSelectForm = new Ext.form.FormPanel({
        padding: 5,
        items: [
            startField,
            endField
        ],
        buttons: [
            {
                text: 'Сформировать',
                plugins: 'defaultButton',
                handler: function () {
                    var form = dateRangeSelectForm.getForm();
                    if (form.isValid()) {
                        onSubmit(startField.getValue(), endField.getValue());
                    } else {
                        Ext.Msg.show({
                            title: 'Ошибка при валидации',
                            msg: 'В форму внесены некорректные данные',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                }
            },
            {
                text: 'Отмена',
                handler: function () {
                    dateRangeSelectWindow.close();
                }
            }
        ]
    });

    var dateRangeSelectWindow = new Ext.Window({
        title: windowTitle,
        modal: true,
        width: 300,
        items: [
            dateRangeSelectForm
        ]
    });

    return dateRangeSelectWindow;
}

function makeDuplicateEventWindow(windowTitle, onSubmit) {
    var periodField = new Ext.form.NumberField({
        id: 'period',
        fieldLabel: 'Период (в днях)',
        allowBlank: false,
        anchor: '100%',
        minValue: 0
    });
    var repeatCountField = new Ext.form.NumberField({
        id: 'repeat_count',
        fieldLabel: 'Количество повторений',
        allowBlank: false,
        anchor: '100%',
        minValue: 0
    });

    var duplicateForm = new Ext.form.FormPanel({
        padding: 5,
        items: [
            periodField,
            repeatCountField
        ],
        buttons: [
            {
                text: 'Дублировать',
                plugins: 'defaultButton',
                handler: function () {
                    var form = duplicateForm.getForm();
                    if (form.isValid()) {
                        onSubmit(periodField.getValue(), repeatCountField.getValue());
                        duplicateWindow.close();
                    } else {
                        Ext.Msg.show({
                            title: 'Ошибка при валидации',
                            msg: 'В форму внесены некорректные данные',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                }
            },
            {
                text: 'Отмена',
                handler: function () {
                    duplicateWindow.close();
                }
            }
        ]
    });

    var duplicateWindow = new Ext.Window({
        title: windowTitle,
        modal: true,
        width: 300,
        items: [
            duplicateForm
        ]
    });

    return duplicateWindow;
}

/**
 * Создать окно для показа загрузки файлов расписания
 * @param chairId идентификатор кафедры
 * @param scheduleStore хранилище расписания
 * @returns {Ext.Window} созданное окно
 */
function makeScheduleFileUploadWindow(chairId, scheduleStore) {
    var uploadField = new Ext.form.FieldSet({
        xtype: 'fieldset',
        title: 'Файлы расписания',
        autoHeight: true,
        layout: 'anchor',
        items: [
            {
                xtype: 'fileuploadfield',
                name: 'filedata',
                anchor: '100%, 0%',
                emptyText: 'Выберите xls файл для загрузки...',
                fieldLabel: 'File',
                buttonText: 'Обзор'
            }
        ]
    });
    var count = 0;
    var uploadForm = new Ext.FormPanel({
        fileUpload: true,
        multiple: true,
        anchor: '100%, 100%',
        autoHeight: true,
        bodyStyle: 'padding: 10px 10px 10px 10px;',
        labelWidth: 50,
        defaults: {
            anchor: '95%',
            allowBlank: false,
            msgTarget: 'side'
        },
        items: [
            uploadField
        ],
        buttons: [
            {
                text: 'Добавить еще один файл',
                handler: function () {
                    count++;
                    uploadField.add(
                        {
                            xtype: 'fileuploadfield',
                            name: 'filedata' + count,
                            anchor: '100%, 0%',
                            emptyText: 'Выберите xls файл для загрузки...',
                            fieldLabel: 'File',
                            buttonText: 'Обзор'
                        });
                    uploadField.doLayout();
                }
            },
            {
                text: 'Загрузить',
                handler: function () {
                    if (uploadForm.getForm().isValid()) {
                        uploadField.items.each(function (item) {
                            if (item.getValue() == '') {
                                uploadField.remove(item, true);
                            }
                        });
                        uploadForm.getForm().submit({
                            url: '/schedule/scheduleupload/',
                            waitMsg: 'Загрузка файлов...',
                            success: function (form, action) {
                                uploadWindow.close();

                                var msgBx = Ext.MessageBox.wait('Подождите пока составляется расписание...',
                                    'Составление кафедрального расписания');
                                Ext.Ajax.request({
                                    url: '/schedule/parseschedule/',
                                    method: 'POST',
                                    params: {
                                        cid: chairId
                                    },
                                    timeout: 300000, // 5 min
                                    success: function (response, opts) {
                                        if (scheduleStore) {
                                            scheduleStore.reload();
                                        }
                                        msgBx.hide();
                                    },
                                    failure: function (response, opts) {
                                        msgBx.hide();
                                        Ext.Msg.show({
                                            title: 'Ошибка при составление расписания',
                                            msg: 'Не удалось составить кафедральное расписание. Что-то пошло не так...',
                                            buttons: Ext.MessageBox.OK,
                                            icon: Ext.MessageBox.ERROR
                                        });
                                    }
                                });
                            },
                            failure: function (form, action) {
                                Ext.Msg.show({
                                    title: 'Загрузка не удалась',
                                    msg: 'Не удалось загрузить файлы, возможно вы указали не xls файлы?',
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.INFO
                                });
                            }
                        });
                    }
                }
            }
        ]
    });

    var uploadWindow = new Ext.Window({
        title: 'Составление кафедрального расписания из xls файлов',
        modal: true,
        width: 500,
        items: [
            uploadForm
        ]
    });
    return uploadWindow;
}

/**
 * Создать окно редактирования кафедрального расписания
 * @param chairId идентификатор расписания
 * @returns {Ext.Window} созданное окно
 */
function makeScheduleEditWindow(chairId) {
    var structRecord = Ext.data.Record.create([
        {name: 'id', type: 'int'},
        {name: 'week', type: 'int'},
        {name: 'day', type: 'string'},
        {name: 'pair', type: 'string'},
        {name: 'teacher', type: 'string'},
        {name: 'teacherId', type: 'int'},
        {name: 'lesson', type: 'string'},
        {name: 'lessonType', type: 'string'},
        {name: 'auditory', type: 'string'}
    ]);
    var proxy = new Ext.data.HttpProxy({
        url: '/schedule/getchairschedule/?cid=' + chairId
    });
    var store = new Ext.data.Store({
        proxy: proxy,
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            root: 'schedule',
            idProperty: 'id'
        }, structRecord)
    });

    Ext.grid.PivotGridView.prototype.getCellIndex = function (el) {
        var rowEl = Ext.fly(el).findParent('tr', 2, true);
        if (rowEl) {
            var i = 0;
            Ext.each(rowEl.query('td'), function (rowCell) {
                if (rowCell == el) {
                    return false;
                }
                i++;
                return true;
            });
            return i;
        }
        return false;
    };

    // Текущая выбранная запись в ячейке и ее индекс в хранилище
    var record = undefined;

    // Действия контекстного меню
    var addAction = {
        text: 'Добавить занятие',
        handler: function () {
            var wnd = makeEditVstuLessonWindow(chairId, 'Добавление вузовского занятия', {tableOpts: {grid: pivotGrid, rec: record}});
            wnd.show();
        }
    };
    var editAction = {
        text: 'Изменить занятие',
        handler: function () {
            var wnd = makeEditVstuLessonWindow(chairId, 'Изменение вузовского занятия', {tableOpts: {grid: pivotGrid, rec: record}});
            wnd.show();
        }
    };
    var deleteAction = {
        text: 'Удалить занятие',
        handler: function () {
            Ext.Msg.confirm('Удаление занятия', 'Вы точно хотите удалить вузовское занятие?',
                function (btn) {
                    if (btn == 'yes') {
                        Ext.Ajax.request({
                            url: '/schedule/deletevstulesson/',
                            method: 'POST',
                            params: { id: record.get('id') },
                            callback: function (ops, success, response) {
                                var j = JSON.parse(response.responseText);
                                if (success && j.success) {
                                    pivotGrid.store.reload();
                                } else {
                                    Ext.Msg.show({
                                        title: 'Ошибка при удалении занятия',
                                        msg: 'Не удалось удалить вузовское занятие. Что-то пошло не так ...',
                                        buttons: Ext.MessageBox.OK,
                                        icon: Ext.MessageBox.ERROR
                                    });
                                    Ext.MessageBox.getDialog().getEl().setStyle('z-index', '100000');
                                }
                            }
                        });
                    }
                }
            );
            Ext.MessageBox.getDialog().getEl().setStyle('z-index', '100000');
        }
    };
    var pivotGrid = new Ext.grid.PivotGrid({
        store: store,
        loadMask: true,
        autoScroll: true,
        tbar: [
            {
                text: 'Импортировать из xls файлов',
                handler: function () {
                    var wnd = makeScheduleFileUploadWindow(chairId, store);
                    wnd.show();
                }
            },
            {
                text: 'Экспортировать как xls файл',
                handler: function () {
                    Ext.Msg.show({
                        title: 'Ожидание',
                        msg: 'Процесс экспортирования может занимать несколко минут. Пожалуйста, подождите...',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    });
                    document.location = '/schedule/getxlsschedule/?cid=' + chairId;
                }
            }
        ],
        listeners: {
            cellcontextmenu: function (grid, row, col, e) {
                var recordIndex = row * grid.topAxis.getTuples().length + col;
                record = store.getAt(recordIndex);
                // Контекстное меню
                var menu = new Ext.menu.Menu({
                    items: (function () {
                        var items = [];
                        if (record.get('id') < 0) {
                            items.push(addAction);
                        } else {
                            items.push(editAction);
                            items.push(deleteAction);
                        }
                        return items;
                    })()
                });
                e.stopEvent();
                menu.showAt(e.getXY());
            }
        },
        sm: new Ext.grid.CellSelectionModel(),
        aggregator: function (records, measure) {
            var text = '';
            for (var i = 0; i < records.length; ++i) {
                var lesson = records[i].get('id') < 0 ? '</br>' : records[i].get('lesson');
                var auditory = records[i].get('auditory');
                var lessonType = records[i].get('lessonType');
                if (lessonType) {
                    text += lessonType;
                }
                text += lesson;
                if (auditory) {
                    text += '\n ауд. ' + auditory;
                }
                text += '\n';
            }
            return text;
        },
        stripeRows: true,
        columnLines: true,
        viewConfig: {
            forceFit: true
        },
        leftAxis: [
            {
                width: 10,
                direction: 'ASC',
                dataIndex: 'week'
            },
            {
                width: 90,
                direction: 'ASC',
                dataIndex: 'day'
            },
            {
                width: 60,
                direction: 'ASC',
                dataIndex: 'pair'
            }
        ],
        topAxis: [
            {
                direction: 'ASC',
                dataIndex: 'teacher'
            }
        ]
    });
    return new Ext.Window({
        title: 'Кафедральное расписание',
        modal: true,
        width: 900,
        height: 600,
        layout: 'fit',
        items: [
            pivotGrid
        ]
    });
}

/**
 * Создать окно редактирования вузовского занятия
 * @param windowTitle заголовок окна
 * @param options опции - { tableOpts: {grid, rec},
 *                          calendarOpts: {calendar, tid, lid, day, start, end} }
 * @param chairId идентификатор кафедры
 * @return {Ext.Window} окно
 */
function makeEditVstuLessonWindow(chairId, windowTitle, options) {
    var mapper = (function () {
        var map = [];
        map['1 пара'] = [1, 2];
        map['2 пара'] = [3, 4];
        map['3 пара'] = [5, 6];
        map['4 пара'] = [7, 8];
        map['5 пара'] = [9, 10];
        map['6 пара'] = [11, 12];
        map['1 Понедельник'] = 1;
        map['2 Вторник'] = 2;
        map['3 Среда'] = 3;
        map['4 Четверг'] = 4;
        map['5 Пятница'] = 5;
        map['6 Суббота'] = 6;
        return map;
    })();

    var opts = options.tableOpts || options.calendarOpts || {};
    if (opts.lid) {
        var lid = opts.lid;
    } else if (opts.rec) {
        lid = opts.rec.get('id') < 0 ? 0 : opts.rec.get('id');
    } else {
        lid = 0;
    }

    var formPanel = new Ext.FormPanel({
        labelWidth: 75,
        frame: true,
        bodyStyle: 'padding:5px 5px 0',
        width: 350,
        autoHeight: true,
        defaults: {width: 250},
        defaultType: 'textfield',
        items: [
            {
                xtype: 'hidden',
                name: 'lid',
                value: lid
            },
            {
                xtype: 'hidden',
                name: 'tid'
            },
            {
                xtype: 'hidden',
                name: 'nweek'
            },
            {
                xtype: 'hidden',
                name: 'nday'
            },
            {
                xtype: 'hidden',
                name: 'start'
            },
            {
                xtype: 'hidden',
                name: 'end'
            },
            {
                xtype: 'hidden',
                name: 'aid'
            },
            {
                xtype: 'combo',
                name: 'week',
                fieldLabel: 'Номер недели',
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    fields: [
                        'week'
                    ],
                    data: [
                        [1],
                        [2]
                    ]
                }),
                valueField: 'week',
                displayField: 'week',
                value: 1,
                triggerAction: 'all',
                allowBlank: false,
                listeners: {
                    select: function (combo, record, index) {
                        form.findField('nweek').setValue(record.get('week'));
                    }
                }
            },
            {
                xtype: 'combo',
                name: 'day',
                fieldLabel: 'День недели',
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    fields: [
                        'day',
                        'dayText'
                    ],
                    data: [
                        [ 1, 'Понедельник' ],
                        [ 2, 'Вторник' ],
                        [ 3, 'Среда' ],
                        [ 4, 'Четверг' ],
                        [ 5, 'Пятница' ],
                        [ 6, 'Суббота' ]
                    ]
                }),
                valueField: 'day',
                displayField: 'dayText',
                triggerAction: 'all',
                allowBlank: false,
                listeners: {
                    select: function (combo, record, index) {
                        form.findField('nday').setValue(record.get('day'));
                    }
                }
            },
            {
                xtype: 'combo',
                name: 'pair',
                fieldLabel: 'Пара',
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    fields: [
                        'pair',
                        'pairText'
                    ],
                    data: [
                        [ [1, 2], '1-2' ],
                        [ [3, 4], '3-4' ],
                        [ [5, 6], '5-6' ],
                        [ [7, 8], '7-8' ],
                        [ [9, 10], '9-10' ],
                        [ [11, 12], '11-12' ]
                    ]
                }),
                valueField: 'pair',
                displayField: 'pairText',
                triggerAction: 'all',
                allowBlank: false,
                listeners: {
                    select: function (combo, record, index) {
                        form.findField('start').setValue(record.get('pair')[0]);
                        form.findField('end').setValue(record.get('pair')[1]);
                    }
                }
            },
            {
                fieldLabel: 'Предмет',
                name: 'lesson',
                allowBlank: false
            },
            {
                xtype: 'combo',
                name: 'lessonType',
                fieldLabel: 'Тип занятия',
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    fields: [
                        'type',
                        'typeText'
                    ],
                    data: [
                        [1, 'Лекция'],
                        [2, 'Практика'],
                        [3, 'Лабораторная']
                    ]
                }),
                valueField: 'type',
                displayField: 'typeText',
                triggerAction: 'all',
                allowBlank: false
            },
            {
                xtype: 'trigger',
                fieldLabel: 'Аудитория',
                name: 'auditory',
                editable: false,
                allowBlank: false
            },
            {
                fieldLabel: 'Группы',
                name: 'groups',
                allowBlank: false
            },
            {
                xtype: 'datefieldplus',
                name: 'addDates',
                fieldLabel: 'По датам',
                showWeekNumber: true,
                noOfMonth: 3,
                noOfMonthPerRow: 1,
                useQuickTips: false,
                multiSelection: true
            }
        ],
        buttonAlign: 'center',
        buttons: [
            {
                text: 'ОК',
                handler: function () {
                    if (form.isValid()) {
                        var data = {};
                        data.teacherId = form.findField('tid').getValue();
                        data.lessonName = form.findField('lesson').getValue();
                        data.lessonStart = form.findField('start').getValue();
                        data.lessonEnd = form.findField('end').getValue();
                        data.auditoryId = form.findField('aid').getValue();
                        data.lessonType = form.findField('lessonType').getValue();
                        data.groups = form.findField('groups').getValue();
                        data.addDates = arrayDatesToString(form.findField('addDates').getValue());
                        data.day = form.findField('nday').getValue();
                        data.week = form.findField('nweek').getValue();

                        var id = form.findField('lid').getValue();
                        Ext.Ajax.request({
                            url: '/schedule/savevstulesson/',
                            method: 'POST',
                            params: {
                                id: id,
                                data: JSON.stringify(data)
                            },
                            callback: function (ops, success, response) {
                                var j = JSON.parse(response.responseText);
                                if (success && j.success) {
                                    if (options.tableOpts) {
                                        opts.grid.store.reload();
                                    } else if (options.calendarOpts) {
                                        opts.calendar.eventStore.reload();
                                    }
                                } else {
                                    Ext.Msg.show({
                                        title: 'Ошибка при сохранении занятия',
                                        msg: 'На данную пару уже назначено другое занятие.',
                                        buttons: Ext.MessageBox.OK,
                                        icon: Ext.MessageBox.ERROR
                                    });
                                    Ext.MessageBox.getDialog().getEl().setStyle('z-index', '100000');
                                }
                                wnd.close();
                            }
                        });
                    } else {
                        Ext.Msg.show({
                            title: 'Ошибка при валидации',
                            msg: 'Указаны не все поля для задания вузовского занятия.',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                        Ext.MessageBox.getDialog().getEl().setStyle('z-index', '100000');
                    }
                }
            },
            {
                text: 'Отмена',
                handler: function () {
                    wnd.close();
                }
            }
        ]
    });

    // Создания окна
    var wg = new Ext.WindowGroup();
    wg.zseed = 10000;

    var form = formPanel.getForm();
    var trigger = form.findField('auditory');
    var hidden = form.findField('aid');
    trigger.onTriggerClick = function (ev) {
        var auditorySelect = makeAuditorySelectWindow(chairId, trigger, hidden);
        auditorySelect.show();
    };

    var wnd = new Ext.Window({
        title: windowTitle,
        modal: true,
        autoHeight: true,
        autoWidth: true,
        layout: 'fit',
        manager: wg,
        items: [
            formPanel
        ],
        listeners: {
            show: function () {
                if (lid == 0) {
                    form.findField('tid').setValue(opts.tid || opts.rec.get('teacherId'));

                    var nweek = opts.rec == undefined ? 1 : opts.rec.get('week');
                    form.findField('nweek').setValue(nweek);
                    form.findField('week').setSelectedValue(nweek);

                    var nday = opts.day || mapper[opts.rec.get('day')];
                    form.findField('nday').setValue(nday);
                    form.findField('day').setSelectedValue(nday);

                    var start = opts.start || mapper[opts.rec.get('pair')][0];
                    var end = opts.end || mapper[opts.rec.get('pair')][1];
                    form.findField('start').setValue(start);
                    form.findField('end').setValue(end);
                    form.findField('pair').setSelectedValue([start, end]);

                    form.findField('lessonType').setSelectedValue(1);
                } else {
                    var proxy = new Ext.data.HttpProxy({
                        url: '/schedule/getvstulesson/?id=' + lid
                    });
                    var reader = new Ext.data.JsonReader({
                        root: 'lessondata',
                        fields: [
                            'id',
                            'teacherId',
                            'lessonStart',
                            'lessonEnd',
                            'lessonName',
                            'auditoryId',
                            'auditory',
                            'lessonType',
                            'addDates',
                            'groups',
                            'week',
                            'day'
                        ]
                    });

                    new Ext.data.Store({
                        reader: reader,
                        proxy: proxy,
                        autoLoad: true,
                        listeners: {
                            'load': function () {
                                var rec = this.getAt(0);
                                if (rec) {
                                    form.findField('tid').setValue(rec.get('teacherId'));

                                    form.findField('nweek').setValue(rec.get('week'));
                                    form.findField('week').setSelectedValue(rec.get('week'));

                                    form.findField('nday').setValue(rec.get('day'));
                                    form.findField('day').setSelectedValue(rec.get('day'));

                                    form.findField('start').setValue(rec.get('lessonStart'));
                                    form.findField('end').setValue(rec.get('lessonEnd'));
                                    form.findField('pair').setSelectedValue([rec.get('lessonStart'), rec.get('lessonEnd')]);

                                    form.findField('lesson').setValue(rec.get('lessonName'));

                                    form.findField('lessonType').setSelectedValue(rec.get('lessonType'));
                                    form.findField('auditory').setValue(rec.get('auditory'));
                                    form.findField('aid').setValue(rec.get('auditoryId'));
                                    form.findField('groups').setValue(rec.get('groups'));
                                    form.findField('addDates').setValue(stringDatesToArray(rec.get('addDates')));
                                }
                            }
                        }
                    });
                }
            }
        }
    });

    return wnd;
}

/**
 * Задает окно, содержащее группированную-таблицу и отображающее документы и архивы в хранилище для
 * указанного методиста
 * @param options объект задающий параметры для отображения:
 * options = {user: string, id: integer}
 * @return {Ext.Window} окно
 */
function makeStoredDocumentsGridWindow(options) {
    var jsonReader = new Ext.data.JsonReader({
        idProperty: 'id',
        fields: [
            'id',
            'name',
            'date',
            'category'
        ],
        root: 'documents'
    });

    var httpProxy = new Ext.data.HttpProxy({
        url: '/storage/getdocuments/?' + options.user + '=' + options.id
    });

    var groupStore = new Ext.data.GroupingStore({
        autoLoad: true,
        reader: jsonReader,
        sortInfo: {field: 'date', direction: "ASC"},
        groupField: 'category',
        proxy: httpProxy
    });

    var gridPanel = new Ext.grid.GridPanel({
        autodestroy: true,
        store: groupStore,
        columns: [
            {
                header: 'Наименование',
                width: 100,
                dataIndex: 'name',
                sortable: true,
                renderer: function (v, metaData, r, rowIndex, colIndex, store) {
                    var image = '/images/icons/odf.png';
                    if (r.data.category == 'Архив') {
                        image = '/images/icons/zip.png';
                    }

                    return '<span><img src="' + image + '"alt="" align="absmiddle" /> ' + v + '</span>';
                }
            },
            {header: 'Дата создания', width: 25, renderer: Ext.util.Format.dateRenderer('d/m/Y'), dataIndex: 'date', sortable: true},
            {
                header: 'Категория',
                dataIndex: 'category',
                hidden: true
            },
            {
                xtype: 'actioncolumn',
                width: 15,
                header: 'Операции',
                items: [
                    {
                        icon: '/images/icons/view.png',
                        tooltip: 'Просмотреть',
                        getClass: function (v, metaData, r) {
                            var s = 'x-hide-display';
                            if (r.data.category == 'Архив') {
                                s = 'x-grid-icon';
                            }
                            return s;
                        },
                        handler: function (gridObj, rowIndex, colIndex) {
                            var r = gridObj.getStore().getAt(rowIndex);
                            if (r.data.category == 'Архив') {
                                var wnd = makeViewArchiveWindow(r.data.name, r.data.id);
                                wnd.show();
                            }
                        }
                    },
                    {
                        icon: '/images/icons/download.png',
                        tooltip: 'Скачать',
                        handler: function (gridObj, rowIndex, colIndex) {
                            var r = gridObj.getStore().getAt(rowIndex);
                            location.href = '/storage/getdocument?did=' + r.data.id;
                        }
                    },
                    {
                        icon: '/images/icons/delete.gif',
                        tooltip: 'Удалить',
                        handler: function (gridObj, rowIndex, colIndex) {
                            var r = gridObj.getStore().getAt(rowIndex);

                            var title = 'Удаление документа';
                            var msg = 'Удалить документ "' + r.data.name + '"';
                            if (r.data.category == 'Архив') {
                                title = 'Удаление архива';
                                msg = 'Удалить архив "' + r.data.name + '"';
                            }
                            Ext.Msg.confirm(title, msg,
                                function (btn) {
                                    if (btn == 'yes') {
                                        Ext.Ajax.request({
                                            url: '/storage/deletedocument/',
                                            method: 'POST',
                                            params: {
                                                did: r.data.id,
                                                mid: options.user == 'mid' ? options.id : null,
                                                tid: options.user == 'tid' ? options.id : null
                                            },
                                            success: function (response, opts) {
                                                groupStore.reload();
                                            },
                                            failure: function (response, opts) {
                                                Ext.Msg.show({
                                                    title: 'Ошибка при удалении',
                                                    msg: 'Не удалось удалить документ',
                                                    buttons: Ext.MessageBox.OK,
                                                    icon: Ext.MessageBox.ERROR
                                                });
                                            }
                                        });
                                    }
                                }
                            );
                        }
                    }
                ]
            }
        ],
        view: new Ext.grid.GroupingView({
            forceFit: true,
            showGroupName: false,
            autoFill: true,
            startGroup: new Ext.XTemplate(
                '<div id="{groupId}" class="x-grid-group {cls}">',
                '<div id="{groupId}-hd" class="x-grid-group-hd" style="{style}"><div class="x-grid-group-title">',
                '<img src="/images/icons/folder.png" alt="" align="absmiddle" />',
                '{gvalue} ({[this.is(values.rs) == "Архив" ? this.archive(values.rs.length) : this.doc(values.rs.length)]})',
                '</div></div>',
                '<div id="{groupId}-bd" class="x-grid-group-body">',
                {
                    is: function (rs) {
                        return rs[0].get('category');
                    },
                    archive: function (v) {
                        return v + ' ' + num2str(v, ['архив', 'архива', 'архивов']);
                    },
                    doc: function (v) {
                        return v + ' ' + num2str(v, ['документ', 'документа', 'документов']);
                    }
                }
            )
        }),
        frame: true,
        sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
        layout: 'fit',
        autoWidth: true,
        loadMask: true,
        animCollapse: false,
        autoScroll: true,
        renderTo: document.body
    });

    return new Ext.Window({
        title: 'Хранилище документов и архивов',
        modal: true,
        layout: 'fit',
        height: 600,
        width: 800,
        items: [
            gridPanel
        ]
    });
}

/**
 * Задать окно архивирования документов
 * @param options объект задающий отображение нужных виджетов:
 * options = {user: string, id: integer}
 * @return {Ext.Window} окно
 */
function makeArchiveDocumentsWindow(options) {
    var idHidden = new Ext.form.Hidden({
        id: options.user,
        name: options.user,
        value: options.id
    });

    var yearField = new Ext.form.NumberField({
        id: 'year',
        name: 'year',
        fieldLabel: 'За год',
        style: '.x-form-num-field {text-align: right;}',
        allowBlank: false,
        blankText: 'Необходимо указать за какой год провести архивирование документов',
        allowDecimals: false,
        allowNegative: false,
        value: new Date().getFullYear(),
        minValue: 2000,
        width: '50px',
        anchor: '100%'
    });

    var documentsFieldSet = new Ext.form.FieldSet({
        id: 'documents',
        name: 'documents',
        title: 'Архивируемые документы',
        collapsible: false,
        autoHeight: true,
        autoWidth: true,
        defaultType: 'checkbox',
        defaults: {
            anchor: '100%'
        },
        items: (function () {
            var items = [];
            if (options.user === 'mid') {
                items.push({
                    id: 'listener_contract',
                    name: 'listener_contract',
                    hideLabel: true,
                    boxLabel: 'Договор об оказании дополнительных образовательных услуг',
                    checked: true
                });
                items.push({
                    id: 'listener_order',
                    name: 'listener_order',
                    hideLabel: true,
                    boxLabel: 'Приказ о зачислении слушателей',
                    checked: true
                });
                items.push({
                    id: 'flow_funds',
                    name: 'flow_funds',
                    hideLabel: true,
                    boxLabel: 'Отчет о движении денежных средств подразделения "Интенсив"',
                    checked: true
                });
                items.push({
                    id: 'listener_journal',
                    name: 'listener_journal',
                    hideLabel: true,
                    boxLabel: 'Журнал слушателей',
                    checked: true
                });
                items.push({
                    id: 'listener_list',
                    name: 'listener_list',
                    hideLabel: true,
                    boxLabel: 'Список слушателей',
                    checked: true
                });
            } else if (options.user === 'tid') {
                items.push({
                    id: 'teacher_contract',
                    name: 'teacher_contract',
                    hideLabel: true,
                    boxLabel: 'Договор на выполнении учебной работы',
                    checked: true
                });
                items.push({
                    id: 'teacher_report',
                    name: 'teacher_report',
                    hideLabel: true,
                    boxLabel: 'Отчет о выполненных учебных работах',
                    checked: true
                });
            }
            return items;
        }())
    });

    var archiveForm = new Ext.form.FormPanel({
        id: 'archive',
        padding: 5,
        width: 450,
        url: '/storage/archivedocuments/',
        items: [
            idHidden,
            yearField,
            documentsFieldSet
        ],
        buttonAlign: 'center',
        buttons: [
            {
                text: 'ОК',
                plugins: 'defaultButton',
                handler: function () {
                    var form = archiveForm.getForm();

                    var checkboxIsValid = function () {
                        for (var i = 0; i < documentsFieldSet.items.getCount(); i++) {
                            if (documentsFieldSet.items.itemAt(i).checked) {
                                return true;
                            }
                        }
                        return false;
                    };

                    if (form.isValid() && checkboxIsValid()) {
                        form.submit({
                            waitMsg: 'Составление архива документов за ' + yearField.getValue() + ' год.',
                            success: function (form, action) {
                                Ext.Msg.show({
                                    title: 'Архивация завершена',
                                    msg: 'Все указанные документы за ' + yearField.getValue() +
                                        ' год успешно заархивированы.',
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.INFO
                                });
                                archiveWindow.close();
                            },
                            failure: function (form, action) {
                                Ext.Msg.show({
                                    title: 'Архивация не завершена',
                                    msg: 'Не удалось заархивировать указанные документы. Возможно, в хранилище нет ' +
                                        'документов за ' + yearField.getValue() + ' год.',
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.WARNING
                                });
                            }
                        })
                    } else {
                        Ext.Msg.show({
                            title: 'Ошибка при валидации',
                            msg: 'Указаны некорректные данные.',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                }
            },
            {
                text: 'Отмена',
                handler: function () {
                    archiveWindow.close();
                }
            }
        ]
    });

    var archiveWindow = new Ext.Window({
        title: 'Архивация документов',
        modal: true,
        autoHeight: true,
        autoWidth: true,
        items: [
            archiveForm
        ]
    });

    return archiveWindow;
}

/**
 * Задать рабочую программу интенсива
 * @param workProgramFieldTrigger поле-триггер вызывающее данную процедуру
 * @param themesFiled поле задающее темы интенсива
 * @return {Ext.Window} окно
 */
function makeWorkProgramWindow(workProgramFieldTrigger, themesFiled) {
    var data = {
        'themes': themesFiled.getValue() ? JSON.parse(themesFiled.getValue()) : []
    };

    // Определение глобального уникального идентификатора
    var gid = (function () {
        var uid = 0;
        for (var i = 0; i < data.themes.length; i++) {
            uid = Math.max(uid, data.themes[i].id);
        }
    })();
    // Определение уникальности поля записи в хранилише
    var isUnique = function (field, value) {
        for (var i = 0; i < store.getCount(); i++) {
            if (value == store.getAt(i).get(field)) {
                return false;
            }
        }
        return true;
    };

    var reader = new Ext.data.JsonReader({
        root: 'themes'
    }, [
        {name: 'id'},
        {name: 'name'},
        {name: 'number'},
        // дополнительное поле, необходимое для корректного задания темы интенсива (loaded, created, updated, deleted)
        {name: 'status'}
    ]);

    var store = new Ext.data.Store({
        id: 'themes_store',
        data: data,
        proxy: new Ext.data.MemoryProxy(),
        reader: reader,
        autoLoad: true,
        autoSave: true,
        autoDestroy: true
    });
    store.filterBy(function (rec) {
        return rec.data.status != 'deleted';
    });

    var editGrid = new Ext.grid.EditorGridPanel({
        store: store,
        anchor: '100%, 100%',
        height: 400,
        loadMask: true,
        sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
        autoExpandColumn: 'themename',
        columns: [
            {
                header: 'Номер',
                dataIndex: 'number',
                sortable: true,
                editor: {
                    xtype: 'numberfield',
                    allowBlank: false,
                    blankText: 'Не может быть пустым'
                }
            },
            {
                id: 'themename',
                header: 'Тема',
                dataIndex: 'name',
                sortable: true,
                editor: {
                    xtype: 'textfield',
                    allowBlank: false,
                    blankText: 'Не может быть пустым'
                }
            }
        ],
        tbar: [
            {
                text: 'Добавить',
                handler: onAdd
            },
            '-',
            {
                text: 'Удалить',
                handler: onDelete
            },
            '-'
        ],
        viewConfig: {
            forceFit: true
        }
    });
    // Обработка события - валидация редактирования
    editGrid.on('validateedit', onValidateEdit, this);
    function onValidateEdit(e) {
        if (!isUnique(e.field, e.value)) {
            e.cancel = true;

            var msg = e.field == 'number' ? 'Тема с таким номером уже имеется' : 'Такая тема уже имеется';
            Ext.Msg.alert('Некорректные данные', msg);
        }
    }

    // Обработка события - после редактирования
    editGrid.on('afteredit', onAfterEdit, this);
    function onAfterEdit(e) {
        // есди было изменено значение ячейки и статус ячейки загружен
        if (e.value != e.originalValue && e.record.data.status == 'loaded') {
            // статус - обновлено
            e.record.data.status = 'updated';
        }
        // закомитить
        e.record.commit();
    }

    // Обработка добавления записи в таблицу
    function onAdd(btn, ev) {
        var rec = new store.recordType({
            id: ++gid,
            number: store.getCount() + 1,
            name: 'Тема №' + (store.getCount() + 1),
            status: 'created'
        });
        editGrid.stopEditing();
        store.add(rec);
        editGrid.getSelectionModel().selectLastRow();
        editGrid.startEditing(store.getCount() - 1, 1);
    }

    // Обработка удаления записи из таблицы
    function onDelete(btn, ev) {
        var rec = editGrid.getSelectionModel().getSelected();
        if (!rec) {
            return;
        }
        // Если запись была только создана
        if (rec.data.status == 'created') {
            // удаляем запись
            store.remove(rec);
        }
        // Если запись была загружена
        else if (rec.data.status == 'loaded') {
            // получение ее индекса и сокрытие
            var index = store.indexOf(rec);
            editGrid.getView().getRow(index).style.display = 'none';
            // установление статуса как удаленный
            rec.data.status = 'deleted';
            rec.commit();
        }
    }

    var wnd = new Ext.Window({
        title: 'Рабочая программа интенсива',
        modal: true,
        layout: 'anchor',
        width: 400,
        items: [
            editGrid
        ],
        buttons: [
            {
                text: 'ОК',
                handler: function () {
                    // получение всех строк хранилища
                    var themes = [];
                    var realCount = 0;
                    for (var i = 0; i < store.getCount(); i++) {
                        var rec = store.getAt(i);
                        themes.push(rec.data);
                        if (rec.data.status != 'deleted') {
                            realCount++;
                        }
                    }
                    // установление заголовка программы
                    workProgramFieldTrigger.setValue('Рабочая программа (' + realCount + ' тем.)');
                    // установелние значения скрытого поля
                    themesFiled.setValue(JSON.stringify(themes));
                    wnd.close();
                }
            },
            {
                text: 'Отмена',
                handler: function () {
                    wnd.close();
                }
            }
        ]
    });

    return wnd;
}

/**
 * Создать окно для просмотра журнала слушателей
 * @param chairId идентификатор кафедры
 * @param methodistId идентификатор методиста
 * @param fromDate дата начала обучения
 * @param toDate дата окончания обучения
 * @return {Ext.Window} окно
 */
function makeJournalListenersWindow(chairId, methodistId, fromDate, toDate) {
    var reader = new Ext.data.JsonReader();

    var httpProxy = new Ext.data.HttpProxy({
        url: '/print/journallisteners/?cid=' + chairId + '&from=' + fromDate + '&to=' + toDate + '&mid=' + methodistId
    });

    var columns = [];
    var document = {};
    var store = new Ext.data.Store({
        id: 'store',
        proxy: httpProxy,
        reader: reader,
        autoLoad: true,
        listeners: {
            'metachange': function (scope, meta) {
                document['id'] = meta.documentId;

                var fields = meta.fields;
                for (var i = 0; i < fields.length; i++) {
                    var column = {};
                    if (fields[i].name == 'listener') {
                        column['id'] = fields[i].name;
                        column['header'] = 'Слушатель';
                        column['width'] = 100;
                        column['sortable'] = true;
                        column['dataIndex'] = fields[i].name;
                        column['hideable'] = false;
                        columns.push(column);
                    }
                    else if (fields[i].name == 'listenerPhone') {
                        column['id'] = fields[i].name;
                        column['header'] = 'Телефон';
                        column['dataIndex'] = fields[i].name;
                        columns.push(column);
                    }
                    else if (fields[i].name == 'note') {
                        column['id'] = fields[i].name;
                        column['header'] = 'Примечание';
                        column['sortable'] = false;
                        column['dataIndex'] = fields[i].name;
                        columns.push(column);
                    }
                    else if (fields[i].name.indexOf("theme") > -1) {
                        column['id'] = fields[i].name;
                        column['header'] = fields[i].name.substr(fields[i].name.length - 1);
                        column['dataIndex'] = fields[i].name;
                        column['tooltip'] = meta.fieldsTooltip[fields[i].name];
                        column['renderer'] = function (value, metaData, record, rowIndex, colIndex, store, view) {
                            var data = record.get(metaData.id);
                            var html = '';
                            for (var j = 0; j < data.length; j++) {
                                html += '<b>' + data[j].status + '</b>';
                                if (data[j].type) {
                                    html += ' (' + data[j].type + ')';
                                }
                                if (j != data.length - 1) {
                                    html += '</br>';
                                }
                            }

                            return html;
                        };
                        columns.push(column);
                    }
                }
            }
        }
    });

    var grid = new Ext.grid.GridPanel({
        store: store,
        anchor: '100%, 100%',
        loadMask: true,
        autoWidth: true,
        sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
        colModel: new Ext.grid.ColumnModel({
            columns: columns,
            defaults: {
                sortable: true,
                hideable: true,
                width: 100,
                flex: 1
            }
        }),
        viewConfig: {
            forceFit: true
        },
        frame: true,
        autoScroll: true,
        columnLines: true
    });

    return new Ext.Window({
        title: 'Журнал слушателей от ' + fromDate + ' до ' + toDate,
        modal: true,
        layout: 'fit',
        width: 800,
        height: 400,
        items: [
            grid
        ],
        buttons: [
            {
                text: 'Печать',
                handler: function () {
                    if (document.id) {
                        location.href = '/storage/getdocument?did=' + document.id;
                    }
                }
            }
        ]
    });
}

/**
 * Создать окно для просмотра содержимого архива
 * @param windowTitle заголовок окна
 * @param archiveId идентификатор архива
 * @return {Ext.Window} окно
 */
function makeViewArchiveWindow(windowTitle, archiveId) {
    var jsonReader = new Ext.data.JsonReader({
        idProperty: 'document',
        fields: [
            'document',
            'category'
        ],
        root: 'archive'
    });

    var httpProxy = new Ext.data.HttpProxy({
        url: '/storage/getlistarchiveddocuments/?aid=' + archiveId
    });

    var groupStore = new Ext.data.GroupingStore({
        autoLoad: true,
        reader: jsonReader,
        groupField: 'category',
        proxy: httpProxy
    });

    var gridPanel = new Ext.grid.GridPanel({
        autodestroy: true,
        store: groupStore,
        columns: [
            {
                header: 'Документ',
                xtype: 'templatecolumn',
                flex: 1,
                tpl: '<img src="/images/icons/odf.png" style="cursor:pointer;"> {document}'
            },
            {
                header: 'Категория',
                dataIndex: 'category',
                hidden: true,
                groupRenderer: function (v, unused, r, rowIndex, colIndex, ds) {
                    return '<span><img src="/images/icons/folder.png" alt="" align="absmiddle" /> ' + v + '</span>';
                }
            },
            {
                xtype: 'actioncolumn',
                width: 10,
                header: '',
                items: [
                    {
                        icon: '/images/icons/download.png',
                        tooltip: 'Скачать',
                        handler: function (gridObj, rowIndex, colIndex) {
                            var rec = gridObj.getStore().getAt(rowIndex);
                            location.href = '/storage/getarchiveddocument?aid=' + archiveId + '&name='
                                + encodeURI(rec.data.category + '/' + rec.data.document);
                        }
                    }
                ]
            }
        ],
        view: new Ext.grid.GroupingView({
            forceFit: true,
            showGroupName: false,
            autoFill: true
        }),
        tbar: [
            {
                text: 'Скачать архив',
                handler: function (btn, ev) {
                    location.href = '/storage/getdocument?did=' + archiveId;
                },
                scope: this
            }
        ],
        sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
        layout: 'fit',
        loadMask: true,
        frame: true,
        autoWidth: true,
        animCollapse: false,
        renderTo: document.body,
        autoScroll: true,
        hideHeaders: true
    });

    return new Ext.Window({
        title: windowTitle,
        layout: 'fit',
        modal: true,
        width: 500,
        height: 400,
        items: [
            gridPanel
        ]
    });
}

/*** Вспомогательные функции ***/

/**
 * Склонение числительных
 *
 * @param number количество
 * @param titles варианты склонений [для одного, для двух-четырёх, больше четырёх]
 * @return string
 */
function num2str(number, titles) {
    var cases = [2, 0, 1, 1, 1, 2];
    return titles[ (number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5] ];
}

/**
 * Перевести дату в строку (формат YYYY-MM-dd)
 * @param date дата
 * @returns {string} строка YYYY-MM-dd
 */
function dateToString(date) {
    var str = '' + date;
    if (date instanceof Date) {
        str = '' + date.getFullYear() + '-' + (date.getMonth() < 9 ? '0' + (date.getMonth() + 1) : date.getMonth()) +
            '-' + (date.getDate() < 10 ? '0' + date.getDate() : date.getDate());
    }
    return str;
}

/**
 * Перевести строку (в формате dd.MM.YY) в дату
 * @param str строка
 * @returns {Date} дата
 */
function stringToDate(str) {
    var pattern = /(\d{2})\.(\d{2})\.(\d{2}|d{4})/;
    return new Date(str.replace(pattern, '$3-$2-$1'));
}

/**
 * Перевод строки дат через запятую в массив дат
 * @param str строка
 * @returns {Array} массив дат
 */
function stringDatesToArray(str) {
    var arr = [];
    if (str) {
        arr = str.split(',');
        for (var i = 0; i < arr.length; i++) {
            arr[i] = stringToDate(arr[i]);
        }
    }
    return arr;
}
/**
 * Перевод массива дат в строковое представление через запятую
 * @param arr массив дат
 * @returns {string} строка
 */
function arrayDatesToString(arr) {
    var str = '';
    for (var i = 0; i < arr.length; i++) {
        str += dateToString(arr[i]);
        if (i != arr.length - 1) {
            str += ', ';
        }
    }
    return str;
}