var menutoolbar;
var itoolbar;
var calendar;
var workarea;

var teachersFilterStore;

var intensiveId = 0;
var teachersCalendarFilter = 0;
var auditoriesCalendarFilter = 0;
var otherIntensivesCalendarFilter = 0;

/**
 * Создание рабочей области кабинета методиста
 *
 * @param chairId идентификатор кафедры
 * @param methodistId индентификатор методиста
 *
 * @return Ext.Viewport рабочая область кабинета методиста
 */
function makeWorkArea(chairId, methodistId) {
    menutoolbar = makeMenuToolbar(chairId, methodistId);
    itoolbar = makeIntensivesToolbar();
    calendar = makeCalendarPanel(chairId);
    teachersFilterStore = new Ext.data.Store({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'fio'
            ],
            root: 'teachers'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getteachersforfilterpanel/'
        })
    });

    return new Ext.Viewport({
        layout: 'border',
        renderTo: Ext.getBody(),
        items: [
            {
                region: 'north',
                xtype: 'panel',
                items: [ menutoolbar, itoolbar ],
                autoHeight: true
            },
            {
                region: 'center',
                layout: 'fit',
                xtype: 'panel',
                items: [
                    calendar
                ]
            },
            {
                region: 'east',
                xtype: 'panel',
                layout: 'accordion',
                title: 'Фильтр календаря',
                collapsible: true,
                collapsed: true,
                split: true,
                width: 350,
                items: [
                    {
                        xtype: 'panel',
                        title: 'По преподавателям',
                        collapsed: true,
                        autoHeight: true,
                        items: [
                            new Ext.list.ListView({
                                store: teachersFilterStore,
                                emptyText: 'Нет преподавателей в базе',
                                singleSelect: true,
                                height: 200,
                                columns: [
                                    {
                                        header: 'ФИО',
                                        dataIndex: 'fio'
                                    }
                                ],
                                listeners: {
                                    click: function (dataView, index) {
                                        teachersCalendarFilter = dataView.store.getAt(index).data.id;
                                        refreshCalendar();
                                    }
                                }
                            })
                        ]
                    },
                    {
                        xtype: 'panel',
                        title: 'По аудиториям',
                        collapsed: true,
                        autoHeight: true,
                        html: 'Список с аудиториями'
                    },
                    {
                        xtype: 'panel',
                        title: 'По интенсивам',
                        collapsed: true,
                        autoHeight: true,
                        html: 'Список с интенсивами'
                    }
                ]
            },
            {
                region: 'south',
                xtype: 'panel',
                title: 'Сообщения системы',
                collapsible: true,
                collapsed: true,
                split: true,
                height: 200
            }
        ]
    });
}

function makeMenuToolbar(chairId, methodistId) {
    return new Ext.Toolbar({
        autoScroll: true,
        items: [
            {
                xtype: 'tbbutton',
                text: 'Слушатели',
                menu: [
                    {
                        text: 'Список слушателей',
                        handler: function () {
                            showListenersGridWindow(methodistId);
                        }
                    },
                    {
                        text: 'Добавить нового слушателя',
                        handler: function () {
                            var listenerAttributes = makeListenerAttributesWindow(0, 'Добавление нового слушателя', null);
                            listenerAttributes.show();
                        }
                    }
                ]
            },
            {
                xtype: 'tbbutton',
                text: 'Договоры',
                menu: [
                    {
                        text: 'Список договоров',
                        handler: function () {
                            showCotractsGridWindow(chairId, methodistId);
                        }
                    },
                    {
                        text: 'Оформить новый договор',
                        handler: function () {
                            var contractAttributes = makeContractAttributesWindow(0, chairId, methodistId,
                                'Оформление нового договора', null, false);
                            contractAttributes.show();
                        }
                    }
                ]
            },
            {
                xtype: 'tbbutton',
                text: 'Отчёты',
                menu: [
                    {
                        text: 'Сформировать приказ о зачислении слушателей на учебный курс «Интенсив»',
                        handler: function () {
                            var dateRangeSelectWnd = makeDateRangeSelectWindow('Формирование приказа о зачислении на период',
                                function (start, end) {
                                    location.href = '/print/printorder/?mid=' + methodistId + '&from=' +
                                        dateToString(start) + '&to=' + dateToString(end);
                                }
                            );
                            dateRangeSelectWnd.show();
                        }
                    },
                    {
                        text: 'Сформировать отчёт о движении денежных средств по подразделению «Интенсив»',
                        handler: function () {
                            var dateRangeSelectWnd = makeDateRangeSelectWindow('Формирование отчёта о движении денежных средств ' +
                                'по подразделению «Интенсив»',
                                function (start, end) {
                                    location.href = '/print/printflowfunds/?mid=' + methodistId + '&from=' +
                                        dateToString(start) + '&to=' + dateToString(end);
                                }
                            );
                            dateRangeSelectWnd.show();
                        }
                    },
                    {
                        text: 'Сформировать журнал слушателей',
                        handler: function() {
                            var dateRangeSelectWnd = makeDateRangeSelectWindow('Формирование журнала слушателей',
                                function (start, end) {
                                    dateRangeSelectWnd.close();

                                    var wndJournal = makeJournalListenersWindow(chairId, methodistId,
                                        dateToString(start), dateToString(end));
                                    wndJournal.show();
                                }
                            );
                            dateRangeSelectWnd.show();
                        }
                    }
                ]
            },
            {
                xtype: 'tbbutton',
                text: 'Преподаватели',
                menu: [
                    {
                        text: 'Список преподавателей',
                        handler: function () {
                            showTeachersGridWindow(chairId);
                        }
                    },
                    {
                        text: 'Добавить нового преподавателя',
                        handler: function () {
                            var teacherAttributes = makeTeacherAttributesWindow(0, chairId,
                                'Добавление нового преподавателя', null);
                            teacherAttributes.show();
                        }
                    },
                    {
                        text: 'Кафедральное расписание',
                        handler: function () {
                            showScheduleEditWindow(chairId);
                        }
                    }
                ]
            },
            {
                xtype: 'tbbutton',
                text: 'Аудитории',
                menu: [
                    {
                        text: 'Список аудиторий',
                        handler: function () {
                            showAuditoriesGridWindow(chairId);
                        }
                    },
                    {
                        text: 'Добавить новую аудиторию',
                        handler: function () {
                            var auditoryAttributes = makeAuditoryAttributesWindow(0, chairId, 'Добавление новой аудитории', null);
                            auditoryAttributes.show();
                        }
                    }
                ]
            },
            {
                xtype: 'tbbutton',
                text: 'Интенсивы',
                menu: [
                    {
                        text: 'Список интенсивов',
                        handler: function () {
                            showIntensivesGridWindow(chairId);
                        }
                    },
                    {
                        text: 'Добавить новый интенсив',
                        handler: function () {
                            var intensiveAttributes = makeIntensiveAttributesWindow(0, chairId,
                                'Добавление нового интенсива', null);
                            intensiveAttributes.show();
                        }
                    }
                ]
            },
            {
                xtype: 'tbbutton',
                text: 'Хранилище документов',
                menu: [
                    {
                        text: 'Архивировать',
                        handler: function () {
                            showArchiveDocumentsWindow(methodistId);
                        }
                    },
                    {
                        text: 'Документы и архивы',
                        handler: function () {
                            showStoredDocumentsTreeGridWindow(methodistId);
                        }
                    }
                ]
            },
            {
                xtype: 'tbbutton',
                text: 'Успеваемость слушателей',
                menu: [
                    {
                        text: 'Индивидуальная',
                        handler: function () {
                            auditorView.showIndividualAchievementWindow();
                        }
                    },
                    {
                        text: 'Сводная',
                        handler: function () {
                            auditorView.showSummaryAchievementWindow();
                        }
                    }
                ]
            },
            {
                xtype: 'tbbutton',
                text: 'Выход',
                handler: function () {
                    Ext.Ajax.request({
                        url: '/json/logout/',
                        success: function (form, action) {
                            Ext.Msg.progress("Выход из системы", "Производится выход из системы");
                            location.href = '/';
                        }
                    });
                }
            }
        ]
    });
}

/**
 * Создаёт объект панели интенсивов, которая должна содержать кнопки с
 * наименованиями интенсивов, которыми заведует методист.
 *
 * @return Ext.Toolbar объект панели интенсивов, который не содержит кнопок с
 * наименованиями интенсивов. Для обновления набора кнопок на панели необходимо
 * вызывать функцию обновления состояния панели интенсивов
 * (@see refreshIntensivesToolbar).
 */
function makeIntensivesToolbar() {
    return new Ext.Toolbar({
        autoScroll: true
    });
}

/**
 * Обновляет состояние панели интенсивов. Вызывать при инициализации панели или
 * при добавлении нового интенсива в базу.
 *
 * @param chairId идентификатор кафедры.
 * @return Ext.Toolbar обновлённая панель, которая содержит кнопки с
 * наименованиями интенсивов.
 */
function refreshIntensivesToolbar(chairId) {
    new Ext.data.Store({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            fields: ['id', 'name'],
            root: 'intensives'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getintensivesfortoolbar/?cid=' + chairId
        }),
        listeners: {
            'load': function () {
                itoolbar.removeAll(true);

                this.each(function (record) {
                    itoolbar.add({
                        xtype: 'tbbutton',
                        text: record.data.name,
                        intensive_id: record.data.id,
                        handler: function () {
                            intensiveToolButtonClick(this.intensive_id);
                        }
                    });
                });

                itoolbar.doLayout();
                workarea.doLayout();
            }
        }
    });

    return itoolbar;
}

/**
 * Создать календарную панель
 * @param chairId идентификатор кафедры
 * @returns {Ext.calendar.CalendarPanel} календарь
 */
function makeCalendarPanel(chairId) {
    return new Ext.calendar.CalendarPanel({
        id: 'schedule-calendar',
        showDayView: false,
        showMonthView: false,
        showWeekView: true,
        weekText: 'Недели',
        activeItem: 1,
        weekViewCfg: {
            startDay: 1
        },
        calendarStore: new Ext.data.JsonStore({
            storeId: 'calendarStore',
            root: 'calendars',
            idProperty: 'id',
            data: {
                "calendars": [
                    {
                        "id": 1,
                        "title": "Занятия"
                    },
                    {
                        "id": 2,
                        "title": "По преподавателю"
                    },
                    {
                        "id": 3,
                        "title": "По аудитории"
                    },
                    {
                        "id": 4,
                        "title": "По интенсивам"
                    }
                ]
            },
            proxy: new Ext.data.MemoryProxy(),
            autoLoad: true,
            fields: [
                {name: 'CalendarId', mapping: 'id', type: 'int'},
                {name: 'Title', mapping: 'title', type: 'string'}
            ],
            sortInfo: {
                field: 'CalendarId',
                direction: 'ASC'
            }
        }),
        eventStore: new Ext.data.JsonStore({
            root: 'tasks',
            baseParams: {
                iid: intensiveId,
                tid: teachersCalendarFilter,
                aid: auditoriesCalendarFilter,
                oi: otherIntensivesCalendarFilter
            },
            proxy: new Ext.data.HttpProxy({
                url: '/json/getcalendarschedule/'
            }),
            fields: Ext.calendar.EventRecord.prototype.fields.getRange()
        }),
        listeners: {
            rangeselect: function (calendarObj, dates, onComplete) {
                if (intensiveId != 0) {
                    var lessonAttributesWnd = makeLessonAttributesWindow(0, intensiveId, chairId,
                        'Добавление нового занятия', dates.StartDate, dates.EndDate, function (form, action) {
                            refreshCalendar();
                        });
                    lessonAttributesWnd.show();
                } else {
                    Ext.Msg.show({
                        title: 'Нет выбранного интенсива',
                        msg: 'Чтобы добавить занятие, сперва необходимо выбрать интенсив',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING
                    });
                }
                onComplete();
            },
            eventclick: function (calendarObj, rec, el) {
                if (rec.data.CalendarId == 1) {
                    var menu = new Ext.menu.Menu({
                        items: [
                            {
                                text: 'Состав слушателей',
                                handler: function () {
                                    var staffWnd = makeListenersLessonStaffWindow(rec.id);
                                    staffWnd.show();
                                }
                            },
                            {
                                text: 'Отметка посещаемости занятия',
                                handler: function () {
                                    var presencesLessonWnd = makeLessonPresenceWindow(rec.id);
                                    presencesLessonWnd.show();
                                }
                            },
                            {
                                text: 'Дублировать занятие',
                                handler: function () {
                                    var duplicateWnd = makeDuplicateEventWindow('Дублирование занятия',
                                        function (period, repeatCount) {
                                            Ext.Ajax.request({
                                                url: '/json/duplicatelesson/',
                                                method: 'POST',
                                                params: {
                                                    lid: rec.id,
                                                    period: period,
                                                    repeat_count: repeatCount
                                                },
                                                success: function (response, opts) {
                                                    refreshCalendar();
                                                },
                                                failure: function (response, opts) {
                                                    Ext.Msg.show({
                                                        title: 'Ошибка при дублировании занятия',
                                                        msg: 'Не удалось продублировать занятие',
                                                        buttons: Ext.MessageBox.OK,
                                                        icon: Ext.MessageBox.ERROR
                                                    });
                                                }
                                            });
                                        });
                                    duplicateWnd.show();
                                }
                            },
                            '-',
                            {
                                text: 'Редактировать занятие',
                                handler: function () {
                                    var lessonAttributesWnd = makeLessonAttributesWindow(rec.id, intensiveId, chairId,
                                        'Редактирование занятия', null, null, function (form, action) {
                                            refreshCalendar();
                                        });
                                    lessonAttributesWnd.show();
                                }
                            },
                            {
                                text: 'Удалить занятие',
                                handler: function () {
                                    Ext.Msg.confirm('Удаление занятия', 'Вы точно хотите удалить занятие ?',
                                        function (btn) {
                                            if (btn == 'yes') {
                                                Ext.Ajax.request({
                                                    url: '/json/deletelesson/',
                                                    method: 'POST',
                                                    params: {lid: rec.id},
                                                    success: function (response, opts) {
                                                        refreshCalendar();
                                                    },
                                                    failure: function (response, opts) {
                                                        Ext.Msg.show({
                                                            title: 'Ошибка при удалении',
                                                            msg: 'Не удалось удалить занятие',
                                                            buttons: Ext.MessageBox.OK,
                                                            icon: Ext.MessageBox.ERROR
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                }
                            }
                        ]
                    });
                    menu.showAt(Ext.EventObject.getXY());
                }
            }
        }
    });
}

function intensiveToolButtonClick(id) {
    intensiveId = id;
    refreshCalendar();
}

function refreshTeachersStore() {
    teachersFilterStore.load();
}

function refreshCalendar() {
    calendar.eventStore.setBaseParam('iid', intensiveId);
    calendar.eventStore.setBaseParam('tid', teachersCalendarFilter);
    calendar.eventStore.setBaseParam('aid', auditoriesCalendarFilter);
    calendar.eventStore.setBaseParam('oi', otherIntensivesCalendarFilter);
    calendar.eventStore.load();
}

function showCotractsGridWindow(chairId, methodistId) {
    var wnd = makeContractsGridWindow(chairId, methodistId);
    wnd.show();
}

function showListenersGridWindow(methodistId) {
    var wnd = makeListenersGridWindow(methodistId);
    wnd.show();
}

function showIntensivesGridWindow(chairId) {
    var wnd = makeIntensivesGridWindow(chairId);
    wnd.show();
}

function showTeachersGridWindow(chairId) {
    var wnd = makeTeachersGridWindow(chairId);
    wnd.show();
}

function showAuditoriesGridWindow(chairId) {
    var wnd = makeAuditoriesGridWindow(chairId);
    wnd.show();
}

/**
 * Показать окно отображающее текущие документы и архивы в хранилище
 * @param methodistId индентификатор методиста
 */
function showStoredDocumentsTreeGridWindow(methodistId) {
    var wnd = makeStoredDocumentsGridWindow({user: 'mid', id: methodistId});
    wnd.show();
}

/**
 * Показать окно архивирования документов
 * @param methodistId индентификатор методиста
 */
function showArchiveDocumentsWindow(methodistId) {
    var wnd = makeArchiveDocumentsWindow({user: 'mid', id: methodistId});
    wnd.show();
}

/**
 * Показать окно редактирования кафедрального расписания
 * @param chairId индентификатор кафедры
 */
function showScheduleEditWindow(chairId) {
    var wnd = makeScheduleEditWindow(chairId);
    wnd.show();
}