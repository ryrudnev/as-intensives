var executedDirtyHacks = false;

function loadDirtyHacks() 
{
	Ext.grid.PivotGridView.prototype.getCellIndex = function(el) { 
	    var rowEl = Ext.fly(el).findParent('tr', 2, true); 
	    if (rowEl) { 
	        var i = 0; 
	        Ext.each(rowEl.query('td'), function(rowCell) { 
	            if (rowCell == el) { 
	                return false; 
	            } 
	            i++; 
	            return true; 
	        }); 
	        return i; 
	    } 
	    return false; 
	}; 
	(function() { 
	    var originalFunction = Ext.grid.PivotAxis.prototype.getTuples; 
	    Ext.grid.PivotAxis.prototype.getTuples = function() { 
	        var result = originalFunction.call(this); 
	        this.lastTuples = result; 
	        return result; 
	    }; 
	})(); 

	executedDirtyHacks = true;
}

if (!executedDirtyHacks) {
	loadDirtyHacks()
}