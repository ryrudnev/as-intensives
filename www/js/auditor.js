/**
 * Created by Роман on 19.06.14.
 */
var auditorView = (function() {
    var view = {};

    function dateToStr(date) {
        var str = '' + date;
        if (date instanceof Date) {
            str = '' + date.getFullYear() + '-' + (date.getMonth() < 9 ? '0' + (date.getMonth() + 1) : date.getMonth()) +
                '-' + (date.getDate() < 10 ? '0' + date.getDate() : date.getDate());
        }
        return str;
    }

    view.showIndividualAchievementWindow = function () {
        var colModel = new Ext.grid.ColumnModel([]);
        var store = new Ext.data.Store({
            proxy: new Ext.data.HttpProxy({
                url: '/auditor/getindividualachievement/'
            }),
            reader: new Ext.data.JsonReader(),
            listeners: {
                metachange: function (scope, meta) {
                    var fields = meta.fields;
                    var columns = [];
                    for (var i = 0; i < fields.length; i++) {
                        var column = {};
                        column['id'] = fields[i].name;
                        column['sortable'] = true;
                        column['dataIndex'] = fields[i].name;
                        switch (fields[i].name) {
                            case 'student':
                                column['header'] = 'Слушатель';
                                column['flex'] = 1;
                                column['width'] = 100;
                                columns.push(column);
                                break;
                            case 'group':
                                column['header'] = 'Группа';
                                column['flex'] = 1;
                                column['width'] = 50;
                                columns.push(column);
                                break;
                            case 'type':
                                column['header'] = 'Тема занятия';
                                column['flex'] = 1;
                                column['width'] = 100;
                                columns.push(column);
                                break;
                            case 'quiz':
                                column['header'] = 'Тестирование';
                                column['flex'] = 1;
                                column['width'] = 100;
                                column['renderer'] = function (value, metaData, record, rowIndex, colIndex, store, view) {
                                    var data = record.get(metaData.id);

                                    return data.grade == -1 ? ' ' : ('Оценка: ' + data.grade + '</br>' +
                                        'Кол-во попыток: ' + data.attemptCount);
                                };
                                columns.push(column);
                                break;
                            case 'srs':
                                column['header'] = 'Домашняя часть';
                                column['flex'] = 1;
                                column['width'] = 100;
                                column['renderer'] = function (value, metaData, record, rowIndex, colIndex, store, view) {
                                    var data = record.get(metaData.id);

                                    return data.done == -1 ? ' ' : ((data.done ? '<b>Сдана</b>, ' :  '<b>Не сдана</b>, ') +
                                        'Кол-во попыток: ' + data.attemptCount + '</br> Вариант: ' + data.variant);
                                };
                                columns.push(column);
                                break;
                            case 'mod':
                                column['header'] = 'Модификация';
                                column['flex'] = 1;
                                column['width'] = 100;
                                column['renderer'] = function (value, metaData, record, rowIndex, colIndex, store, view) {
                                    var data = record.get(metaData.id);

                                    return data.done == -1 ? ' ' : ((data.done ? '<b>Сдана</b>, ' :  '<b>Не сдана</b>, ') +
                                        'Кол-во попыток: ' + data.testingCount + '</br> Коэфф. сложности: ' + data.kc.toFixed(2));
                                };
                                columns.push(column);
                                break;
                            default:
                                break;
                        }
                    }

                    colModel.setConfig(columns);
                    grid.reconfigure(store, colModel);
                }
            }
        });
        var grid = new Ext.grid.GridPanel({
            store: store,
            anchor: '100%, 100%',
            loadMask: true,
            autoWidth: true,
            sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
            colModel: colModel,
            viewConfig: {
                forceFit: true
            },
            frame: true,
            autoScroll: true
        });
        var mainPanel = new Ext.Panel({
            title: 'Результаты',
            region: 'center',
            margins: '3 3 3 0',
            layout: 'fit',
            items: [
                grid
            ]
        });
        var formPanel = new Ext.FormPanel({
            name: 'form',
            border: false,
            padding: 5,
            items: [
                {
                    xtype: 'combo',
                    fieldLabel: 'Преподаватель',
                    name: 'teacher',
                    store: new Ext.data.Store({
                        autoLoad: true,
                        proxy: new Ext.data.HttpProxy({
                            url: '/auditor/getteachers/'
                        }),
                        reader: new Ext.data.JsonReader({
                            fields: [
                                'id',
                                'teacher'
                            ],
                            root: 'teachers'
                        })
                    }),
                    anchor: '100%',
                    valueField: 'id',
                    value: 'Не важно',
                    displayField: 'teacher',
                    triggerAction: 'all',
                    allowBlank: false
                },
                {
                    xtype: 'combo',
                    fieldLabel: 'Группа',
                    name: 'group',
                    store: new Ext.data.Store({
                        autoLoad: true,
                        proxy: new Ext.data.HttpProxy({
                            url: '/auditor/getstudentsgroups/'
                        }),
                        reader: new Ext.data.JsonReader({
                            fields: [
                                'id',
                                'group'
                            ],
                            root: 'groups'
                        })
                    }),
                    listeners: {
                        select: function (combo, record, index) {
                            var s = formPanel.getForm().findField('student').getStore();
                            s.reload({params: {gid: record.get('id')}});
                        }
                    },
                    anchor: '100%',
                    valueField: 'id',
                    value: 'Не важно',
                    displayField: 'group',
                    triggerAction: 'all',
                    allowBlank: false
                },
                {
                    xtype: 'combo',
                    fieldLabel: 'Тема занятия',
                    name: 'type',
                    store: new Ext.data.Store({
                        autoLoad: true,
                        proxy: new Ext.data.HttpProxy({
                            url: '/auditor/getlessontypes/'
                        }),
                        reader: new Ext.data.JsonReader({
                            fields: [
                                'id',
                                'type'
                            ],
                            root: 'types'
                        })
                    }),
                    anchor: '100%',
                    valueField: 'id',
                    value: 'Не важно',
                    displayField: 'type',
                    triggerAction: 'all',
                    allowBlank: false
                },
                {
                    xtype: 'combo',
                    fieldLabel: 'Слушатель',
                    name: 'student',
                    store: new Ext.data.Store({
                        autoLoad: true,
                        proxy: new Ext.data.HttpProxy({
                            url: '/auditor/getstudents/'
                        }),
                        reader: new Ext.data.JsonReader({
                            fields: [
                                'id',
                                'student'
                            ],
                            root: 'students'
                        })
                    }),
                    anchor: '100%',
                    valueField: 'id',
                    value: 'Не важно',
                    displayField: 'student',
                    triggerAction: 'all',
                    allowBlank: false
                }
            ]
        });
        var searchPanel = new Ext.Panel({
            title: 'Поиск',
            region: 'west',
            split: true,
            width: 280,
            collapsible: true,
            margins: '3 0 3 3',
            cmargins: '3 3 3 3',
            layout: 'anchor',
            items: [ formPanel ],
            buttonAlign: 'center',
            fbar: [{
                text: 'Искать',
                listeners: {
                    click: function (btn, e) {
                        var form = formPanel.getForm();
                        if (form.isValid()) {
                            var teacher = form.findField('teacher').getValue();
                            var group = form.findField('group').getValue();
                            var type = form.findField('type').getValue();
                            var student = form.findField('student').getValue();

                            store.load({
                                params: {
                                    teacher: teacher == 'Не важно' ? 0 : teacher,
                                    group: group == 'Не важно' ? 0 : group,
                                    type: type == 'Не важно' ? 0 : type,
                                    student: student == 'Не важно' ? 0 : student
                                }
                            });
                        }
                    }
                }
            }]
        });
        var win = new Ext.Window({
            title: 'Индивидуальная успеваемость слушателей',
            closable: true,
            width: 880,
            height: 500,
            plain: true,
            layout: 'border',
            items: [searchPanel, mainPanel]
        });
        win.show();
    };

    view.showSummaryAchievementWindow = function () {
        var colModel = new Ext.grid.ColumnModel([]);
        var store = new Ext.data.Store({
            proxy: new Ext.data.HttpProxy({
                url: '/auditor/getsummaryachievement/'
            }),
            reader: new Ext.data.JsonReader(),
            listeners: {
                metachange: function (scope, meta) {
                    var fields = meta.fields;
                    var columns = [];
                    for (var i = 0; i < fields.length; i++) {
                        var column = {};
                        column['id'] = fields[i].name;
                        column['sortable'] = true;
                        column['dataIndex'] = fields[i].name;
                        switch (fields[i].name) {
                            case 'listener':
                                column['header'] = 'Слушатель';
                                column['flex'] = 1;
                                column['width'] = 100;
                                columns.push(column);
                                break;
                            case 'id':
                                break;
                            default:
                                column['header'] = fields[i].name;
                                column['flex'] = 1;
                                column['width'] = 160;
                                column['renderer'] = function (value, metaData, record, rowIndex, colIndex, store, view) {
                                    var data = record.get(metaData.id);

                                    var mod = data.mod == -1 ? '' : data.mod == 0 ? '<b>Модиф.</b> не сдана </br>' :
                                        '<b>Модиф.</b> сдана </br>';
                                    var srs = data.srs == -1 ? '' : data.srs == 0 ? '<b>Домаш.</b> не сдана </br>' :
                                        '<b>Домаш.</b> сдана </br>';
                                    var quiz = data.quiz == -1 ? '' : data.quiz == 0 ? '<b>Тест.</b> не сдано' :
                                        '<b>Тест.</b> сдано';

                                    return  mod + srs + quiz;
                                };
                                columns.push(column);
                                break;
                        }
                    }

                    colModel.setConfig(columns);
                    grid.reconfigure(store, colModel);
                }
            }
        });
        var grid = new Ext.grid.GridPanel({
            store: store,
            anchor: '100%, 100%',
            loadMask: true,
            autoWidth: true,
            sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
            colModel: colModel,
            viewConfig: {
                forceFit: true
            },
            frame: true,
            autoScroll: true,
            columnLines: true
        });
        var mainPanel = new Ext.Panel({
            title: 'Результаты',
            region: 'center',
            margins: '3 3 3 0',
            layout: 'fit',
            items: [
                grid
            ]
        });
        var formPanel = new Ext.FormPanel({
            name: 'form',
            border: false,
            padding: 5,
            items: [
                {
                    xtype: 'datefield',
                    id: 'from',
                    vtype: 'daterange',
                    fieldLabel: 'От',
                    name: 'fromDate',
                    format: 'Y-m-d',
                    allowBlank: false,
                    editable: false,
                    anchor: '100%',
                    endDateField: 'to'
                },
                {
                    xtype: 'datefield',
                    id: 'to',
                    vtype: 'daterange',
                    fieldLabel: 'До',
                    name: 'toDate',
                    format: 'Y-m-d',
                    allowBlank: false,
                    editable: false,
                    anchor: '100%',
                    startDateField: 'from'
                }
            ]
        });
        var searchPanel = new Ext.Panel({
            title: 'Поиск',
            region: 'west',
            split: true,
            width: 210,
            collapsible: true,
            margins: '3 0 3 3',
            cmargins: '3 3 3 3',
            layout: 'anchor',
            items: [ formPanel ],
            buttonAlign: 'center',
            fbar: [{
                text: 'Искать',
                listeners: {
                    click: function (btn, e) {
                        var form = formPanel.getForm();
                        if (form.isValid()) {
                            var from = form.findField('fromDate').getValue();
                            var to = form.findField('toDate').getValue();
                            store.load({
                                params: {to: dateToStr(to), from: dateToStr(from)}
                            });
                        }
                    }
                }
            }]
        });
        var win = new Ext.Window({
            title: 'Сводная успеваемость слушателей',
            closable: true,
            width: 900,
            height: 500,
            plain: true,
            layout: 'border',

            items: [searchPanel, mainPanel]
        });
        win.show(this);
    };

    return view;
}());