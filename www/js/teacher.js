var tabSwitcher;
var teacherViewport;
var calendar;

function makeTeacherViewport(teacherID, chairID) {
    var schedule = makeSchedulePanelTab(chairID, teacherID);
    var privateInfo = makePrivateInfoTab(teacherID, chairID);
    var docGeneration = makeDocumentsTab(teacherID, chairID);
    var documentStorageTab = makeDocumentStorageTab(teacherID);
    var achievementTab = makeAchievementTab();

    var exitTab = {
        title: 'Выход',
        id: 'logout_tab'
    };
    tabSwitcher = new Ext.TabPanel({
        xtype: 'tabpanel',
        renderTo: Ext.getBody(),
        activeTab: 0,
        items: [
            schedule,
            privateInfo,
            docGeneration,
            documentStorageTab,
            achievementTab,
            exitTab
        ],
        listeners: {
            'tabchange': function (tabPanel, tab) {
                if (tab.id == 'logout_tab') {
                    Ext.Ajax.request({
                        url: '/json/logout/',
                        success: function (form, action) {
                            Ext.Msg.progress("Выход из системы", "Производится выход из системы");
                            location.href = '/';
                        }
                    });
                }
            }
        }
    });
    var exitButton = {
        xtype: 'tbbutton',
        text: 'Выход',
        region: 'south',
        handler: function () {
            Ext.Ajax.request({
                url: '/json/logout/',
                success: function (form, action) {
                    Ext.Msg.progress("Выход из системы", "Производится выход из системы");
                    location.href = '/';
                }
            });
        }
    };

    var w = window,
        d = document,
        e = d.documentElement,
        g = d.getElementsByTagName('body')[0],
        width_x = w.innerWidth || e.clientWidth || g.clientWidth;
    var viewport = new Ext.Viewport({
        layout: 'border',
        renderTo: Ext.getBody(),
        autoScroll: true,
        items: [
            {
                region: 'north',
                xtype: 'panel',
                items: [ tabSwitcher ],
                autoHeight: true,
                width: width_x
            }
        ]
    });
    return viewport;
}

function makeDocumentsTab(teacherID, chairID) {
    var teacherData = {};
    teacherData.id = teacherID;
    var contractButton = {
        xtype: 'tbbutton',
        text: 'Сформировать договор',
        width: 200,
        handler: function () {
            var contrWindow = makeGenerateTeacherContractWindow(teacherData);
            contrWindow.show();
        }
    };

    var reportButton = {
        xtype: 'tbbutton',
        text: 'Сформировать отчет',
        width: 200,
        handler: function () {
            var contrWindow = makeGenerateTeacherReportWindow(teacherData);
            contrWindow.show();
        }
    };

    return new Ext.form.FormPanel({
        title: 'Составление документов',
        id: 'doc_generation',
        anchor: '100%',
        autoHeight: true,
        padding: 5,
        region: 'north',
        items: [
            contractButton,
            reportButton
        ]
    });
}

function makePrivateInfoTab(teacherID, chairID) {
    var teacherIdField = new Ext.form.Hidden({
        id: 'teacher_id',
        name: 'id',
        value: teacherID
    });
    var chairIdField = new Ext.form.Hidden({
        id: 'chair_id',
        value: chairID
    });
    var surnameField = new Ext.form.TextField({
        id: 'surname',
        fieldLabel: 'Фамилия',
        allowBlank: false
    });
    var nameField = new Ext.form.TextField({
        id: 'name',
        fieldLabel: 'Имя',
        allowBlank: false
    });
    var patronymicField = new Ext.form.TextField({
        id: 'patronymic',
        fieldLabel: 'Отчество',
        allowBlank: false
    });
    var rankField = new Ext.form.TextField({
        id: 'rank',
        fieldLabel: 'Должность/звание'
    });
    var attNumField = new Ext.form.TextField({
        id: 'att_num',
        fieldLabel: '№ атт. ВАК'
    });
    var diplomNumField = new Ext.form.TextField({
        id: 'diplom_num',
        fieldLabel: '№ диплома'
    });
    var loginField = new Ext.form.TextField({
        id: 'login',
        fieldLabel: 'Логин',
        allowBlank: false
    });
    var passwordField = new Ext.form.TextField({
        id: 'password',
        fieldLabel: 'Пароль',
        allowBlank: teacherID == 0 ? false : true
    });

    var jobField = new Ext.form.TextField({
        id: 'job',
        fieldLabel: 'Место работы',
        allowBlank: true
    });

    var birthdateField = new Ext.form.DateField({
        id: 'birthdate',
        fieldLabel: 'Дата рождения',
        format: 'Y-m-d',
        allowBlank: true,
        vtype: 'daterange'
    });

    var birth_placeField = new Ext.form.TextField({
        id: 'birth_place',
        fieldLabel: 'Место рождения',
        allowBlank: true
    });

    var address_indexField = new Ext.form.TextField({
        id: 'address_index',
        fieldLabel: 'Почтовый индекс',
        allowBlank: true
    });

    var address_cityField = new Ext.form.TextField({
        id: 'address_city',
        fieldLabel: 'Адрес',
        allowBlank: true
    });

    var passport_seriesField = new Ext.form.TextField({
        id: 'passport_series',
        fieldLabel: 'Серия паспорта',
        allowBlank: true
    });

    var passport_numberField = new Ext.form.TextField({
        id: 'passport_number',
        fieldLabel: 'Номер паспорта',
        allowBlank: true
    });

    var passport_issue_dateField = new Ext.form.DateField({
        id: 'passport_issue_date',
        fieldLabel: 'Дата выдачи паспорта',
        format: 'Y-m-d',
        allowBlank: true,
        vtype: 'daterange'
    });

    var passport_issue_placeField = new Ext.form.TextField({
        id: 'passport_issue_place',
        fieldLabel: 'Кем выдан',
        allowBlank: true
    });

    var insurance_numberField = new Ext.form.TextField({
        id: 'insurance_number',
        fieldLabel: 'Номер страхового свидетельства ПФ РФ',
        allowBlank: true
    });

    var tax_numberField = new Ext.form.TextField({
        id: 'tax_number',
        fieldLabel: 'ИНН',
        allowBlank: true
    });

    var saveButton = {
        text: 'Сохранить',
        xtype: 'tbbutton',
        plugins: 'defaultButton',
        handler: function () {
            var form = attributesForm.getForm();
            if (form.isValid()) {
                form.submit({
                    waitMsg: 'Сохранение данных о преподавателе',
                    failure: function (form, action) {
                        Ext.Msg.show({
                            title: 'Ошибка при сохранении',
                            msg: action.result.error_message,
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                })
            } else {
                Ext.Msg.show({
                    title: 'Ошибка при валидации',
                    msg: 'В форму внесены некорректные данные',
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.WARNING
                });
            }
        }
    };
    var attributesForm = new Ext.form.FormPanel({
        title: 'Личные данные',
        id: 'teacher-attributes',
        anchor: '100%',
        autoHeight: true,
        padding: 5,
        region: 'north',
        url: '/json/saveteacherdatasafe/',
        autoScroll: true,
        items: [
            teacherIdField,
            chairIdField,
            surnameField,
            nameField,
            patronymicField,
            rankField,
            attNumField,
            diplomNumField,
            jobField,
            birthdateField,
            birth_placeField,
            address_indexField,
            address_cityField,
            passport_seriesField,
            passport_numberField,
            passport_issue_dateField,
            passport_issue_placeField,
            insurance_numberField,
            tax_numberField,
            loginField,
            passwordField,
            saveButton
        ]
    });

    var attributesStore = new Ext.data.Store({
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'surname',
                'name',
                'patronymic',
                'rank',
                'att_num',
                'diplom_num',
                'job',
                'birthdate',
                'birth_place',
                'address_index',
                'address_city',
                'passport_series',
                'passport_number',
                'passport_issue_date',
                'passport_issue_place',
                'insurance_number',
                'tax_number',
                'login',
                'password'
            ],
            root: 'teacherdata'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getteacherdatasafe/' + teacherID
        }),
        listeners: {
            'load': function () {
                console.log('this is embarassing');
                var rec = this.getAt(0);
                surnameField.setValue(rec.data.surname);
                nameField.setValue(rec.data.name);
                patronymicField.setValue(rec.data.patronymic);
                loginField.setValue(rec.data.login);
                rankField.setValue(rec.data.rank);
                attNumField.setValue(rec.data.att_num);
                diplomNumField.setValue(rec.data.diplom_num);
                jobField.setValue(rec.data.job);
                birthdateField.setValue(rec.data.birthdate);
                birth_placeField.setValue(rec.data.birth_place);
                address_indexField.setValue(rec.data.address_index);
                address_cityField.setValue(rec.data.address_city);
                passport_seriesField.setValue(rec.data.passport_series);
                passport_numberField.setValue(rec.data.passport_number);
                passport_issue_dateField.setValue(rec.data.passport_issue_date);
                passport_issue_placeField.setValue(rec.data.passport_issue_place);
                insurance_numberField.setValue(rec.data.insurance_number);
                tax_numberField.setValue(rec.data.tax_number);
            }
        }
    });

    if (teacherID != 0) {
        attributesForm.on('show', function () {
            var smthng = attributesStore.load();
            console.log(smthng);
            console.log('it should load info now');
            console.log('teacherID = ' + teacherID);
        });
    }

    return attributesForm;
}

function makeSchedulePanelTab(chairID, teacherID) {

    calendar = makeCalendarPanel(chairID, teacherID);
    var form = new Ext.FormPanel({
        title: 'Расписание',
        items: [calendar]
    });
    return form;
}

function refreshCalendar() {
    calendar.eventStore.reload();
}

/**
 * Создать панель календаря
 * @param chairId идентификатор кафедры
 * @param teacherId
 * @returns {Ext.calendar.CalendarPanel}
 */
function makeCalendarPanel(chairId, teacherId) {
    var schFields = Ext.calendar.EventRecord.prototype.fields.getRange();
    schFields.push({name: 'realID', mapping: 'realID', type: 'int'});
    var scheduleStore = new Ext.data.JsonStore({
        autoLoad: true,
        fields: schFields,
        root: 'tasks',
        baseParams: {
            teacher_id: teacherId
        },
        proxy: new Ext.data.HttpProxy({
            url: '/schedule/getcalendarschedule'
        })
    });

    return new Ext.calendar.CalendarPanel({
        id: 'schedule-calendar',
        height: 1000,
        showDayView: false,
        showMonthView: false,
        showWeekView: true,
        weekText: 'Недели',
        activeItem: 1,
        calendarStore: new Ext.data.JsonStore({
            storeId: 'calendarStore',
            root: 'calendars',
            idProperty: 'id',
            data: {
                "calendars": [
                    {
                        "id": 1,
                        "title": "Занятия"
                    },
                    {
                        "id": 2,
                        "title": "Интенсивы"
                    },
                    {
                        "id": 3,
                        "title": "Консультации"
                    }
                ]
            },
            proxy: new Ext.data.MemoryProxy(),
            autoLoad: true,
            fields: [
                {name: 'CalendarId', mapping: 'id', type: 'int'},
                {name: 'Title', mapping: 'title', type: 'string'}
            ],
            sortInfo: {
                field: 'CalendarId',
                direction: 'ASC'
            }
        }),
        eventStore: scheduleStore,
        listeners: {
            rangeselect: function (calendarObj, dates, callback) {
                var menu = new Ext.menu.Menu({
                    items: [
                        {
                            text: 'Создать вузовское занятие',
                            handler: function () {
                                var date = dates.StartDate;
                                var day = date.getDay();
                                if (day == 0) {
                                    Ext.Msg.show({
                                        title: 'Невозможно создать занятие',
                                        msg: 'Воскресенье - выходной, добавить занятие нельзя!',
                                        buttons: Ext.MessageBox.OK,
                                        icon: Ext.MessageBox.WARNING
                                    });
                                } else {
                                    var cur = [date.getHours(), date.getMinutes()],
                                        pair = getCorrectTimePairs(cur),
                                        options = {
                                            calendarOpts: {
                                                calendar: calendar, tid: teacherId, lid: 0, day: day, start: pair[0], end: pair[1]
                                            }
                                        };

                                    var wnd = makeEditVstuLessonWindow(chairId, 'Добавление вузовского занятия', options);
                                    wnd.show();
                                }
                            }
                        },
                        {
                            text: 'Создать черновик интенсива',
                            handler: function () {
                                var lessonAttributesWnd = makeTeacherLessonAttributesWindow(teacherId, 0, chairId,
                                    'Добавление черновика интенсива', dates.StartDate, dates.EndDate, function (form, action) {
                                        refreshCalendar();
                                    });
                                lessonAttributesWnd.show();
                            }
                        },
                        {
                            text: 'Создать консультацию',
                            handler: function () {
                                var lessonAttributesWnd = makeConsultationAttributesWindow(null, 'Добавление консультации',
                                    dates.StartDate, dates.EndDate, function (form, action) {
                                        refreshCalendar();
                                    });
                                lessonAttributesWnd.show();
                            }
                        }
                    ]
                });
                menu.showAt(Ext.EventObject.getXY());
                callback();
            },
            eventmove: function (calendarObj, rec) {
                switch (rec.get('CalendarId')) {
                    case 1:
                        var date = rec.get('StartDate');
                        var day = date.getDay();
                        if (day == 0) {
                            Ext.Msg.show({
                                title: 'Невозможно создать занятие',
                                msg: 'Воскресенье - выходной, перенести занятие нельзя!',
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.WARNING
                            });
                        } else {
                            var pair = getCorrectTimePairs([date.getHours(), date.getMinutes()]);
                            var time = {
                                day: day,
                                lessonStart: pair[0],
                                lessonEnd: pair[1]
                            };
                            Ext.Ajax.request({
                                url: '/schedule/changevstulessontime/',
                                method: 'POST',
                                params: { id: rec.get('realID'), time: JSON.stringify(time) },
                                callback: function (ops, success, response) {
                                    var j = JSON.parse(response.responseText);
                                    if (success && j.success) {
                                        refreshCalendar();
                                    } else {
                                        Ext.Msg.show({
                                            title: 'Ошибка при переносе занятия',
                                            msg: 'Не удалось перенести вузовское занятие. ' +
                                                'На это время уже назначено другое занятие',
                                            buttons: Ext.MessageBox.OK,
                                            icon: Ext.MessageBox.ERROR
                                        });
                                    }
                                }
                            });
                        }
                        break;
                    case 2:
                        var lessonAttributesWnd = makeTeacherLessonAttributesWindow(teacherId, rec.id, chairId,
                            'Редактирование занятия', rec.get('StartDate'), rec.get('EndDate'), function (form, action) {
                                refreshCalendar();
                            });
                        lessonAttributesWnd.show();
                        break;
                    default:
                        break;
                }
            },
            eventclick: function (calendarObj, rec, el) {
                switch (rec.get('CalendarId')) {
                    case 1:
                        var menu = new Ext.menu.Menu({
                            items: [
                                {
                                    text: 'Редактировать',
                                    handler: function () {
                                        var options = {
                                                calendarOpts: { calendar: calendar, lid: rec.get('realID') }
                                            };

                                        var wnd = makeEditVstuLessonWindow(chairId,
                                            'Редактирование вузовского занятия', options);
                                        wnd.show();
                                    }
                                },
                                {
                                    text: 'Удалить',
                                    handler: function () {
                                        Ext.Msg.confirm('Удаление занятия', 'Вы точно хотите удалить вузовское занятие?',
                                            function (btn) {
                                                if (btn == 'yes') {
                                                    Ext.Ajax.request({
                                                        url: '/schedule/deletevstulesson/',
                                                        method: 'POST',
                                                        params: { id: rec.get('realID') },
                                                        callback: function (ops, success, response) {
                                                            var j = JSON.parse(response.responseText);
                                                            if (success && j.success) {
                                                                refreshCalendar();
                                                            } else {
                                                                Ext.Msg.show({
                                                                    title: 'Ошибка при удалении занятия',
                                                                    msg: 'Не удалось удалить вузовское занятие. Что-то пошло не так ...',
                                                                    buttons: Ext.MessageBox.OK,
                                                                    icon: Ext.MessageBox.ERROR
                                                                });
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                        );
                                    }
                                }
                            ]});
                        break;
                    case 2:
                        menu = new Ext.menu.Menu({
                            items: [
                                {
                                    text: 'Отметка посещаемости занятия',
                                    handler: function () {
                                        var presencesLessonWnd = makeLessonPresenceWindow(rec.id);
                                        presencesLessonWnd.show();
                                    }
                                },
                                {
                                    text: 'Редактировать',
                                    handler: function () {
                                        var lessonAttributesWnd = makeTeacherLessonAttributesWindow(teacherId, rec.id, chairId,
                                            'Редактирование занятия', null, null, function (form, action) {
                                                refreshCalendar();
                                            });
                                        lessonAttributesWnd.show();
                                    }
                                }
                            ]
                        });
                        break;
                    default:
                        menu = new Ext.menu.Menu({
                            items: [
                                {
                                    text: 'Редактировать',
                                    handler: function () {
                                        var lessonAttributesWnd = makeConsultationAttributesWindow(rec.data.realID,
                                            'Редактирование консультации', null, null, function (form, action) {
                                                refreshCalendar();
                                            });
                                        lessonAttributesWnd.show();
                                    }
                                },
                                {
                                    text: 'Удалить',
                                    handler: function () {
                                        Ext.Ajax.request({
                                            url: '/schedule/deleteconsultation/',
                                            method: 'POST',
                                            params: {id: rec.data.realID},
                                            success: function (response, opts) {
                                                refreshCalendar();
                                            },
                                            failure: function (response, opts) {
                                                Ext.Msg.show({
                                                    title: 'Ошибка при удалении',
                                                    msg: 'Не удалось удалить консультацию',
                                                    buttons: Ext.MessageBox.OK,
                                                    icon: Ext.MessageBox.ERROR
                                                });
                                            }
                                        });
                                    }
                                }
                            ]
                        });
                        break;
                }
                menu.showAt(Ext.EventObject.getXY());
            }
        }
    });
}

function makeTeacherLessonAttributesWindow(teacherID, lessonId, chairId, windowTitle, startTime, endTime, onSuccess) {
    var lessonIdField = new Ext.form.Hidden({
        id: 'id',
        value: lessonId
    });
    var intensiveIdField = new Ext.form.Hidden({
        id: 'intensive_id',
        value: 0
    });
    var intensiveField = new Ext.form.TriggerField({
        id: 'intensive',
        fieldLabel: 'Интенсив',
        editable: false,
        submitValue: false,
        allowBlank: false,
        anchor: '100%'
    });
    intensiveField.onTriggerClick = function (ev) {
        var intnesiveSelect = makeIntensivesSelectWindow(chairId, this, intensiveIdField, themeField, themeIdField);
        intnesiveSelect.show();
    };
    var startField = new Ext.form.Hidden({
        id: 'start',
        allowBlank: false
    });
    var startTimeField = new Ext.form.TimeField({
        id: 'start_time',
        fieldLabel: 'Время начала',
        submitValue: false,
        allowBlank: false,
        value: startTime,
        anchor: '100%',
        increment: 10,
        format: 'H:i'
    });
    var endField = new Ext.form.Hidden({
        id: 'end',
        allowBlank: false
    });
    var endTimeField = new Ext.form.TimeField({
        id: 'end_time',
        fieldLabel: 'Время окончания',
        submitValue: false,
        allowBlank: false,
        value: endTime,
        anchor: '100%',
        increment: 10,
        format: 'H:i'
    });
    var themeIdField = new Ext.form.Hidden({
        id: 'theme_id',
        value: 0
    });
    var themeField = new Ext.form.TriggerField({
        id: 'theme',
        fieldLabel: 'Тема',
        editable: false,
        submitValue: false,
        allowBlank: false,
        anchor: '100%'
    });
    themeField.onTriggerClick = function (ev) {
        var val = intensiveIdField.getValue();
        var themeSelet = makeThemeSelectWindow(intensiveIdField.getValue(), this, themeIdField);
        themeSelet.show();
    };
    var teacherIdField = new Ext.form.Hidden({
        id: 'teacher_id',
        allowBlank: false,
        value: teacherID
    });
    var auditoryIdField = new Ext.form.Hidden({
        id: 'auditory_id',
        allowBlank: false
    });
    var auditoryField = new Ext.form.TriggerField({
        id: 'auditory',
        fieldLabel: 'Аудитория',
        editable: false,
        submitValue: false,
        allowBlank: false,
        anchor: '100%'
    });
    auditoryField.onTriggerClick = function (ev) {
        var auditorySelect = makeAuditorySelectWindow(chairId, this, auditoryIdField);
        auditorySelect.show();
    };
    var statusField = new Ext.form.ComboBox({
        id: 'status_name',
        hiddenName: 'status',
        fieldLabel: 'Статус',
        mode: 'local',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: [
                'id',
                'statusText'
            ],
            data: [
                [0, 'Черновик']
            ]
        }),
        valueField: 'id',
        displayField: 'statusText',
        value: 0,
        triggerAction: 'all',
        allowBlank: false,
        anchor: '100%'
    });
    var typeField = new Ext.form.ComboBox({
        id: 'type_name',
        hiddenName: 'type',
        fieldLabel: 'Тип занятия',
        mode: 'local',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: [
                'id',
                'statusText'
            ],
            data: [
                [0, 'Теоретическое'],
                [1, 'Практическое'],
                [2, 'Лекционно-практическое']
            ]
        }),
        valueField: 'id',
        displayField: 'statusText',
        value: 0,
        triggerAction: 'all',
        allowBlank: false,
        anchor: '100%'
    });
    var attributesStore = new Ext.data.Store({
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'intensive_id',
                'start',
                'end',
                'theme_id',
                'teacher_id',
                'auditory_id',
                'status',
                'type'
            ],
            root: 'lessondata'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getlessondata/?lid=' + lessonId
        }),
        listeners: {
            'load': function () {
                var rec = this.getAt(0);
                statusField.setValue(rec.data.status);
                if (rec.data.status == 1) {
                    Ext.Msg.show({
                        title: 'Занятие утверждено',
                        msg: 'Изменение параметров занятия приведет к смене его статуса на [Черновик]',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING
                    });
                    statusField.setValue(0);
                } else if (rec.data.status == 2) {
                    lessonAttributes.close();
                    Ext.Msg.show({
                        title: 'Занятие проведено',
                        msg: 'Занятие уже проведено и не подлежит редактированию',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING
                    });
                    return null;
                }
                // Получение данных об аудитории
                new Ext.data.Store({
                    autoLoad: true,
                    reader: new Ext.data.JsonReader({
                        fields: [
                            'id',
                            'name'
                        ],
                        root: 'intensivedata'
                    }),
                    proxy: new Ext.data.HttpProxy({
                        url: '/json/getintensivedata/?iid=' + rec.data.intensive_id
                    }),
                    listeners: {
                        'load': function () {
                            var rec = this.getAt(0);
                            intensiveField.setValue(rec.data.name);
                        }
                    }
                });

                // Получение данных о преподавателе
                new Ext.data.Store({
                    autoLoad: true,
                    reader: new Ext.data.JsonReader({
                        fields: [
                            'id',
                            'name'
                        ],
                        root: 'auditorydata'
                    }),
                    proxy: new Ext.data.HttpProxy({
                        url: '/json/getauditorydata/?aid=' + rec.data.auditory_id
                    }),
                    listeners: {
                        'load': function () {
                            var rec = this.getAt(0);
                            auditoryField.setValue(rec.data.name);
                        }
                    }
                });

                // Получение ланных о теме дополнительного занятия
                new Ext.data.Store({
                    autoLoad: true,
                    reader: new Ext.data.JsonReader({
                        fields: [
                            'id',
                            'name'
                        ],
                        root: 'lessonthemedata'
                    }),
                    proxy: new Ext.data.HttpProxy({
                        url: '/json/getlessonthemedata/?ltid=' + rec.data.theme_id
                    }),
                    listeners: {
                        'load': function () {
                            var rec = this.getAt(0);
                            themeField.setValue(rec.data.name);
                        }
                    }
                });

                lessonIdField.setValue(rec.data.id);
                intensiveIdField.setValue(rec.data.intensive_id);
                startField.setValue(rec.data.start);
                endField.setValue(rec.data.end);
                themeIdField.setValue(rec.data.theme_id);
                teacherIdField.setValue(rec.data.teacher_id);
                auditoryIdField.setValue(rec.data.auditory_id);

                typeField.setValue(rec.data.type);

                startTime = Date.parseDate(rec.data.start, "Y-m-d H:i:s");
                startTimeField.setValue(startTime);
                endTime = Date.parseDate(rec.data.end, "Y-m-d H:i:s");
                endTimeField.setValue(endTime);
            }
        }
    });

    var attributesForm = new Ext.form.FormPanel({
        id: 'lesson-attributes',
        padding: 5,
        url: '/json/savelessondata/',
        items: [
            lessonIdField,
            startField,
            startTimeField,
            endField,
            endTimeField,
            intensiveIdField,
            intensiveField,
            themeField,
            themeIdField,
            teacherIdField,
            auditoryIdField,
            auditoryField,
            statusField,
            typeField
        ],
        buttons: [
            {
                text: 'Сохранить',
                plugins: 'defaultButton',
                handler: function () {
                    var form = attributesForm.getForm();
                    if (form.isValid()) {
                        var startDate = dateToString(startTime);
                        var endDate = dateToString(endTime);

                        form.items.item('start').setValue(startDate + ' ' + form.items.item('start_time').value);
                        form.items.item('end').setValue(endDate + ' ' + form.items.item('end_time').value);
                        form.submit({
                            waitMsg: 'Сохранение данных о занятии',
                            success: function (form, action) {
                                onSuccess(form, action);
                                lessonAttributes.close();
                            },
                            failure: function (form, action) {
                                Ext.Msg.show({
                                    title: 'Ошибка при сохранении',
                                    msg: action.result.error_message,
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.WARNING
                                });
                            }
                        });
                    } else {
                        Ext.Msg.show({
                            title: 'Ошибка при валидации',
                            msg: 'В форму внесены некорректные данные',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                }
            },
            {
                text: 'Отмена',
                handler: function () {
                    lessonAttributes.close();
                }
            }
        ]
    });

    var lessonAttributes = new Ext.Window({
        title: windowTitle,
        modal: true,
        width: 600,
        items: [
            attributesForm
        ]
    });

    if (lessonId != 0) {
        lessonAttributes.on('show', function () {
            attributesStore.load();
        });
    }

    return lessonAttributes;
}

function makeIntensivesSelectWindow(chairId, intensiveField, intensiveIdField, themeField, themeIdField) {
    var currentId = intensiveIdField.getValue();

    var intensivesStore = new Ext.data.Store({
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'name'
            ],
            root: 'intensives'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/json/getintensivesforgrid/?cid=' + chairId
        })
    });

    var intensivesGrid = new Ext.grid.GridPanel({
        store: intensivesStore,
        anchor: '100%, 100%',
        height: 400,
        loadMask: true,
        sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
        columns: [
            {header: 'Наименование интенсива', dataIndex: 'name', sortable: true, width: 550}
        ]
    });

    intensivesGrid.on('rowdblclick', function (gridObj, rowIndex, e) {
        var rec = gridObj.getStore().getAt(rowIndex);
        intensiveField.setValue(rec.data.name);
        intensiveIdField.setValue(rec.data.id);
        intensiveSelectWnd.close();
    });
    var intensiveSelectWnd = new Ext.Window({
        title: 'Список интенсивов',
        modal: true,
        layout: 'anchor',
        width: 650,
        items: [
            intensivesGrid
        ],
        buttons: [
            {
                text: 'Выбрать',
                handler: function () {
                    var rec = intensivesGrid.getSelectionModel().getSelected();
                    intensiveField.setValue(rec.data.name);
                    intensiveIdField.setValue(rec.data.id);
                    if (currentId != rec.data.id) {
                        themeField.setValue('');
                        themeIdField.setValue(0);
                    }
                    intensiveSelectWnd.close();
                }
            }
        ]
    });

    return intensiveSelectWnd;
}

function makeConsultationAttributesWindow(consultID, windowTitle, startTime, endTime, onSuccess) {
    var startField = new Ext.form.Hidden({
        id: 'start',
        allowBlank: false
    });
    var startTimeField = new Ext.form.TimeField({
        id: 'start_time',
        fieldLabel: 'Время начала',
        submitValue: false,
        allowBlank: false,
        value: startTime,
        anchor: '100%',
        increment: 10,
        format: 'H:i'
    });
    var consultIDField = new Ext.form.Hidden({
        id: 'id',
        allowBlank: false,
        value: consultID
    });
    var endField = new Ext.form.Hidden({
        id: 'end',
        allowBlank: false
    });
    var endTimeField = new Ext.form.TimeField({
        id: 'end_time',
        fieldLabel: 'Время окончания',
        submitValue: false,
        allowBlank: false,
        value: endTime,
        anchor: '100%',
        increment: 10,
        format: 'H:i'
    });
    var themeField = new Ext.form.TextField({
        id: 'name',
        fieldLabel: 'Тема занятия',
        allowBlank: false,
        anchor: '100%'
    });

    var attributesStore = new Ext.data.Store({
        reader: new Ext.data.JsonReader({
            fields: [
                'id',
                'start',
                'end',
                'name'
            ],
            root: 'consult'
        }),
        proxy: new Ext.data.HttpProxy({
            url: '/schedule/getconsultation/?id=' + consultID
        }),
        listeners: {
            'load': function () {
                var rec = this.getAt(0);
                consultIDField.setValue(rec.data.id);
                startField.setValue(rec.data.start);
                endField.setValue(rec.data.end);
                themeField.setValue(rec.data.name);

                startTime = Date.parseDate(rec.data.start, "Y-m-d H:i:s");
                startTimeField.setValue(startTime);
                endTime = Date.parseDate(rec.data.end, "Y-m-d H:i:s");
                endTimeField.setValue(endTime);
            }
        }
    });

    var attributesForm = new Ext.form.FormPanel({
        id: 'lesson-attributes',
        padding: 5,
        url: '/schedule/saveconsultation/',
        items: [
            consultIDField,
            startField,
            startTimeField,
            endField,
            endTimeField,
            themeField
        ],
        buttons: [
            {
                text: 'Сохранить',
                plugins: 'defaultButton',
                handler: function () {
                    var form = attributesForm.getForm();
                    if (form.isValid()) {
                        var startDate = startTime.getFullYear();
                        startDate += '-';
                        var startMonth = startTime.getMonth() + 1;
                        startDate += (startMonth < 10) ? '0' + startMonth : startMonth;
                        startDate += '-';
                        var startDay = startTime.getDate();
                        startDate += (startDay < 10) ? '0' + startDay : startDay;

                        var endDate = endTime.getFullYear();
                        endDate += '-';
                        var endMonth = endTime.getMonth() + 1;
                        endDate += (endMonth < 10) ? '0' + endMonth : endMonth;
                        endDate += '-';
                        var endDay = endTime.getDate();
                        endDate += (endDay < 10) ? '0' + endDay : endDay;

                        form.items.item('start').setValue(startDate + ' ' + form.items.item('start_time').value);
                        form.items.item('end').setValue(endDate + ' ' + form.items.item('end_time').value);
                        form.submit({
                            waitMsg: 'Сохранение данных о занятии',
                            success: function (form, action) {
                                onSuccess(form, action);
                                lessonAttributes.close();
                            },
                            failure: function (form, action) {
                                Ext.Msg.show({
                                    title: 'Ошибка при сохранении',
                                    msg: action.result.error_message,
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.WARNING
                                });
                            }
                        });
                    } else {
                        Ext.Msg.show({
                            title: 'Ошибка при валидации',
                            msg: 'В форму внесены некорректные данные',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                }
            },
            {
                text: 'Отмена',
                handler: function () {
                    lessonAttributes.close();
                }
            }
        ]
    });

    var lessonAttributes = new Ext.Window({
        title: windowTitle,
        modal: true,
        width: 600,
        items: [
            attributesForm
        ]
    });

    if (consultID) {
        lessonAttributes.on('show', function () {
            attributesStore.load();
        });
    }

    return lessonAttributes;
}

/**
 * Создать таб - хранилише документов
 * @param teacherID индентификатор преподавателя
 */
function makeDocumentStorageTab(teacherID) {
    var archiveButton = {
        xtype: 'tbbutton',
        text: 'Архивировать',
        width: 200,
        handler: function () {
            var wnd = makeArchiveDocumentsWindow({user: 'tid', id: teacherID});
            wnd.show();
        }
    };

    var showStorageButton = {
        xtype: 'tbbutton',
        text: 'Документы и архивы',
        width: 200,
        handler: function () {
            var wnd = makeStoredDocumentsGridWindow({user: 'tid', id: teacherID});
            wnd.show();
        }
    };

    return new Ext.form.FormPanel({
        title: 'Хранилище документов',
        id: 'documentstorage',
        anchor: '100%',
        autoHeight: true,
        padding: 5,
        region: 'north',
        items: [
            archiveButton,
            showStorageButton
        ]
    });
}

/**
 * Создать таб - успеваемость слушателей
 */
function makeAchievementTab() {
    var showIndividualButton = {
        xtype: 'tbbutton',
        text: 'Индивидуальная',
        width: 200,
        handler: function () {
            auditorView.showIndividualAchievementWindow();
        }
    };

    var showSummaryButton = {
        xtype: 'tbbutton',
        text: 'Сводная',
        width: 200,
        handler: function () {
            auditorView.showSummaryAchievementWindow();
        }
    };

    return new Ext.form.FormPanel({
        title: 'Успеваемость студентов',
        id: 'achievements',
        anchor: '100%',
        autoHeight: true,
        padding: 5,
        region: 'north',
        items: [
            showIndividualButton,
            showSummaryButton
        ]
    });
}

/**
 * Получить корректное время начала пары
 * @param time время начала пары
 * @returns {*} корректное время начала пары
 */
function getCorrectTimePairs(time) {
    var starts = [
        [8, 30],
        [10, 10],
        [11, 50],
        [13, 30],
        [15, 10],
        [16, 50],
        [1, 2],
        [3, 4],
        [5, 6],
        [7, 8],
        [9, 10],
        [11, 12]
    ];

    function toMin(arr) {
        return arr[0] * 60 + arr[1];
    }

    var min = [Infinity, -1];
    for (var i = 0; i < starts.length / 2; i++) {
        var dif = Math.abs(toMin(starts[i]) - toMin(time));
        if (min[0] > dif) {
            min[0] = dif;
            min[1] = starts.length / 2 + i;
        }
    }

    return starts[min[1]];
}