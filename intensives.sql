SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `intensives`
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_general_ci;
USE `intensives`;

-- -----------------------------------------------------
-- Table `intensives`.`document_storage`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `intensives`.`document_storage` (
  `id`       INT          NOT NULL AUTO_INCREMENT,
  `path`     VARCHAR(255) NOT NULL,
  `name`     VARCHAR(255) NOT NULL,
  `date`     DATE         NULL,
  `category` INT          NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `path_UNIQUE` (`path` ASC))
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `intensives`.`chair`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `intensives`.`chair` (
  `id`           INT          NOT NULL AUTO_INCREMENT,
  `name`         VARCHAR(255) NOT NULL,
  `abbreviation` VARCHAR(45)  NOT NULL,
  `deleted`      TINYINT(1)   NULL DEFAULT 0,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `intensives`.`teacher`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `intensives`.`teacher` (
  `id`                   INT          NOT NULL AUTO_INCREMENT,
  `surname`              VARCHAR(255) NOT NULL,
  `name`                 VARCHAR(255) NOT NULL,
  `patronymic`           VARCHAR(255) NOT NULL,
  `rank`                 VARCHAR(255) NOT NULL,
  `att_num`              VARCHAR(255) NOT NULL,
  `diplom_num`           VARCHAR(255) NOT NULL,
  `login`                VARCHAR(45)  NOT NULL,
  `password`             CHAR(32)     NOT NULL,
  `chair_id`             INT          NOT NULL,
  `deleted`              TINYINT(1)   NULL DEFAULT 0,
  `birthdate`            DATE         NULL,
  `job`                  VARCHAR(45)  NULL,
  `birth_place`          VARCHAR(45)  NULL,
  `address_index`        VARCHAR(12)  NULL,
  `address_city`         VARCHAR(50)  NULL,
  `passport_series`      VARCHAR(8)   NULL,
  `passport_number`      VARCHAR(10)  NULL,
  `passport_issue_date`  DATE         NULL,
  `passport_issue_place` VARCHAR(45)  NULL,
  `insurance_number`     VARCHAR(18)  NULL,
  `tax_number`           VARCHAR(18)  NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `login_UNIQUE` (`login` ASC),
  INDEX `fk_teacher_chair_id_idx` (`chair_id` ASC),
  CONSTRAINT `fk_teacher_chair_id`
  FOREIGN KEY (`chair_id`)
  REFERENCES `intensives`.`chair` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `intensives`.`listener`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `intensives`.`listener` (
  `id`                     INT          NOT NULL AUTO_INCREMENT,
  `surname`                VARCHAR(255) NOT NULL,
  `name`                   VARCHAR(255) NOT NULL,
  `patronymic`             VARCHAR(255) NOT NULL,
  `group`                  VARCHAR(255) NOT NULL,
  `phone`                  VARCHAR(255) NOT NULL,
  `address`                VARCHAR(255) NOT NULL,
  `passport_series`        CHAR(4)      NOT NULL,
  `passport_number`        CHAR(6)      NOT NULL,
  `passport_givenout`      VARCHAR(255) NOT NULL,
  `passport_givenout_when` DATE         NOT NULL,
  `email`                  VARCHAR(255) NOT NULL,
  `additional_info`        TEXT         NOT NULL,
  `deleted`                TINYINT(1)   NULL DEFAULT 0,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `intensives`.`intensive`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `intensives`.`intensive` (
  `id`       INT          NOT NULL AUTO_INCREMENT,
  `name`     VARCHAR(255) NOT NULL,
  `chair_id` INT          NOT NULL,
  `deleted`  TINYINT(1)   NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_intensive_chair_id_idx` (`chair_id` ASC),
  CONSTRAINT `fk_intensive_chair_id`
  FOREIGN KEY (`chair_id`)
  REFERENCES `intensives`.`chair` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `intensives`.`auditory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `intensives`.`auditory` (
  `id`      INT         NOT NULL AUTO_INCREMENT,
  `name`    VARCHAR(45) NOT NULL,
  `deleted` TINYINT(1)  NULL DEFAULT 0,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `intensives`.`lesson_theme`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `intensives`.`lesson_theme` (
  `id`           INT          NOT NULL AUTO_INCREMENT,
  `name`         VARCHAR(255) NOT NULL,
  `number`       INT          NOT NULL,
  `intensive_id` INT          NOT NULL,
  `deleted`      TINYINT(1)   NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_lesson_theme_intensive_id_idx` (`intensive_id` ASC),
  CONSTRAINT `fk_lesson_theme_intensive_id`
  FOREIGN KEY (`intensive_id`)
  REFERENCES `intensives`.`intensive` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `intensives`.`lesson`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `intensives`.`lesson` (
  `id`           INT        NOT NULL AUTO_INCREMENT,
  `intensive_id` INT        NOT NULL,
  `start`        DATETIME   NOT NULL,
  `end`          DATETIME   NOT NULL,
  `theme_id`     INT        NOT NULL,
  `teacher_id`   INT        NOT NULL,
  `auditory_id`  INT        NOT NULL,
  `status`       INT        NOT NULL,
  `type`         INT        NOT NULL,
  `deleted`      TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_lesson_teacher_id_idx` (`teacher_id` ASC),
  INDEX `fk_lesson_auditory_id_idx` (`auditory_id` ASC),
  INDEX `fk_lesson_intensive_id_idx` (`intensive_id` ASC),
  INDEX `fk_lesson_theme_id_idx` (`theme_id` ASC),
  CONSTRAINT `fk_lesson_teacher_id`
  FOREIGN KEY (`teacher_id`)
  REFERENCES `intensives`.`teacher` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lesson_auditory_id`
  FOREIGN KEY (`auditory_id`)
  REFERENCES `intensives`.`auditory` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lesson_intensive_id`
  FOREIGN KEY (`intensive_id`)
  REFERENCES `intensives`.`intensive` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lesson_theme_id`
  FOREIGN KEY (`theme_id`)
  REFERENCES `intensives`.`lesson_theme` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `intensives`.`lesson_to_listener`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `intensives`.`lesson_to_listener` (
  `id`          INT        NOT NULL AUTO_INCREMENT,
  `presented`   TINYINT(1) NULL DEFAULT 0,
  `lesson_id`   INT        NOT NULL,
  `listener_id` INT        NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_lesson_to_listener_lesson_id_idx` (`lesson_id` ASC),
  INDEX `fk_lesson_to_listener_listener_idx` (`listener_id` ASC),
  CONSTRAINT `fk_lesson_to_listener_lesson_id`
  FOREIGN KEY (`lesson_id`)
  REFERENCES `intensives`.`lesson` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lesson_to_listener_listener`
  FOREIGN KEY (`listener_id`)
  REFERENCES `intensives`.`listener` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `intensives`.`contract`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `intensives`.`contract` (
  `id`             INT          NOT NULL AUTO_INCREMENT,
  `number`         INT          NOT NULL DEFAULT 0,
  `chair_id`       INT          NOT NULL,
  `date`           DATE         NOT NULL,
  `listener_id`    INT          NOT NULL,
  `hours`          INT          NOT NULL,
  `start`          DATE         NOT NULL,
  `end`            DATE         NOT NULL,
  `total`          DOUBLE       NOT NULL,
  `paid`           TINYINT(1)   NULL DEFAULT 0,
  `receipt_number` VARCHAR(255) NULL,
  `receipt_total`  DOUBLE       NULL,
  `receipt_date`   DATE         NULL,
  `document_id`    INT          NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_contract_listener_id_idx` (`listener_id` ASC),
  INDEX `fk_contract_chair_id_idx` (`chair_id` ASC),
  INDEX `fk_contract_document_id_idx` (`document_id` ASC),
  CONSTRAINT `fk_contract_listener_id`
  FOREIGN KEY (`listener_id`)
  REFERENCES `intensives`.`listener` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contract_chair_id`
  FOREIGN KEY (`chair_id`)
  REFERENCES `intensives`.`chair` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contract_document_id`
  FOREIGN KEY (`document_id`)
  REFERENCES `intensives`.`document_storage` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `intensives`.`auditory_employment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `intensives`.`auditory_employment` (
  `id`          INT        NOT NULL AUTO_INCREMENT,
  `start`       DATETIME   NOT NULL,
  `end`         DATETIME   NOT NULL,
  `periodic`    TINYINT(1) NOT NULL,
  `period`      INT        NOT NULL,
  `end_period`  DATETIME   NOT NULL,
  `auditory_id` INT        NOT NULL,
  `comment`     TEXT       NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_auditory_employment_auditory_id_idx` (`auditory_id` ASC),
  CONSTRAINT `fk_auditory_employment_auditory_id`
  FOREIGN KEY (`auditory_id`)
  REFERENCES `intensives`.`auditory` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `intensives`.`methodist`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `intensives`.`methodist` (
  `id`         INT          NOT NULL AUTO_INCREMENT,
  `surname`    VARCHAR(255) NOT NULL,
  `name`       VARCHAR(255) NOT NULL,
  `patronymic` VARCHAR(255) NOT NULL,
  `login`      VARCHAR(45)  NOT NULL,
  `password`   CHAR(32)     NOT NULL,
  `chair_id`   INT          NOT NULL,
  `deleted`    TINYINT(1)   NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `login_UNIQUE` (`login` ASC),
  INDEX `fk_methodist_chair_id_idx` (`chair_id` ASC),
  CONSTRAINT `fk_methodist_chair_id`
  FOREIGN KEY (`chair_id`)
  REFERENCES `intensives`.`chair` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `intensives`.`teacher_schedule`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `intensives`.`teacher_schedule` (
  `id`         INT      NOT NULL AUTO_INCREMENT,
  `start`      DATETIME NOT NULL,
  `end`        DATETIME NOT NULL,
  `teacher_id` INT      NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_teacher_schedule_teacher_id_idx` (`teacher_id` ASC),
  CONSTRAINT `fk_teacher_schedule_teacher_id`
  FOREIGN KEY (`teacher_id`)
  REFERENCES `intensives`.`teacher` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `intensives`.`teacher_vstu_schedule`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `intensives`.`teacher_vstu_schedule` (
  `id`               INT          NOT NULL AUTO_INCREMENT,
  `teacher_id`       INT          NOT NULL,
  `lesson_name`      VARCHAR(45)  NULL,
  `lesson_start`     INT          NOT NULL,
  `lesson_end`       INT          NOT NULL,
  `auditory_id`      INT          NULL,
  `lesson_type`      INT          NOT NULL DEFAULT 0,
  `additional_dates` VARCHAR(255) NULL,
  `groups`           VARCHAR(255) NULL,
  `day`              INT          NOT NULL,
  `week`             INT          NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_teacher_vstu_schedule_teacher1_idx` (`teacher_id` ASC),
  INDEX `fk_teacher_vstu_schedule_auditory_idx` (`auditory_id` ASC),
  CONSTRAINT `fk_teacher_vstu_schedule_teacher1`
  FOREIGN KEY (`teacher_id`)
  REFERENCES `intensives`.`teacher` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_teacher_vstu_schedule_auditory_id`
  FOREIGN KEY (`auditory_id`)
  REFERENCES `intensives`.`auditory` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `intensives`.`teacher_contract`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `intensives`.`teacher_contract` (
  `id`          INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `date`        DATE         NOT NULL,
  `start_date`  DATE         NULL,
  `end_date`    DATE         NULL,
  `teacher_id`  INT          NOT NULL,
  `payment`     FLOAT        NULL DEFAULT 0,
  `document_id` INT          NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_teacher_contract_teacher1_idx` (`teacher_id` ASC),
  INDEX `fk_teacher_contract_document_id_idx` (`document_id` ASC),
  CONSTRAINT `fk_teacher_contract_teacher1`
  FOREIGN KEY (`teacher_id`)
  REFERENCES `intensives`.`teacher` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_teacher_contract_document_id`
  FOREIGN KEY (`document_id`)
  REFERENCES `intensives`.`document_storage` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `intensives`.`teacher_consultations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `intensives`.`teacher_consultations` (
  `id`         INT          NOT NULL AUTO_INCREMENT,
  `teacher_id` INT          NOT NULL,
  `start`      DATETIME     NOT NULL,
  `end`        DATETIME     NOT NULL,
  `name`       VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_teacher_consultations_teacher1_idx` (`teacher_id` ASC),
  CONSTRAINT `fk_teacher_consultations_teacher1`
  FOREIGN KEY (`teacher_id`)
  REFERENCES `intensives`.`teacher` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `intensives`.`document_to_staff`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `intensives`.`document_to_staff` (
  `id`           INT NOT NULL AUTO_INCREMENT,
  `document_id`  INT NOT NULL,
  `teacher_id`   INT NULL,
  `methodist_id` INT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `fk_document_to_staff_teacher_id_UNIQUE` (`document_id`, `teacher_id`),
  UNIQUE INDEX `fk_document_to_staff_methodist_id_UNIQUE` (`document_id`, `methodist_id`),
  CONSTRAINT `fk_document_to_staff_document_id`
  FOREIGN KEY (`document_id`)
  REFERENCES `intensives`.`document_storage` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_document_to_staff_teacher_id`
  FOREIGN KEY (`teacher_id`)
  REFERENCES `intensives`.`teacher` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_document_to_staff_methodist_id`
  FOREIGN KEY (`methodist_id`)
  REFERENCES `intensives`.`methodist` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `intensives`.`contract_to_lesson_theme`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `intensives`.`contract_to_lesson_theme` (
  `id`              INT NOT NULL AUTO_INCREMENT,
  `contract_id`     INT NOT NULL,
  `lesson_theme_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_contract_to_lesson_theme_contract_id_idx` (`contract_id` ASC),
  INDEX `fk_contract_to_lesson_theme_lesson_theme_id_idx` (`lesson_theme_id` ASC),
  CONSTRAINT `fk_contract_to_lesson_theme_contract_id`
  FOREIGN KEY (`contract_id`)
  REFERENCES `intensives`.`contract` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contract_to_lesson_theme_lesson_theme_id`
  FOREIGN KEY (`lesson_theme_id`)
  REFERENCES `intensives`.`lesson_theme` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

USE `intensives`;

SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;