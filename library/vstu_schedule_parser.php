<?php
error_reporting(E_ALL ^ E_NOTICE);
require_once 'excel_reader2.php';


// $test_arr = parse_schedule("fakultet_elektroniki_i_vychislitelnoy_tehniki-4_kurs.xls", 
// 	array('Ляпина',
// 	'Романенко',
// 	'Жукова',
// 	'Пошаев',
// 	'Кузнецов'
// 	), null);

// foreach ($test_arr as $key => $value) {
// 	foreach ($value as $name => $val) {
// 		echo '' . $name . ': ' . $val . '; ';
// 	}
// 	echo "\n";
// }

function find_lesson_name_and_time($schedule, $column, $current_pos)  
{
	$name = ''; 
	$name_row = 0;
	$time = '';
	do {
		$val = $schedule[--$current_pos];
		$name = $val['val'];
		$name_row = $val['row'];
	} while(!preg_match('/[А-ЯA-Z \t\-]{3,}/u', $name) || $val['col'] != $column);

	do {
		$val = $schedule[--$current_pos];
		$time = $val['val'];
	} while((!contains_time($time)) && $current_pos > 0);

	return array($name, $time, $name_row);
}

function contains_time($str)
{

	$valid_time = array('\b1\s*-\s*2\b', 
				  '\b3\s*-\s*4\b',
				  '\b5\s*-\s*6\b',
				  '\b7\s*-\s*8\b',
				  '\b9\s*-\s*10\b',
				  '\b11\s*-\s*12\b');
	foreach ($valid_time as $key => $value) {
		$regex = "/{$value}/ui";
		if (preg_match($regex, $str)) {
			return true;
		}
	}
	return false;

}

function find_day($str)
{
	$days = array(1 => 'понедельник',
				  2 => 'вторник', 
				  3 => 'среда',
				  4 => 'четверг',
				  5 => 'пятница',
				  6 => 'суббота');
	foreach ($days as $key => $value) {
		$quoted = preg_quote($value);
		$regex = "/{$quoted}/ui";
		if (preg_match($regex, $str)) {
			return $key;
		}
	}
	return 0;
}

function contains_any_of($str, $arr) 
{
	foreach ($arr as $key => $value) {
		$quoted = preg_quote($value);
		if (preg_match("/{$quoted}/ui", $str)) {
			return $value;
		}
	}
	return '';
}

function contains_auditory($str) 
{
	return preg_match('/([A-Za-zА-Яа-я]+\s*\-\s*)?[0-9]{3,}/ui', $str);
}

function find_auditory($schedule, $borders) 
{
	$leftBorder = $borders['l'];
	$rightBorder = $borders['r'];
	$topBorder = $borders['t'];
	$bottomBorder = $borders['b'];

	for ($i = $leftBorder; $i <= $rightBorder; ++$i) {
		for ($j = $topBorder; $j <= $bottomBorder; ++$j) {
			$val = $schedule->value($j, $i);
			if (contains_auditory($val)) {
				return array('row' => $j, 'col' => $i, 'val' => $val);
			}
		}
	}
	return array();
}

function find_lesson_borders($row, $col, $data)
{
	$left = $col;
	$right = $col;
	$up = $row;
	$down = $row;
	$current_border = $data->borderLeft($row, $left);
	while ($current_border == '' && $left > 0) {
		$current_border = $data->borderRight($row, $left - 1);
		$left--;
		$current_border = $current_border . $data->borderLeft($row, $left);
	}

	$current_border = $data->borderRight($row, $right);
	while ($current_border == '' && $right <= $data->colcount()) {
		$current_border = $data->borderLeft($row, $right + 1);
		$right++;
		$current_border = $current_border . $data->borderRight($row, $right);
	}

	$current_border = $data->borderTop($up, $col);
	while ($current_border == '' && $up > 0) {
		$current_border = $data->borderBottom($up - 1, $col);
		$up--;
		$current_border = $current_border . $data->borderTop($up, $col);	
	}

	$current_border = $data->borderBottom($down, $col);
	while ($current_border == '' && $down <= $data->rowcount()) {
		$current_border = $data->borderTop($down + 1, $col);
		$down++;
		$current_border = $current_border . $data->borderBottom($down, $col);
	}

	return array('l' => $left, 't' => $up, 'b' => $down, 'r' => $right);
}

function find_additional_dates($schedule, $borders) 
{
	$leftBorder = $borders['l'];
	$rightBorder = $borders['r'];
	$topBorder = $borders['t'];
	$bottomBorder = $borders['b'];

	$full_sell_string = '';
	for ($i = $leftBorder; $i <= $rightBorder; ++$i) {
		for ($j = $topBorder; $j <= $bottomBorder; ++$j) {
			$val = $schedule->value($j, $i);
			$full_sell_string = $full_sell_string . $val;
		}
	}
	$matches = array();
	preg_match_all('/[0-9]{1,2}\.[0-9]{1,2}/ui', $full_sell_string, $matches);
	$ret_str = '';
	foreach ($matches as $key => $value) {
		foreach ($value as $k => $v) {
			$ret_str .= $v;
			if ($k < count($value) - 1) {
				$ret_str .= ', ';
			}
		}
	}
	return $ret_str;
}

function parse_schedule($schedule_file, $names, $groups) 
{
	$days = array(1 => 'понедельник',
				  2 => 'вторник', 
				  3 => 'среда',
				  4 => 'четверг',
				  5 => 'пятница',
				  6 => 'суббота');
	$data = new Spreadsheet_Excel_Reader($schedule_file); //"fakultet_elektroniki_i_vychislitelnoy_tehniki-4_kurs (1).xls");
	$column_count = $data->colcount();
	$row_count = $data->rowcount();
	
	$schedule_array = array();
	for ($i = 0; $i < $row_count; $i++) {
		for ($j = 0; $j < $column_count; $j++) {
			$val = $data->value($i, $j);
			if (!empty($val)) {
				$schedule_array[] = array('row' => $i, 'col' => $j, 'val' => $val, 'span' => $data->rowspan($i, $j));
			}
		}
	} 
	// $schedule_size = count($schedule_array)
	$current_day = 0;
	$current_week = 1;
	$used_days = array();
	$structured_schedule = array();
	foreach ($schedule_array as $key => $value) {
		$row = $value['row'];
		$column = $value['col'];
		$text = $value['val'];
		$name = contains_any_of($text, $names);
		if (!empty($name)) {
			$borders = find_lesson_borders($row, $column, $data);
			$lesson_name_time = find_lesson_name_and_time($schedule_array, $column, $key); 		
			$lesson_name = $lesson_name_time[0];
			$lesson_time = $lesson_name_time[1];

			$auditory_array = find_auditory($data, $borders);
			$auditory = $auditory_array['val'] or '';
			
			$matches = array();
			preg_match('/([0-9]+)\s*\-\s*([0-9]+)/iu', $lesson_time, $matches);
			$lesson_time = $matches[1] . '-' . $matches[2];
			$structured_schedule[] = array(
				'name' => $name,
				'lesson' => $lesson_name,
				'time' => $lesson_time,
				'day' => $current_day,
				'day_words' => '' . $current_day . ': ' . $days[$current_day],
				'week' => $current_week,
				'auditory' => $auditory,
				'border' => '' . $borders['l'] . ':' . $borders['r'] . ':' . $borders['b'] . ':' . $borders['t'],
				'additional_dates' => find_additional_dates($data, $borders)

			);
			if ($borders['b'] - $borders['t'] > 3) { // dirty hack for double pairs
				preg_match('/([0-9]+)\s*\-\s*([0-9]+)/iu', $lesson_time, $matches);
				$start = $matches[1] + 2;
				$end = $matches[2] + 2;
				$lesson_time =  $start . '-' . $end;
				$structured_schedule[] = array(
					'name' => $name,
					'lesson' => $lesson_name,
					'time' => $lesson_time,
					'day' => $current_day,
					'day_words' => '' . $current_day . ': ' . $days[$current_day],
					'week' => $current_week,
					'auditory' => $auditory,
					'border' => '' . $borders['l'] . ':' . $borders['r'] . ':' . $borders['b'] . ':' . $borders['t'],
					'additional_dates' => find_additional_dates($data, $borders)
				);
			}
		} else {
			$day_number = find_day($text);
			if ($day_number) {
				$current_day = $day_number;
				$already_used = in_array($day_number, $used_days);
				if ($already_used) {
					$current_week = 2;
				} else {
					$used_days[] = $current_day;
				}
			}
		}

	}
	return $structured_schedule;
	
}
