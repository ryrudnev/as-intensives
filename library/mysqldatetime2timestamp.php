<?php

/**
 * Convert MySQL datetime to PHP time
 */
function mysqldatetime2timestamp($datetime) {
  //example: 2008-02-07 12:19:32
  $values = split(" ", $datetime);

  $dates = split("-", $values[0]);
  $times = split(":", $values[1]);

  $newdate = mktime($times[0], $times[1], $times[2], $dates[1], $dates[2], $dates[0]);

  return $newdate;
}
